-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
-- 
-- Host: localhost    Database: community_new_newdata
-- ------------------------------------------------------
-- Server version	5.6.23-log 

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `action_instance`
--

DROP TABLE IF EXISTS `action_instance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `action_instance` (
  `oid` int(11) NOT NULL,
  `executor` varchar(255) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `score` decimal(19,2) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `tag` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `rank_oid` int(11) DEFAULT NULL,
  `action_type_oid` int(11) DEFAULT NULL,
  `object_key` int(11) DEFAULT NULL,
  PRIMARY KEY (`oid`),
  KEY `idx_action_instance_rank` (`rank_oid`),
  KEY `idx_action_instance_action_typ` (`action_type_oid`),
  CONSTRAINT `fk_action_instance_action_type` FOREIGN KEY (`action_type_oid`) REFERENCES `action_type` (`oid`),
  CONSTRAINT `fk_action_instance_rank` FOREIGN KEY (`rank_oid`) REFERENCES `community_user` (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `action_instance`
--

LOCK TABLES `action_instance` WRITE;
/*!40000 ALTER TABLE `action_instance` DISABLE KEYS */;
INSERT INTO `action_instance` VALUES (22,' ','2015-02-13 14:49:22',300.00,'Watch educational video',' ',' ',2,11,NULL),(23,' ','2015-02-12 14:49:28',300.00,'Watch educational video',' ',' ',2,11,NULL),(24,' ','2015-02-11 14:50:34',600.00,'Buy efficient appliance',' ',' ',2,9,NULL),(25,' ','2015-02-16 14:51:05',500.00,'Install shower timer',' ',' ',2,7,NULL),(26,' ','2015-02-15 14:51:34',200.00,'Create a team',' ',' ',2,1,NULL),(27,' ','2015-02-09 14:51:48',500.00,'Achieve a common goal',' ',' ',2,2,NULL),(28,' ','2015-02-10 14:53:25',100.00,'Add device',' ',' ',2,6,NULL),(29,' ','2015-02-12 14:53:40',200.00,'Provide personal info',' ',' ',2,5,NULL),(30,' ','2015-02-25 14:54:24',100.00,'Read water saving tip',' ',' ',4,10,NULL),(31,' ','2015-02-25 14:54:44',300.00,'Watch educational video',' ',' ',4,11,NULL),(32,' ','2015-02-25 14:54:50',300.00,'Watch educational video',' ',' ',4,11,NULL),(33,' ','2015-02-25 14:55:21',200.00,'Provide personal info',' ',' ',10,5,NULL),(34,' ','2015-02-25 14:55:21',150.00,'Provide household info',' ',' ',5,4,NULL),(35,' ','2015-02-25 14:55:21',200.00,'Provide personal info',' ',' ',7,5,NULL),(36,' ','2015-02-25 14:55:21',150.00,'Provide household info',' ',' ',9,4,NULL),(37,' ','2015-02-25 14:55:21',100.00,'Add device',' ',' ',9,6,NULL),(38,' ','2015-02-11 14:55:21',100.00,'Add device',' ',' ',2,6,NULL),(39,' ','2015-02-10 14:55:21',200.00,'Provide personal info',' ',' ',2,5,NULL),(40,' ','2015-02-25 14:55:21',200.00,'Provide personal info',' ',' ',4,5,NULL),(41,' ','2015-02-25 14:55:21',150.00,'Provide household info',' ',' ',9,4,NULL),(42,' ','2015-02-18 14:55:21',200.00,'Provide personal info',' ',' ',2,5,NULL),(43,' ','2015-02-25 14:55:21',150.00,'Provide household info',' ',' ',6,4,NULL),(44,' ','2015-02-17 14:55:21',100.00,'Add device',' ',' ',2,6,NULL),(45,' ','2015-02-25 14:55:21',100.00,'Add device',' ',' ',10,6,NULL),(46,' ','2015-02-25 14:55:21',100.00,'Add device',' ',' ',7,6,NULL),(47,' ','2015-02-25 14:55:21',200.00,'Provide personal info',' ',' ',7,5,NULL),(48,' ','2015-02-25 14:55:21',200.00,'Provide personal info',' ',' ',6,5,NULL),(49,' ','2015-02-25 14:55:21',100.00,'Add device',' ',' ',9,6,NULL),(50,' ','2015-02-25 14:55:21',100.00,'Add device',' ',' ',7,6,NULL),(51,' ','2015-02-25 14:55:21',200.00,'Provide personal info',' ',' ',7,5,NULL),(52,' ','2015-02-25 14:55:21',200.00,'Provide personal info',' ',' ',3,5,NULL),(53,' ','2015-02-25 14:56:12',200.00,'Invite a friend',' ',' ',9,3,NULL),(54,' ','2015-02-25 14:56:12',500.00,'Achieve a common goal',' ',' ',4,2,NULL),(55,' ','2015-02-25 14:56:12',200.00,'Create a team',' ',' ',6,1,NULL),(56,' ','2015-02-25 14:56:12',200.00,'Invite a friend',' ',' ',5,3,NULL),(57,' ','2015-02-08 14:56:12',200.00,'Invite a friend',' ',' ',2,3,NULL),(58,' ','2015-02-25 14:56:12',200.00,'Create a team',' ',' ',7,1,NULL),(59,' ','2015-02-25 14:56:12',200.00,'Invite a friend',' ',' ',7,3,NULL),(60,' ','2015-02-25 14:56:12',200.00,'Invite a friend',' ',' ',3,3,NULL),(61,' ','2015-02-25 14:56:12',200.00,'Invite a friend',' ',' ',4,3,NULL),(62,' ','2015-02-25 14:56:12',200.00,'Create a team',' ',' ',7,1,NULL),(63,' ','2015-02-25 14:56:12',200.00,'Invite a friend',' ',' ',10,3,NULL),(64,' ','2015-02-25 14:56:12',500.00,'Achieve a common goal',' ',' ',5,2,NULL),(65,' ','2015-02-25 14:56:12',200.00,'Create a team',' ',' ',9,1,NULL),(66,' ','2015-02-25 14:56:12',500.00,'Achieve a common goal',' ',' ',4,2,NULL),(67,' ','2015-02-25 14:56:12',200.00,'Create a team',' ',' ',4,1,NULL),(68,' ','2015-02-25 14:56:12',200.00,'Create a team',' ',' ',4,1,NULL),(69,' ','2015-02-25 14:56:12',500.00,'Achieve a common goal',' ',' ',4,2,NULL),(70,' ','2015-02-18 14:56:12',200.00,'Invite a friend',' ',' ',2,3,NULL),(71,' ','2015-02-25 14:56:12',500.00,'Achieve a common goal',' ',' ',7,2,NULL),(72,' ','2015-02-25 14:56:12',500.00,'Achieve a common goal',' ',' ',5,2,NULL),(73,' ','2015-02-09 14:56:59',300.00,'Watch educational video',' ',' ',2,11,NULL),(74,' ','2015-02-25 14:56:59',300.00,'Watch educational video',' ',' ',7,11,NULL),(75,' ','2015-02-10 14:56:59',300.00,'Watch educational video',' ',' ',2,11,NULL),(76,' ','2015-02-25 14:56:59',300.00,'Watch educational video',' ',' ',4,11,NULL),(77,' ','2015-02-25 14:56:59',100.00,'Read water saving tip',' ',' ',3,10,NULL),(78,' ','2015-02-11 14:56:59',300.00,'Watch educational video',' ',' ',2,11,NULL),(79,' ','2015-02-25 14:56:59',300.00,'Watch educational video',' ',' ',3,11,NULL),(80,' ','2015-02-25 14:56:59',100.00,'Read water saving tip',' ',' ',10,10,NULL),(81,' ','2015-02-12 14:56:59',300.00,'Watch educational video',' ',' ',2,11,NULL),(82,' ','2015-02-25 14:56:59',300.00,'Watch educational video',' ',' ',4,11,NULL),(83,' ','2015-02-25 14:56:59',100.00,'Read water saving tip',' ',' ',7,10,NULL),(84,' ','2015-02-25 14:56:59',100.00,'Read water saving tip',' ',' ',4,10,NULL),(85,' ','2015-02-25 14:56:59',300.00,'Watch educational video',' ',' ',5,11,NULL),(86,' ','2015-02-25 14:56:59',100.00,'Read water saving tip',' ',' ',9,10,NULL),(87,' ','2015-02-25 14:56:59',300.00,'Watch educational video',' ',' ',10,11,NULL),(88,' ','2015-02-25 14:56:59',100.00,'Read water saving tip',' ',' ',9,10,NULL),(89,' ','2015-02-25 14:56:59',100.00,'Read water saving tip',' ',' ',5,10,NULL),(90,' ','2015-02-25 14:56:59',300.00,'Watch educational video',' ',' ',3,11,NULL),(91,' ','2015-02-25 14:56:59',100.00,'Read water saving tip',' ',' ',3,10,NULL),(92,' ','2015-02-14 14:56:59',300.00,'Watch educational video',' ',' ',2,11,NULL),(93,' ','2015-02-25 15:09:54',200.00,'Create a team',' ',' ',6,1,NULL),(94,' ','2015-02-25 15:10:23',500.00,'Achieve a common goal',' ',' ',6,2,NULL),(95,' ','2015-02-25 15:11:16',300.00,'Watch educational video',' ',' ',6,11,NULL),(96,' ','2015-02-25 15:11:22',300.00,'Watch educational video',' ',' ',6,11,NULL),(97,' ','2015-02-25 15:11:40',100.00,'Read water saving tip',' ',' ',6,10,NULL),(98,' ','2015-02-25 15:12:02',600.00,'Buy efficient appliance',' ',' ',3,9,NULL),(99,' ','2015-02-25 15:12:09',250.00,'Reduce time in shower',' ',' ',3,8,NULL),(100,' ','2015-02-25 15:12:14',500.00,'Install shower timer',' ',' ',3,7,NULL),(101,' ','2015-02-25 15:12:44',500.00,'Install shower timer',' ',' ',7,7,NULL),(102,' ','2015-02-15 15:15:55',100.00,'Declare Consumption',' ',' ',2,12,NULL),(103,' ','2015-02-18 15:16:16',100.00,'Declare Consumption',' ',' ',2,12,NULL),(104,' ','2015-03-24 17:02:29',100.00,'Read water saving tip',' ',' ',4,10,NULL);
/*!40000 ALTER TABLE `action_instance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `action_instance_action_area_vi`
--

DROP TABLE IF EXISTS `action_instance_action_area_vi`;
/*!50001 DROP VIEW IF EXISTS `action_instance_action_area_vi`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `action_instance_action_area_vi` (
  `oid` tinyint NOT NULL,
  `der_attr` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `action_instance_daily_vi`
--

DROP TABLE IF EXISTS `action_instance_daily_vi`;
/*!50001 DROP VIEW IF EXISTS `action_instance_daily_vi`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `action_instance_daily_vi` (
  `action_type_oid` tinyint NOT NULL,
  `date` tinyint NOT NULL,
  `daily_occurrence` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `action_instance_name_view`
--

DROP TABLE IF EXISTS `action_instance_name_view`;
/*!50001 DROP VIEW IF EXISTS `action_instance_name_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `action_instance_name_view` (
  `oid` tinyint NOT NULL,
  `der_attr` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `action_type`
--

DROP TABLE IF EXISTS `action_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `action_type` (
  `oid` int(11) NOT NULL,
  `check_time_elapsed` tinyint(1) DEFAULT NULL,
  `milliseconds_time_elapsed` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `repeatable` tinyint(1) DEFAULT NULL,
  `score` decimal(19,2) DEFAULT NULL,
  `reputation` tinyint(1) DEFAULT NULL,
  `participation` tinyint(1) DEFAULT NULL,
  `area` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `gamified_application_oid` int(11) DEFAULT NULL,
  `active` bit(1) DEFAULT NULL,
  `thematic_area_oid` int(11) DEFAULT NULL,
  PRIMARY KEY (`oid`),
  KEY `idx_action_type_gamified_appli` (`gamified_application_oid`),
  KEY `fk_action_type_thematic_area` (`thematic_area_oid`),
  CONSTRAINT `fk_action_type_gamified_applic` FOREIGN KEY (`gamified_application_oid`) REFERENCES `gamified_application` (`oid`),
  CONSTRAINT `fk_action_type_thematic_area` FOREIGN KEY (`thematic_area_oid`) REFERENCES `thematic_area` (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `action_type`
--

LOCK TABLES `action_type` WRITE;
/*!40000 ALTER TABLE `action_type` DISABLE KEYS */;
INSERT INTO `action_type` VALUES (1,0,NULL,'Create a team',1,200.00,1,1,NULL,'Create a team',NULL,'',3),(2,0,NULL,'Achieve a common goal',1,500.00,1,1,NULL,'Achieve a common goal',NULL,'',3),(3,0,NULL,'Invite a friend',1,200.00,1,1,NULL,'Invite a friend',NULL,'',3),(4,0,NULL,'Provide household info',1,150.00,1,1,NULL,'Provide household info',NULL,'',4),(5,0,NULL,'Provide personal info',1,200.00,1,1,NULL,'Provide personal info',NULL,'',4),(6,0,NULL,'Add device',1,100.00,1,1,NULL,'Add device',NULL,'',4),(7,0,NULL,'Install shower timer',1,500.00,1,1,NULL,'Install shower timer',NULL,'',2),(8,0,NULL,'Reduce time in shower',1,250.00,1,1,NULL,'Reduce time in shower',NULL,'',2),(9,0,NULL,'Buy efficient appliance',1,600.00,1,1,NULL,'Buy efficient appliance',NULL,'',2),(10,0,NULL,'Read water saving tip',1,100.00,1,1,NULL,'Read water saving tip',NULL,'',1),(11,0,NULL,'Watch educational video',1,300.00,1,1,NULL,'Watch educational video',NULL,'',1),(12,0,NULL,'Declare Consumption',1,100.00,1,1,NULL,'Declare Consumption',NULL,'',2);
/*!40000 ALTER TABLE `action_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `badge_action`
--

DROP TABLE IF EXISTS `badge_action`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `badge_action` (
  `badge_type_oid` int(11) NOT NULL,
  `action_type_oid` int(11) NOT NULL,
  PRIMARY KEY (`badge_type_oid`,`action_type_oid`),
  KEY `idx_badge_action_badge_type` (`badge_type_oid`),
  KEY `idx_badge_action_action_type` (`action_type_oid`),
  CONSTRAINT `fk_badge_action_action_type` FOREIGN KEY (`action_type_oid`) REFERENCES `action_type` (`oid`),
  CONSTRAINT `fk_badge_action_badge_type` FOREIGN KEY (`badge_type_oid`) REFERENCES `badge_type` (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `badge_action`
--

LOCK TABLES `badge_action` WRITE;
/*!40000 ALTER TABLE `badge_action` DISABLE KEYS */;
INSERT INTO `badge_action` VALUES (5,1),(5,2),(5,3),(6,1),(6,2),(6,3),(7,4),(7,5),(7,6),(8,4),(8,5),(8,6),(9,4),(9,5),(9,6),(10,7),(10,8),(10,9),(11,1),(11,2),(11,3),(12,7),(12,8),(12,9),(13,10),(13,11),(14,10),(14,11),(15,10),(15,11);
/*!40000 ALTER TABLE `badge_action` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `badge_instance`
--

DROP TABLE IF EXISTS `badge_instance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `badge_instance` (
  `oid` int(11) NOT NULL,
  `score` decimal(19,2) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `rank_oid` int(11) DEFAULT NULL,
  `badge_type_oid` int(11) DEFAULT NULL,
  PRIMARY KEY (`oid`),
  KEY `idx_badge_instance_rank` (`rank_oid`),
  KEY `idx_badge_instance_badge_type` (`badge_type_oid`),
  CONSTRAINT `fk_badge_instance_badge_type` FOREIGN KEY (`badge_type_oid`) REFERENCES `badge_type` (`oid`),
  CONSTRAINT `fk_badge_instance_rank` FOREIGN KEY (`rank_oid`) REFERENCES `community_user` (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `badge_instance`
--

LOCK TABLES `badge_instance` WRITE;
/*!40000 ALTER TABLE `badge_instance` DISABLE KEYS */;
INSERT INTO `badge_instance` VALUES (1,500.00,'2015-02-25 14:50:34',2,10),(2,500.00,'2015-02-25 14:51:48',2,5),(3,500.00,'2015-02-25 14:55:21',2,7),(4,500.00,'2015-02-25 14:55:21',7,7),(5,500.00,'2015-02-25 14:55:21',9,7),(6,500.00,'2015-02-25 14:56:12',4,5),(7,500.00,'2015-02-25 14:56:12',5,5),(8,500.00,'2015-02-25 14:56:12',7,5),(9,1000.00,'2015-02-25 14:56:59',2,13),(10,1000.00,'2015-02-25 14:56:59',4,13),(11,500.00,'2015-02-25 15:10:22',6,5),(12,500.00,'2015-02-25 15:12:02',3,10),(13,500.00,'2015-02-25 15:12:44',7,10);
/*!40000 ALTER TABLE `badge_instance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `badge_type`
--

DROP TABLE IF EXISTS `badge_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `badge_type` (
  `oid` int(11) NOT NULL,
  `area` varchar(255) DEFAULT NULL,
  `needed_score` decimal(19,2) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `hd_image` varchar(255) DEFAULT NULL,
  `key` varchar(255) DEFAULT NULL,
  `importance` int(11) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `checked_image` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `hd_checked_image` varchar(255) DEFAULT NULL,
  `sort_number` int(11) DEFAULT NULL,
  `active` bit(1) DEFAULT NULL,
  `image_2` varchar(255) DEFAULT NULL,
  `imageblob` longblob,
  `hd_image_2` varchar(255) DEFAULT NULL,
  `hd_imageblob` longblob,
  `checked_image_2` varchar(255) DEFAULT NULL,
  `checked_imageblob` longblob,
  `hd_checked_image_2` varchar(255) DEFAULT NULL,
  `hd_checked_imageblob` longblob,
  `thematic_area_oid` int(11) DEFAULT NULL,
  PRIMARY KEY (`oid`),
  KEY `fk_badge_type_thematic_area` (`thematic_area_oid`),
  CONSTRAINT `fk_badge_type_thematic_area` FOREIGN KEY (`thematic_area_oid`) REFERENCES `thematic_area` (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `badge_type`
--

LOCK TABLES `badge_type` WRITE;
/*!40000 ALTER TABLE `badge_type` DISABLE KEYS */;
INSERT INTO `badge_type` VALUES (1,'Water Saving Insights',NULL,NULL,NULL,'area',NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1),(2,'Saving Water',NULL,NULL,NULL,'area',NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2),(3,'Participation',NULL,NULL,NULL,'area',NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,3),(4,'Profiling',NULL,NULL,NULL,'area',NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4),(5,'Participation',500.00,NULL,NULL,'area.level',1,'Engager',NULL,'Engager',NULL,1,'',NULL,NULL,NULL,NULL,'Engager_PArt_40.png','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\0(\0\0\0(\0\0\0���m\0\0LIDATx��mO�P��(~_��xc��Q�31�h�Ňh@&:6�\0�$$� ���n�֖�1v�[����ޮuf	\'9Y�.�/�{���{;:N�?�]=�L�����~5�6��훕0��_K��3ҋ�ΖB���3U �H�g�~�1D��}�8�O���Z�	�	X��/�����k�4ż����燃��,\'�&`5q�[\n��d3p�\06Y�9��}�Å-8ޓ@]f7oMVl��!�\0��]���P��8\\��c%<��M��N��p�#ӂxԣ�\\�����@IҠ��uD�������A�R���j��}��R=���I^\'��G�	�kðj�������S\r�Xofݥ�Gjp�ˣ\\&N��Y��/�Y\0���v@To��s��8�w��m�i��L%�&8\\��o�,[Y/B��7��6IMb}j�\r���? ��Ð�����˘��8���\rU&A� { ��Tppaȓr�uX��<�J�X���@�w�ڡ�y�������< .7-\n�i���$ȯ�nR.�U8H@�?�3;~���I
�Q��M����������@�X�X˕1n�ɾl7n� ��Mb���@ Q=l�P�i��c{C��n�\r~\'ʢz����MB�; �
֖	 ޞ\0%��f=⽩ڳT׻�eDEY��h34��ZC�0�ٍڟ�zf�.?�l�Lq�T�{�|4�n|r�\'A�V� �{��Fk&����w&�\Z^�[ˬ�ʾ�y�(gO���\"�/���Y�s�6��>���2�V?���x�TO�ߑ��aH5�c���>��94�3�Ĥ���MA;�=<\r�-=�/E�ᱚ��.���-.�����-.0��\n�4l� ����!��!\0\0\0\0IEND�B`�','Engager_PArt_60.png','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\0<\0\0\0<\0\0\0:��r\0\0\r�IDATx��[�s[��i�-�0��N����}�	�ؖ?��[[f�Bi�Ж��;�/I�\'!$!N�H�v�Y� K��8�%k�Z�d����|Wr�E�W�Ґ��7R�Xҹ�v���t��\\�\\������������r�>rMޟ��?��X���z$�2�h���h���Ԏᇦ�m�a�=��۷`\Z���*e�=�6�7&���SfGj`lO��ۥ�p��4�<J����Gw��c�C��=��<���d�2�S�<�{��{.e�2@�>l��=���8���Dm.R��(��I�&[!�lO�\r��4Y��Vkkt��������~l�����F�������P���
�2��cf�u��]W(���r��4k���u���	R�|�4�(򡉢�ͤl���`�x���[�4��l� )-�,G�3�1�D�c�ǜ��PW׷nX�ф��ۤɻ7i�q}q�h�IӖ�O��e�T�(E�Yp?EpiEx�� Hi�?矧\"��c��Qe����Q�#e�Λ ���C�X}�_s�n`p\'x�M\Z��H@��0�􊀗.܀�V���Aʴ\"�=�f[���������7>�<���ɻR&��I����\n�K��2Hs������mN�o�����]�g��77�=��p�>��+u*�k��:�R\Z�Z�4��8�Ӝ⻩c��E�<�S�_=���>���\\��sHaԨa�u\0\\�4�:|���h���5��7\"�\0��+�������Lڣ^��ٹ�)-�\\���F����:֬���R��;��L濇?�G��\"�f��;����5uotciPZ���5����)mNJ<�#�C���`/��H#��f{ �1�gu���UG��؍3��5]p��~��Q�<�Y���)yL�����\"��F#C�Vv>�jRQ�����;��zy�Z)�Y�����\n�D�g.�����hL�5-vp��p���&F�R�9��S��W���}�����\\\n����(fF�Q!���k����	�\\�1z�`�ɉ��0\r�Tӌ7]�R�W��m�v���Ԁ�8�Wi.�.��
\rE̼~L�e�jV\'\'-n��8��v�q�!\0n���ePI\0�]S>���tL����hѮ��>�ne�p�<�D�丑9(��3YR��zβ��3���\r&�!� �!\0n� Vu_�&����h>7G5G�_��FLhb�<C�Fx��<?�\Z�O�hv<(�z�M?��/48�j+�`NGYe)�\'�=+�U��^�f/\\�9%�!�_!\0P])�;*�R�B�f�Ay�����%D6��	:��.\nn<��f�1���\"-ۜ/���䪵
�����)*���\0�N)~�4���ؾ�7j��S9�\0�GF$}�Q���zp���\0^�O��F3�}a��\Z����릪l8��G8s�*�qZz�8Z�Cg(��-���Ν<vA�n���]I	�\'/3�~\n2��_�W�R���.]���ut;�\"Z�#��&�ٷϰ0@��Z�|y�kx��g�THf9����\\����S��X�	�=t�4�G!�r6�(��g�}T���~�GKc�0م������\r�,�{��M7n�@!�\Z(��� ��A��rS7!���u|�i�
P��\"]�,��yIi�;\"�n�z��f����}5٦����dי ���\nw�<(Ü���\0�\Z� ��);]�1���H�%�q0_�9���f���>���4)�+ƕ4��2�\Z6@3ˉNGw���:m/��k�~X�pum�\n�#�Sv�(�DA\Z\r@\0�تta�>r�����������R��\'A!��p�_&���f��TG[�P�Jp���$�s�
\Z��;,:R��Xז����uFA���f���2��r�=븮�NfV	��,� �:x5�r�X��HB9�#c>���1yt�2��H�g�q���ϰ��$�H��B�ݺ~�#��C��Z}��
5\r�9t�jD�\0~��41��\\\0��^V��V�:���oq�_`SQ	!��\"m�:*��~�F)���#�sS\n�ި�B<��<?�������B�`���\0�6UIiD��Ŭg�rW��a�ܐ�3W$}���c����$Q�z)���J\Z�C�$\n\n���$p0����}Bݰ���kZ�\"�)���nH9!��X đ��7�)�7L�\"^Uxm�)���υx��x$��gR2#��i�O�cfd���#Z�[�{K��+ܴ\"[V\0����6�8]��H�Ei�\Zi9?;\'�&���d�ШM�$q2��e��h�v,��g^�B���6f^��4BPUd�u��x�@U��X�UKz����̇�pq��S��5�\0F[���wp!̝%�9�3�.
H�V��9������}��^VIhH���h$����u�kd��\ZI�RIT����>�3܌V���s�.�Ko�t�0�\r�4��H81`
����B�4������i��蟇)R4��F��kǴ�~��k��s|E�
Բ�\n���̜Z��K��V����*#�@���#[,c\"����s%x����?=)7)��i��лHa5+��\"�!�53o��ԥ��C���?�,X��{D:��C��q��\Z�:�KZr�A�al�\r�!�����=G��s�ܬ��~�n\r@5��(���Q�SE/�íU�a���3\0xD����
]�PGMA\nrtA/!*�B�%�a�S2V�F��\Z̺�Ґ�0���O�Z<L:\0Jdݶ�!4VH�ѥ����\\4-i-���D�#Ӛd�,�x���`�%M�8�\'ut�ا�)4���l�R�/kR��/kv,(7J�_j�M�6�ne�OlZk�MkP,��b��u��jd��
��J�u\r�.�iɈM�Z#^�����J��}	�/��.MI��2�k8�$[-���a�e�a`�Ũ���Y�~�^`g���򡷺kNm�j�&G�<��+5��fZ���X��4�y�H@���#����G��E��J���Ȗ6ӢM�FU6Ө��M	
N��Z�]ʵ���K���qq,���j�9u��m�v�r�T6�ADL㏧K��:l�c4����Pv8�j��X�^��](�Qӝ�P���4�\Z��\Zz��8����q���!i���z�ET8���f�\\F��ґ ��Pl�kOd��ڎ<,����*��סl����~��0�Zz��t|��ǖ\0\Z�[8�g\ro�T;�Q���w\Z,U�[:��4��2�Eǖ����Ƶc��>ǭ>���E\Z��`ZŚ��>Q�޷��!\ZTY���åY�ޛX����XR��*�2�Z�27]������Yt��n���e��_,����8���Ǉ!��A�������\'��\\w�O�>�6���f��\07��ӭ; ^�+\0OJc3�>��*��+�\n�_�osJ���\nw�<(�2p* ޡg!����×<f�3~,`�ᝪulޯ��NG��F��J���\r�m�%�eG(��AOY}ݗ�����X���|��ݺ&9|cX�pa����5�o����?�ڴ�h�ǖ\0\0\0\0IEND�B`�',3),(6,'Participation',2500.00,NULL,NULL,'area.level',2,'Influencer',NULL,'Influencer',NULL,1,'',NULL,NULL,NULL,NULL,'Influencer_kdeq_40.png','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\0(\0\0\0(\0\0\0���m\0\0�IDATx���OSA��C�|pM41�����`|t��\rP�\"J��4c$($> ��\"�)�Pe��Y�TJX#Q�+(��ۛ��Ν�m�i�INܦ��7�|sfBB��?����}#�:���ؒQn�|�M;�i��Yz�d�G���z�T�\0�_M�)B����~#p���]�,��F\n����g��I[��)�ЩhO�M�_��	��(�����S\n��7p�\0z�9��k��3�B��⋘��5��[�\ZBp�Q
�FL�q����Jx\0�6~pȭ���L
�Q�����g�88	��Y�7}����\r�ng\Z��g.����ư�X�^:�EV���oD��C��lðj�s�w�\'�j����Yw�Uq\\����S7~V}͔��Z+Zɋŀ�*ؤN\"�\r�ТN���L%M0��\0Ƙ;d�~��ЕZc�/HMb}�f�rݻ3�:���b�\"NA>s瑻P�.QZ0��8 ��mK:T��P��l:�9Ŷ�b_��4Ak�y��y���S�I�\0�r�b�cj7&A_L>��&�Ɋ퇠�_�����p�79@�OZ���P�+��U]b#`���N~�*l���9I��\\=����xhܤk��F
� �4�������Xn=is�X�]��L���f���/e[c�g��l�͈�^j7\'
\0��t�X\"X�!���*��2��,㖴�Qc��j��/G������b0�#��ސH��f1����ԓ m�C�=�?ߞJm&�ޣ���N�jx�nu,��K($�n��nʉ�ls��W���5n���5���2�V_VqC���V_G~����~�[{� z_M��FA�V2�*:4���Y�:�hH\r����R��	��B@�>���((�߂�3h���B\0�K\'�Y\n�\0\0\0\0IEND�B`�','Influencer_kdeq_60.png','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\0<\0\0\0<\0\0\0:��r\0\02IDATx��[	P�g���nmvv7S3I4* x%1f<bRQ�)j2��ٝIfSCf���(�ʡ\"ݍҊh41�ze�B���F����\\� ��}�ȡ(/�����\ZT��n\'���U_u����^��|���?]?]C���?�Y����r��񿬓�����F��F�V��<3{�]��\0�����S�,U4o�����T����t�h�L1K/�^T#SJ��V�L�����
Wiސk�K��\ZT\\8G��}s}r�[�J�o�<��]� u�������R���J�J�T�I�D\'S�h]� �6W�j��\\�
������{�Jn�O�4�M\r��}) L�b��p�:���697�>�o�0 ����j\\�\ZU5C\'Q�,�T�^�\0^uk�p~M,j�b`�F��x��#�jd�-���W�\\ZyN~(��(�,E�̠��C�y�2.���O�>R���k�@��\"U#�J�m��6�� �����oIM� �E��!��	p�8߻Kݻr��!w�?�i����������5�E���<���~�8����{���\"��J	�K�����l�\'�H2@mnQ\r��7�,� G�Љ\">�\"��y�<�;C�]��Ѕ�.;�o��\ru���� �;��{@�ŵ�&�\0|����LX��P8[^V�a)���5rտP3�/.�Ԕ��T�=n�\"��8ő3ޯ��[�f^��w������?b�F>GQ]@_ZM���)�uj+��\0�inn���6̒�俵��aMo�lX�lw���\n؉��}ī�l��s\'�S\'�uR\'���a���4`���p��Yc�\"k��x���S����eN���x����X4(�YNc�죀5
�#IQ-��2�bd��y\'�����\\��+�;o��I��]��a����c��&\"�(@��\Z�\r�?�jy8.\'hq���G���A�ϐ��a-�\\��ȸ{k�qѐIE�m�#�\r8s� �VE[n\r�tt�NWp�n�4��0NWhk5MݛGV�t�&\'�bdL{T\n�Y��4�(P�*�+#���5�\n��O?�����p 
M����!��m��\'�켑��cudiiN39)v�UfF37f���D*,\0�Тb��qc\"��	�~m��VS���=��
W��юʰT����F/F��rѤ����)R\r�(�+�]q�`n�uk�Aլ�\"�	h�8��g/�Zq-�/��oO�H?�_J��)#Tz������\0w^���_!z�g��[�L��c[��\"���I�\rڹ!�թ�?�I�	�CB�����ҍ��/�Cs����݉���y���(���$��s� #̀���բ��ߗ_@S�9TQt�^�������B��pq��7Ye���ޙI�\n�G����Z�6��-�e���/Bm�`ؑ��զ�6�g=� ��ܮ����>�E��V�\0��\"D��+b�l���������Y��:`�}<���e��Rt>:.�=�RN9\nW�ũ�!кn�a\rմ��O8Ev�ϯ@ŧ{��rmYU(u݅�3�;�\n\'k�	�)�ߍ�\Z�#\Z�-���%����2��2�T�ēp@$��d����b����5B#;������^��ο[A5˄���\"�;�\\G���|���-�j�+�Ǹ��gHu�D�1�A�m�6�\\��]�!7���e�m`�nXM�\\宛�q�J:Z������4��X�So�a����>������)m�E��W���م�\"y��c a�;N���Qdkd隆윰]�Y���~a�tcÍ=��RH#�������ϝ�v߆�)Z�_nEa�~h=w�����܊Ж�]�B�⽨p�J?�3W#v�B��/B����U���}�lо�����#̐��_��Ȇ{P6f�A�m����N�6��=�E�+��B��-���u�·뮢���Ha_�c
���Ο)�Q;w��
�^����2��w�.�(*�l�ݴf9Ur�p^�1ͼ���\"������Y%����e����-H����N��j�]h<]\n��}�ֈ��\"y�\'��ܐ���^�(�6i�n�s����)���-�{���$�)��Jewq��	�hW��@��A�y��z�\nme�M4q�����bs�u��F{\"�aYo4i���}q%�����r��6.���ׂΔ�.��pǍ���I`+\0%�N-^���Z�~�;��V��t��쀥��� WK(^���ȝ(75VQ�{P�;\r��
Ѱ?��ț����Vi��ϔ��&{X2���^���7[,Q�)�7��-VW���x0���-��Fke4���U�y�E5IŊ�a������\'4\Z\n�\\���^�M?8��#�Y*�
jT:j:B�L�^!^k7�D�E�L�5k���Z~��7���hoh1�;e�� ��6��\"��Ȝ �~���\n�7��\nJi������i��~qM�\\|�]�ּ\Z\\N��tA@x_=Y��%�ќ�)�E���u�R���Z���8��T~����鸠�A[y����j�f�z�{�������ꡤ����2۴8��k�`��7�S5��p�:-���+��)�/ߐ�]\'�Z�Gg����/��ܶ$$�V��_� �W�8���⃳Ḝy]�:��૵����P9,D�������ݦ53�Y����fNӶB��OS��B%Q�vԷຖ�kL��U�,܁\"��hH֊H>x1+3D���7V!r�_�J�_�Tj^�D>�&y��G�Q�m2Z�jq�H>�^�ĜW�r�\n��X2��[$�@X�ߨh@OGw��$]�]!ʿL@��>d}�9R���t��\rD��D�-jcs`�ɆAI�v�	Fڇ�P�8��
b��&�ic�ql�H�L$���(^��ڂ�iK�@|�n�Q
��*���R��@Ԓ�qCխ]��� �z�%�&I��b�>�U9v!�_��Y�|�\rʗ�G&}���^���6!�U_~�\n;RE6�8��q���]��Ȳ[J��7r�?�,��S\'aap���ע���5����i`ݎ�h#��\' o5��r�1$����vA������uʼ�+�D5wp��K�A��y��\'nR��	�ʮ�%�|��R�UEԘ��I��������@\ZeB�8��!��C��/�̊ ���]A�֋���C�����h��G�Σ�[� ��,�2�ُ�Hr��mj���gy�e@���P\Z+n�ń|����%H���j�1@v>8�-�c��a@9� 1	���\'\"G�Q%N�)^Ƞ�2�4��Hl�mOs�jZ��i�~�\n��f��=�G6\0��j��z��o��&Pŋv�Ի�!�@�l����Ha1� P[������|��E��ynۑ939��l �g�EL���k�%Z�Ӣ.=è^���FP����-�oz��~\\H�n���ꯢ)����#��P\'�!�3�\n�������6��צ�( �
�Y1��r��\Z=���\n6��lB�$�6Ҭڴ�6ڴ�6�{��lܝh�TS����ա$�ҧ�#�d�\0Ո[-�Q�VK�-[-��^�MQjܮoy�uէ� �e3�&z���%C��đ�j)r�T9��b.K��ö��)Mc�w$Zs��y嚨a�R�n�@���	��Kl�-m�a(Ǜ��Υ�6�ۥT���D�\'S{ U{S��w�-)�Ƹ\"���c  xX�KM�O����NV�T�yaͣo���=������mB���!a����b�)��)�cHl�O�\r�\rK�?�4�#ı�h�җ}�#$1y�!8�;G-�j�(����l��m�c��C޴�f����|��^=\'?ɇZL���P
���1�İ�!��\'�������6Gv��`�cK�:���1+G�<9��,�\r5k:���`����~��қ�xv����I�8���`��5����wM���;z�\r꾚}<\'��.Uͥz��%�Tfd,)�{��R��L�A1��9ˣ��v���7J�0����5VY�y|��\0sc��̠��Ǉ�^j\Z[��C�kV)^a����zX07f����G\0HO��-Q|M�U�)>}�G\0���}�[4����Jew1���|�#\0�X���e�g���Gxȣ����������J�h��6z`9��l���Ȇ{P?��<~�\'�I����\\!�{�w7�h|\"��k�9/���Jew�\r7��O�O����w[k�9�f\0\0\0\0IEND�B`�',3),(7,'Profiling',500.00,NULL,NULL,'area.level',1,'Beginner Profiler',NULL,'Beginner Profiler',NULL,1,'',NULL,NULL,NULL,NULL,'Beginner Profiler_KQ4d_40.png','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\0(\0\0\0(\0\0\0���m\0\0wIDATx���JA�}� �E�G��Bo\n�B�� �J��x)��hb\r�J4�*�D%�d��q6���sf�Kb�ݙ��]��?�\\$?���g�ƞ���u�ON��2*�$���G�H?�t���z���H�����Ұ`P���S-up�l�Eh`���q����v�Z��OMzelX@\'�Z6�.\'��
�)�H������{d\0Q�+ԋ T��q�����=����߆��,6$���Y%�\Z��A��gט+�\'{�p�� ���dC�fQt0x�x!} F��x�0�����g/z�#��&���\n*�s�Yt3�+	��؄Rb\n*?f݇� ���3�Q8?@�>$^n�p��P��3���0�y�	N\Z�US��n�\'X�I�?l�@盡�y��Y:��#F��!��8���&���3�P�~����^�p�k�gQ��T���?��+�\0���fu���\0(���d�F p�G����GY��g`�9��&\0g��G)8�Y�[mS{�gm랂׋�P=���~/Ǵ[~��l�W�,���)�*��v?��T���pL��c������Ob��C�4\re��.Md0�_+T����n�KS$�����G��#�G�x~s��0#���\0��\\`)%/\0\0\0\0IEND�B`�','Beginner Profiler_KQ4d_60.png','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\0<\0\0\0<\0\0\0:��r\0\0
IDATx��[�s[gO�J:�2	m�<�\03<�\'��O��@)o�Δ3�$��Ҕ���ٜZ��I�B\'i���J����r%%vbKr�h���.��|8绒���u�e&��/����}����;ڴ��z��Z\0�6OO�y�_fG���qyѹU�qo���۴k��Ϻ#�;�?���\'\'[!�і�������4�}7s���q{���M%�Tb�\Z����+B�ʋ��I)8�x�1t�:��x����\0y��P~��m-��X��^f\0c�0n�\Z�|Z��Q��e5�M*s2J���!����跨�߲ �,^�w��=� v��g�|�މ�����)9���\Z��#0\'�� y yJ˗��8��1���B~arq\'H�A&�8u��c ����\0-�>�٠p�R`H��a�~V�8�������\"�3��FӮF]7h��@n�c���=�u��>|�ɣ�����\0�~�[�}KXCRh�-~~{�K��?p5�x2�m�E���ޣ�\\� ��J#��`�$`��{@��
x�� ����6Я\r��������̇;�$�x��E=���\Z�#\0����E�\\c�]�\r<n=l9��� 3A�KI��}.5�C�<�K|�.u����5	0��Y&��0�-���|���ASi������Hz0�JHtF�j�j���\0��x��R��z6���L�7Evl�+S��h[��Tv�����$�ā%��#��Y�ql5ۚ\nx��9�)cn
�jF�[D����ў�7ec����\"�)�=�] 8s�=L����a[\n9�q�4��ߺ�e������S��9:�=�\0�}Cxl�.疦 �����\ZG�W~�u�I�P\"��-�#/t-*�:ˉU!a\Z��Q&*0���\\P�E���)�Jw)Ey���m���)�Q��R��$qЁ\"���/��\"QAu�J�)`�v�K���뢲��J����3g\rZO4�7�6�i\'(C�uT�1O0��\n���(k�c,��V>}��oa�x[��E�蘄E����K��<�/n��m�\n��u����@��\nű%��r��[��|P�Ơ
�x�
?�C�0-��q�7��{7�@���9@]�2��6
�,MI��#=�0io��J��\"�O?�0�gYr���];?�/z�c��KO�A\n����0��pP���2���\r����2�l��]��4�5�e�(��g0��7H`(F\n�]a-��ԃ\"[��\nj�{.C=Ҹ�*f�A�9 ��q��a�&�d\"`�;Fv����p#�l���>�x=`$)��!��2$CHNrN�.�#�_LO�y���F�yP$!s���D\Z�X���������Q�s��yd��Qyڱu`�R�]$Í<���oh\\]�\Z��̥���������|��t�UC����=N��Ŗ6��1[PA_�`�\"q~$G�C���^c�$]}�e��\\jʱ}`2��7&+��Ŏ���Y}q#�}��_���[ME�Z᱋\nڧ��S;6�qoSbn����`-�5?�6�QD`4q~/���C��7��-e�&Wd(��w6\0����\';L����쇒�6�Ɠl���0\Z3�HefP�03�C~�;Y#P�Z.h}��~��Y�A)�p�&�
����-�2Pf��M��@Y�EdۧtI+��c�\\P@O��;\r9��\n6�w�ƍ�V]�ݕ%�ZfƆ��]@�{k䟘��-M� q�h��,������ ҙ�[@�G���Q����U�E�g�[�)�h\\Ox�(<�u� JKu��3�i���Tϭ�2���Z�.���跂Jg�Ѐ�fҸ��<Y_Zv�<�@��\0yCt�()7�kH>���iV�X�F�z2��~�xC��x�Щ��o\06��^{�\0����j��S���`��P.\Z�G`��4�����Z@ke\0T�F�G*g�@`I�c�&�j�\"�4=�
��@�++Lz�M�ڝe����Z��?ii�0��E7o�\Z�4�I2���\0�YKZ\n�P�Pl>6���I4���n\\��u�V�ML���*ʷ0�M7�^Y����j3=���� p�E\\�G0���u>f�x��8am��k˦E��uHIMRȪ֢qs�`)ʤ��^%*��A\nC��N\'/�n��W�vզ�\"��ўMk�#>�o�W#]���2QU�=�$ʼi/�pW!�\"��V@�����k��������#��Z�s��UK9qi�U
KZXjRa�4���.�&  3�6��L&>�5�T��cggV�z��g)`g���<��ګ)4�J���8�so��4����J���g�XbQt��\"I��n�bL�0�~kdW\0�.�������e�mץ1ב�ץ�.�(�,�L��B��!�(D��Ϙ@�>b��`l��]�.���KY�+�Z�B|�΅8Q��R�@���ʍ�\0������qƣ>��Bܲ$�/i3�v�>��v��6ypW@yj���2�Ro�A�Y���m=�<���\n�C-��0�\r��ҹއZjg�hl)7O�<n�s�����YFc� L[�����.�L[\"\ZW#k�`Z�3�Ĝ�V��]=�lL	����g�}�p)�އ έ\r�^E����\\$e�ZR���}.�(N<�a;i\'/�,>݌��lu|��$IAݱ��F+�W�O����j6�Ӧ\r�c�C�\0ic��])�~-�A��O��s��c &8�\Z�� �-+��\0ȼra�a�� �d�1�o�llީ�������Ƀgo� c��:��7��-�!�GD~�*M��oLV*��d�� ��o�瑑`�� ����W��;����}�vh�N2��7&+��E2��~|σu������X�X�\0\0\0\0IEND�B`�',4),(8,'Profiling',1500.00,NULL,NULL,'area.level',2,'Advanced Profiler',NULL,'Advanced Profiler',NULL,1,'',NULL,NULL,NULL,NULL,'Advanced Profiler_mCoc_40.png','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\0(\0\0\0(\0\0\0���m\0\0�IDATx����K�@\0p���	�z*�����C�\'?iK�E�^<�� xAAۭ���\n])��\"\"���~�q���������M�e���L&�؀,{ɏ7o&oRWwwj\\z��K}f�fJ1L�k�F�[מ2�2�D�t ��V�z��Ӝ�*��skuξ�Ep&���v��� /K��PL`Y*s�ǾCs��M��[�Uѓ諈?8��l�+�UMs�ŹzB��D�B�?=�WT�:�|w�d~e����Iܭ��D�q�����G��kw�%Ni��\"�:�<�8�\'�C\0\rǋt���ưp7�*���qÈ����l@z�	��/�\0p01����W�n� SI\08���2����)�s�������lv��!�rA��pyf4�Ia`�q���
䁷7zMp����_gi��臢q�;�\n��$��X��D(
��\"8�&�9f�wg�Ҫf��｠�\r�\"Eq�c����-(�8Y�!���Dz�Qj�W��1� K@s���������X}Xؚ�g	Y؉P�r&25�\"�c\r
N��UZ�곢q	zf��f6.���sO8��4�Z@�4�폐���^W���+(�c�<#?�I�Jq�_�@]z\'|ir5�.Mx�!��r��q�sui\nŵ3�P|��ǣP|~�:������|��L(�u���\0\0\0\0IEND�B`�','Advanced Profiler_mCoc_60.png','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\0<\0\0\0<\0\0\0:��r\0\0
�IDATx��[kP��6�NҴ5�c������3���id�t����6izM��A��h�f25�j`wA�&�������e�(�^/(��߷�]v�e�]`9=���p�߂��μ�(��g����=ߢEփ��X��u}�#н���͒!S��seK�ӻ����W�L��~e\0�И�~㓜>w�оiqKK�g�;�ʷ�|W��V�f�_%��D�h*D��R2����:9��nސs�3�ٍzGc�>{Ӗ,{s�3�<����=f�7E��2O2�J$��dѴ��٪��_ۥ�2A2��zp6f�h��>+��g{q��k��.s��6s
o,(�����u���{�����˟v�J�/Y��d��}���a��=�9[��rp�)��ν഼|}�jVw<\r��4�q���AlȀ����jZ \\C��7��7���E_��ٴ��?�``錊&���\n����v_�١S�U rxkA�( �0Fu-lC��M���t�`�Mǝ6��fr��Wx��E��eS��C���?.��\\�螕-�-΀�}�����;,��@G��rOG<sx	#�ƈ;�r���\\]v9�X�J:�u���2����G%K�
x>��h��t�*�p�����4f�C�xC�e�� G@����vW?�h�񍷉f�B�I�J)L�G�ӶO��ٍ���la&U��ݢ��h.]�oڄ{��Ѹ��\08i &7��Z�������o�Jo���Aʼ	�	x͌+�i,��\r9��!{(Ҫ��I\Z3����:���0xlexӿcF{4��v���s�S��l��Y�1EvN`#��h\"@{����{ \'�o,��ު��V轎w4������Xz(S��3;\'�\0�H�f���\\�����|���s��g��l����Aq:ӔȔ�]�:iQ��Y�+$$T\\��������k�q�\0�~����(�yxpv}\0<F~��?E�Mfp;�,��\'	)2h9�9��)(�Y�U%�a�;�Ȝx�Y
�#2DZ�&�.�����(Y�\0S�&q�h*�J]W�T���$�ERPLTX����@4��激��G�!�\n��@�|������jo.���r�J�����[٢&X-�-A\0�o��>�Yc.z�XtzS��ϰ�/���ј`�Q��l�l�77@m�\ZX��E�u���B�?����	�n���1����7�z���6?��YY��~j�KTv�}������
W�x�Qe�Yj3uY�Z�\ryb��]4+-��R���X���$��}\"�lD@�0����1R����i[�ڭQ=(�eȩ�%��c�x`in��mX��}U?�(��0������dE�Ȩ�2ʪ9<�`�h<\r0�{�\'���W�{�y�:����hM�	�E�I���\"4]�n5yP$!�Z{բ�4�#.�ф�\r�+~����d��0�/l����,�;Y�h���H�yPI�_�iZA����Ѳ\r���3�=����F�,��F�^�X� �T�\'+��Ř6�<Ҙ�ſ�{��� �����K�Y��?V��������L&9��d���TW��1E�����\"�ފ���+Q56�.����rn�w��C[%Y�bR�1�����4V�ƃg@���� ��������0�Q(a�.3%#��^\Z4f/�X�j+��`\r{�;���߄�
�Jc��� Jdl_�K�,�c��!確�ͥ�kv_9�zJi܇�e`k֨��	R�έ|�m���2�O��\"=꼥*�	,��	�}Q)}\'ii�ޭ$*��^��N�C���ϕAp�h<[�\Z<p�e��ܮ\n&�H�zzj�ͭ�̠R��o�h�PYJNxh�����c,:�\Z�֤s��Olebl��\ZG�Y� ��&�\Z٢k�۝Ą����	��1x�ڱ_݀*�ǬF\nm��gJ�{���]�ٔ�xjt�ai�1����A�P󠋳yP$���Q֜O���s݀-Y&�V��n@���(��{>FzK)�q��kޜ�y����|�x�C�̧w1�R�&�㨰�0{u�&�9�>�}\'Kl��������������6B��\0@�a*a%(��(�Ť����|˝�g��pͤ�t3qBY]M\Z\'l\0���%�ƴx�I����=� .O�队Q�s�����g�d�Dp,�����o��\0�E�-�E���&&\"���ZuݞՎ	8o¨{�	���~��/� ad� �j�x�������1��MeQmZf����}K�=[=���<P���7ء\\���B��կS�C/�J�6�#^���p#>��T8����
W0S7�L�B�y >z\r�\Z���a���\'��F���<:w\Z�و���T�ʬW-X��d�r��>���
�o��[�\"Fѻ��;�{b����\n�:��>��i�Ϫv�U
\r�$t��С��h�ix�	�к�E��4�q)�E�J�\0{c�9V�1�Q���8��`G��i����Y���\'Zf�.U,�\rTn�� ��+). 6d����+ٽ�}�M�i�x�u�#���;�e1.ĕ�	mo2���� �z��\r���l6
���g<\"if���=x�3�
�𘒹�\0����<00eS�.%C-G��XP9瑇p���4�VJftj�Z�lx�Ũ�P�̱%��+�T��L|
<��И\"��]hޢ��Ҵ���mִ�g*n0�\'\Z�:�1�t���H�}
9zH	jʙM�p)�,Ѭي��8u�t(å$����R��Tz�m�t&�i�E&�����Rg|8}��(�Kژ�⼎G+[B��)���6uYJk�Qm@�jH�\\\\��H�!.��U<��E��HW�h	= �#\0�2A4��\\̴k��J�\"3���#\0�TP�N�,�xqw=
�� ��L�5쬳�<� Ŏ�JѴ�g�?���\n��d��������d�ܷ��P�s��,q[�|�=�s�\'����;�1{9�����J�\"np?>��`�#�����@K�\0\0\0\0IEND�B`�',4),(9,'Profiling',5000.00,NULL,NULL,'area.level',3,'Super Profiler',NULL,'Super Profiler',NULL,1,'',NULL,NULL,NULL,NULL,'Super Profiler_tttM_40.png','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\0(\0\0\0(\0\0\0���m\0\0�IDATx����KSq\0p�Q{1B(DBz������{\n#�|�d�B�D��L�MTm)��6���lf��lC��b����~�+:�����ݺ���~8�{�=�[Qq�0���+�#ᵏ$����\'W�p5lF��-Ğ�h7� ꊠJBi��/�ɵǵ��1z��l�OW�R�i� <����;�C>{;�bZ�jE���S=R�8A4E���T��8�@]H�P\nY<s� ����\0���6.R�L�nm\Z�r������q��[d����Scs��;�D�\'�K��Pq�HfeC�d� ,$�adϞ�\Z~W5�,R_�f�P�vU��^2��`�sp?�a\\�\"�e
�}��༎KL ��9�p�G��!�}R\ZXn�n������t� F�Nu@>�1�\0�5��|Wq�u�\r�f\"ep�&3��/1p������]�2�\"\"eq�1#2�}\r�����q����H=8�y�!�l�#�5���lr��U�;�)��G��-\n����\Z��)��.
�u�4^��!�x���Υ�I��n��̧�B��1����h+ƠnF�~F�r�Y%��.�\"+�����&��nBl���I��ϻ4�^�`� w3l��7�V���d�k�%.���a��G���vv��0-�	�<J��CvxZ�[\0\0\0\0IEND�B`�','Super Profiler_tttM_60.png','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\0<\0\0\0<\0\0\0:��r\0\0jIDATx��[	P��N���ij&I�ITMt4j��1ĴI�3͝v2���L��a� ���&j<�p�!����\r�c/@%A#,������e����~��]��G�L���eǅ����}�������~�~�<�\0n�]���M�Q��Ru�^u_�Y�l�(r�Y�}Nm�y��G�dqk��G��U|��&��֜?�*.����05Hk�ޛ���+\Z�	cad�>?8[����2a�B�P�LY_�Nߘ_{��tͱ-o5d��{�TdG�a��/�Kx/���m����^�?��2�5�l����� M�#P��q+k�Wv)�W�q+�kb���/��V}�\\��bʯn�\"mu�йz1�Ia�����n.�����`=���`.\n���p0�cA0�\\��^ ?��VM�(b|@}���A���I��ʔ���ԧ�U�m�A����~�g7\r,�Q����A��ҋ��h;F�� �I*`�(Z|�L�Q��{j�V0�Շ|�/9�h_�+f�����:�/1*�W��7��O�sް�p�L����9��\Z$�\r&�g���Ւh-�b�$���BJ�=��E�W���x?�$���Y���_�:��ڬ?y_g�L�g �i�?��MC\0��HY3R���t\0�\\>P� ʸ��4�Q� �K�����������K���:�j�q�Ct_�4_4P����������e�����R���\rܣ��������͕��(�+VY�����?��Sz��:�b�\Z��;U�������\0xM�(���H�e�Hs�g��`��.׀e ���X������u��_�D��&n����ޔ�)A9���\";�ccY:��TY �Y�A��^��c�A������t͡���(\\̵G���f=��qt��P6�E{v:@]��d.���#��/�y6�TgA�7���`P&�3��� M��eouƦ7=Tg��		N�.��=�1�kо6=ڻa���\r�Bo�e0��@��dxc�ͨ��[�jK qSQd�����\n��&	7`�\07��\"��X������.�%�K\"A��d���:�3���FY0�\Z}%/�A��������,�	���6}��b`���]��KP��#��@�Oo,W�+@���K�0��˘�\Z�-5\0F1w`i�;�T��Թ�g��d����4���֢��,1)�=�?�}��}�0�;�P�fCm�XSX��B{}!�w7�T.�������TP�]\0&�	��uY�ZN{ڗ��b��R#�X쏡��^[Ē�\'W���O�B��T��$tC��T�~81`ּ;t���dE�%�]�
0�c��\\]f%TƯ�
��g��e�֒�iy�R���eĂhڻ���4.�&�k�_{w+��^%�{��ی]��.˨�|�ץGFu�<(�eZ�r\0�#\Z��p���ւ(��؇�ꠣ���9!���䖷4�o�ۥ�F�yP\\HH.i<<���W�tO\0��
�?^U }�P[�T_�T���QM��ٮ��=�]��e�Ay�S\Z�:8\0M��P�� �x�a¹��젵��.uړʔ\'\Z�	��s8��æ�uh<`�V]%Tf�
��?\0���@�s>������1nŠ*u}y�c\0�I�|c���衺�	\ZSd	�7)��0܏����g\"�w���l<?,a9j�*M�\Z�1��D\0�F���R\0�з������2�9��a^P9*�:ָN���]\0�> Af�޵N-�<gSm\Z1��pL��iL`����L���e����^���^H�n�|FiL�%��GvҀ�6L��za(��v��6�m�~:�E[]V��xJ��x��4i�
�����n���.+t\Z+Ay�h(M��v
p�h�:i��iY�޶�d6�f�o7@ӥ�p>v�FoE�\0z��7��S(K	����Є�w��z,���t�2��ZU\'���S�k3� \Z�*w��N���^2Yi�c	�\\
[�EFش����
~i_(=������W_����%�������ar�\n<���3p��p F3.yD~��{�M��I��|!�pO�$�]5��[�v�<P{H>���C���2Ǳ��ڙ+��Ҙ\r �xL�;��7���{zt`B�;���k���xx{�e���/ݴ�S6\0D���\n0R���n���A�t��`�(�Y+R��)9 �{�U�-�G\0�\0?dk��Ff���[<,Y	��N]��=�\r6c��,)L}��|m����>h�U@E�+ ܹ�3\Z{l���ዱ�\"�Vw&�f�[��MBuU�2��;���I	�WkQ����2^�&Y6�sv�>_�)�G�x�#��k&c�\rٴ�iϺ�i�j%C��*z�PT��}���	��6���a��Vm1Xe_��,�_m�K�[��ޕP�_�`�qF��6�:}�6��D�o�;��.��OC���5�0�^]E���@y�kP�/\0�b��s�����\Z��bV�Ęŋ��;�{\'4vi�gL����}A؋n�Z�#����kh�A}a&�w�\\��P�?����m�
\n�����B�~(�^ ɛ��|a��g�sV>��%}S���!X]����1j���J=������c.���HJ���O��ۄ�B��?\"����JU(�3�3�R���xt���)/ؠ
�\rn�Ky�t5�o ٱW����Ay�S���}�J��mА�!�\nBP��9O�]�-y�89.��Z:�XH �ǁx�
���	H=&$������\0��(]�lQVg�#洼�Oej� �4�3|�ɳ���4��Fp�܎<%�ܨ�&`R��Z�jq9�ʪJۘ����È�,��\'����-��1Ԓ�������#@\Z[�:\'y��>O�c�\0L{�h�Ɩ숱%�p*�EZ��]f�كi�g��\\����1w�$�g�����۳30v8j��JF[��D���H�pi!�å$IA���:K�g�p)��T��I�
r;%|-ހv������E]l���v;������I�uY�Z2��o�ƀx�s@��jH�\\��9 �j>��2Kl\"�a,O�4�7��m-�d̴3+�f�����\0r*�y�~�Z�Iw=��Cd\"`��!�I�t�>��S�\r�)V+�7&+��Ef���6�2?��x(���,���=�s.r�%g��� /mb�7�����J�\"n?��x~�n���1k@L_:ZT\0\0\0\0IEND�B`�',4),(10,'Saving Water',500.00,NULL,NULL,'area.level',1,'Beginner Water Saver',NULL,'Beginner Water Saver',NULL,1,'',NULL,NULL,NULL,NULL,'Beginner Water Saver_3LLZ_40.png','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\0(\0\0\0(\0\0\0���m\0\0�IDATx���n�P��(>��?|�=�o������Ƹ�L�43.q���`2�G���J)+� Vv���Fd����P%�IN!�����{n�Z^w��u�n��Vw��v�oiv�f�n��2��7����e���k�k^!�j{qf`EӹUнgQPI���B�ͩ�ޒ(b��<���퍩���	T�\0rǴg\n\'
�\nRNP	rTs� :�&�n%6Ĥ[�>�]%@lRwSF��ۏ��$� J���j�.L�ۚ�E���n�A0��(�:jM�adk�X��Ad=���%]���/=������Ґ�c�J�p����ă\'_耨�R��VTw�T?8���!j9*`�}2���g\Z$��T�2���J�1{@^{��!䌎�����lyz�l[��t����j��&�E��RS\n��^jy��%���&���F���0����f�3�A���F0�{0 6�f�E����;� 6� ���P2;���X8��pI)�:�:,���\">{0~�p��N�	�㢗�$�bAFnU[�%�+�G/<z�rKV��\r?L]\\Zy��W\nf:��\"�_��g�Δ
:~ �pq��,�UM�jK���{��(�C� ���?�t��3=gvp������<��뷈1��]`����M�oԿ�Sy�\0\0\0\0IEND�B`�','Beginner Water Saver_3LLZ_60.png','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\0<\0\0\0<\0\0\0:��r\0\0
jIDATx��[Io[���\"�;R8u/\Z��lW]hVM�i��t�6@�:�Yh3��[lն,Y��ĩj[�`�\ZLqII�8��|�q)��|�|���%�/pAA��s�w������-K���� �{=�\\H?�	�O�3 �C�G��Gzf������ӑ\'���c}�P�D��L ������쑤�c�箨�+b����tE�O\'�Z�xXk����N����˓�~�Z�_��UO|L������`��s�+���+�~������;��UgĘ�C�������XXK�R�3gPwP�����s�c�^i��+���H��ͪ/姝��w�6@�D{\'���n�ӿpF����\Z��L�T�n�E�Nh*���x��9rD3��S�[�
�ڰ�\n��R�Шw!G}�9��)�^�rz �>sɯ��S�@k+}k��r�:��Ϻ\"�YGX�g�G��Ѥ�XFlW�Xߓ1���:GU�\0�ٴ����w[���v���+ށ���H8���G�s7ih���;�\\L=��2�A���yӫ\"�\0е6�4�-i��l}::G<O}s�^����g࿹��L�w\r�H8���~�i{�hne�&9M�ۀ���-Sw�A�S�;����*/!��.���L�\'~�k��.R��l���8�Q�<�\nG;��O���� �a���4��~�I���G�yK %�h�@��L�垐Hq�ǯ���\'����Ȯ���`�F�6\r��*�u��3�뗦4�f��&X�mwp%��*hYBz����bo�� (�Y�1\"k ��y<����Jt�f�`3���NS�R[��h=�� (Ԭ��F�+3�x�V���۹��\r\\{���\n�Yn=���
�54��,E�\"��]�K�8ʶAW�\ZZV�O:\nq�J\r(���D���&X���Mf)�Y�By�2�+P���\\Z��yw�vj�O37x�C�#\r��
�C��PPv���{:nP��.3Xs�K�4��[
�sۯg��^$~��h`�rD�?u����[�`���RyZ^�\0k�,G��-�.2k����s�117&ڷ#X���� �W8�u��j�n��Ѽ�\'�X�Zkm�(Α^�m�)������LY�w�<WmCEv�-��$��J��ZԊ�T�C��W1Z��?�܊xx燭�d�|C�r�҅� �;,�N�#\r�t*a����X�|��D:�� [N�U��V#w\"C�$TZ���P�N��Y.�jW\0�G:u9�|rh��zd0��A���Sa 0�y�I*ό\\?�J=�\Z
��n��	�\"xd\\*�0�h?7xP	��V��=]9�LV�\\����m��D%�\"��+\'����[\0�J����4<(+����b�0�T,�ZQ�s�驤�zʋ�	���A��������Nch\'�f�\rV^��\\:����L`I�iͽ}�ׯ��&9|cX�p��If(��]Y]�\rxyeM��ݳ*���@`�;�f�%O�����,
�Q���q�q!0���/�4��3.�-�������t��3`�[�l5�1nC�*�#Jv���>�L4>;#�A�����Nw�y�ME��t6�\Z��%VN�dN����<\\\\dy�L\'� ����	��a�6�Hg�+�V(�Y��M�\Z�ty^��	�x2�@Z#��;��U�B+����Ĝ
�x+]��!�]���Q\"�q:ε�f��jF�j[���Q�~9O}�u1��J�\0�5Ӊ,}Ȁ?���(ر-U��ְ��a��C�?5���R�����F�w�n���� KK�R��ΰѐ�tT#|+��hVZR�ɫ˯� �L,� .��gsi::����5Tl��������2���%��=���^D����EMX;V�|����{c����KA��5� ���a��i	CC����Qy��� TaIْ�=�d��h���u\\OJo�-j������A�Q�`M�e�I��|��x#i�P�Z�������H�7���͑(w��\"-a\0\0�v���֞�4l�o�3��V� [͕y6.C\0 �WX*�,�6�+wfp�-���,8�1�w8�M��4j񌅥�\0y��8�n4�g8�H���I��x�����k�w��I�C~��¥�D�E�m{�C�N�&����3��c�۴܏�ie�i�3�r����̴����9��c����������T��t^�iCuڴ���\\6�x�6����:�{����$���u�cZ�Q�v��x����_[=j��XeOq]#�,F♒\0U,�	�����h=�� �O�f߭F�{�:4��p;@\\��*��@����CE_��v��(�� \nu�d1�_]�7�kܯ�iV)���?�:p�E���P�H��`w��y��e�0m�EO9��	��V�Ka�s?�,�ⴔ��$�Lژ�Q\0�>���0A}Ģ�\\�2r�T�~\\���SѬ�q����k�EH;�\r̶PM�3P�O|-�!����ض���q���^�|��� G$�2?���+�*x\Z�K���h3���PP&1�;�Wp��ϯ�ؾ�Qϴ���8�G�u���\0�Z��j�RK����K6�\0o\\[J�ڒ!n�\"Ʋ�ݜK-��Y���^�3�{mi�D��f6�T���4ji��i�j�����g�޻w��٘	jS�ޕ+�_.��hG�%(���˥��^.�6�\\����@�E���.�~Y�0�>�u\r\Z�7���b3�c�6�\\���\n�os�����3��LLY<Z�BΉ�
�
�
�z0@�
ⴋ�k��>πAlo3��\n���\0,i��@�k�V*�En�#\0�(�\n�g1��=���<h?<2���Ň<ҧ\'c:>�q��/���x��J�b@=	��7�Cw�n2�~h<b|����8����a��7��\nw�۶Խuom��5;R��}�\0\0\0\0IEND�B`�',2),(11,'Saving Water',3000.00,NULL,NULL,'area.level',2,'Advanced Water Saver',NULL,'Advanced Water Saver',NULL,1,'',NULL,NULL,NULL,NULL,'Advanced Water Saver_4Lz1_40.png','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\0(\0\0\0(\0\0\0���m\0\0�IDATx���jQ��(>�ЅO�BW�
�\rBw�\"�h]
A�Ŕ��*��X46X\Z�&F��Mҙv2�����4�q�؋!�̜{\'\r���l�������w`�4�aHi�����I)퉔*�0Gc��ykY޸Wb�+�p�PS��Y�Li��Й7����R������ZI��\r0�\r��=���j�A\'׋�n��2E�����(���@M\n K,{�p��� E�D\0� �z��$��ѴZ\"pY�\n˅=!@�tSV����O!H\\A���r:�њ.驢�`,~���!t���jo ���f2%�4Z���D���{����UO)�_|5jp�u�
���@W��[@��i\"Q: ��@���u�;�R_��C�rT�/Zxca��i�F?�G�v/���^�ނ�����s	��ʟ, +/���L�2_|��\r	\\ذ`���Q����-��	q�0@�1e�ND��?~��}�5m,PA)��~�8�(�\0]o�ԣά5��:����Y�_MO�R����������÷��C��b9\'5�H���z~C�ix����q���ҏ�^�zW}��kZy�T�pJ�UV���\\0�*b���=���2z�I����\"�X~�K��\'�쏴���|�tzp_�E������BXR}`�w��.�}����G}���	�_>`���it�o�����L(�\0\0\0\0IEND�B`�','Advanced Water Saver_4Lz1_60.png','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\0<\0\0\0<\0\0\0:��r\0\0
�IDATx��[[p�N���4�äC��C��3}l����C��K�$��N�����PlA���ġmҦM[�`K�@ �1�eK�\0[7��ll���ݕd[�_9=�J�l�Պf����،>�s��;����;�^_��ѝ�����m�^bm��g��������ć�ؤG�77�gS�a�-��^��f��m�@-.�1�[}��Qs�ݡ]f�Rhu�E�.ź�.�5Չm��֭vɑ����ؽ�Yz�__���\0��x\"?�x�g,y�ŭ�2��~�y���;�nE�z�p�S���R�C%�=D��⴩V6ل ��Za��Z�v��U��8v6����r�mn	?du�~���I\09�1@�}�T��O�.����Q��E��� �a��5U~ZwR�u��\"e�J�v]�r\Z\"��1L�ԉc�	��On����nC�b���q���FK��S�j���K%e�#9ʀ��\rS	�G�j��&�\"��\0��<�F������}Rz�Zp���6�sF��,t�??��R�>^�s`%���qD�tq${5`\0=�>�Q��U���뿀�u������Bh�d���.���L���y�j�I\\� �u�?�_v1F ۢf~�i�j
��Ιa�hk��ͳ�o �o21�]����Q��Z�&�6��\0sz�αO��6ڄP�=p��%�smf$��=���]�̴M�\'@H��T�fp2�ٵ��7���zi;���2�ވ�,��`^�@3 8Y�qb{�N��Z/��pݓ��E\Z\'�&���N�\Z@�l����R���!���P�HcD�XM��ޖ�&9oV����f`l�����5���.HO���QԬ����Q�W��\rȇ�2\Ze��4���!�����β�$��Q���2Uw��+4H5�b���!Z]ѝЂ�ސ,f��rd{��[q �TBgK�-v�ڳ�#J��a��MRxx�����L<�ל0��\0��7\';��wu�����%��*F`*��m�K���\Zء�)J���5K�Zz�����6׉�8tt\\fg�\'�A;�\0��n]�=#Ů�\\��I��!ni-�~� h4�����M��h��z�pﺞ�9v�k�0QqiP�?F��]���e�K_��c�몍�-&��mvqA��Ă���&��\Z��I���*_�zzGi��RGh�i����cL���Ck��^Z�`y�9�\r�nQ\"���ML_^�$�L{d�r�$z��;�H�ٌ���P#�n8���2nej7]����k�Pkp�F\'�)�5�?{����:��\n�t.�rNM�\'�9#��b��LF��H��b����._N/!ԑI���j.,m�Hk�e��0#����y��ͭ��M�@Yʨ���+���o�\r\rq=�����\\ajR+ppgc`�{h��f��{	���a�`�mY�J��9%(��;:E66%�kz-�zf#b���bE����9\"��TL1p�
؜x�d�84>MF�X~S�VW�����\Za�MMC^�����}���k�Զ�n�0��m��O]6x�	������`�&��>��$9����0NX�̬�\0��\0�5���`��$���3ip�v;ô��%ʪ��Lm쐡�S�-#\0� /��~�+]���2d�(��������[\'`�L��EwF��J\'����<���a�T�3��6�N �(����~zC�� ����4j��&!B��T�`���ih����(�����J?�R�����M�tI
RT�Rt.0�\rX�s����{��q���Y����r?ײN�	�2�w>�1Y�\0{g\0�v��\0���L�q3�?6ep��WU����Ϙ�2%K�\Z�d�[��$��n[��Bl��\03u���{����R\'����z��)�b{��Z&k�9:�y��ǵ�8�C��4b �ۣc�qk�^< ��T��X�}h��8ʺ��d�[~1J�}3�]��*�\n]*uDF�|N���=�Y7��Yj�g� ���F��9� ��{���=,K�=D��Uft�\"G������Tz>���HZU��?T��7G.qƤ��hs���ʲ^�`9ZÎK�\0\0��\0���&8�xCO��7F�iQ��!�d��`�	��Q�72%�7����[��˃����%z��8+c����\\>3��L�����Ą�-���A*g�]�ViO+��{���d�w�L�Y��)��r�_)�YX��x�Z����\"v�sX����ɑ����wFG��憠�;FAf߉y��o�X��\\\'�s\\��N�3:���x[��͡\'�i��UWǄZ>��%D��M�1�����y�����z�Y���.�?_c�YUzǴ�(�A�GM{�>��h�ku����\"�Y���-��8�kS�f�;�2]G-f��4�ZJ�8jA=p��8�S\r��SlF:zǴ�jwp|Zb��AP�Y����$�dӐ��Q����h�6�:jI��.97��4D�]���\0�wJ�Bo�*���!j���?�R>���LH`cj��k���ô+��Tg3���q)@�\"� ����&j]l\"��*�pc0��击���I�cEO� �9.M�4x �HC�?i	QAS�6�����|�H�f�V0H4�h��ɚ�N�@\\a���\Z�7>2r ���`v�Y�f��+%�����ʚV��Q�U���aVU\\v���]y�4�;�\"�Wf���.��+x��K-\0���K��X���o����f�/e��R	�f�jb�O�8&y�%��%E�,�f���U̍�v)\r��o��4�,�8���iv/%���q���W��f��ͽ\\� �x�eb\'�W].�f�ri�:1Lt��].����G�d@�2{}�?o� �%^�_���\r?h9�)�_�i6�ә� �� �\0�1��-� >�����<���H[�.�ֳ^���Bg�w3wC90�!m���[�\0��\n4��g�������<�Ǵ� jݫ����U���* �ڥs�:�<���(�!V`���2��k<0,x-�pkߒ}���t��S����wp\"�!9���b����m�\Z���+���`��\0\0\0\0IEND�B`�',2),(12,'Saving Water',7000.00,NULL,NULL,'area.level',3,'Super Water Saver',NULL,'Super Water Saver',NULL,1,'',NULL,NULL,NULL,NULL,'Super Water Saver_xsyZ_40.png','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\0(\0\0\0(\0\0\0���m\0\0�IDATx���KSa��K�P�����Р����.���@��ZDW�E!�?2��+��c��	3+7s;;�S���Z53��=Gޘs�9��m5�� �!�����~߷�f?�a�4�K��r�qi�1��#�\'�SÑ�3>���PGw�b�KZ\n��}}O6����RN��9��Š�\0�y(�(9hN��b\\@�h�W��\n�C�\0�ĲW�
�RNP	{NN�ܓ8�ԁ(̉xz��J�88�馬�oX\rWeױ�|�5(��ZT	�RE���=����_P,�FB�n�]}���-�Efs�� ٽ(=����Y����/P�;ς,j0Еp�|�Ը�:\n\r�r�
r�XZY��J��y���Q5#\r��Y�z��O:䨗�⑻�� �����6΂��\"u=�
(��t!�\'�e��5�\nHxϿ� ߱\0��)8pc\Z�T���f��ا�,���ɚ�-j<�D��W^���O�М{$Jo�ԣ.�u��` �\rڼ�Z���K����gWb�Q��C����ϮB��t��Mh+����,����\\rD(����^�{N�nq\r��ESA��\n�0��;3�*��}8`B��C炉�	8+�@��*���������Y{s�tz�/�*�k�0W��$���ލ+vq�����x<���\"k��{���\'��(�?*�q�)7�\0\0\0\0IEND�B`�','Super Water Saver_xsyZ_60.png','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\0<\0\0\0<\0\0\0:��r\0\0�IDATx��[	p��ױ���c B�LL,A�\'�JE�cQk˴�j�Lkk[��\nHv	k��(A9b�ͱ��$*G �#7$s��������$����mvIJpw��Ep�o�<&���~��~��k���|w$��o{�7(\r?T\Z�	�r�-��]�Io��\'=���ݣuL�\'�<�L��eja��j�ħ�;oV\Z����%�\n�po�����h[�Ћ��Fkj�QT�YdU/�\nՋ��������\Z�����\\~�S��+���:�� �n�~�I�V��g�\r�(��f�њ�0X�F[��`�࿷\'�����+��q��b�������������������;3�5�s��O�\n˶WW�����|��m�z��T���!�b�虄�����\rJ� ��	���T�X�d�0#��t��!2��Z7�׹�>5߿4�O[�\'�X�8��曟S���b`=5*�T��8��c4{��
J��s�6��HC��oZ)�0#��H���C$��=S`7:��(::�w\"�����.��������|yli3���x�u\r�ۏ�+��+�X$8=�M0YG\0s�\0{)�j �x��	
���e������<g�~�� |u��F���(��\"�\Z_�D0�)���(��e��X��/ϱ�J)~y���G���^�K]���j `�\0f��v�*�<��!���h��&oi\Z�-�o�3/`k)�z$B�\Z\rhH\0�D:\"�����K4��\'t\\dHӛ\"���[��y�\Z*��[�($�{����>���ڛBV���^�>ƽ=)�3��JoTm?�����#UBiL�����^;�-Ge��T�<�#���ƚv�L��uU�%\\�<�>HlLE5+ (�8��]l�8$�%:	��9�|�X�Dd��+t�K�E�Yj=^!!�[��<Ↄ|l��g�;`J�9�y���D@}�D�$(E����e�O}��`�.ٟ۪\\Pn�ξs ��g�=��v��HOO���Db�6X��5,���h�\n�d�������Q�qx��E�\\�2�}�xOǙs���Ku��z�����Z ܧ��1q�?(�`\nZ<u+,E��6��8��{�N�\Z������rAGk]pG�����\Z;�_�\n�}�B/���s2�P�,�tqMC\'\'��0\\|��������Լ���H�\0��V���)�FK��l�?��6��i�\nx=�����W���}\Z��F���fX`��z��#\n�,\Z-Q���0�}:Yz:�+��mcg�=7��a�l;��[!lg�gT��F����wf��փb��Q�,U\'{Sy-�}��	�pj�<z�k� �/�Z ,�Ur�b�4��.����Ȩ� E��\\��ιMǡ	ih8`�����8y��N�������h���|��|����$�XӍ7�JH�b>:�G0=���^�c�z6K�g��Ewesm+r��x�x�ws��Z�\0��}�����c�m����\0d5v���f��쉘�M��#Z~�\n�8�b���[��j�Z��4�Je�^���=n�\r��{ ������ARZ[�d�C8:�>�n	�0�����J�b0�	TSc*�Ğ�X���E�}�\rV���ͬ��V]\Z4�12�/e�IJ��4	����SAե�y�K��>�5�|椙�Jm8�N���E�㌌��4�*�)5�����c�����\\���U���@a�.����¯M�j����>Qj끌z7KM9g��
V�\Z᎔���B�S����b�^{���u����v؆���G`�Γ�VJΝG!|G}�i�7�����M�v���\rn\Z�� wޫ����vـ[��`K�\r�}r�&
�i�
��%c�m�
��V�����6v�4��h�aP`go?�ֹ`��C0��\Z�����%�.��^�p;����_k�aqJ\r4�� �X�ϰL��\\	���y�Fx�õ^���#-�5��ԅ:x�ͼ���e���*1�]p�_:qUa\r�4C��0�#��%i)�/-��l�$��ޤ*$�]�أ�rH�ՅB7<��\0�I�0��\0�\Z~���e�ၞ(b�of�ay���F�$У�p��6
�b^3�[fg����
�V��Ja��#�t@��\00o\0��ZSxAiy{q]�)x����ς�\rӳ��1w?�:���:����o�a�L�^	���X@��\0�_�s���؞��x�Jk
�{��0x��4u��>�c>�)�į�_��[n��^��K�,�^jG���[J!��r����o[�Y<\Z�xD��� ���\Z��SP&��pN:Ԇ`N���
��=�o��Z��Wn\'gX��;]��4�����c0{k9�P3�k`NJ��><b�-����x̦5�1ԏ��i)�:���1})J���}�����D\Z��ا�u0�z�����f�ǉ��M�M\n#��5iun�k�&(�YJc�,�\r�n��f&#Y���Og�F<=���O�S�Ʋ��Z襁�<FPO;QW�a��
�\"S(��d�O�3b+��ClLE5KiL�e`������|�S�g��_�^�`:zq���4jSk
E���	�˩g23�D��Z|��	_� I9�x�\0��k��bl���2��Y�@Ӳ=�i��c\Z\\\'�T�!���kپ^��˟���ZX��\n�L�A��Q�d[�g���(0f��ʓ���s)�wJ;�>���������O�A?��~2���}�`��*��r���m5,}g}Z sv7���hC#�Y�{y��g��� ��gVc�+�<{o4�p��� E��j��)�L*ҝ��r���r�+Y\\�CZA%{���^H���
^�R�Z��L��a���K-b�%�+��J,�oE����1Z �-�03]�k�om	#K`��9B��4��Gһ�/�a�R\Z�z1����jZ/� ��������j��l��֙�=˥���0�
�,��x������\"QA}�Z�6��˥��V���$퍿�^���Z�b��\\ic����V����-��Ƶ��e���K��W��^g#�N�\0icς8\\����C��Ϣ�Db�����l���\0q�#\0b��9\"��8��J�\"n�A�-CN\r�4�҈��s5|ȃ�A�u%}� �aJ�b
E�����l��|c�R���+�$�[�!��<2�q���0aS������n�{�\"/Ӛ��AN�|c�R�]$��[�1���Ur� �\"#|��I�\0\0\0\0IEND�B`�',2),(13,'Water Saving Insights',1000.00,NULL,NULL,'area.level',1,'Smart Saver',NULL,'Smart Saver',NULL,1,'',NULL,NULL,NULL,NULL,'Smart Saver_Jiiq_40.png','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\0(\0\0\0(\0\0\0���m\0\0�IDATx���RRQ�}�� ��G�jjj*Ӳ�i����!�f���� P9�MPqw��A=�}PIg\\3�����������鹏����q �zL9 �i�鈎�K���3>a_ٝ��*��	�W�]̹��K�g�����G7�(�\ZQ�2lOGtT�vP�({�*f�W����#3���U8��W��.�Ye
����l�3� �=�n�D\n�$�*\"����U�����;6��Sl#1#$!u�X	?���)�f�̗Z<wģ[O��� ���U��L裐�TO�H��&s�,̻��J�UKJ̿�`��8�tp�Y<τ�Z��F�<�NBTE�@�Pş��	ؕ���c�{��p<��\',�^�j������_TM[��8Y�{Qs��k$�����^��g��l��������dc��,��6�@��楯i��ڂ�5!U�J{��r�Ђ��{�|�����p˳$�)}�M
[Е���o��\0ɕ|�f��<]�)�RU;6��>�i)���
b���\0���5G,�!�\nUY:� �ԛ8�\'M\"S�x
7CX�tS�Y�\"���t-�-��\0\"�qD�)�@@��`�.` ��SYn�CM\Z޷�\n��q���V_���\0a�7 ��`���((�̚�!�!�����l�J��ڋ�HDl�`\0DO6z�H�P<���\Z��S��Qu#@U�1jL49z�R&�1`zl��s�q@�^p���R
��o;�F-2�8N����\0Q8N����V0�!Qm� �@l5��$S���-\Z�D+ L\r\0bY��vmv���\np�ݬ\r�Vх�}��\n�&_^E�V3+�ҪP.\0E�����>4���J����.���\nƭ���a|?��ܚgcX\nW-*\'�{]���Ww��ѝx�vɆn�
�;�\n�>.�?%$���	��\0\0\0\0IEND�B`�','Smart Saver_Jiiq_60.png','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\0<\0\0\0<\0\0\0:��r\0\0sIDATx��[[oc���Җ\nQ͔�<���H�x@*O����\0J��\"��R%:�/孝Ba(f�2308�Ĺ�f�����8�Վo��w;���o��[�\'q&�}��9��cl簿���ַ�޾瞻���H���wޟɸ��}(Zp=�/:ORç����h���X�V��H�c�`�#����<YӃ���P���/?�7��՛V�y��\'���\'c5��mK��e
����!{��S�v]p��~7�z�+��2��ˍ�ԗ��ڗ��џU������-_V���iv$;���٣m�$Z�k!s�q�jZ6%xXL��
�`㫶H�+��:�oe����\'�|̟�����|Y�-�Y����$��)X�مQ��s����r6b�48�ݚ���f\rd\n\Zh8�@�h#)�Vr���jY���/�\"]��R�?4yZ4��wl\'Ǩ7m{�]�3o�>�V\\d��|�@~D�?��7y\'�K�p��x���1�
�A�Ќ
����-!���������)������A,ތ��*�o�/��\"K_q�%�+\rn��Ӗ��oX\0k����O��\"4�L���o&�g�	�
�����`���,���N�\n�o�����&��[�l�����_p�q��.�\'��[�于�=I�C��z�lJ����S=�Q�v�]��ٴ� ތ�EF1��7@H�Q�@+�Y\'`����kdg�5g�����b?9Q��eU�v����
�$,|��AlLf�P��=㪥O&fٍK�������9���\'���zd�*���ٽ3po���{�bo�1Jb���� �N���}/����肇�Sf�Ir.��f�S�:��{���)%���xϙ#[�Gؘ	\n1��m���L~t?����ͭ$)Q��x���])��0�E1y�^�nK��[`oNY/]T �r�ф��lh~�r�qZX�h��倓�\0��z����Xt�E�&�#�21h]��U-��e\n5��8�K�)�-((�\n�Y�}ֵ�p�l~�\n�Y��^g������0�s�w�+9$@[�ޢ�ޏd��~Z�\\�T1H\\P�{z�\r���!N�H�YS������f��|9%����I\n����.�bA��q�vS��uM�W�pew�LK�
l�I�)���O,_�)�V���J�]�力�b�\nD�\0Ч�� �ä1x�I�v�u���9*��(�e���7�ڎHqz���u��Z������{Qb[/�+�����e���Ȍ�ȓ���\']���t��@nO?{K\r+�ؚ\0���/\'�ŵk��m�l���\n-��)��\0��L���Q������c���7�\0���Zf��Ҳ&���YHGe\r�@5�\0 ��2�\Z����L<�����.��-��Z���U<��7v?���ɏ	�n�qiX{\">@���X�����=��皀Q�k:�V�1�^q}^&>���E�γ���n��	ff���x���h�O���Qܷ(����+2?͋�B��M���!1���C���V�AI[&��C�\'\r�Bb��Ő� �v��Ĳ��\0\0��N\r����VQa�x/���@���� ���/�Uܝ=U3��Ld��W�2s7~8��d�\Zn�A�-SOa�Ƭ��rr^u��V7�d�Q�.��2�oj���ߤ6�yos��GF��<�kek��q�l���}�5@��;y�\0\Z)���D,�]�Y����tC�\r=(�r����0�mn��TX�J��`P��7�[��L�H���\0�,|  �p�Vc��i�\'-$����:���ޓ4V-��h�Gơqљ�<uPl̏>��\"\Zn�A��\"����[N\n# �Ż=�S��?y�ﲋ5�X�M���L�Sd��oAzK�	��O,���8��T�[Rf��؀�b�����~�`UTp=�V*����4�zY#8�\nIK=�[ܷ���t������Vy=<�*��,)3Ky��zA�\rjn\n�Vu�AW%���m�{z� c��<U���]�x�\'��/�(��v!�\"�\Z9>o�?������\\�\0�c�Ҳ�+e����1Xh��}m�\Z�Hc�X� Gj�49Y
0�\n�@h湕��`��v���`���[* 5\Z\0C<@^��
\0E��O��lU��k��\Z\'��\n��OcW�\'��O,Έ�i�� W�\'���@
��EaqX�$�x���y\"D��&��ba��,\0��_#5���5l�86`u8DRN�Qȷ���`�(�MfH�\\0��tDŔ+y���\"���}ȿ��P_��w������J��\rX�K�\rXq�Yh^L��j�?3S��#���5��8ȱ�E��١l1\"o,�<RS��vs<{��88̥-��4H�_ i��5�O���5\\n���)\n	@���Z�,������<| ,�5���.ѭ�^���\Z�5X	p�i�\\O�łKX�PKM��H+�\0���H
)	̋�!+ŝ�o-���+bA��7|���૚�Dxd���\\����=���aq-/V�xH-�\n�\0�h���Š�����H�\n�u��AL,\r)�π�vp^�[��
�a��ғQ~
i�)-a]03b�B�s2b5<��j�-��9��֝�������9�=����5��J�\\I�Z8�(kJK)�#u{��O�8X�e+�;�}�B�(�Jo�&�AP��d+�gv��
#�CRrm+\r?���ŚҲ�xpDz~o�W(PJ���P�I���|�44�R�\\5E�:H_�x��K\\�q�j[��5�ߵsY�8������ؽ*�����P\r4�CE+�COP�#��Cw���b5�@������	c�����PdZ#X�raEV\\�+����0p�E		pȳ7����~W�p�Ô���.������=\'4���pk�p]C{gc{M�]k���â@p =��wF���s�ݯ�L�����A�n�l�9�����]�\\�ZHΓ���l�8\'�\ruo90)%K�V�����d߳:��槐�e{%w�M3oiM�b���] -��\ZY���jUp�ڴv�mZ��h�g�׈�\0�y9W0\ZwPNPU��{�5�߈�و�V�;m�U�[- �]��Q��U�_�V���b6������j��L�U�fZ��`4�@Z(n��
�Wz_LX=��u���L3��ֻ���]�x�A_�w��ҶK��
-��^5��\n��He������l��n���\rq��Б��v��
�:\\��p�\'��@Q����r��\r�8z]M)%��r�q�cy���V����JH4ב\'APȳ�Y\0�]+�����JGp��-{��G�t�����:t�C-�\"C�8- ��n}�����C-��׵�K�<����R �xџS̾�c=�t�cKj�jV����Zs]ueC�cK\r�ư,������}�73�ɷ{��>G���Xs�\0k�� �f�`Z\nn|� �*�4���9a�;x�Uq����?\\:�)˗��e�����I��p���.���,���r��x��K;\\z@���K�S��Qe���a�R1,CC.BAy���Ç\\fN[��Qw��R�% OO�8��Ɛ�w�x��!�������lQ�
�u�	�-��Lk�V*��h�i?@��;�Y�x���;�#������(g�:b��9\'~��`��XB�n��\n���]D�\r=��ŏ<*�� ��0=��t��}�n�\'<����c
�A�}c�R�]Díj��u��z��{^\\h�\0\0\0\0IEND�B`�',1),(14,'Water Saving Insights',3500.00,NULL,NULL,'area.level',2,'Expert Saver',NULL,'Expert Saver',NULL,1,'',NULL,NULL,NULL,NULL,'Expert Saver_TmZX_40.png','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\0(\0\0\0(\0\0\0���m\0\0�IDATx���NQ�y� �Ec4nQ�Db�F�1�D��,�T��`������ŲنB���e�:�I樂Y��p���0���,�93YY ���u,7;�J�^�������<h�x�wJ�8���������[����\0,8�c`΀�Р� ��o��3*E,[-bFyD� Z2W)f�;Ҿ�pF�i�5�Ȫ{o�<d��QF}�/�l.\\��n�mo��f��\0\"�%X�+����\'_�P�u����t�o�2�d3k�Q+�\r�=j;�J;/��z��^֙#$A\"�Niτ��y�I��S4c�������<�ɟ9�Y~�q�TkFQ�1�C-�?�\"�lmc��Y|%JQ|j?Þ;/� o���ѫ�H|��\n�љ^dym��������j=y�:T&E����*��{z��5�u��l����M��X��,��,��b��O�����Xb�ݷ	��uE��\Z�₁�D�������Ĵ����śBc�,Lе�c5���q�~\0�rEɑ��m	3��U:p%΋r\'�f��ѵsC��vҼp\'�9UI��q���FPuh�(�\'?��kM쓦\0����Á���,�������R\rs�� wiQĠu0����)�F�Ŏ�4i����tfQgH\'��M/�p���ND�`Ҡ��E��\\K���2Z�ȣ�qWѕK ��&ѕD�@�#lz!�^�\\W�$��^��!=��o6J2#\"�u(p�\"��qx�{�}���R����\"�yh����	��YH\'���kQ�W�\n08�=�p9�A��x��X �:�[t|��검n \0�F��`t��M�fM��M����edaMݬ��\r_^E�tV#+�Ҫd�Xt.\0Eo��4!�ѥ���AgI��y�\r�nEJ��!&Ǟ�7����jzj�7�>��ã}��-tO>��7��,���f���\0\0\0\0IEND�B`�','Expert Saver_TmZX_60.png','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\0<\0\0\0<\0\0\0:��r\0\0!IDATx��[YP[�N���i�L&���C3ig��>�!3�S��6yh\']\'i��N��Yl���&����`c6�Z����Mm�f��/��m|z�s�\0�+����z���$��=�|������??
;� K;Jn�x��ox�\Z,{��\\�S�N��XZO�
9ޭ/��7��qm}^ۜ��ֵ�)S[�c~���[1�}��/���o\\j
�,>C�%`Ȳ��V��Z�\'�>׹�9߽٭�l��y�K\r��tSK�������� �m�>d
�m�_��\r ?X��j���e��������PUW���f�4ēƻ�r�&s������s[v�����u�
<��\Z���������������~���:�h5�s������Sm�(�{LT,�\Z!@RF��tر�2��W�ge�ג�i4m\"���녞-�:ϖ״��_d��>ƹ�o\r,j���g�1�\r>��~>Ώd�6ɲ������8�S����u������#�Xk�9V�h�cm�Ɲ�RԐ����#/�8� �\'��/7��3w�_e��\\���s%�<.�0��k�G� |��
�홎8���~gQC�[坻_\n�#�G�֠���?Zƃ�2��$�� ���۰�r\\�I�Il�y���8��k\n7��!��\n\\c��%u��ƈ]L�H�LN�̺շ���z�rʽ�?
��EMc���ٯ_�\'m���7@H�Q�@g���\'fbJ i�\08�@n��x�u�x�l�{R~���Ȇ����`�X�.F�gG{�����\r�;1!+�ŨYI�)�!�](`�t���Ot�]�`�3A3�\r\"�Y�=~O�\r6�B�r\Z#�j�r60�\"^��~��=MM�j8��Л4
���Ut����?��\Z��a�6����7N�w쀹�(l�GͪO[�v�U����@��V�|�Z���x���\0�ʬ��<�<�<q����Ad`o}C��{賗CBBMd���w��X�\0E\Z�[p�7�W��)��Z>�Z>h[�`7R�`-g�)26�	hՑf�F�b\"�q��Y���\'��2��Ϣ����\0�� �h����o\\`\n�B��Ғ3xB.Hi�~�nJ��k�HN��#>:q�:���k�\"j1�v�ҧY��\Z�ט�b��u��g!�ð=�r�o�\\j�� ��.,��{����\0t��)��.��ĺ�*��/�\r�{��O��Bd���ܭE\r���5��\r�t��m@]d-�B?��I;{O��1URu��Fã}4x���\Z9}���;Ig:r�};�{*k�Ï�iG}G�;rAB��\r��i=�~�7�O���m��\n���F܃e��*��;��:�y�\0 \0�颉�c:&oO���	�vc��q��tH�N�N��}]�o�o[���Bd�����>\\w��e6�eT���9�5f��:�@��� ˙y-�j���:F*#�oM�����7�Im˺9J7n]�~��E�x��ʹǲ���SN�-T֖N�g3H\'m�˨�.
֒A��a6��)��v�H��_���v�`�j<WM�8���`\0�}��%R$\'_ں�N𣣧��^��&_����B!�9.&>�}��N��T%G!F��]e9֭�8��X�\n�:��C����a��%��tG�\0G�*Q�I�ܢ��4�kH�:��Yy9��2&�̪멼-�Z�/�N\\��\\���fhH忡#�/g�A.�>�ǜ�+#F\Z~\Z��c�!��朑��b��L,��̽
�Wں�JZ��Չ�Sњ��ͻ)��M^�Ԫ%�u�����m��wi ?O:�\"���wq�.�s�K��.� ���_�o]Q��Ear�qfd�i��m+��\r3(�rfz+%`��T��7����z��m��b�K���gb�����z�������_D���Ad;kދު��b��+�ԕ��b�����a�^-���m�w�#��m�����G�}&�I�S�Y■�!\'A2���\"2.f�+ׇ��;�]��\0����*���l*��on�vT���(��V��\0#����@���r�[��B�x��}����Tvj�����Y{�=����{��{��p\\;Geg�%��ƕ��I��1��\r��D�֓��]�eH0da���b4�����\'�|�Dq8���D�+�!Du���\\\\���p�< 
��q_��m E|Tu�KO��v#�y�O�w�w�;,\'-��8Rt\r���g 8�\Z\0J*tpyH*�r��C�0`\"0Z�ە3�2���1gF	��:��5~�����˻��QS��Ҝ��c.��!�vK�Bt��ȥvVK�����۲Ld#2\0e0��m���n��\"�t9�4~ �\'� .*Zk\n�wmnX�V�%\"��� O��v�Q�̾\0��\0��0G4T\Z��ϋH�V�O1�*\Z�^��$&j�HG�6�gJK\r#�l�����u�H��Q#|�߇����,}��6<5J�zG\Z�b��]���8�EH�)+i�T[�~h5�O?�8�D ���\n���1����b�����a\0���![s+���gA�jHkN����鱍N\"\r������<>����T<q�[��rQл�\"-�$��>v�ʔҺ��i�� 07�����_Ķ��Pd%�1 .�\\�)w�iVXA�0�;��� u?0� \" c�;�.`��I5���IC������0�\"< -k��o����[j;_K����I\"�����J\Z\Z�\r�<�
u�#O����@��tr�<�e�%��t1@��}���0�����ΫTJˍ�K˅� �2�1,d%\" �A�<}��=��[.�F!>P��ŵ)5�\'�Q�:@��UoQ\n��ڕ���2	�uŋ��&-C�!�̓�i� s� �C�2�N���-!����$H \"u�\0�#�T����=p��k �,�Bh�����1������6pG �\nL?��Q�j�a6��S�^�����7�\0�fj��|�W�u?d���(��a���[�o�s�,� A������S�q��=�+�v���(����i~O,#� \0����P?�2�!#�ϝ鍔���Jđ�����0ҁ��E�s02�{j������^�tjf�J�\nFWz愈#�|�#��.�/m1��Jo�0e�wT���F;���1݀�İ\0хh�U���\"2��Ш]�sT\r�1�)�1�|i�\nc ���Ǎn����\0x$�mL��&��.� ~�m�!��h F��������э�e�[-5�Ea�e�~��Z��R���;*`�r
J�څ�<\\�*���[-����t�%ܗ}ƸXn��]��	i���<��Qf�NZ�/�8S�Tc�i�vY�1�L
os�ֿj��c�]z�\"s����e�˓3@c�w�A;Z�\"�1���v�vi膸� ;v�n�������\Zx�+$u�j�u�&�b`�!V 2�B�B-��\r���]C��ۙ�偅�\'\\�6��-FiSpNPQy�M�~\0*����ϻ|jÖ��!�7)���<̬g�lj���eS
��II�>I[�T��%�2#���M-�Ą�E��B��-1q-�Vee�1��mK����1�`cDrTfT�6}�Ҙ#
���E޶4鴇��,*̡=�
ߘ/���LU)<�1Mj��x�7�ͳ���������C&�5{v��bonY��kDyhs��~l.����EA)�K7�B���6�η}X6���}�p}1��@+rq�Z��oj��)^(z��Q�[qYb-�㋱A\\,�hc��)�� ��=`xC��o�n	�f��f�W\0�W\0�=�)�����_�X�\n�w�YX<ծ����G�|��\Zп���0�N�l[Oqne�����ՙ��Ԃ�1F��.b���2߿o���x@pn���^�����@��v_�3���iLx	Cr̍1J�t����x~8�#���\'�A�_�\0\0\0\0IEND�B`�',1),(15,'Water Saving Insights',6000.00,NULL,NULL,'area.level',3,'Guru Saver',NULL,'Guru Saver',NULL,1,'',NULL,NULL,NULL,NULL,'Guru Saver_mwMY_40.png','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\0(\0\0\0(\0\0\0���m\0\0�IDATx��iKTa��(}���Q/\nl���E3��Q���rD�JLG��]���-�m���5��J-:�?��8��y�[\n80ܹ���ϹwӦ\r���k�q�m��3i����Z�� ����C7ʎ�ެ>��P9M1[\0�ר��d�A�
<8�/\0ߪ�rZ1�¦��y/5kP�\0��j���e �o��1��\"��\n�n6�$�2��U�S
�$���5\'�%Þ����9R���SիtՀp��n�h���944�3�ZsO-�g�҅lw>�����j�\ZG��e�D�s��U�ePfC��u����#�����$_;���DO:�e�)WH��b�o��#�9�B-����@���#�\'��R�E{�Q�Z�P�i��\Z#[��i�.ә���ueXM����^�u���\n8a�S_�ĔJ�ŁU�K�5�hd|`�{�\0�\0l쪴[�6 �LZ+Z�����|����R@�N:���D\r�Jt��7�|Ϸ�	\n+8��C���8�9��MH�
o%��W٬�G����M���r�y f�S{�[Ԑ�T�(5\r��%	3���\n8�ɖ��ao{M�E
7v9%I���Y\0��0`���Q\'�/y;��hY+�&�IU�B�-�2�r��f1@���$���������pH[�
>�z1}�\"�Hq�@��7ͅu�O\';-/ �4~��C���ٍ ���	@\\Gà��e������\ZcNDQWn0�5\'�!��Q|i���(6���`��\\9T��* ��)8ݓ�m�}fЪ��5&��(1�0=D\'�C ʰ�_?�� �A�]Yr�G��v��Zf���N�D�D�E-�>\n��\r.����`8��h5[ը[�=8�msY\0$`\0��]�v�v�Mc�nW�2�W���fmwi�]X�W.[�� $E��ªf�7_Z��X\Z:��C�YY��^�C�{p�_-�iD�d7i��jF��*E�K��!)k�٘%h�k!+2���oV��Ǻxy�.^�-\0]�/0��+�\r��߲�e�g8��\0\0\0\0IEND�B`�','Guru Saver_mwMY_60.png','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\0<\0\0\0<\0\0\0:��r\0\01IDATx��[yP�g���ݚ���LM�V2G����Vm���$^�\n�FA��D�gTAT��K��h�>�/��V/������&�Hw�&������\"�~�}��}����g?������WQyQ�������!�ײ6�odƨ��Ma�ڦ��a@���?ڬ�����
�s}?�l�{ ��G���fWI���K]s���:�DQ/ݫ6F�˪�f�m�Lk
�}���U\Zb��V8�d���J=��G\00������ȭ��.�����EH���ZGh5)�#N�kjcԍ���[[3৛��:g��\\z6�]�7��^�W�T��&��U|�!�0�3,�]�����w2�Y��?ȌR{�Q���x�k���=��(��#�j���q\'��1Ȫ
��z\n<rFcm�8���r���>*G��\'`�a\n6kg<\n��L��fB`���,|O&���\nV^>�*��[\'9C��K�!-(\"�R�.)ԍ�Ȭ	���;�[��#|��#�\\��>\n�K�=8�\\�߇}y❒�������u�\r���S�\Z\r��XQIǠ Z��(��= \0���:�#|E�]�+��6j�E��/X{x�?��2�C�\n�/UuR\'�Q�H\0n3P���\0����7h���[�3[C��s��cb��\\� �ȭ��R
��U4�Z��~��rGj��ψ����+���o��{If�����\'p4j�0)���ˀ}U� ��VW:�}c�nf`�a޿���������k+о�3E�����C,1;�f͌\'4Ƿ�����Ync3X3���1ٵ�4r��K6n�h �_�r��; o��ި�����Ğ�6��Z��7rjw�\n��K�|=E�i8|<�[�7kdcL��\0x�Wg��G�}}�Օ���WO���`�[?�����ؘ	����UT\Z#�i���)���+@K �
C�I%�]�A�)\r�y@;Qi^LH$7yob�b\"r�nO+���?c�-��:dQa����f!a���v\'���E��r���D�w��g��O��R9�O�[�	��X�=��Fr�p�X�	�-�:m�\0k�Q��M�1,NlRd Ϧ�-i�i,*x�*�dcnenyCK\"�v����]�-��M-�]���x�&#�����X�1���Z�#v���{Qu�\02<~�&g��#��4�a[H�\\?�f��f��X.�N��Y�ڗ+��} ڳ�S���P�憖�=a��ؐ�5���6�	[4_�}}��Y��R�i?���3k�M�d���GC��X�䵒�%;wʺ}+��KN���bQk\Z��!6� ���_��ǩ��h�pէ�q�U&ސ��+HA}��n�΄{���\Z��т���ļr-���n�ɝ. \ZfM�� ��ef�����>܇^�ick檖*�0���ǼW�\r��q�% ��
q�R�hq�X�z���� ���&�w��x�z޻���+k<�&|�%qñ2m�+\'b�f*6(?����n\"�t������,1k)��Gl,�T~���P2�oM�[m�h�p�@ށ�y��1<��{o�u��]<z����RԖ�P�\"�����4{l��B�a/$�ye�����e���,/��L�=��d���ܶ<k�h�\\�:���Y��Z����ZKU|�
����=���8$ \"�p3RJ��_������~����y�*^E#j\n�\r�K]�r�L�G���*�a?����7�n�X��\nku��*\\ؖB�X���Gq�=�ʶ��bYoe�=���5�UD#\\�ҿ��ĉX�0^��ؒ3�%�PEm�u�S�̽G]�F�l����m�;WH������}�<2��+g� s�\"�H�h���7Π8���Zc���[[���?���
�������޿
�?����¨ϰ4v�ǒ�aX�)fG�!�%(?��67�Q��]Et�\'6��$2�3��=�\'����v���d�tD�^��8WJJ�.��[��C7�8��VB���
��UIQԘI{�xї��@�[���c�J@ǌ�o��j�;ǉ$����v��vT�ٳ�x����=���X�o4��h;\n�v��q5x����_�ryG����<��_�Q�H�M�AY��B*�4�I7��g���f�<�1��q<�&����8{lS�G�_$�z2��1�[j��Y����1Wn����l%���bq�\"[�+uU��Kf�X��]��\rs�\n��~�\nKc�R#NY�iL��S8�h�j�P{�\0�w�ꞸҀ��-X7V���lFe)�8dՄ��i1��SL�����7�\\��\Z�	��?\'�F �����3J�@�̲m��>|�1�}΍9JUZTWB�֟ߏ�KD*Ex��|�}tS���v�S��aX��v��o\n_f��!�Zq{F/k���������)b
�@��b��䯝�-����:��>`9I�d�r���Y߼-K�f>v���n�;��[�hV;�}�\\@`�M~���u[B��aBZ�UUVď��:����f�����b�p��3�\0݌?��W-���\0���\rK�y��W�ѣ��a��dW��;�B�C���a�Wp��p��\"��m�L�<[�+&	�����ϑu$
��aw�?��xz������lz�r,TX7�\0��ƀ͢�ۓM�N�Jbb{$mƅΓ��6�)E�z%֦�@jp��1jP��=Q��ww��=D��c�Nsĺ���Μ�eq��GbE��`ko�P\0���}�%
��\"O��� �����@J� W�:��I��gl�1d���Y+k\Zw}��e�Ю�l�S&X�}�H\ZO#��F�J��\"&���m#����=�V�\Z�.��,8�#\'���1�*W\Z9(`&+��쨞X����D��ŒX�g�PXn\rYF^^9V����mK��3����h��T:.땕�I*�X�U�C�ߔo�JZ�D;o3���I^�$�%ђ�Μ�`�b����1R�m����ƒ��ì��ݰ+�|�4��\"�;{�O{����Ζc_�H�#�f �M��\0���v�Mn��r95�sᚪN�ac�,�)ڂړ�P�F#�`�n�j�AơV��ZiivK����v�C�!ΪT�h\Z+i8�����<��6	�3�ao�Vh^\0�}1�0 y���Oi�P!��+y\nrʢq��y2uH+���Q�g�	���%� E�w�Aaeh�V��� �eAV#ET�jl��\Z���zg�6g�A�~o\Z_;`&��Y#鰹��!F��6�w\rǢ���J����E�-���,�H)���|�l��KVh��\"�,�=[�9�А��j���
�,�����ջg�s׎AQ�����W\"�&�\r�I2|C���.�8;q�������Ă�O�,q� {��M� �&6��ڋ���ao\0� ]m>㵖���Y��IQ�b�\'H�+���9�\'Z�����P�$ �)��V���r��/t�0���]ȩ����\0�r%���Ȇ�s.vL�)����Z�em\0`l�̰6�yYn2��m��9��kكK�O�1��gD`�u9�z��[�)N2�?�M3�5�S��ބM9_cA��aQ�p�%���\"�Օ�����4��7�ke�׿�#������vg�t��*,=�j��8s������<������R��(�>!�ܱ;P7+#l�!^oL[omL;�)D$��>\\�u�\"��%\"�X�k4\\�>%%e���6����\r�C���MGǭ�W/��X\Zk����W]�û����\0�{�2��Q�
�c��������I���YF�$��l� �Z��md�m���i��}m9L{9�2��ᛙ�Yy���I�ZW���qXe\"+-�~q���\Z�f�aZ��R��{l=.}y�$�4�����
��n ���#;�G&�ӑ��=e�M{�����7J�xL�r ����]c��(�۷��lQC6R������Vv��T�\'���;�| R0�����W�{Ҫ���� γRK��\nQ,�֜X�4E�Gf�џ	�l�=]y��-A������C�w\r���/+��R�Y�ŗ��#e�\0�0r�&��1b�w���w|m��t�Gn�>ʵ%�%i���eI���b\'�.�!ӵ ˣ�������/y�ז�:*��uҲ^�m��4\0DL�v����ċe��4\"(޳��o�bګ{����	��C�z��6�A�ٳ���R&~T�BZ�ڶws���1�EVP,*x�����.��Lf���i/d�)�ׇ����m\\&#�ژ�\"+�������o��E����첄���>|k����`m��_�~�i&��ڈ���o�ɍ�#\0�Л>�Q*���qű\'l��ϲų��������A����@�K�<r��ͬߨ��ι1G��.r��ԏ�C�DdD$�ǅ<m����:�M�� �*跡y�?“���r�ȁۏ�c<?=!�� �|�O_9\0\0\0\0IEND�B`�',1),(16,'Consumption Reduction',NULL,NULL,NULL,'area',NULL,NULL,NULL,NULL,NULL,4,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2),(17,'Consumption Reduction',180.00,NULL,NULL,'area.level',1,'Starter Saver',NULL,'Starter Saver',NULL,4,'',NULL,NULL,NULL,NULL,'Starter Saver_1WKc_40.png','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\0(\0\0\0(\0\0\0���m\0\0XIDATx��Xil\\����?*���I-�D�6R*��-��J**!�Q�B��\"�a
ib��%$i��x��qb{��2�&ޗx�͞��۱g��sz��f���h�t�f�̛w�s�=�~/#�sx��a�q���e|�:�ȷG!�z�_�S�(t�r\'��	HGy�0��/d�;�-�+���ܝ@�T:G�aB���`�p�Pj���G�+�b�:�J�莡�@����!w�s֣.\' {_|�!}��YD����nl۾;^���{�ir�Q6I	RG=���Ec����� �y�m�ܶ\r�v��3�v���\r\"�F���2��}������6c�|�\Z�`U�^R�7B%��]�&S5�~7����E�~�c	�sгJ��J&��i�q��N��C@�Vl�eT�Xj<�vEa�T;�J���29�r��p�̊ý3(v\'�AU���\Z{�{�A�7��jȃ���J�=	Թb�c<��|��8��r�C[��PG�6�[\'��
{�Lv9����9��o0�6>���YL:mس�%�<t��$�����Q�USq�r�Y4�\";�h\"�B��YRT���n�ځ�D��>���x�����)\\�\0�������XZZR߿y� Lc^ԓ�lRL�Q���b=Y��.t,~v��I\'~{߯�\ro��v\\w�uزe
��6}w��.337�p}�Q�� ;XH�;w��3`\'����ROۑ�9#V4e�D``ќr|�Ns�,O�b�c�a͚5��kP__�k��V1�Z�^��-�܂իW#;;7�t��^^x�a,md����B�@�G�\"(�b����ع\0��Or�#�Ս��n�]�7nT`���+hmmA��#���q뭷��_�7�x�bR^��!t�-`�ބ�j��1�jF95�#�:��Y`]�E3�Ã�i78�U�V)v��M�6!��{�����\n�u��\",
�n���)���vo���Y@���b\r�Vd��#�6{���\0<�O���/�N\'��N����?�8�z=�d-�������܂X<�B�QE��x�X��M�oh�e:�\0�yl����ј�&�-���QX?A�)#�v��9���W�d7��W�Ɉ\\s2�����������v�����\r)�|�A��bk\n`�;	#��4�Ѡ�$Pk��?�h�(�<;�Z@��:����*d������/<���{Bݰ�B���_����O>�}�����2t�b�I��v)pm��f��A�Q�b����g�\r�_Y���<b]D��پN�\"s��q��̝������ �2��\"�L���D��0���	��c��V�Ʌ��dͻ-\rR\\��~���DPH��\n�W(�fׂ������7�t	�1�3�h�l_m�7��v,�8���a�a��
8p4?ܼoqx��%���OG�ۘbQ���d�ː�j=V񿚽IdG�=�����g�����2�6�Y���cf��e�e���-��>���	�y�3ذv\r~��MЛ�1x�,|7����ԑN5C��գ\rүM<V�v�9����˩�υ`E4�SA-�z����5v#WS�
�O����a��x��\"<��6ܳ�g��W���n^�۾���+-�� �\0S��4�~8Ţd��)��(Z��T���l�}R�l�v�2)-υ��$iC2���?AW��7�^�\r��\Z6�����O��_܉�_�
�g���>_����f\\���(j�:}��qe9fwJ��v*8�˵}\\D!q�ǂ���s�O����E1$ �N\06���������j�WR���F�\r; ���% LM���sq���L���_���Ɣy��y�\Z�0H90����2�ױh���(�ΫP��B8�UTӗj�1�:���=-�d\"����l8�T�t���S��\0-�vY��1\r�ɫi��v#E�J��.�Xh=���Y��\Z�f4N�eh�`k��4@�����.�lĿD�VKG�?~E���`Tyb�7�>). [��rF����^J�\n�(a0]��R��>H\0W��~~ߓJ�\nS��ؖ�*�)��ռw�GL>���e�\r}�[��r{Dm��\'�0��ԂT�0���v���}`6���}\\lP΋�����L\"��KE�B%���~d���D�]�g�%��B(; �O�t�*��}�\\���R!�  �2�8��9���=%\0U�4� -�ɋ\0���}
�2s�d�D�gxo�-�������u���۴�BO��A�Pˋ\Z�,z��H�;�z�A1�AF5�����Zz�/�y��)&{<V���8���B�dT6ìѤ�#3(RsbX�x.#�ݥ�Q�D!���c婀�$�Y+Q�H����-Ԉ�lu���DP�T{K\\dO�3��q�=u����#�#�Qm[@#��y��$*ܚ�ۼa�Qs��VWA��쁢Â)�0Ĭjn��bR��v�>�gi�pgUN��`�!!��ߖ��l�Ž~�\\�	�$�4Xq�Չw�8�2�:y�3���\Z1�[A�ѹ
�:�YI)���3d\\��Ek�n$�t\'.Kc\"5�\\\'���[,���(�8T�;��c!��@^?q�Ί�mn���C7���.\'����_��90H�ɦ<O)vF�@)�/{[)�FO\nh*ڦS�K\\\n9\'��Փ��X\'\Z?w^�tdyg���3�ÝS8:@����᠐@n��+��v��!�Iof��i�)�g$�:�Ʀ�1�i����\n�hJ�~\r^�-� �3��S�R�7�}%��,��y\Z�kx�ʊ�16�%޿I��%�Gf���4m�4��͒G�0�]4`�[�5����Q�H\Z�|�Ecj�R�/�i�\Zߨi��SY�L���\"�����d�@f����s0���Se0��΅P$E�R�U�#
e�q2J�<Vz�*�5o{��\"�>��|�}��\n^w��u�oo���UޏW��a��a2�p��\r�|h���AU��3?v��p|4�\\z��@�L��AfǨ��lNp��e��<w�ӑhY�!��R>�,׽���+%C8X1�cе9��q�*��f�M�K\"y��\'�+�(�:N�:�m�-�������qʣ	y��B��0����8W [��E�؄�0�����a/�:&q�fg��~΃e��K�2��O��a.��9LbdN*:�4�Ql�\n��/����Q�/h�F0������J�`����7L\n�|d@\0\0\0\0IEND�B`�','Starter Saver_1WKc_60.png','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\0<\0\0\0<\0\0\0:��r\0\0�IDATx��{	t\\g��{�s�LLCఄ�	�L�{8Is�OB�	�\rt�@t��aIp6�d�����v6�q�(�\"k+�T���RI%��[�%۲�R��R�$վH�o���W��t�!��u�u�J��{����w���i�� ��d�/ǣ���A8=��V��Q������]��xO�ru�U�:½�����@ b�+\'B�+*��碡6z6ϖ����2�1��	y_��]�n$�}�x�`&�2��2Fw���2�\\s	ϧB�
����C��^���O�o0�\"WFg\"cr&P�H�h6�)q�bx���S���O�����K�`�7yx@�:אoOd\n1�h�+�\Zw��I�)z9�EȠ����b�OM���Oލg�����5z2�O{�Z `?P�^!���)ԻShdX��iG�����iE�с���?����]����₂={w�~����>w�ϲ?�Y���P����n�Svj2��YQ0���(`��{�`&�fz�ڙD	3�3e�@�td�z.v���t����W���W=�m�\'�ч^ۿ�5������EQ^^�i��+�x�L��^w8t�_4�D�e�K�����wߍ�o{M\r(�Z@�-3Z\\i�tû�!�/�2�\\��P?�	��ˮ���Ύ�����n��u_����/���.l��^<���x��G&x���⎚��P�l�$�B[�\n+q���/[�?��\'����;��w��gGa�Z\\1��h�A�����q��bG\ZM�$F�IK�T�﹗[[���P_��܈��3���@�c�;y����z��4���&R<6��4�0�.��I>g:#�{�Ǻ�q�ي���ع�0v�ۏ�\Z�\\R��Mu���շ�&O5B`θx$��E���0H�M]���7��Xuu��SO=���˝�ݎ��\Z
ǥ�$����� P��4�&��R{�|��ɿ���$��W�E\0=<�����J<��k�8�C;��≓���$��9�����I�|&s$��3wvv~���������;w�/���#���vcf΁��y�M�Q���U��EQ�ˠ|6��Q\'���p�P�_�v�$��q7:x� Awy�h�-��^�juIh�+�JU=[@�.��ή�Ǘ�?�����[��/��_~���Ӳ䘳�n����4�<y�\n/��#:�r\Z����9���>X�΢�� �ͭ|?\nSC^+,�ͽ�c(Q^v���N�m,Q��&��e>��(N���ȟJf��%�$��.�\r�%�m��-Gv�|\Zm�͘��̸�sp�]h����}/c��Sx���K;s�8QR����\\�����b0������ٷ�y9MV��<uzW��I��ffZ�8�h`���IL��@Ч��2�\n�p��{����裛�*+;�jk��߇��D�Qz؎��e���aq!��ܓ8TR�C\rV<��v4�ի�GMM\rjkk���g~����*��ײa�1���D[<iz��	���\\�Q���M�̹���yx���x��GFG�<�����@,�TWW�	��c���>��̠��cC�8p�(~�e�L����DEe%~��_a˖-x��`6�184��.��;�x����0��tp\'�Co-�E�����F�7�Va�E{�g�����y�>ký&�1�t:�o}
r��|�#��rrr��o.� �^y���WA\r\rs{;������ n��F<���x衇�����~�L]DyE�=�Z��p��)���XH,��s���iT8l��9#�z��	�8¿�v�ȁ;M�E����T�m>�k��V���>���6<���x�P�����������?�i\\q���O�7�x7�t�����k���
��X�{؊��\0�RnZ�]�N��`(gA���\\,s��g�5:ʒ�F	��\n�y���������G��{��JL���-��ꫯ�G?�Qu���?���z|��Faa!�j*QZ\\\0JQ���j!>�����o�/~�
�\0�7o�7��\r�Ba45��-w�b����U.+�����E�5p5A��\"����x���f��z|��@,}��Sx�K��g�����F\"�BuUn��f|�����>�9\\w�u�ꪫ�������_��L(�=�`0��R��1���g���z�x��/����w�w/ڧ�cy����%ho4Û9�&��&�\\I���U�#ȟ	g�)|&��aG�w��dW�2x��*:��Tf�ڱ>��=*�>��O����\'?��X��k_���D��[��Y<�B��/ݨ\"��+��\'>�	���[���k��R����v�k.��1�B/���x=nQ^��f+��s����ʡ�O�LL(rNۗ`�$B�tz��:���3����\n\n��x<�(s��K0�W���
[�Ƿ�vnax�{�öm�p*/��5�q� �y�M8?>�
���Sx�g��#�`���x��g��f�����E��*���X��!i�zS�������*���M��p�.Jʝ��2\Z�#�Ɉǧ鱑`b��ڶH�7�0[�4\nfҙC�g���vO�\Z:66�cǎa���8}��*K��X��>r�\0���(�?����3p����l�W�Ͳ�p4���:���*�&��`�z[���2+[���nsI�,�9�*5%!��\\2.J��Ky;=�Ԥ�\"�����-�x��,Dn�ȿ���(0\Z#ye�2H$�XZZRy��?3���f�Ȇ`�C��PT���\"���������=�^�-�``��̱�>_��������\0X�m7�*��z��D����>���G3fO�R�o����#��_��C�Rz�8KY��P�&p�<���\Z��L��5c��^ 0;u�$�c\'O��2���\0kq�P���Ft��47)�ef8��[P�Њ���(�������c#ѯ���W	i�cZ��]�sZ�v#龜^!�@GB�|��z�q\rKW�?v���;��w�S]�堕�^�l<c�s��	�3\\\Z���>���\"~��n�UW0 m�09�b��\'����E�u���Y�(�Ǿ7N�Wb��g������e��\'\n�[oA�\'���kD�O�
X����e�]uUqz9�Ў3��Z�b>+\"��ed 8Ig����m8�[�P�-4W��0څ�(e�H����5�d�S��\Z�H~~�#x`���4tD`���G�g����k=�0��q�Z{j��b,H�<A=�c��b��P@���ֽ���2k�xڣwU�p�K�X����*�i��(��o���Ro��G}�ڜ��rg�!�gƹ��s�urRZ��Z�=l o�T�(v/Ë�&���d\r&���0D�E^�`\"u		�d���8VV�\'���׊�P{f�
��!ր?�I�*��L�����ݾ:�FC��24�� ��\Z���ҵ���Poˬ��U�����\n�P�n{,#t_�<�`؈�����&�L/�Q�H/���B0���~z�߿�PJcP<�����E��$�9����G,k����qԎ�`ÁU�ח ��\"�>6�^��t�b�e��\"�\Z��5�^-<��v��*�H\'�ۂaJ��I�Lb{#C@&��#â�+V�{�٭��]+��!�>v������cQͤ�ͧ\"���6<���q�u���]�lE^M;z�Y輦�_~�ן�e�Kֳ^�җzg�o�Pv�T�Yy�!�O�w��Rt&��P,4��k�d1��o3:%���d;�=�دN�t-R������1t�ڭ�%�Nx`�^���&�3���y�{4�^8���=�۾z+n��W���߁m��@nE3��Q���#_(�LI�����ڣY��e
sW@
`	kԹ5�26/�B�y�\nl��3
�g`�|viy3�LѬ�s�:D���b��U�Nֻ�XØ�\r]8`�Į#����
���o��U|����?n���77���n`w�ඇP�֍>ǒb�P^�׼1$�}\ZKgs�r�)0	���N+����֬dg\Z�N	}��U�k ��\0���ڣ/��i6¼���Ͳ�$`�*E2iH+��B�m<i�Wo��x��]������>���F�*���_�/��p�_�\rn��o����x�ɝ(��E�\\\0��$F�!�3�UV�k��|\Z��\r@�IfbZH�֕�խ�͒�Mg7�s*�a;��c>��Ԥd:��E|��șM�Q�~L�-b�5��sej�h乏\0�nQ���#�o(ŏ~�S|���;~�bS���� [}\n���e<w��\ZM��ƹ�4�Y�ϑ��S�f����Y,�Ruy#���k�n=�$�Z����}�	x(�Ƹ\Z��()�fn\'�O�aS����,�\\4�T����D�)����y!���2;|(6�������dB����_^�Ev?�$���U�\r����xd!�HM�&�,`�<�.
�Kl�uQ�RͿ���[�W&#I�~�
z���E;��\ng\"��8&x_�*m1���(aHH�f�bhH�K9����gMb�k��\rts1��}�$!�7N��ӹ�\n�/�*��pb��d���\ntl��W���I]u��5��v��)�7[��\n��%�{����<���@���%
ϻ�D7�����#ʮ#�jnt&�fm[��%�
�����X�Dd�-e0Κ:��F[�-�b�x{X�*��K���\0�o�{��:`�D��=ʦd�&uY����PKЛJ�n�-�����q��g�6m���N��}^�HDI=G\"��Ʌ��l���F����a���xX�})��8�Nm�ཬ*�`$�j
zz���Kl�A
�]����*�9��S<�*�u}ۧ.�yf�\0���	��w�z�����!��y�&����zjYt��B:�T���d�DȌQ�\ryW�-�|F�q�����t6J)�F�Ni����I%�6��.EZ�\0� 8�O��Ȅ\0�O�#`��(YT4����Ҕ%-a�\rux��4Mm��X�4D#���ҔP��H	�~)�-�DL�h�������N\Z�i]I��]\Z�p�~�\0��tB+�g�i��Seo�jP7!-	i�X =�1�V�4�m{}Z�&�k����V~��I(��L�u2J ���,��b�o1Ӷ�87u�\"T(R��8�%Tku�V�Z/649��4�sV}�&�G@���X��,�N�%`Y��*���xXuJi=�5�pP\"d�2��>ͤw�[YU��U�_�^�z�kcz�\"�1��x��t9?k$Gm�K�2��%3E���Y�H�z}.+خ?���
��z�e:��w���X��n�e�xuT<+M�_��lHo;��w�7x8���>Qz��L=3A��-3�MVR��ZFf�_\0w͆٧
��\'��>�D������D���yX�`�:P+䍊���Y��K���I�����)�;c)Aʅ�z�`�am��l�^�ϩ\r-�>��f�@j/,��bm��e��--}p��Fl�d\0U�*��!1�.��g6ER�;\Z��r�{\r��_!�\"G\\�K�j\r΄:� ��ZW<:���^�{�bMo�H�v����Wezð���Ѥ�����֭���`\ZI<���0��P����l��ڸ�d�:��_U�����/���N���O�,d\n�_��b���OJ�!/�q����h4�F����:yuo\0ݣ���w�؆a�\0/.�(�����0��S�n��V�z�n����k���F�_Dͅj&�	(���\0L�
(��p<��^{!H�~��� ��(�\n=�>\0��GGC �\nk�2H�x�`e,��ʊ�\r���z�9�lCYo�2�ͷ�f�;��XG�K������k�3*�s��ׁ����xp�-�x�<�c;�u;�Z�,����mC�dl\Z\n&�HL�����t���Y�*�ߜ7q��^�Y�i�����N�N��f��KhK?ڵ�Ӣ��ַ�[>����@g��K�e��)C�3<�G�d�C>��F^�%[�����ن�\nx|��\n�/�ȑ�)+q>�>�\Z[^�P�7���V�H�$-��J�>�SO��jJ�[�:�9�W�DX70�\"����iֲ�գ�\'[���mJWTI�ƕ�sK��ZF\r=W22�S�N��#
85@N����qj,���~�O����\\>�Fߤ�X__k��L��ꝗ��$�i�z�H󲅨B�f�im���Ҷ&t�/��.O�q��yU7�I���e�>�H����\\����Yt����:YQʦ�J�A��{P0<�c�^�w��y��X�-!��vG�\'ab*Xƽ�ds�e�ia��@��x(eo,c�B�/�|.�\r��j��lG��\n�v=�-YSM�f�5i�<�{�ɱY�.�L.�/:x�f.�a.J�,\'��-3x�r��IVËx�˃#��o���;�F�Z�#�����a��M_�o\r��S�tN�7��e W!U��.#Ȫ�_\\�emn�i�Y=\nI�����E ������<I�qJR�|158���XsOX�0Ѓ2��hˣ\'_�h����=��G��!/Z���Ɛ\\Ig��	tOG0xq~�8�o��2�y6�t�c(sh���e��̫	^6��Vcܸ��� �E���g?�}�mЦ�0\\�ʴ�j�>u\\7�-٧\n-��0�j���9�}�I���/���\\�������C~�\rR�-cv)�X*��8E�{mc.�y�B;�|�gMS�� \\��#!e�?��i�2�_J=����-I}�wB)3�\r�I�1m�Ʌk�M6���qe�f	�\"-�V�F��ò:��������P�
��/��I�o��
�����Y��B^�Ճ>���L�\0�S{a�x텿�Ik�#���4\n��[��q�v��#=c�@u�w�!���3��x�$�N�����Bz�\Z�M!�Ԛ2�%�Ͼ��I2H�\\&�2�8�J��&z�D��q|$�y�}/TOb_�y�=W>��\rS8�r�J�y�����9i�cOɹ�]� ~69����`��w�����dd�t(�h��9�b�\n��(3y�&�m�\r\Z�-i>lBd�/ޗ�7��7���&��i{���d�W�_R�ؙ�y��zVa��b��z�^���x��b�(�l��d��d��6��:���V����:��[�������sf1�;\'BWL.�I���y(�j�P/ A�;)��#����PQhΤZʹ�%=䘂���Py _E�����ں���p�2���r��8 Q��\\20c����]�atM�R��������Gi9kX;�������i��䏯��uD~]1z�0~��7Z���^vK�����E�#�?ƌ��Y�z���!��WK�4UW�.-�T�����狧#O&C;K�cO����ӡ�@�g.��z���3�]��;�7O���w<5<(r�f`e/��ŵ��B���U�N�����XY�7��՚ �^eK�n���F�f��΢0�А^0��)Qc���ř���i/��3���
�?c.���w�5�|�����<��eE+���J��/����靜\0\0\0\0IEND�B`�',2),(18,'Consumption Reduction',160.00,NULL,NULL,'area.level',2,'Saving Scout',NULL,'Saving Scout',NULL,4,'',NULL,NULL,NULL,NULL,'Saving Scout_FduM_40.png','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\0(\0\0\0(\0\0\0���m\0\0qIDATx��X	TT��J1\ZS\\r�Ikb���h�ML5�E��\rQ�(.�A5��D\\X\\@@@v��Md�U����l��&���=��m����i��̜y�޻�~߽��G\"�/����� ����ל�z��XI`=�̬���ޤG߀FH�\Za\Z.�q�\n���8uW� 9\'N��,iH̯*�F�C#0\nn�AW�Z�7 KT:����rG�04���*�T��\n�h�����B��9���@�R�?�n[F�]Q�溜1bג��P*�M��ᐨ�e��b�a.�qJ���Y^^σ�1r�P�|j�UK��i�*�&�� �㹐&����o����x�员�v/l�aa�\n��U-��9$kq([�󀌍�������O_���P#\n��J#,��x?�\r���as�����^���_�&� ȉ�\n�J��\r����#O��2Y[[�ʊ\n��Br��a\n�\"�	�cTx7�>�OD`���p8����ؖӁM���\"���U�$Q�wXjӫJH���\\R�~&�̲���I�PQ��`���0s���)����oE�+�-�N�FCM������r�oj�����T�i�Tx#Z��&�#Aj����?y�|\0rsr�����M���}��8 ו���O��T*���7ww��w�����\nTcU�����L�G�M�K��0�T

����sԿ���S-\'�7=z���尴���̀�/� �;w�x���۷oǲe�PU�S6�-[��7tX���K=3������\n&�	���q�w���a���ӧ�\"..��=6�w�\\��0\0&&&8s������\"p[;�׍�-p����Tl�7��+�
OES�l��������8���cȐ!x��E\0ii�8��I�dr�;3338P8o�<�<[����o��,=�o��xC���jX��O&�����1	���V!��������&9�YV www�����,,,��q>���d�k������Þm����6��n��k=Y\\���=ٲ�\n�(�k�B�p&M���������\ZƧ�0����?A���������bvvuc�+�q3=\r���RD�����\'O����\r�q����:켭�K�N�G���k0#Q����F��P�/5a\' Y���A�L�7�W�I����QC{��WRl4օ�aʧsy��3Ǫ��r���o���V���awN
�e豑�^C�8��6R5>��\'���
���a��#���b**���ē\"d�����o�)�]�kԨ���f�հ���8����]^��ꊵ+�`�֭Xs�|��x�G��p(�� [���3uXwK���d\rfI�1-A��c�0�h�Q�0�UcW�?K�30�dbBp��IS�\'so?�]p^��o[�dJ�q��qC��6x�>��z ����lQ3���Ë9�����@n+vgS0,�&��ĵ<M��T��$%&ũ0$��U�_)1�|���ς\\���^�/D5�=���\'�����;s���ێ],��ޘ\0�����+�CXu\'�z��ðl�7�)j��B��1{�d�����L=6�N׈,�a��>A�q�zئ˰&������G���1��gt����hK;��������7��d �l�i�Ga�i��,�R��\'�aH3�\Z9�p�m{��d��\nZp�s π}<GV+CU�v>O#�Ga��©g�T����E���ZH�z\'����<\'�`�	�R>�?�\'/M��b\\%X�(Xm�Ĥ��a�TK���8��������ͭǥ���\Zp����c�z����\0Eos�,R�KR��NR��k͘DR�K;p<_�`Ƅ�Ώ~��0��+2�X�^�K*�OnƊT5VF��yS	^�k�dj����P���.�ߌ+%�Hlb�p��|����6��\r\'�\"��؋ s\r��q�X\\X�/�{lǎ���V���׊��\r�S⹳��Q� ��� NF��I��fX���)B�k���W�EG����2����;�ѕzH�!��BKSՁ��-�g�9�;Sb�	��Q� �C,�^�2S/�ى,�`�����J�ۅ��zLHb�W÷L�ˁ�@�,�k�~������%<�QH��34ؗ׎#��)H �D\n#����w�A��v�h �\n�r� =
u8H�q��c�Pf����^�^\\�\"�E�i9{2T�\\��\"s�\r�q���ǯƴਿ��7\0���P��TnRM�jڟ�t�<hK,���\0�f��\n\0�15A�-��+e\"��z�x%�4G�K�D���9>HPs~k5Mչ��q,�\0�W����\0�2�<���^���-����=\0���%(D�y�J���5���$�e`���#Ô� {,f%Y��mY-\"�1���i��`	\\I�>�A lǹ{	{� �A���ڰ\'�ǰ{RE���\0����\Z8�7#�bۛGg	��v�\07��4q�B	�cPd~G� �yT�r^@(�`���-0@ʾK��z`RM �*�pA`�~ ����,/�;����ݲ������*��ӈy�)��~�q\rb�,H�^h�Ȫ$�B+��p7�I��&�,mi�B�!��T��ӽ���9/��\r�mbɣ���]����}Jz��%�#{�El}p
�hu��.U�� +�	g�ߒǛ,��2g�I���SJ?�`���QP��_�>�L�[���\"]�u��at�,$J7��͏ĩ���7�U\0F{��̝���(<��sgi�nk���:c�y��Z=�Y��9�6�[kg���z�?Ps�jΈ��ׇa�Tr=�C�0����D!#���B3��][��;��\\�8�� {|���U(��PV��\"8!�ɾ�\\Wf����0�p�=Z�Wwg७RtMA�ۖ\n��*H���扼���0]r��~X/�8�v3����dц���eX�.(OGj�D-�\"= �q��LX��Iq��r���\\;��9�̹cdc�W!&���uI�v=
a�Q�Wܒ!9p�8]����N4ѕl̀���q�ci���8�Ml�r/c�9	�&P�L�(���bgV��g���gjŶ\\��VL�E����Ú)5��oa�\'�����-&\'���T$ �\"��v���8nɤ�}i9�	mg���X9��\'g1��r1K����#gµ��\rܠ;��M�K�����}�7K+�K�`��Ͱe��ܛ���+1�D�^�sǊa�zfk�!Y�����;1m�\\/�1^N�p�\'�������T�{,���L��l2jG�]H3_D�Kӄ����ړ
Ȕ�u���.����:pJįk0 Y��f�|]�#%�{�XE�u�R��>��֪1���\0F`p�G�^U���5�3�dF�`G�^wF\"�ɀ�)�T��`�������&�	1*��U�߱\"�nN�ɼP�.��H���ëK�1~�o:�����4����jq��5���\Z�)�=m?N>C)�עx�7��V�R\\o��p� 0?�����VB��R�22�S\0sC Y�病��X.����UW1f]<J�u��f>��\r��\Z1,��n������2�\rZAֻ��M��ǄV����w��5v��	���f�Z\nk�4���էr�ǈ{��J���k\n�݌�v�]��[rLg<Y\\��j`���×`�j0�����)9|�_k@�����eO����Ё�*-J�ux��ì����^��=W\0\0\0\0IEND�B`�','Saving Scout_FduM_60.png','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\0<\0\0\0<\0\0\0:��r\0\0!�IDATx��{ X�������5\ZS4E�I��hL��K�F����\Z
�l��� �T)R�WA��XP�a:3�g�뼃�$����{��}?ϳ^�y��{���> ��?��y��B]�uJ905����o��k�W~�O\0L�*Y�(�,mw\rQaPhQ����%�_�.��z�P����>��G��EWA�Z`����$UcMRqž,� ��r�uj7�-7^]����N��4�*55�W@�\'��\nu����� n(\r������³�Jz͎.;�\"E���&�4@�@�\0��}�-�Ԑy�����C�s�����U�����\"Ǫ�?�j;�G��*��W�����G����,�\rQ�W��_T�Ņ2ȼKA�\0w�z�\\���?��1[[ۺ��4.I/�k���
y�\r\r�*!�h@�`ޤG{���F��\'L�Ae��_�W	�\r5��~Qw��6�Q�[���˩S�֮����M�5{�s���w��ߴ�y��G��Z74^�aj�3��w�s�H|�	�U���\0��D� ��3��+P�l^E_�ғ������-@�����޷�3���z���	��\'���\n��\\Q��w�����S\\y��08�9?05��톌�{�\'�����4����p�E�1=J���j�P�P\r�2�}KM\r��}b^�@�3�h��`W�Ry5.6�|��ڵ������\'`��VY.���K@&k�7���
�\\�eh�G�s��z�
:N_��cg��oǡk�>��go|�p#Ɯ�Ô%,b��A�05޹�@�\0��aN{��}�\'�W��k	���刈����A�iHN����(;r�ǎ�������`8{�\Z��Ͷo�Q��V؃|^ez�D��\\�pNÀc1l�!k�c��mm�۝��.7L\ny�qq:�T�3�`���Ud�aĄH�dh�r+{�[�Ull����ϋ+��O�t|RTT\\��~�z�o�S�%=�*	Tn��<lP�����*F��*0.��1#�9�<Â�R��튡K6aB�=�O(�H����d4��\\��	��:2��,���������[{zzv���|����}�|.��4������2�1���a@9��р�NX9: ����]s��ȭ��f&�����X��U�3�N�cI�?ǕcF��bT�F�p5�\\T�a���LО%h�V�_����6^�-��^���u\Z;f�������\'����Cd޹��.�h��4d6���ލ2��k�r	�}�q<$������+������0�T$~�)�����*1�!=9V�c�����e���(N��Y�\\�L_�=���\n\\y\\v�yY�W�Nk>�b�+�!2<�s����PPT��+�6�C�P�#	�<�yh}�����D�߻Ui��jT�R�\"��f3F-Y�ɎaX�Q�_����Df��0�����j�7K��,U\"Ed�}Niz�d\\bµǥ{�-,�a����<�.����+���
��#������\Z�\nNǏc�\'|l�I�f!*�O��;����
�QUY)����b���X|�
�/k�2I�%�u��<�����^��H�=��o �Q��B%���iď���U�5诮]�8�r��DF�h4�|}}�f�Z�_�r����C �<�����w�˰��抴K1���������T�]��QQHMMC|\\\"�as�\r���R��RI/�cޥrL��0��R>�����Z�P߯u���m�l�����M�T���rURRd��{?M����)/?}��}�֭q��-�ۻ���CQI1���7�uC�A����Ϥk?��̙3 
,��q�0{�,<yp�ݼ0h�J��-����XzY/��uI�I�j���`8���:|�&s�Q�W�O��Cx>�P��/l\'���a��세�D�us��F۶m%�ڵYdo	���/�8����\rP*ط�\0\Z4h ];q�D:tH.��ҹ3ee
�ةs�%�۲I^W�L2`a����1�$�=K�P��/Bx�z��h2|�dx��{>&�j\r
���|;��_6h��u�֕㜻 �
�4i��u�C��5k>��4m�...��{�.(#��۶I������&MB����� (�@TH&L����%�W\rXA�K������qZL*,J�AT\\����V��%PtV%�����$�\'�o��/�o��R�O �92n�B+�p��]�|$n֬)�x�\r4j�H ���\\DHp\0��AEsp�+]+�ܰaC�ĵ�~�)lllPUU�@_o��<��`��_�>ـ�W�X��о�|&kO�J��ݟz�S�nE�\r.P�x�s�iF�
�?2V\\.��Oy7�e(�Icw���1y�,1ӦO��ߚ�rAA<�]1u�DTT���zQ^���̌�\Z�:�a��a[�����X�b�*zz�e��f�dz� ��D_�ϝ	X(9ILF/�s-FO�B��\r��O���0|�*ןZ�\\�Od��;���}Ȋ\rALl�Ν��={�3�p�С\0GGG���O܏�?��[7��{gθ`ժUYQ�aݺu���B9I�(���zb���؞��nzxk�֩���W��KEh�1���S�V��!\"��x7�� G=_��3Ŧ�EXy�W<�����Ӓ�\n�#�p�Bv���|o,~��NK5����8|�������x��^�|�z�뀁_�3[���\'\0e���ÇQTD���:SĈ���p�Ě�L�H�b��Jؤ�%̀�.Ĉ�=�Ө�&��02J+\r�t 6\r��~�������]�L(�4�jjj:�C����7~Q�&�T7!5l���8u)9��
�&�����V�Ee���FF*���`��
�����\'Qc����z�wn\\���s0s�3�ݨ��k�ؑa�M�[�+��a��a�򊞡]���a�z\n�<�*l8�7�?\ra>So�����=[�AqF�M��I�/�%�ju�������s\n�a?Z?Ԉfu�/S�����Ȉ�@�����<�A��\'v�;�M��V�\n`������/@,1l!�|��P����;��]`埉#9/a�\Z���w�
�ؒZk� �W��*���������[��*L��.��4��Ty���y>�ί���>y�ne���� �<��z�vLZ\rd��&���(��S�~]�/�Gb��uش�\ZA(-�v�]�8���EK1~��x����e��X듆�[b�f[�^�
�/_�57bَX���7�8�� oVb��*�_7bw�\0m���\nlN�\'�������#�0~$s�b>fh�$k��|�PJͭ���/n�X�L�W�N��o�[�9���y�u�R26�Ɇ�7��d����(%f�d��s���\'b�������\r����dKf���#��Y�Y���Z�<R!�H���\nx<����J쉻�=IE8��]��~��5#vf�^&�\r��ж�-d}�MГX��w����̭d>k��b4񑣮�Y��iQ�ɨ�;�������\'�9ʜ�\r2wjV�e���a�%�q�հH}	
�ܑ\'��o�1�ts�\0[��n��]�Sا����J��N�\0_5�+��p�Y�u\'�0}�\r�;�m�7p�V5�fq�����[\n�\nl#�M��)�ڼ���$���u���f�a�ͽBu���7�����&��,��k�Fw\"�����?��$�8NM~�f^����T�_��Art	-c�WI�8�7���`Wy)���ͺIے��E(f\Z��L��`1L��0�1K ���\n������U�A���\'�am�a7p��ScDȪ��5=�2\r�����V��|�b�ZuE�vm9Y\Z��;���F�1����5�L)GkO�����7�t}Z��a]��i��L���(�4C����C{�� 1���$��h�$\Z��5zMz\r��=�M�s�a�s�a��d��\0��]�	���i/�\r���n�No�È�S��%�ӕ8}�Nު�Q>xÈ}�	�����#��iFlM5bC�kk�L-%�	ZL����3�s�]U�YO�0Z��Vb���R�y��䗧�?�6�\"��5WW�R`�E�[��T1��~_���3O�1g,���Y�\\��/Իk��.���Ҡ۰t��� 0��f۝���)+m0r�<����K�-~��֧ᜥ��c\\�$`FA\"��Y�n0�r&Ko#`�\n�L�B//\'W,�ΞMֶ�^�`�<8\\M/k������\Z�)C�䦦njl`j����i.G��}Y���v����U\0��@��h��X�i$�y�z��[|!��P��<Mۍ�3���WCѽg/t��1>���m�7��ﳫ�2s!������>�g�s�ͩ��;F�ʮ��ە8|�H/�e��e�(1�6)�\rRX�&`I�Q}-���\n�a=�)7���W`)���^5\\ok0>���y5��7����0UdY�C�^�-G4f)z��܋�\'�,cX��L���Ғ�R�����{�	�Mstl�o�i��-��C�6����u���X� �c�{���5~\\��*I�ܭ\"�*8eW�ɫp��}�����]��.�c��X��M�L�%~!Aω�c��2\"��_���9Y��W����[7�R��S~N����\ry2�L%Á��V,�s�\0%:����j?�F���\"�%ja���\n��FMB�ACi����X�,0r�\"XXn�������
\'\"R�W�x% ����A%\\�\Z�r�R�m}\\\n�J3y�n�-��\n̚�{�K	zAϥ�E��\'�,�}YM�sa�rj@�K�������|��F ]����R�!jo�@%>����3�Z,X���m9Ckoj[��g\"1�1�OGc��e�Eށ�-�K��A&(X�\n_t\rB�Tp\r�\nnw+p��i�u�wO0�������^�F�Bn�������#56�<��\'�h��:��r4�)���2t�Q&6�1<B�S�\\Y��h�E�^��bC�n�H\0#D9b��N4��A+���*�5WlK��W�ؓ���L-��\ZA`Q�]����g��g#i�5���q��9\\\r��
�:�H�F)��7��;�(����ٛR\rRMa-�ڒ�0dg0�D�7�$;3N\r��rx?x�#���\r\Z�������^YK�<4v+B�Wꪌ�JA�j)L&�r�G�����vYϘO���9B0��,*��j�F
+x��G5�W�W�ẁo��|C��\r`vQR��k�e�lM�\"��,�\"Sc��M���~\r���d�fr� �b<���1�nE��=�W���˵����Y��U�\'���i#���\r+�~��?�\0}�q\r��W�L� ��?l��\0�������\0<�a;9NE����^�O��z\rZz�E�E�s�D\r��P��\0,G*,�])8�y�\Zi�$����\0x]-�MTA�̿�\'� 
��M���+<l�?>H�W���rIq�~\r�9�FtPp���x�Y[K�+���H���˭��Fk�L��w·��4w/���2?q�B�Ρ\n���?RRN����:,�b��4��N1H���H@k�Ո��hd��𚋏�	��uH
/;�Z�\nM�p�,AZ��i����j�I�9�t�$��4��fб�г����⥕�4@c�<���/{֖%���QC.��4��-�T��.� 	����G\0>q��l�$�o�ί�rؓ�u�n5�VKe�$��YZjEH�Uܓ)�VlZ��Ǌ�iu�h$����T�.� _�0��k1��tM���Eb�W�.��=KЀL��\'Cz��R�	�w��2_��y�N�\\ꒆ�,I::N\'���!��p���zxw�PKIЗ\nE?�B�w�\\ �ay�����$�QcL����ItPO�X������6��L᱁`�z\r�˒�1+R�ɡrIxL���s�!َ.�eL�3�3U~dN7b��@Q5��+s�s_���b�qh@�m��<�,�Ş�`�i�:id	�hWM��3�����0��T��5gݕB����}�S�>���5�sW���f���]������R��.�R����Ű`�oq�S��\'��k&��b�]S\n��R,��������@j
�4d��J�|*�X��SZ�%X�O�TC/˛�H/Oa��\"�������X�61_��dز�;tgn�>�B�*�o5��˅�$3
Ϻ2g���0o���m�wE����E� t��0�ݚZ��,/�.�`�����	&ӛ�ع��TC�ߒ`�d�K��!\nX_x��>�ܧ@�bh�_��X�ae�\nM2�)�� *��W�R\Z�~�p�H��(1�%���rI�
����S��L����A1\0�%��Э!WK���H8%�m�g_��-E$������ibAE4���bjP)F��aJhS��N��O���$0���g4�F����n0՗h�-�N晖25�y �|V�iA�<�]�r4 �|�Lu��h\\&%#��\"��%6���^,��YX����ۅ�g�Ű���f�+\Z��G�)@�	ꨨ�\"o��a�.i�5}��R��A��֫�B?z�b�c6:��;�\\|���>@o�����]ts��w��E/�Xг�R����c�vf��c.,�\n.�T�l�Z����V	(x��Chj��(��2t����\Z\"c��b.K^.���xg�ZF�\\EYK���Y�\Zň�~���.>�u�`V�k���5���(3����n�ａ
��
��]*�mKB/�L|~�&��H��V��fuZ��A˵q�ˊ4[��6&`�W.��>���m��\"}���\r]�\\WY˜J�27-��vʰ��ɝ������M/KFV�o.S�s�I_3�73���N�=�2�̀0�k��4��\n��*��x� q��ߌuVsAW�Va�}&�ڤ�׾,|I\0]lSі�:�����ih�#\rVƢͪ��)-�܆���?�\0#��`�}���y�����������]�Do,F=�)5?��{��C�ˣhzz*�B��vQ����\r���z��ш���\n�m�W�M�Q\Z�ؤ� u[�똫)zI+[Ij�<�_vY/�$�$q8��o��c�g�>�\0��D;���%oئ���L��ks���{\r��7�k>f��1��-Xl�d�;��y�7�JUɋ���� sٽD\Z�!hq��z3� E���JR`S|�l�%-���7�\'b�,��)f�ݘR!�ur�4���b���lO\"\"eE�n��D1?�a�=9�C\\bT`	��k����pg�mLB;z�ŶT4�|���D��wP�@6d�nceB�����u|�:��Ov\\�-6�˝���\r�(�]KLu��P�\Z�E�Y�z�xd�=��=�y���6CN�y	�д��:�z��k��N�\'Z�l.e�L|/�_ɟ�L�II���疀&�,1��:6D��v��s���U�;I��hԟq�쮡�.zw�%4Z٬��Eb��۔�:說1��-|le����{��=7�*�g�+�qD%�s�I�ۈ�4M/ȥ����}CżZ�Q��	ct�ޏ(]�Ȃ�,���	��?�x!�	�b�Đa���\"��4�	����,�oY%�����S���}�쮣��4�\rl2Pw[\Zz�Kǚ�و��BW��$/��z�\"������^��+�����J���k�\\K���e������})N�p���Hѵh0��S�(cZi�(\ZoѨ���M�r?c�)U�L�6�4����y�����]�������ъ�7���S��Gr���\Z���z s0#\"Ijh*j�[b�Or�i��M�ғ���i�#m�+��S,���\Z�3-�g�I�G|�3� �ю�L��^41��e&��⁣U��s_��Xa�hc��X�t��/���h0�@��$�	UK�ѕھ���h�-��%���0ԟ��
�Pw^d�#PQ�Z�/�b0�.	��J���	�(�t�m�@�:����x�Z���B�}o?��1��ŷ_�U`\r�Sw���2�]�fB��oQ�v��?�X��מ��/i�h\"�)0�� m\0ɯ���$�~\\���/�{b<܅��ą\'����R,.��:7���@6���n��?�:�|�e�[ţ�,_�9���~A0��9?� �sY\Z[���������k�F�to�+,��o� ������Ӥ���Q�7�6#�����s^���B�&�ve�Kb�C�\"o�wZ P�S����@|ņ�\n�����\Zۤ�բP���
C�z~\0�l��)��\"޷����BM�-�X�tOSf�I��q3�;��m\'�w���is6����]�F�@\rZH��bC��\ZnލSP!`D�	}���b6e�k�d�iu�_��=���H�8fH��Zh� 8]�Fg
�W�b[\\1v\\|�%��aԎx�X�OV�a�]�i��#0g+&:$<��F�C���o4���RJ��(mƅ�:�L(K\\����d]��@�f78�-fh):�v��W�F�2��k�{m���m���� d��X������D�\'��\"Y�L\r���#��w�ϐ]Z��[r��y�����\r��b�]z�q\"�������h��_j��K����e���am-���R|P�v���+��%uN
+B]��\r��6|�3.�������#��ה���?��8�m�=w`���+UO���W �ש�\0�����I������T>\0\0\0\0IEND�B`�',2),(19,'Consumption Reduction',120.00,NULL,NULL,'area.level',3,'Saving Ambassador',NULL,'Saving Ambassador',NULL,4,'',NULL,NULL,NULL,NULL,'Saving Ambassador_fzUB_40.png','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\0(\0\0\0(\0\0\0���m\0\0
]IDATx��X\rLTW~OJ��\"nQ�Yڡ�2�L�N��\"-:�4Tv)		\r�������6$�����֮+�eK4��..[�)2K3)-a�-��jK�����~��;���f��3o������w�wιcY?�89+�$����{��2��*�����^�&,O?�w`?�.������\0��\'X�>��X.������Î?�W.N�@6����;/��%c�a����`7����`�� �?�&�o��+%�`\'`���HܱKR��`�ay�4u\r��\r�x�뽳 �9�uk�b��U����O=��}�z��6�Y[���-����n�$�A��2y��&����Jf���;��LA^�444���,\"GKh�
tj!xc��	V�,�b�Y�K|#Ҷ�\"�S$�%	�>-��Нe������2�U��R��Φ۲����G�p�8��^�M������*��	X�8�;�]�uV�2�4�NN�����a�/�����x��`��
+�ʵ2}�_㣏���N���-YX�6V9�r�O�W\\�y�#���C���?$���ʵ��u�d��]��=��b|~���������>S�.��n=L�te�1�֮���\\Ʈ�����Ù�\r�
���{sԂ�C���ŕ��`͊5�w�^5�g�dee���Ǐ��T�\n��X�4T0j\\Mp���[�{�LR�I߳n�܄��6<�䓸���ۭ�.7�<�02:�e˖!**J]�7o�:+�{��f\n�D\\M���mؤ*?!\"��t|���GW
����{�r�J,_�\\((�W硡�������\0�۷Oݷ\'P\0�������\n��!���:�ۛag^�}��pw\\�� ����9%� t���\n��d�6���q��5l�٬��ڵK�,��%*���`{����\0��a#\ZI=��R\Z�pҎ#��-�]��}���ըڽ]����Q���]��u	(\"K�~�1�� ET���^\\�q��^Υ�2/V��r�+	�xT�&HoH�G Ka���~�_��L�������9V?�ot����J��\0��{9��[Ǝ���y��\Z���\0+	��̔W�`��CZ�R�׿C�_��p<_מ5PQn	�?S]���Æ-ۑ�����6��?vᗅ{�8�y��ɸ4�������yz?��t\nj\"�V�z��{%��\\)�8lR��zXU$��x|�,�Jڳn��U�&�5�L��\ZP7�8)g-��:����\".ƛ��

�@�r�
ȟd:\Z��z.���\"k�WOhW
��ɰV���%?����0DpG����n�3��=:>$N�-%v*��i�q\r��
׎aQ��븪�\\�Z�Xϱ:�X=����d�5,�<\"\Z^0����Fq���\0���@b��PM��k��>��R\nys�,�E�	�n�gI!|`�sxxW1V.��\rkY�=�B�+.��0lN���&u<J�)�x�t=�0����l|�M�{L�8� ��ZsR�ۚr)�rs��f0���X��H�{��������x�f-�\0ocl�LӔ��:a{\\
F\0*EK�1�Γ2(�֤���\Z��5��H������
�-ʈ\0�M�s�a�]��C\"�J�[����)3�|~�h<\roc�Д�B暌�OiU,N�)5q0 #��͜��yq��T�q��=�:�R��2�5�y!+��܇L��@�U�Hi��k��8�\Z�Qa�`��4��)��U�sXE:���4��&��H���n�w+���͍�t�B��)�Q�[\"��U�z��������Ƣ���i�B��M�ԍ�t�n.5�S8��H�)ĂC��es���N<�5��\0�Z����;�o�� �����	��X~$\'�S�uE۔bRe�ˏ42(��ɋQְ�$�@�\n7]��%��\0�����\0Jɫ	�|4�Y�Yd\0J�+�T�X���wY1h��\r��\'��3zc�X<�`X �t��c���8\r��-�<4�/:� ���f��&U��qqq����-��L�\'b��Х��3Ť�H.2A�h+�����qq���;߉�[\0�:/��>��k���5��*���y(3a#���M��z)$�}J�I�1�_��8�h���tsʠn,E�*�Z�RO+t�j����/)�;x^�����\Z�W�7��Iҷhv�̖@*�SD@
�Tٳ62�r���~;�uN8��
*E�r���n��|T/V=�M��5Mk`�^\Z怫5�N��@�ĝ}��Z�]7]�Jת���q���m{�L��렒�p;�~p��j��j��+��r%&��� �L*��<1�Xs�z�f.7�Oj�%�e����֒MW�؅[��S�y��1�_f�<j������EI7�������Zb��0�
;Uƍ�\'��Z�y�r�$HW]$�Ѹ϶�E+R��U��j��ﷰ�~o��9Rڥ�yӸ���v��>�n)}����r�̘$c&EE�$R�����*e=G+UL�K����7�W����E8NNj��s�R�)�/��fW$& �pL\'\"=��\"S�Z�/4\r��X�)�j[�r���
{ѯ`\'>
;�16%��I�7�~��+\Z�6>,���?cq+���!݂�˥Mώ$r����,�Taj�i�T����JT\'�v�a\'&����C/�Q�./R������@��d��|K�H[��>mҳe�+���ϳ�_��kb8ۤ���pA#��\n�?��^�Msa��E���m�����M�֝���c��{Z׀SJ�ZX��_�d�6�I�d��ŷ�]z���d���(���a�c�kiq�$Z\Z�ƾ�f>���A�O���[��)��~��I�o�`�9�X�Tn/�OS0�;�U˗H%Kzqi�	�?����ي��Y�:����s�<Y
�]M�����	D:�\0�9��S��� �\\c�ݠ� �c\rG�h�Ë��)��3�<�i��\0\0\0\0IEND�B`�','Saving Ambassador_fzUB_60.png','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\0<\0\0\0<\0\0\0:��r\0\0�IDATx��{ TVW���@���X���\"�JGT{W�\rco(V�F��������%��͂{��Q#13��$�{��=�3��wʝ;�Yw.k���������Կ���{C.=��~�#��9UqK査�������u��i8�,lB�AE��Uo��4z���]�PY�<�����Ƨ�j�3��Sf��,T�/դC��ߜ^��S�S�Ż\'�G�-U��*��f~\"�f�0E�A���:Y�V�\Z�ˉ��Sa���q�W�L,@��7�<
��`\n�1�BN�Lv�s�*��w����
���,����6���u������U�\\���*�\nT�v¬��j`L��K��~�[� S*5��p��|�Eѝ����E��ʲz�����M����r�3��S~�jz*�:T�%(�|�v��* ʇ�\'`\n#x�p�8�\Z�?)���~���+�Ӕ�0��o����3��v�~����!AZM���Ͱ\rX\r��f��90�X�\\����p*��Ӗ���A9���~�#P��T. V�؜��zn���������jUTʅ�W8;9!:\"o.���
���7l�Q=���H�r�o��醪�Q�E\n\\�}Q>h(���u���ߦ�:�Y���;K�
S��\n%�)�֨SϜ�e�K�N�v�:9tȐo+U������]���˚GǠcJ\n�}{�\0��R�)�e*��ڄ*\\%�g]�\\��b��IBKx ���^C�z��sU{;TG��Ǵ�P�h�n�s���P�n9��,�U��4;3s\nƏ��޽1�l��C�ݹ�|t��߾�V���mX��7U�_R=ɒ�y���}�Y(�N��z�l�H^��(_�%*�KA��	����,��0�7���uܬZ��?��=5��_Mf��?�F��}\Z��uZ��;ｷ��ŷ/~$�0S���\'~��<l
m�&���Q��o6��o@�Wթ��C�����2,���7���Ơq�`��FFyOL��熼ϝ�ӛ�P�!����~R᚝�\r��ϯ�K�&M<#\"\"��б���7??��ҹ����t{�x��Ǩ��\n!���0���v�j!����[�x\'B��b�<��z�Y�CX��\Z�cn����X!T3�o4%@�.�)�<\'��!�~\rG��{\'Z��7Q�_=,$$�nr��
���eK��G��k�b4h9���`�S�~�+����SP�9	)�R1c�X��ΛH�О(���	�؃89����.T\'�%�8�N�)�A�����T워�?dvms��n�χ�W�:�5��t`N-�7_ݵsG:�$L�o�޴7o��Y3�\"�윭�����+_�uоSk�Z�\0���բB�.:��˗ �e4jթ��	P��A���w �d���0��c��UU;�����١�et^\\�Ak���[�`���\r��0%#O�#G�b��Ÿ~㺘3�\\��ʕ+An�w�@b�0l���?*F����[������q��kW�=����:��.b�4�V��x��(��P���v
2Os1�=�z��$��[|��?tIYI������U�:F��#7\'���m�� Z�N��[7q��-dggc�[K��.6زn-\n�`������c�������e���Cػg7����}P!t:Lݨ����:loIpq��\0�����?�y����<;���<��er���i����
>�ހ�mZ#���>m�`�R�Jغu+&L����H]+S���>��{���`�Z�0f{{{��� ]�t���bNV&<�:��P�����䭩��y.��Gҙ�P�=s��l�d��gV�����*��XN6���j�.�&gbʬ�8p\0\\]]\r�������U��c�!2<m�[�܅
�=w.���a2�`ccGGG����<�T�}۷ :�7�fҎ\'���J�+=v;��IT��d:���҆}��E�r�9�L��	�/S�׆����/n�Y-�uJ[|y������\\�:;;�z��?�h���\Z5��C��`��I�ykkk�l����\n͛7ǫ�ǉ��ж �3	n|	To�N��	�-�;���,O�c)4ĉ5��s��YO |�%T|�bL�wC���pv�K}�S�tcp�o��[M�M\'��U�VEŊ\r��* -�\'��5��p��i̝;��ɩP��q�11\r\ZfP��v����n�K��5T_��E�ݙ�v�$$�v��fd9���<���Ʌ0N S����k��	�\r�_��_�?cw\'���y|\\�Xa�L�������,(o\04<��7n��/_��K�U��|�;w�\r�`�r���0�ija^.~�j9\"���.�	T�q_��wC;��ܶ��CU�
\Zt��)��Ao�|�M�Z�KTi\0C��(�~�뿯[��Օ��G�VW��J&y3\\�z��gM�����5g�϶X��F���鉥K��f������_���a������`�Btt4����Wc���X�t:\'�C�Ɲa-��j\0��@��C�ݹ߁L�R�Ş�Ȣ	:��z�2RWS�I�i�蘹\\��w	�?�\\��K��[2i�����Q1��Tc�ٱ�$�axrW6���;$%%S��r���Cff&JJJ�2�Lx�VEth06o�/_�b��w1s�L�Z�\n999�ëWx��c��`�������d��� PC	r��Av�MiM�-�t<�E�R:6a��%��B��]}j�y�>z^���u�[��*N;�T�y�\rY�ú�-T�n���Ѹ�{Ϟ>ŋ��Y� \ZE�˲o1f@O�s
��S�\"0o�t���{��	?�h�x��>�o�\0ڥ�Z�<�%���GނJ\'�������׵-�\\�,�{^��N���#k �w�>��0�cϠv�őI[�F�d�N\'�vg�\n�uF93�7l���mX��@X�Hd�O�G��W��\Z����W�{�6�x
~���j\'��6�j�Ոh3C�$c��e8����m�tÇ�1r �i�\r/E�L23��h\n��k8A����[ڞ�8�	�#�M%h)��g����;B	�X���%55����R\r:�J͸��yގ:j�祪o�<Ȭ�\n�k�i�
pX��I��>)�/^��/>Ôi3��bmohT�P#�B3��\\Q?4I�{�א�h��	�C�4$����B�d�8���#�\r�=�6=�V���\Ztg��T&%�����C8>iҞ#���ŋ3\\��i�Qm��5�����Zbv����U0]�7%�j�Yl(�L�w`��L����WĶ��h�cl���ax\Z ۋ���i��Y��C�~�v�K���GT���\r�I[|�꺝�އ\ZG�c	v��6�I��^7H�%L� �NWu��zJAȭw����ꓠ�<o�\ri/ �;�*|�Z�f7pS�U������2����&LA\'�N��r�ātx��l�6���|�\nX��x��O�$L&��x>U�]�!5.\0~@�Y���*�\Z + �5\rj�c:,O��r�o��,�q��:6�$�.|v�K����c\nt/̧PKӽ<��S���{��
1dQ��#��vI�K���+�������U�u�����3��4;���@�ܨo��5�qKr����dn�]�դ����1�~��g�
�f�涜c�\0��$Ng�AV��>$�5���(��߇�{tW�v��:!iA�bvM	8��a�7���ܰ+ڞ�Y��\\���ȝ�?��W?��8:�L�t��	y:q�6����یМ��(�V�!�v�g| 5�Փ����ym�}����}��/>�k�F�j���\n1�����G\Z�j�{5�2NT����Q�(�8�i�tl�(e$��Lia�HD�х��q5W=@��s�ð�<h��y�Eꝛc��<�2���f�v�b1�Mrt#M�S-��$��n|y�8�ϩN�9!�a�jj��G���F�Ũ�<\rB�ùA�=�P�F�N*��ΰO�N[\\����J�A�&��Tь�\Z�x�c�k���G�0%Ϋ�TT�څ�I�V��8���Jk��j@����ˇǘb��=;�\0�b�m��|�\"�f���I�2�.bL��Iq�$���+�4V��a�lPť<B����a>h��ի�����ր��+���u�V��\"���\0�� �չ<�n�e\n��ډ��q:\"�ƟԺ�5=�HE%�019|Bgk�hfs�����*�`{\Zտ|�TV�]F�~����j��\Zpx��$\n�m8�IT�������j�Y��c9Ԯh��5]P�Fxh`�i�&�Q�<�m��������.��%sK���B���HKAO#��8A<���b��D?ɾ(�d��%K���yML(��G�^�w�s� G��$�m��E��w�\Z�)ǵGN�7zʪ>]�?����$$RmR8�)��������J�y.TՊ�\n�<�+��)�
\Z���I� rV6���\n�ϿG�A���RTYM���or��@��,Q��_�I��t�GV{��˒r������[,��q����ߠ��b�u��
�����<ğ�F��_Pu�~ps����~\0��R$K�\"�Ng�u?�V;�x�	�/�C΢�#Q�8Pm=��F��ό�����M%���~����\Z�XLP��0L����:&O&��w��\Z]l|M�\'Qk-,w����m�%��1�� u����p]r�r\"������k!������a��x�_��\rO@ESU��A�\nu�Mf���D��;���Y��	H��p]T��u���g���-�?yAy�H2���[JQw�c�ؒ�Z����ds�=�ҙ<��d�=#�i�~�  KQqC��RTt&؎W��IL41�D%��q｀�l�NG�_֝%|%�0帍���a��\"5lD�a��]�P�{���z����_6�Ͱ2�L3����?�g�K����(٥��.A���l���h��S�.{D�E��	x��\'�,��4n�R�J��������7u5��μ�\\���(,��=���$��C�w���2�o�AUGVF�[�jm����:�����İ���)I�:�/F��o����>%�d���D[@{�G&#��^Dp
^���fx���Ô4��\"���?UR`	M�E�9�\Zz��\'ʿ[�gRb��Uv�K�*S��FNj����7 SM\"������2���~����ν�\rO�ƥ�$��[~\r�׀��룧p\\E���:�����5኶����~ɸt\'�h�9�{bs��L�yX1wz�.엳��H��dxR��	�T���3
=e�MO�K�шs����n�t�$�V2ۃn�Ƣ��V��-���\'���\0K~6�\"q��!;�$�K��oJh\"hN��\"�i����x1�;��,%��f��.�i��p<�)���ՠ�pYu��{���H�tm��Uգ��f�~B�)�%���k���[p&�\\՞��U�V�o����h���<E��G����l\n��?3+�o)!�O�`m��eZ�BcAϷxi)���4��#a�N1�) *=ຮ�{ܴt5/��O�Kz�\"��H@���u5�4C�\ZpU�ª1\Z��p;�Gc:��L�e�� �X:u��LЩG!�K�mc`K�6� ��oOՍ��/��`?}ڹ�\Z��oI������g[�%��t�!e�p��@۰�~$�����)��;�A��ؤ�S����#���	��9�m���uAc���g�¦�,@��1Wz�Ƣt�������n���\nz��}9����[01�M�v_����/�)�>��O!����{lx\n׵�`��c��}�c���F��ғ��Rq�=��R&�{dr�����amtT9 ��9���?�	9S���Mڏr	�.��&Ol}����j���4<�;��e�\ZI��n�K�$���E]�H ���.�5T�Rd|�}X�6k�y�u���1�?�\ZU�<��8�%�=3Y�GU��vV��;
໺����\Z�i����\0�PYd+��l1�h��&_�+���t�$�MWVc\r#J�Ϡ\Z���+��!߷�AR3�y�Y�������~��|o!N���/�K�Fo���rY�z�Rњvh3�.l��G��%�Z��
	x�m���l
�VJ�ɷt�1��~���PNl_YN�}��O;����R�%�nOuoKI�=�K(���gL�i���lN�f����G]��sePG^�d��?|Z9�7f�Y`6Şg\\>�?��`X���2m\n�*�����dPW�c�M���VO�������\"���,����dV<�H�&p����Q����F4h�K�MVq��ނ��}ϵ�J��^K��\"2�mM��d)M����O&6CU$`�h���؂%Su��e�H{�m?Xy֟���)���}\r�ӱL�*b3��,�AKB %�0�A\'��}���E���mqL��9e|���������%����n�Sm>��VW�\nkj�NI�DS�(I0YS��T�!N��&N�\n(��K��sK���۞�^�l�[j,>K�%%!�k�y��\Z]	QF�C\np���fZ\n�4���q;�n�H� �і�!��G�Ѫk ��=\"Ct�3���&���0�*��t�d\n�T���G�jC�d���T�\'L�͹ߚ�����SVJ�Ȱ,�����>���%an����>}��UT����u�)�q�t
-��pQ�ةt.�[�������h$��#]���Z=��a�ݤ<𚾧�%�HE$	NK����QU+� Y��*�Q����3`��S�)PN����N9\0��̰*�u�Y�\\-�Pu�]gN���n%=��I\r�E��o�gt2\"�O2����Ů�z;��Z@K6&*) $-@p��9�VX��3ь>���E:�w�t\'%\Zt���I0C���9���I|��Z��V���ɞ��P�\rS��L2��n+�1+������������Co�:�=���� M�Sf�x�����-�J�)����!+QB��(oڅ�w�qw�;�{�|�\"\r��qo��IP]/�o<�����&5�9\r0���5Uړ`�s6tR��ar���a\nL���Tk(L���*S�]8�1}u�={����E������=����G֨�9���}����|Z�.k:���\n��G��T����V���-�=�v�uvֵ�rEKg�2�\0lwQ_�Ʋ<ڢ@K\"����L���dfS��C�����50U[FU�#+?���3a5f�,�����ywQ��l���_^T���I����85=kYyԟ�\Zl�=7�o�n	I�/��}���%ٙ4���$ii
��_�dn�[r�fR4�X0+0\Z�F�%�jɉ�O����S�Ym]Ȍ�L���T�M�-C�������l=���|�}yW�<q�ٱҌ2��ؿ�n�n�s��ϯ���Wpv���	�+�,��Il�cu,_*v-l{[&E�;�^ˍ��e�|V���8p�P˄]�I�n\\#ןӎ1�\n��:��}��;��iL��a�ٖ���m\'n�҆��d�\n�P�F�q[Ts�B���rmc�k����Ǻ{EM��>������z�O�N�;�}i�0�gE&�lJ��}2m\n�� ��.+y��{�|�(5�q���N�\\�!%���,��J��׊L~��گ\n\'��U\'���p�*���kL��095�N�d�u5j��Y)w��j�-�N-�ښU�o}��_>�����n_�<��X(s�?Np��I]a�[���T{Y��`#Y�;�l~�u80G��o*�\n�i�Z��)�1����!�
�M����Iun0��w��MW��[	�J?J#���o�n�(y�;%��+�Y]������KVi�s�/O�U���	�L��d0άHlvҡ0���Ϭ���T�[&D�p�޶��˷W1��0�X~��juFwO%\nHqP� ����v�/
� �k\0�\'����ddXJ�S(�̊^[�M{���dm}����:�r����*{�zǣE��Gg��޷�Yʱ��C/�\n}n�/º��%��F1��2e�<�㻨t�I��dt�Gt��D�=i�Q:��U�\\�)���sh9�<����)�!�bd}���oa���H�1��y�9��7!�6�1�;����� ս d,.��t�ٟ��	�3��@&;L٩J�m?�\r�Wݫ�<��05�3�%�52�WOFN��ج����~�	�.*ŵW��_���{�/��޻���6��}�-ϰ���̿�)��#g��e~\0\0\0\0IEND�B`�',2);
/*!40000 ALTER TABLE `badge_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `badgeimportancebyuser`
--

DROP TABLE IF EXISTS `badgeimportancebyuser`;
/*!50001 DROP VIEW IF EXISTS `badgeimportancebyuser`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `badgeimportancebyuser` (
  `badge_instance` tinyint NOT NULL,
  `user` tinyint NOT NULL,
  `nickname_area` tinyint NOT NULL,
  `importance` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `badgetype_sortco`
--

DROP TABLE IF EXISTS `badgetype_sortco`;
/*!50001 DROP VIEW IF EXISTS `badgetype_sortco`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `badgetype_sortco` (
  `oid` tinyint NOT NULL,
  `der_attr` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `bundle_data`
--

DROP TABLE IF EXISTS `bundle_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bundle_data` (
  `oid` int(11) NOT NULL,
  `key` varchar(255) DEFAULT NULL,
  `locale` varchar(255) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bundle_data`
--

LOCK TABLES `bundle_data` WRITE;
/*!40000 ALTER TABLE `bundle_data` DISABLE KEYS */;
INSERT INTO `bundle_data` VALUES (1,'longDescription','en',NULL),(2,'publishingDate','en',''),(3,'YouGotShareFacebook','en','You got credits because you shared on Facebook '),(4,'Edit','es',NULL),(5,'PowerIndex.From','en','from'),(6,'CompareValidationRule.gteq.field','es','<li>the entered value should be greater than or equal to the value of ${otherFieldName} field</li>'),(7,'Instructions','it',''),(8,'Previous Activity','fr',NULL),(9,'Course Students Number','es',NULL),(10,'error.validation.exception','es','<li>The following exception occurred during form validation: {0}</li>'),(11,'encodedtitle','it',''),(12,'LikeValidationRule.endsWith.field','fr','<li>the entered value should end with the value of ${otherFieldName} field</li>'),(13,'go to homepage','it','Panoramica'),(14,'Add Level','es',''),(15,'Latest topics','it','Nuove Domande'),(17,'Type','es',NULL),(18,'activationDate','fr',NULL),(19,'minVersion','en',NULL),(20,'description (search engine)','es',NULL),(21,'BooleanValidation','fr','<li>the entered value must be a boolean (${pattern}) </li>'),(22,'XsdTypeValidationRule.total.digits','es','<li>the entered value must have at most ${digits} total digits</li>'),(23,'licenseServerOperatingSyste','es',NULL),(24,'Common Data','fr',NULL),(25,'HQ.ReputationTab','es','ReputaciÃ³n'),(26,'CertificateFinded','en',NULL),(27,'error.security','fr','A security exception occurred: {0}'),(28,'vote','it',''),(29,'HQ.UserDashboard.Private','en','My Dashboard'),(30,'HQ.ReputationTab','fr',NULL),(31,'value','it',''),(32,'Last Events','en',NULL),(34,'History Diagram','it',''),(35,'getting-start-link','fr',NULL),(36,'productid','es',NULL),(37,'editStatus','es',NULL),(38,'ModuleName','es',NULL),(39,'icon','en',NULL),(40,'CropImageUnit.noImage','es',NULL),(41,'User last downloads','en','User last downloads'),(42,'Positions','en',NULL),(44,'n.VoteUp','fr',NULL),(45,'YouGotSignUp','fr',NULL),(46,'Store Attachment','fr',NULL),(47,'user','en',NULL),(48,'All','fr',NULL),(49,'LinkParticipation','es',''),(50,'plugins','en',NULL),(51,'ManagementEvent','it',''),(53,'Areegeografiche','en',NULL),(54,'XsdTypeValidationRule.invalid.float','fr','<li>Invalid Float</li>'),(55,'Link11','es',NULL),(56,'Scroller.Of','es','of'),(57,'Area','it',''),(58,'descriptionTmp','es',NULL),(59,'LikeValidationRule.endsWith.field','en','<li>the entered value should end with the value of ${otherFieldName} field</li>'),(60,'TypeValidationRule.error','it','<li>il valore inserito deve essere nel formato ${pattern}</li>'),(61,'tempScreenshots','en',NULL),(62,'Manage Area','fr',NULL),(63,'Link206','es',NULL),(64,'getting-start','it','Come iniziare'),(65,'YouGotApproveAnswer','fr',NULL),(66,'isoCode','it',''),(67,'User last topics','fr',NULL),(68,'ManagementEvent','es',NULL),(69,'DateValidation','en','<li>the entered value should have the format ${pattern}</li>'),(70,'go-to-store','es','Ir al Store'),(71,'users','es','usuarios'),(72,'activeExplicit','it',''),(73,'HQ.UsersList.AnyArea','es','Eliminar filtro zona'),(74,'Overall Participation','fr',NULL),(75,'valideType.error','fr',NULL),(76,'Add Badge','es',NULL),(77,'HQ.TotalCredits','es','CrÃ©ditos total '),(78,'lastVersionUpdateTimestamp','fr',NULL),(79,'updateTimestamp','fr',NULL),(80,'checkCode','fr',NULL),(81,'Query.Next','es','next'),(82,'content','es',NULL),(83,'HierarchicalIndex.Previous','en','previous'),(84,'HQ.YourProfile.NoData','es','El usuario no proporcionÃ³ sus datos'),(85,'Badge Instance','fr',NULL),(86,'noItemsFound','es',NULL),(87,'username','es',''),(88,'GroupName','en',NULL),(89,'Importance','it',''),(90,'tag','fr',NULL),(91,'HQ.ErrorLogin','fr',NULL),(93,'Manage Level','en',''),(94,'Department','es',NULL),(95,'HQ.YourLastAction','es','Su Ãºltimas acciÃ³n'),(96,'HQ.store.vote','es',NULL),(97,'Chunks Text','es',NULL),(98,'community.goToDashboard','it','Vai alla Dashboard dell\'utente'),(99,'HQ.NotPublicProfile','en','Private Profile'),(100,'HQ.Message','es',NULL),(101,'datamodified','es',NULL),(103,'Edit Text','fr',NULL),(104,'invioEffettuato','fr',NULL),(105,'ValueLengthValidationRule.eq','en','<li>the entered value length must be ${length} characters</li>'),(106,'units','fr',NULL),(107,'Your last topics','en','Your last topics'),(108,'Badge Selected','en',''),(109,'moduleDomainName','es',NULL),(110,'View','es',NULL),(111,'Company','es',NULL),(112,'question','fr',NULL),(113,'accepted','en',''),(114,'invioEffettuato','es',NULL),(115,'IntegerValidation','fr','<li>the entered value must be an integer</li>'),(116,'HQ.UsersList.Users','es','Usuario y Ranking'),(117,'Areas','en',''),(118,'Set','fr',NULL),(119,'Update','it',''),(120,'CompareValidationRule.gt','it','<li>il valore inserito deve essere maggiore di ${value}</li>'),(121,'thumbnailblob','en',NULL),(122,'KB Action','es',NULL),(123,'Area of Site','es',''),(124,'PowerIndex.Previous','it','precedente'),(125,'Scroller.To','es','to'),(126,'PowerIndex.Jump','it','vai a'),(127,'Link49','es',NULL),(128,'HQ.UsersList.Profile','en','Profile'),(129,'YouGotsetData','en','You setted'),(131,'Query.To','fr','to'),(132,'oid2','es',NULL),(134,'Nickname','fr',NULL),(135,'Reputation','fr',NULL),(136,'Order','en',NULL),(138,'Store Badge','en','Store Badge'),(139,'XsdTypeValidationRule.invalid.pattern','es','<li>the entered value has an invalid format (${pattern})</li>'),(140,'Headquarter','es',NULL),(141,'MandatoryValidationRule.error','fr','<li>mandatory field</li>'),(142,'Certifications history','en','Certifications history'),(143,'Certifications','it','Certificazioni'),(144,'errors.footer','en','</ul>'),(145,'QueryUnit.wrongGrouping','en','All the displayed attributes must be used as grouping or aggregate function'),(146,'emptyUnitMessage','es','No Results'),(147,'Enable','it',''),(148,'JobStatus.JobGroup','es','Group'),(150,'Certification','fr',NULL),(151,'`key`words (search engine)','es',NULL),(152,'Add Level','it',''),(153,'Forum Badge Title','it',''),(154,'HierarchicalIndex.Jump','en','jump to'),(155,'HQ.NotPublicProfile','fr',NULL),(156,'Reputation','en','Reputation'),(157,'Active','en',''),(158,'Review2','fr',NULL),(159,'PowerIndex.NoItemsShownOf','en','no items shown of'),(160,'Teacher Name','it',''),(161,'Link24','es',NULL),(162,'ModuleName','it',''),(163,'Choose the Display Sorting','fr',NULL),(164,'Badge Selected','es',''),(166,'Serialtable','es',NULL),(167,'`key`','fr',NULL),(168,'Certifications history','es','Certificaciones ganadas'),(169,'Headquarter','fr',NULL),(170,'duration','en',NULL),(171,'Scrool Actions','en',NULL),(172,'Scroller.From','es','from'),(174,'Query.Previous','es','previous'),(175,'Link95','es',NULL),(176,'BirthDate','it',''),(177,'Add','fr',NULL),(180,'Store Attachment','it',''),(181,'Countrytable','en',NULL),(182,'History actions','en',NULL),(183,'tempIconblob','fr',NULL),(184,'City','en','City'),(185,'retries','en',NULL),(186,'confirmForceUpdate','it',''),(187,'Scroller.Jump','es','jump to'),(188,'LikeValidationRule.contains','es','<li>the entered value should contain ${value}</li>'),(189,'Teacher LastName','fr',NULL),(190,'Your last downloads','es','Sus Ãºltimos descargas'),(191,'XsdTypeValidationRule.fraction.digits','fr','<li>the entered value must have at most ${digits} fraction digits</li>'),(192,'Small Photo','es',NULL),(193,'Check private','en',NULL),(194,'Rank Participation Overall','es',''),(195,'certification','es',NULL),(196,'companypartner','es',NULL),(197,'family','es',NULL),(198,'User latest','es',''),(199,'units','en',NULL),(200,'Link1','es',NULL),(202,'Link5','es',NULL),(203,'DateValidation','it','<li>il valore inserito deve essere nel formato ${pattern}</li>'),(204,'LMS','fr',NULL),(205,'CompareValidationRule.lteq','en','<li>the entered value should be smaller than or equal to ${value}</li>'),(207,'multiselectionfield.deselectAll','it','Deseleziona tutti'),(208,'Show Pdf','fr',NULL),(209,'ActionForm2','it',''),(210,'BirthDate','es',NULL),(211,'DateValidation','fr','<li>the entered value should have the format ${pattern}</li>'),(212,'HQ.CertificateTab','en','Certificates'),(213,'confirmNoteDeletion','es',NULL),(214,'CertificateForm','it',''),(215,'Activationtable','it',''),(217,'thumbnailblob','fr',NULL),(218,'file','es',NULL),(219,'confirmForceUpdate','fr',NULL),(220,'YouGotShareTwitter','it','Hai ottenuto punti per aver condiviso su Twitter'),(221,'sendEmail','en',NULL),(222,'HQ.Back','fr',NULL),(223,'emptyUnitMessage','fr','No Results'),(224,'Creation Date','en',''),(225,'Create','fr',NULL),(226,'Link145','en',NULL),(227,'releaseType','it',''),(228,'Filter by region','es','Filtrar por regiÃ³n'),(229,'CompareValidationRule.eq','it','<li> il valore inserito deve essere uguale a ${value}</li>'),(230,'Query.Next','it','successivo'),(231,'Question','fr',NULL),(232,'Monthly Message','es',NULL),(233,'IntegerValidation','es','<li>the entered value must be an integer</li>'),(234,'HQ.YourProfile.Certificate','es',''),(235,'HQ.Credits','it','punti'),(236,'Geographical Area','es','Zona GeogrÃ¡fica'),(237,'note','it',''),(239,'YouGotPostSubscription','fr',NULL),(240,'EMailValidationRule.error','es','<li>invalid email address</li>'),(241,'YouGotPostSubscription','en','You got credits for your subscription'),(242,'serialNumber3','fr',NULL),(243,'CompareValidationRule.gteq.field','fr','<li>the entered value should be greater than or equal to the value of ${otherFieldName} field</li>'),(244,'ActionForm2','fr',NULL),(245,'Partecipation','es',NULL),(246,'universitycc','it',''),(247,'companycc','fr',NULL),(248,'Area Badge','en',''),(249,'YouGotPublishStore','es','Tienes crÃ©ditos porque publicaste'),(250,'JobStatus.JobId','en','ID'),(251,'Add Area','es',''),(252,'CompareValidationRule.lteq','es','<li>the entered value should be smaller than or equal to ${value}</li>'),(253,'Mobile','it',NULL),(255,'Certification Badge Title','es',NULL),(256,'YouGotAnswerVoteup','es','Tienes crÃ©ditos porque su respuesta fue votada hasta'),(257,'serialNumber','fr',NULL),(258,'HQ.YourProfile.Developer','es',''),(259,'HQ.Back','es',NULL),(261,'List of Actions','fr',NULL),(262,'generationDate','es',NULL),(263,'Partecipation','en','Partecipation Score'),(264,'YouGotReadingWiki','fr',NULL),(265,'DecimalValidation','fr','<li>the entered value must be a number</li>'),(266,'moduleDomainName','it',NULL),(267,'HQ.store.authorBy','es',NULL),(269,'author','fr',NULL),(270,'Rank Participation Monthly','it',''),(271,'HQ.ManageProfile','es',NULL),(272,'HQ.NotFromSite','en','You can\'t see this profile, go to the home page and login.'),(274,'Review','es',NULL),(275,'HQ.NicknameNull','it',NULL),(276,'text','fr',NULL),(277,'HQ.CreditsTab','en','Credits'),(278,'Country','it',NULL),(279,'supportedVersion','fr',NULL),(282,'Tag','fr',NULL),(283,'Community User','it',NULL),(284,'Search bar','es',NULL),(285,'Review2','it',NULL),(286,'MandatoryValidationRule.error','it','<li>campo obbligatorio</li>'),(287,'UserOf','en',NULL),(288,'error.validation.exception','it','<li>Si è verificata la seguente eccezioni durante la validazione della form: {0}</li>'),(289,'error.button.not.pressed','fr','<li>Please press a button for form submission</li>'),(290,'Update Badge','es',NULL),(291,'userId','it',''),(292,'Teacher Name','en',NULL),(293,'Scroller.First','fr','first'),(294,'Set','it',NULL),(295,'Certifications','es','Certificaciones'),(296,'HQ.HomePage','es',NULL),(297,'HQ.YourProfile.ProfileUnknown','es','Perfil Desconocido'),(299,'Query.Previous','en','previous'),(300,'releaseType','en',''),(301,'active','es',''),(303,'Type','fr',NULL),(304,'Latest topics','es','Ãšltimos temas'),(305,'User Data','it',''),(306,'HQ.YourProfile.Developer','it',''),(307,'Certification','es',NULL),(308,'configuration','fr',NULL),(309,'XsdTypeValidationRule.max.inclusive','fr','<li>the entered value should be smaller than or equal to ${value}</li>'),(310,'minVersion','it',NULL),(311,'LikeValidationRule.endsWith.field','it','<li>il valore inserito deve finire con il valore del campo ${otherFieldName}</li>'),(312,'Process.metadata.outdated','it',NULL),(313,'city','en',''),(314,'JobStatus.TriggerGroup','fr','Group'),(315,'Locale','it',NULL),(316,'Level of Certificate','it',''),(317,'NoWorkItemsFound','it',''),(318,'Scroller.NoItemsShownOf','en','no items shown of'),(319,'HQ.is','it','è'),(320,'Teacher Name','es',NULL),(321,'Rating','it',NULL),(322,'HQ.PersonalData','it',NULL),(323,'sortOrder','es',NULL),(324,'noNews','it',NULL),(325,'New Badge','it',NULL),(326,'JobStatus.JobGroup','it','Group'),(327,'productid','it',NULL),(328,'No results','en','No results found'),(329,'HQ.Message','en',''),(330,'`key`','it',NULL),(331,'Your last topics','fr',NULL),(332,'Sort Badges Area','es',NULL),(333,'Type of Action','en',''),(334,'JobStatus.TriggerGroup','it','Group'),(335,'Forum','fr',NULL),(336,'MostimportantCertificate','it',NULL),(337,'Delete All','it',NULL),(338,'Modify Level','it',NULL),(339,'iconblob','it',NULL),(340,'Big Photo','fr',NULL),(341,'HQ.YourProfile.Private','es','Su perfil es privado'),(342,'Store Attachment','en',''),(344,'HierarchicalIndex.To','it','a'),(345,'HQ.PersonalCertificates','es',NULL),(346,'JobStatus.TriggerDescription','es','Description'),(347,'HQ.YourProfile.NoDeveloper','en',''),(348,'Rank Participation Overall','fr',NULL),(349,'Scroller.NoItemsShownOf','it','Nessun elemento visualizzato su'),(350,'Confirm Finish','fr',NULL),(351,'User last topics','en','User last topics'),(352,'Sort Number','en',''),(353,'LowLevel','en',''),(354,'Query.Next','fr','next'),(355,'Region','en',''),(356,'articleoid','fr',NULL),(357,'List of Areas','en',''),(358,'Go to ranking','en','Go to Leaderboard'),(359,'view','es',NULL),(360,'YouGotApprovedAnswer','it','Hai ottenuto punti per l\'approvazione della tua risposta'),(361,'notNotesFound','es',NULL),(362,'XsdTypeValidationRule.max.exclusive','en','<li>the entered value should be smaller than ${value}</li>'),(363,'Company Email','fr',NULL),(364,'DemoUsers','it',''),(365,'PowerIndex.To','es','to'),(366,'tempDescription','en',''),(367,'HQ.YouAre','en','You are'),(368,'Latest Published Store Components','es','Ãšltimos Componentes en el Store'),(369,'JobStatus.JobGroup','en','Group'),(370,'tempScreenshotsblob','es',NULL),(371,'Public','es',NULL),(372,'Add Level','fr',NULL),(373,'YouGotAnswerVoteup','fr',NULL),(374,'Province','es',NULL),(375,'lastModified','es',NULL),(376,'Green Check','en',''),(377,'OID','en',''),(379,'Query.NoItemsShownOf','en','no items shown of'),(380,'XsdTypeValidationRule.invalid.decimal','it','<li>Numero non valido</li>'),(381,'Modify Actions','it',NULL),(382,'Authorization','it',NULL),(383,'Link','es',NULL),(384,'noProcessInstancesFound','es',NULL),(385,'`key`word','fr',NULL),(386,'Process.metadata.outdated','en',''),(387,'CertificationCRM','fr',NULL),(388,'averageVote','en',NULL),(389,'errors.footer','es','</ul>'),(390,'answerOf','en','The answer'),(391,'JobStatus.JobDescription','en','Description'),(392,'Modify Badge Area','es',NULL),(393,'CollectionValidationRule.notIn','it','<li>il valore inserito non deve essere uno tra: ${values}</li>'),(394,'maxVersion','it',''),(395,'selectionfield.noselection','it','Nessuna selezione'),(396,'plugins','es',NULL),(397,'HQ.CertificateTab','es',NULL),(398,'licenseServerOperatingSyste','fr',NULL),(400,'Partecipation','it',''),(401,'expirationDate','fr',NULL),(402,'go-to-lms','fr',NULL),(403,'tags','fr',NULL),(404,'Forum Badge','es','Forum Badge'),(405,'JobStatus.TriggerNextFireTimestamp','en','Next Fire Timestamp'),(406,'all actions','fr',NULL),(407,'units','es',NULL),(408,'Vat Number','en',NULL),(409,'LMS','it',''),(410,'HQ.UserDashboard','it',''),(411,'answerOf','es',NULL),(412,'Dashboard','it',''),(413,'community.goToDashboard','en','Go to User\'s Dashboard'),(414,'sampleProject','fr',NULL),(415,'HQ.CertificateTab','it',''),(416,'LikeValidationRule.notEndsWith.field','es','<li>the entered value should not end with the value of ${otherFieldName} field</li>'),(417,'HierarchicalIndex.Of','it','di'),(418,'Go to account','it','Vai al tuo Account'),(419,'XsdTypeValidationRule.min.exclusive','it','<li>il valore inserito deve essere maggiore di ${value}</li>'),(420,'fromPortal','es',NULL),(423,'error.check.minChecked','es','<li>check at least {1} items</li>'),(424,'Latest downloads','en','Latest Components'),(425,'XsdTypeValidationRule.invalid.enum','es','<li>Invalid enumeration value (${values})</li>'),(426,'Set the Icons','it',''),(427,'Public Profile','fr',NULL),(428,'Modify Level','es',''),(429,'Administration','es',NULL),(430,'updateTimestamp','es',NULL),(431,'ratingtag','fr',NULL),(433,'HQ.CreditsTab','it','Punti'),(434,'HQ.YourLastAction','en','Your last action'),(435,'licenseServerVersion','es',NULL),(436,'PowerIndex.Jump','en','jump to'),(437,'Forum Badge','en','Forum Badge'),(440,'CompareValidationRule.gteq.field','en','<li>the entered value should be greater than or equal to the value of ${otherFieldName} field</li>'),(441,'Forum Level','es','Nivel Forum'),(442,'HQ.NoForumActivity','fr',NULL),(443,'PowerIndex.Of','es','of'),(445,'Store Level','en','Store Level'),(446,'oid','it',''),(447,'language','en',NULL),(448,'TypeValidationRule.error','en','<li>the entered value should have the format ${pattern}</li>'),(449,'UserName','es',''),(450,'TimestampValidation','es','<li>the entered value should be in the format ${pattern}</li>'),(451,'Latest Published Learning Objects','fr',NULL),(452,'Link145','es',NULL),(453,'icon','it',''),(454,'editStatus','fr',NULL),(455,'nickname','en',NULL),(456,'store','it',''),(457,'Phone Number','fr',NULL),(458,'ToolVersion','es',NULL),(459,'Query.Last','fr','last'),(460,'City','fr',NULL),(461,'Store Attachment','es',NULL),(462,'HQ.UsersList.Users','it',''),(463,'area','es',''),(464,'Delete All','fr',NULL),(465,'Role','es',NULL),(466,'templates','en',NULL),(467,'Nickname','en','Nickname'),(468,'custom description','fr',NULL),(469,'LikeValidationRule.notEndsWith.field','fr','<li>the entered value should not end with the value of ${otherFieldName} field</li>'),(470,'YouGotShareTwitter','en','You got points for share on Twitter'),(471,'Community User','en',NULL),(473,'Sort Number','it',''),(474,'int','en','Only Integer Number'),(475,'licenseServerOperatingSyste','it',''),(476,'BooleanValidation','it','<li>il valore inserito deve essere di tipo booleano (${pattern}) </li>'),(477,'XsdTypeValidationRule.invalid.float','en','<li>Invalid Float</li>'),(478,'JobStatus.JobDescription','fr','Description'),(479,'error.check.maxChecked','es','<li>check at most {1} items</li>'),(480,'SortCombination','fr',NULL),(481,'CertificateFinded','fr',NULL),(482,'Labels Text','fr',NULL),(483,'YouGotAnswerApproved','fr',NULL),(484,'Locale','es',NULL),(485,'LinkParticipation','it',''),(486,'fromPortal','it',NULL),(487,'Action Type','fr',NULL),(488,'HierarchicalIndex.To','en','to'),(489,'Select a region','es','Seleccionar una regiÃ³n'),(490,'tempScreenshotsblob','en',NULL),(491,'QueryUnit.wrongGrouping','fr','All the displayed attributes must be used as grouping or aggregate function'),(492,'updateTimestamp','it',NULL),(493,'Company Type','it',NULL),(494,'MandatoryValidationRule.error','es','<li>mandatory field</li>'),(495,'HQ.YourProfile.ProfileUnknown','it',''),(496,'resourceType','it',NULL),(497,'int','es','Only Integer Number'),(498,'objectId','fr',NULL),(499,'Monthly Participation','fr',NULL),(500,'latest-badges','es','Ãšltimos Badge'),(501,'note','en',NULL),(502,'User overall position','es',''),(503,'answerOf','it','di'),(504,'Areas','fr',NULL),(505,'CollectionValidationRule.notIn.query','fr','<li>the entered value is included in the list of forbidden values</li>'),(506,'KB Action','it',''),(507,'Scroller.Last','en','last'),(508,'error.check.minChecked','it','<li>selezionare almeno {1} elementi</li>'),(509,'UserName','fr',NULL),(510,'Manage actions','it',NULL),(511,'Forum Actions','it',NULL),(512,'WROWNERID','en',NULL),(513,'Store Item','es',NULL),(514,'Instructions','es',NULL),(515,'Action Type','en',''),(516,'Most Important Badges','es',NULL),(517,'Company Certified','en',NULL),(518,'Website','en','Website'),(519,'HQ.HomePage','it',NULL),(520,'UserName','en',''),(521,'Company Name','it',NULL),(523,'name','fr',NULL),(524,'HQ.UserOf','it','di'),(525,'YouGotShareGooglePlus','es','Tienes crÃ©ditos cuando lo compartiste en Google+'),(526,'userPartner','fr',NULL),(527,'Description','fr',NULL),(528,'XsdTypeValidationRule.invalid.decimal','es','<li>Invalid Decimal</li>'),(529,'store','fr',NULL),(530,'LikeValidationRule.startsWith.field','es','<li>the entered value should begin with the value of ${otherFieldName} field</li>'),(531,'LikeValidationRule.startsWith.field','it','<li>il valore inserito deve iniziare con il valore del campo ${otherFieldName}</li>'),(532,'LikeValidationRule.endsWith','es','<li>the entered value should end with ${value}</li>'),(533,'notAttachmentsFound','es',NULL),(534,'text','it',NULL),(535,'languageCode','es',NULL),(536,'sequence','it',NULL),(537,'university','fr',NULL),(538,'GroupName','fr',NULL),(539,'errors.header','en','<ul>'),(540,'HQ.YourProfile.NoDeveloper','es',''),(542,'Needed Score','fr',NULL),(543,'User Data','es',NULL),(544,'HQ.View','it',NULL),(545,'Modify Badge','en',NULL),(546,'Disable','it',NULL),(547,'LikeValidationRule.startsWith','fr','<li>the entered value should begin with ${value}</li>'),(548,'SortCombination','en',NULL),(549,'YouGotSignUp','it','Hai completato la registrazione'),(550,'Link','it',NULL),(551,'YouGotCoursePassed','it','Hai ottenuto punti per aver passato il corso'),(552,'areasort','fr',NULL),(553,'error.empty','en','{0}'),(554,'CompareValidationRule.neq','fr','<li>the entered value should be different from ${value}</li>'),(555,'HQ.YourProfile.ProfileUnknown','fr',NULL),(556,'licenseServerVersion','en',NULL),(557,'longDescription','es',NULL),(558,'version','it',NULL),(559,'`key`word','es',NULL),(560,'tempName','en',NULL),(561,'longDescription','fr',NULL),(562,'YouGotShareFacebook','fr',NULL),(563,'userOid','es',''),(565,'JobStatus.JobDescription','it','Description'),(566,'go to homepage','fr',NULL),(567,'description','it',NULL),(568,'List of Levels','en',''),(569,'Action Instance','fr',NULL),(570,'Levels of the Area:','it',''),(571,'CompareValidationRule.lteq.field','it','<li>il valore inserito deve essere minore o uguale al valore del campo ${otherFieldName}</li>'),(572,'Private Message','fr',NULL),(573,'XsdTypeValidationRule.invalid.date','en','<li>Invalid Date</li>'),(574,'CheckedItemsValidationRule.eq','es','<li>${itemCount} items checked required</li>'),(575,'Flow38','es',NULL),(576,'Flow37','es',''),(577,'DemoUsers','fr',NULL),(578,'Bio','fr',NULL),(579,'Flow39','es',NULL),(582,'Latest Published Store Components','it','Nuovi Componenti Pubblicati'),(584,'store','en','Store'),(585,'Role','it',NULL),(586,'sequence','en',''),(587,'Mandatory3','es','Choose an area'),(588,'Geographical Area','it',NULL),(589,'Mandatory4','es','Insert a title'),(592,'Query.First','en','first'),(593,'templates','es',NULL),(596,'Mandatory5','es','Insert a needed score'),(597,'Mandatory6','es','Insert the importance'),(598,'Partner','en','Partner'),(599,'go to monthly','en','See Full Leaderboard'),(600,'HQ.slider.error','it',NULL),(601,'HQ.Of','it','di'),(602,'confirmAttachmentDeletion','en',''),(603,'HQ.RankingOverall','es','Clasifica General'),(604,'XsdTypeValidationRule.invalid.boolean','en','<li>Invalid Boolean</li>'),(605,'Certification Score','it',''),(606,'Common Data','es',NULL),(608,'universitycc','en',''),(609,'My certificate','es',NULL),(610,'Vat Number','it',NULL),(611,'answerdate','en',''),(612,'getting-start-link','es','Ir a la GuÃ­a de Usuario'),(613,'Public','en',''),(614,'HQ.UserDashboard.AllActions','it','Azioni totali'),(615,'HQ.UserDashboard ','es',NULL),(616,'YouGotLogin','en','You got credits for your login'),(617,'address','es',NULL),(618,'HQ.Credits','en','credits'),(619,'Query.NoItemsShownOf','fr','no items shown of'),(620,'descriptionTmp','it',NULL),(621,'Flow46','es',NULL),(623,'Disable','fr',NULL),(624,'LikeValidationRule.notContains.field','es','<li>the entered value should not contain the value of ${otherFieldName} field</li>'),(626,'confirmNoteDeletion','en',''),(628,'Flow42','es',NULL),(629,'Course Title','es',NULL),(632,'Flow41','es',NULL),(633,'Flow40','es',NULL),(634,'Modify','fr',NULL),(636,'valideType.error','en',''),(637,'LoginHQ','it',NULL),(638,'Confirm Finish','es',NULL),(639,'Store Badge','fr',NULL),(640,'Last Badge','it',''),(641,'Latest downloads','es','Ãšltimos Componentes'),(642,'Query.Jump','es','jump to'),(643,'Positions','fr',NULL),(644,'Resource Type','fr',NULL),(645,'Manage icons','it',NULL),(646,'City','it',NULL),(648,'YouGotShareTwitter','es','Tienes crÃ©ditos porque compartiste en Twitter'),(650,'Flow65','es',NULL),(651,'NotPublicProfile.Private','es',NULL),(652,'Modify Actions','en',''),(653,'multiselectionfield.deselectAll','en','Deselect All'),(654,'Flow64','es',NULL),(655,'Disable','en',''),(656,'templates','fr',NULL),(657,'History Certificates','en',''),(658,'Sort Number','fr',NULL),(659,'vote','en',''),(660,'Getting Started','fr',NULL),(661,'PowerIndex.Next','fr','next'),(662,'error.button.not.pressed','en','<li>Please press a button for form submission</li>'),(663,'Query.Of','en','of'),(664,'HQ.YourLastAction','it','Ultima azione'),(665,'TimeValidation','it','<li>il valore inserito deve essere nel formato ${pattern}</li>'),(666,'generationDate','en',''),(667,'Link','en',''),(668,'confirmAttachmentDeletion','it',NULL),(669,'templatesblob','it',NULL),(670,'error.security','es','A security exception occurred: {0}'),(671,'Choose the Display Sorting','es',NULL),(672,'ValueLengthValidationRule.neq','es','<li>the entered value length must be different from ${length} characters</li>'),(673,'Serialtable','it',NULL),(674,'HQ.YourBadges','it','Badge Acquisiti'),(676,'YouGotApprovedAnswer','en','You got points for your answer was approved'),(677,'YouGotMonthly','en','You got monthly points'),(678,'Group','it',NULL),(679,'no my actions','en','You never compute actions'),(680,'author','it',NULL),(681,'Answer','es',NULL),(682,'forum','it',NULL),(683,'Badge Instance','en',''),(684,'Dashboard','en',''),(685,'XsdTypeValidationRule.invalid.length','it','<li>il valore deve essere lungo ${length} caratteri</li>'),(686,'DemoUsers','en',''),(687,'sampleProjectblob','en',''),(688,'LikeValidationRule.startsWith.field','fr','<li>the entered value should begin with the value of ${otherFieldName} field</li>'),(689,'notAttachmentsFound','en',''),(690,'Welcome','es',NULL),(691,'Monthly Participation','es',''),(692,'Action Instance','it',''),(693,'HQ.YourProfile.Professional','es',''),(694,'Query.From','en','from'),(695,'Level Badge','fr',NULL),(696,'sortByLevel','es','Filtrar por Nivel de Badge'),(697,'User overall position','it',''),(698,'XsdTypeValidationRule.invalid.time','es','<li>Invalid Time</li>'),(699,'error.check.eqChecked','it','<li>selezionare {1} elementi</li>'),(700,'at','it',NULL),(701,'MostimportantCertificate','es',NULL),(702,'userPartner','es',''),(703,'Save','it',NULL),(704,'Language','fr',NULL),(705,'error.stacktrace','es','{0}'),(706,'Query.Jump','en','jump to'),(707,'activeExplicit','fr',NULL),(708,'by','it',NULL),(709,'HQ.HomePage','fr',NULL),(711,'YouGotDownloadStore','es','Tienes crÃ©ditos que han descargado'),(712,'author','es',NULL),(713,'Add area','fr',NULL),(714,'image','en',''),(715,'History actions','es',''),(716,'Province','fr',NULL),(717,'LikeValidationRule.notStartsWith','it','<li>il valore inserito non deve iniziare con ${value}</li>'),(718,'HQ.ErrorLogin','es',NULL),(720,'CompareValidationRule.gt.field','it','<li>il valore inserito deve essere maggiore al valore del campo ${otherFieldName}</li>'),(721,'HQ.YourProfile.NoData','it',''),(722,'Link49','en',''),(723,'confirmRollback','it',NULL),(724,'Store Badge Title','fr',NULL),(725,'YouGotCoursePassed','es','Tienes crÃ©ditos porque usted pasÃ³ el curso'),(726,'Manage Action','es',NULL),(729,'Generic Message','es',NULL),(730,'HQ.UsersList.AnyLevel','it','Reset'),(731,'Your last downloads','en','Your last downloads'),(732,'Chunks Text','it',NULL),(733,'CaptchaValidationRule.error','it',' <li>Il valore inserito non corrisponde a quello visualizzato</li>'),(734,'Set the Icons','fr',NULL),(735,'tempThumbnail','it',NULL),(736,'List of Badges','en',''),(737,'error.stacktrace','fr','{0}'),(739,'view','fr',NULL),(740,'multiselectionfield.selectAll','en','Select All'),(741,'Last Badge','fr',NULL),(742,'LikeValidationRule.contains.field','it','<li>il valore inserito deve contenere il valore del campo ${otherFieldName}</li>'),(743,'ordinal','es',NULL),(744,'Company Name','es',NULL),(745,'companycc','it',NULL),(746,'confirmNoteDeletion','it',NULL),(747,'HQ.UsersList.SearchField','fr',NULL),(748,'Modify Level','en',''),(749,'CompareValidationRule.lt','it','<li>il valore inserito deve essere minore di ${value}</li>'),(750,'timestamp2','es',NULL),(751,'LikeValidationRule.contains.field','en','<li>the entered value should contain the value of ${otherFieldName} field</li>'),(752,'SortCombination','it',NULL),(753,'YouGotAnswerVoteup','it','Hai ottenuto punti per il voto alla tua risposta'),(754,'HierarchicalIndex.NoItemsShownOf','en','no items shown of'),(755,'Edit','fr',NULL),(756,'Manage Level','fr',NULL),(757,'BigPhoto','it',NULL),(758,'Review','fr',NULL),(759,'YouGotMonthly','fr',NULL),(760,'Contact','it',NULL),(761,'Link95','en',''),(764,'Store Badge','it',NULL),(765,'No results','it',NULL),(766,'datamodified','en',''),(767,'History actions','it','Azioni compiute'),(768,'CertificationCRM','es',NULL),(769,'my badges','es',NULL),(770,'searchUser','fr',NULL),(771,'XsdTypeValidationRule.invalid.date','es','<li>Invalid Date</li>'),(772,'maxVersion','fr',NULL),(773,'DateValidation','es','<li>the entered value should have the format ${pattern}</li>'),(774,'XsdTypeValidationRule.positive.value','it','<li>Deve essere un valore intero positivo</li>'),(775,'HQ.YourProfile.Developer','en',''),(776,'PowerIndex.From','it','da'),(777,'LikeValidationRule.notContains.field','en','<li>the entered value should not contain the value of ${otherFieldName} field</li>'),(778,'YouGotApproveAnswer','it','Hai ottenuto punti per aver approvato una risposta'),(779,'tempIcon','en',''),(780,'Scroller.Next','fr','next'),(781,'CollectionValidationRule.in.query','en','<li>the entered value is not included in the list of valid values</li>'),(782,'HQ.UsersList.FilterArea','it','Filtra per compentenza'),(784,'Teacher','en',''),(786,'Partecipation down','en',''),(787,'Add area','it',''),(788,'YouGotApproveAnswer','es','Tienes crÃ©ditos porque usted ha aprobado la respuesta'),(789,'Monthly Partecipation down','fr',NULL),(790,'Industry Type','en',''),(791,'Vat Number','fr',NULL),(792,'YouGotReviewStore','en','You got points because you made a review'),(793,'Store Badge Title','en',''),(794,'CollectionValidationRule.in.query','it','<li>il valore inserito non è incluso nei valori permessi</li>'),(796,'Oid','en',''),(797,'TagStore','es',NULL),(798,'Chunks','es',NULL),(800,'Teacher LastName','it',''),(801,'resourceType','es',NULL),(802,'XsdTypeValidationRule.invalid.timestamp','en','<li>Invalid Timestamp</li>'),(803,'Chunks','fr',NULL),(804,'sortOrder','it',NULL),(805,'Certificates','en','Certificates'),(806,'FoundationYear','it',NULL),(807,'EMailValidationRule.error','it','<li>indirizzo email non valido</li>'),(809,'HQ.NotFromSite','it',NULL),(810,'no my actions','es',NULL),(812,'answer','es',NULL),(813,'tempIconblob','es',NULL),(814,'Fax','en',''),(815,'reissues','es',NULL),(816,'HQ.YouAre','es','Usted es'),(817,'HQ.PersonalData','fr',NULL),(818,'userPartnerCert','es',''),(819,'CertificationCRM','en',''),(820,'HD Checked Image','fr',NULL),(821,'Profile','it',''),(822,'EMailValidationRule.error','en','<li>invalid email address</li>'),(823,'error.check.eqChecked','es','<li>{1} items checked required</li>'),(824,'Link24','en',''),(825,'go-to-lms','it','Vai al Learn'),(826,'ToolVersion','it',NULL),(827,'searchUser','en','Filter by name'),(828,'HQ.UsersList.Profile','es','Perfil'),(829,'HQ.RankingOverall','fr',NULL),(830,'HQ.RankNoResults','en','No users in this rank with this properties.'),(831,'XsdTypeValidationRule.min.inclusive','it','<li>il valore inserito deve essere maggiore o uguale a ${value}</li>'),(832,'downloadCount','it',NULL),(833,'List of Levels','es',''),(834,'Manage icons','en',''),(835,'KB Badge Title','it',''),(836,'HQ.YourBadges','en','See your badges '),(837,'Update Badge','en',''),(839,'XsdTypeValidationRule.invalid.pattern','en','<li>the entered value has an invalid format (${pattern})</li>'),(841,'HQ.Answer','en','Answers'),(842,'level','it','livello'),(843,'Clear','it','Reset'),(844,'Scroller.From','fr','from'),(845,'Course Students Number','en',''),(846,'LikeValidationRule.notContains.field','fr','<li>the entered value should not contain the value of ${otherFieldName} field</li>'),(847,'HQ.UserDashboard.Private','fr',NULL),(848,'YouGotLogin','es','Tienes crÃ©ditos para su inicio de sesiÃ³n'),(849,'HD Checked Image','en',''),(850,'Action Area','it',''),(851,'objectId','en',''),(852,'XsdTypeValidationRule.total.digits','fr','<li>the entered value must have at most ${digits} total digits</li>'),(853,'Community Participation','it',''),(854,'Badge:','fr',NULL),(855,'Filter by badge type','es','Filtrar por tipo de badge'),(856,'HQ.UsersList.AnyLevel','fr',NULL),(858,'Query.To','en','to'),(859,'company','it',NULL),(860,'Link11','en',''),(861,'confirmForceUpdate','es',NULL),(862,'userPartner','it',NULL),(863,'XsdTypeValidationRule.min.length','it','<li>il valore deve essere lungo almeno ${length} caratteri</li>'),(864,'tempTags','fr',NULL),(865,'HD Image','fr',NULL),(866,'Scroller.Jump','en','jump to'),(867,'content','it',NULL),(868,'Area of Site','en',''),(869,'activeExplicit','en',''),(870,'levelCertificate','en',''),(871,'of','it','di'),(872,'Enable','es',NULL),(873,'HQ.ParticipationTab','it','Partecipazione'),(874,'nickname','it',NULL),(875,'tags','es',NULL),(876,'approved','es',NULL),(877,'CheckedItemsValidationRule.max','fr','<li>check at most ${itemCount} items</li>'),(878,'YouGotCoursePassed','en','You got points to have passed the course'),(879,'Administration','it',NULL),(880,'Areatomail','fr',NULL),(881,'Certification Badge Title','it',''),(882,'screenshots','fr',NULL),(883,'error.security','it','Impossibile accedere: {0}'),(884,'Big Photo','en',''),(885,'XsdTypeValidationRule.invalid.boolean','it','<li>Booleano non valido</li>'),(886,'tempName','es',NULL),(887,'User latest','en',''),(888,'Area Badge','es',''),(889,'Level of Certificate','en',''),(890,'Labels','fr',NULL),(891,'PowerIndex.First','it','primo'),(892,'XsdTypeValidationRule.invalid.timestamp','fr','<li>Invalid Timestamp</li>'),(893,'HQ.RankCompanyNoResults','es',NULL),(894,'MandatoryIfTrue.error','it',NULL),(895,'Credit history','en','Points and Rewards'),(896,'HQ.YourProfile.PrintPdf','fr',NULL),(897,'FloatValidation','en','<li>the entered value must be a float</li>'),(899,'Bio','es',NULL),(900,'Filter','fr',NULL),(901,'Teacher','it',NULL),(902,'XsdTypeValidationRule.invalid.date','fr','<li>Invalid Date</li>'),(903,'View','it',NULL),(904,'userPartnerCert','fr',NULL),(905,'Monthly Partecipation User','fr',NULL),(906,'confirmForceUpdate','en',''),(908,'YouGotAnswerTopic','es','Tienes crÃ©ditos por su respuesta a la pregunta'),(909,'CompareValidationRule.eq.field','en','<li>the entered value should be equal to the value of ${otherFieldName} field</li>'),(910,'User overall position','fr',NULL),(911,'Link','fr',NULL),(912,'confirmDeletion','it',NULL),(913,'error.empty','es','{0}'),(914,'universitycc','fr',NULL),(915,'Labels Text','es',NULL),(916,'reviewCount','en',NULL),(917,'rejectionReason','es',NULL),(918,'community.users','fr',NULL),(919,'no my actions','fr',NULL),(920,'area','fr',NULL),(921,'XsdTypeValidationRule.min.exclusive','es','<li>the entered value should be greater than ${value}</li>'),(922,'NotPublicProfile.Message','en','This user set his profile as Private'),(923,'HQ.store.vote','en','Rating'),(924,'XsdTypeValidationRule.invalid.length','en','<li>the entered value length must be ${length} characters</li>'),(925,'Article','fr',NULL),(926,'YouGotVoteStore','en','You got credits because you vote up'),(927,'Small Photo','en','Small Photo'),(928,'XsdTypeValidationRule.fraction.digits','it','<li>il valore deve avere al massimo ${digits} cifre decimali</li>'),(929,'History Diagram','en',NULL),(930,'Type of Action','es',NULL),(931,'expirationDate','en',''),(932,'approved','it',NULL),(933,'Store Badge Title','es',NULL),(934,'YouGotReadingWiki','it','Hai ottenuto punti per aver letto'),(935,'useridentifier','es',NULL),(937,'HQ.YourProfile.At','fr',NULL),(938,'contactoid','en',NULL),(939,'CompareValidationRule.lteq','fr','<li>the entered value should be smaller than or equal to ${value}</li>'),(940,'WROWNERID','es',NULL),(941,'Involved Actions Associated','it',NULL),(942,'XsdTypeValidationRule.invalid.pattern','fr','<li>the entered value has an invalid format (${pattern})</li>'),(943,'link','fr',NULL),(944,'XsdTypeValidationRule.positive.value','en','<li>Must be a positive value</li>'),(945,'getting-start-link','it','Come iniziare'),(946,'Forum Actions','fr',NULL),(947,'confirmRollback','es',NULL),(948,'HQ.YourBadges','fr',NULL),(949,'emptyUnitMessage','it','Nessun risultato'),(950,'serialNumber3','es',NULL),(951,'Modify Badge Level','it',''),(952,'LikeValidationRule.notStartsWith.field','es','<li>the entered value should not begin with the value of ${otherFieldName} field</li>'),(954,'HQ.UsersMonthly','fr',NULL),(955,'Text Chunk','fr',NULL),(956,'HQ.store.download','it',NULL),(957,'status','fr',NULL),(958,'Latest Badge','fr',NULL),(959,'styleProject','it',NULL),(960,'units','it',NULL),(961,'Needed Score','en',NULL),(962,'HQ.no.badges','en','No badges'),(963,'Most Important Badges','fr',NULL),(964,'LMS','en','Learn'),(965,'plugins','it',NULL),(966,'Importance','fr',NULL),(967,'New Area','es',NULL),(968,'voteCount','fr',NULL),(969,'serialNumber3','it',NULL),(970,'User Data','en',NULL),(971,'my badges','fr',NULL),(972,'nickname','fr',NULL),(973,'Forum Actions','es',''),(974,'Public Profile','it',''),(975,'getting-start-link','en','Go to Getting started'),(976,'questiondate','it',NULL),(977,'my badges','it',NULL),(978,'Enable','en',NULL),(979,'Go to account','es','Ir a Cuenta'),(980,'forum','fr',NULL),(981,'Company Type','fr',NULL),(982,'Action Instance','en',''),(983,'Public Profile','es',NULL),(984,'Modify Badge Level','en',''),(985,'Add area','es',''),(986,'HQ.NoStoreActivity','it',NULL),(987,'CheckedItemsValidationRule.min','fr','<li>check at least ${itemCount} items</li>'),(988,'PowerIndex.Last','en','last'),(989,'Go to ranking','es','Ir al Ranking'),(990,'HQ.YourProfile.Certificate','it',''),(991,'activeExplicit','es',NULL),(992,'Internal','it',NULL),(993,'emptyUnitMessage','en','No Results'),(994,'LoginHQ','fr',NULL),(995,'noProcessesFound','it',NULL),(996,'CompareValidationRule.lteq.field','en','<li>the entered value should be smaller than or equal to the value of ${otherFieldName} field</li>'),(997,'FormNotEmptyValidationRule.error','fr','<li>at least one field must be filled</li>'),(998,'tempDescription','fr',NULL),(999,'url','en',NULL),(1000,'editionoid','en',NULL),(1001,'Add Area','fr',NULL),(1002,'Answer','en','Answer'),(1003,'CropImageUnit.noImage','en',NULL),(1004,'my badges','en','My Badges'),(1005,'HQ.UserDashboard.Public','it','Dashboard'),(1006,'Partecipation','fr',NULL),(1007,'OID','es',NULL),(1008,'role','es',NULL),(1009,'HQ.CreditsTab','es','CrÃ©ditos'),(1010,'alias','fr',NULL),(1011,'BirthDate','en',''),(1012,'Webratio Activation','fr',NULL),(1013,'Logout','es',''),(1014,'Partner','fr',NULL),(1015,'BigPhoto','en',NULL),(1016,'Scroller.To','it','a'),(1017,'company','en',NULL),(1019,'Image','en',NULL),(1020,'Title','fr',NULL),(1021,'PowerIndex.From','es','from'),(1022,'QueryUnit.wrongExtAttributeCondition','en','One or more conditions are based on attributes outside the table space'),(1023,'All','it',NULL),(1024,'Latest Badge','es','Ãšltimos Badge'),(1025,'ValueLengthValidationRule.min','es','<li>the entered value length must be at least ${length} characters</li>'),(1026,'sortByLevel','en','Filter by Badge Level'),(1027,'New Level','es',''),(1028,'User','fr',NULL),(1029,'id','it',NULL),(1030,'Choose the Display Sorting','it',NULL),(1031,'Level','fr',NULL),(1032,'error.check.maxChecked','en','<li>check at most {1} items</li>'),(1033,'Badge:','it',NULL),(1035,'Country','en','Country'),(1036,'HQ.YourProfile.Professional','fr',NULL),(1037,'New Level','fr',NULL),(1038,'lms','en','LMS'),(1039,'Certificates','fr',NULL),(1040,'multiselectionfield.selectAll','it','Seleziona tutti'),(1041,'YouGotActiving','en','Activing'),(1042,'Your last downloads','fr',NULL),(1043,'CreditCardValidationRule.error','it','<li>numero di carta di credito non valido</li>'),(1044,'LikeValidationRule.contains','it','<li>il valore inserito deve contenere ${value}</li>'),(1045,'Type of Badge','fr',NULL),(1046,'Image','fr',NULL),(1048,'Module','en',NULL),(1049,'NoWorkItemsFound','fr',NULL),(1050,'serialNumber','es',NULL),(1051,'Chunks Text','fr',NULL),(1053,'Scroller.Last','es','last'),(1055,'HQ.YourProfile.At','it','presso'),(1056,'editionoid','fr',NULL),(1057,'area','en',''),(1058,'Modify Area','it',NULL),(1059,'Partecipation up','fr',NULL),(1060,'List of Actions','en',NULL),(1061,'error.check.minChecked','en','<li>check at least {1} items</li>'),(1062,'lastVersionUpdateTimestamp','it',''),(1063,'Scroller.Next','it','successivo'),(1064,'Big Photo','it',NULL),(1065,'oid2','it',NULL),(1066,'HQ.UserOf','es','Usuarios de'),(1067,'valideType.error','es',NULL),(1068,'timestamp','en',NULL),(1069,'HQ.UsersList.FilterLevel','en','Filter by minimum level'),(1070,'notNotesFound','en',NULL),(1072,'Certification Badge','es',NULL),(1073,'`key`word','it',NULL),(1074,'Filter','it',NULL),(1075,'HD Checked Image','it',NULL),(1076,'errors.footer','it','</ul>'),(1077,'HQ.rank.monthly','it','Top Users'),(1078,'HierarchicalIndex.Next','en','next'),(1079,'Nickname_Crm','es',NULL),(1080,'sortOrder','en',NULL),(1082,'All Reputation Actions','it',''),(1083,'HQ.YourProfile.Professor','fr',NULL),(1084,'confirmAttachmentDeletion','es',NULL),(1085,'HQ.UsersList.FilterLevel','fr',NULL),(1086,'HQ.UsersMonthly','es','Top users'),(1087,'voteCount','es',NULL),(1088,'tempIcon','fr',NULL),(1089,'tempThumbnailblob','fr',NULL),(1090,'language','es',NULL),(1091,'HQ.YourBadges','es','Ver Tus Badges'),(1092,'Text Chunk','en',NULL),(1093,'First Name','es',NULL),(1094,'YouBecome','es','Usted se convierte en'),(1095,'confirmCancelProcess','en',NULL),(1096,'Process.metadata.invalid','fr',NULL),(1097,'Manage Badge','en',NULL),(1098,'tempDescription','es',NULL),(1099,'XsdTypeValidationRule.negative.value','en','<li>Must be a negative value</li>'),(1100,'Manage Area','en',''),(1101,'view','it',NULL),(1102,'int','fr','Only Integer Number'),(1103,'GroupName','it',NULL),(1104,'Add','it',''),(1105,'date','it',NULL),(1106,'GroupName','es',NULL),(1107,'itemId','en',NULL),(1108,'HQ.UsersList.SearchField','it','Ricerca utente:'),(1109,'Module','fr',NULL),(1110,'HQ.store.update','fr',NULL),(1111,'styleProjectblob','it',NULL),(1112,'YouGotSignUp','en','You sign up'),(1113,'Latest downloads','fr',NULL),(1114,'Process.metadata.invalid','it',NULL),(1116,'levelCertificate','es',''),(1117,'HQ.UserDashboard.Private','it','My Dashboard'),(1118,'Twitter','es',NULL),(1119,'confirmDeletion','es',NULL),(1120,'image','fr',NULL),(1121,'CollectionValidationRule.in','it','<li>il valore inserito deve essere uno tra: ${values}</li>'),(1122,'iconblob','fr',NULL),(1123,'HQ.ErrorLogin','en',NULL),(1124,'deactivationDate','it',NULL),(1125,'CompareValidationRule.neq.field','it','<li>il valore inserito deve essere diverso dal valore del campo ${otherFieldName}</li>'),(1126,'icon','fr',NULL),(1127,'CompareValidationRule.neq.field','fr','<li>the entered value should be different from the value of ${otherFieldName} field</li>'),(1128,'ActionForm','en',''),(1129,'HierarchicalIndex.First','es','first'),(1130,'UserOf','fr',NULL),(1131,'HQ.no.certificates','es',NULL),(1133,'averageVote','it',NULL),(1134,'sampleProject','it',NULL),(1135,'university','en',NULL),(1136,'HD Image','es',NULL),(1137,'HQ.no.badges','fr',NULL),(1138,'CollectionValidationRule.in','en','<li>the entered value must be one of: ${values}</li>'),(1139,'XsdTypeValidationRule.invalid.decimal','fr','<li>Invalid Decimal</li>'),(1141,'ValueLengthValidationRule.eq','it','<li>il valore inserito deve essere lungo ${length} caratteri</li>'),(1142,'description (search engine)','en',''),(1143,'reissues','it',NULL),(1144,'country','fr',NULL),(1145,'Course Students Number','it',NULL),(1146,'note','es',NULL),(1147,'Name','fr',NULL),(1148,'User last downloads','es','Descargas de usuario'),(1149,'postedAnswer','fr',NULL),(1150,'XsdTypeValidationRule.negative.value','es','<li>Must be a negative value</li>'),(1151,'objectId','it',NULL),(1152,'community.goToDashboard','es','Ir al Dashboard del usuario'),(1153,'accepted','it',''),(1154,'N/D','en',''),(1155,'HQ.YourProfile.NoData','en','The User did not provide his data'),(1156,'LikeValidationRule.notStartsWith','fr','<li>the entered value should not begin with ${value}</li>'),(1157,'Certification Badge','fr',NULL),(1158,'retries','es',NULL),(1159,'Fax','es',NULL),(1160,'styleProjectblob','fr',NULL),(1161,'Query.NoItemsShownOf','it','Nessun elemento visualizzato su'),(1162,'ValueLengthValidationRule.neq','en','<li>the entered value length must be different from ${length} characters</li>'),(1163,'ValueLengthValidationRule.max','es','<li>the entered value length must be at most ${length} characters</li>'),(1164,'noProcessInstancesFound','en',''),(1165,'YouGotShareFacebook','es','Tienes crÃ©ditos cuando lo compartiste en Facebook'),(1166,'Query.Of','es','of'),(1167,'HQ.ReputationTab','it','Reputazione'),(1168,'Course Title','en',''),(1169,'HQ.store.download','en','Download'),(1170,'HQ.YourActions','en','Your Actions'),(1171,'Teacher','fr',NULL),(1172,'YouGotActivingClient','en','You setted partner profile'),(1174,'screenshotsblob','en',''),(1175,'Contact','es',NULL),(1176,'Manage icons','es',NULL),(1177,'Show Activity Details','fr',NULL),(1178,'HQ.ManageProfile','en','Go to account'),(1179,'time','es',NULL),(1180,'styleProjectblob','es',NULL),(1181,'Common Data','it',NULL),(1182,'HierarchicalIndex.Of','en','of'),(1183,'UserOf','es',''),(1184,'Latest Forum Topics','en','Latest Forum Topics'),(1185,'Monthly Form','fr',NULL),(1186,'HQ.RankNoResults','it','Nessun utente in classifica con queste proprietà'),(1187,'userId','fr',NULL),(1188,'List of Areas','es',''),(1189,'Private Message','en',''),(1191,'Community Participation','es',''),(1192,'Add Badge','fr',NULL),(1193,'sampleProject','es',NULL),(1194,'Store Item','en',''),(1195,'Province','it',NULL),(1197,'HQ.UsersList.GoTo','it','Lista Utenti'),(1198,'HierarchicalIndex.Jump','it','vai a'),(1199,'languageCode','it',NULL),(1200,'FloatValidation','it','<li>il valore inserito deve essere di tipo float</li>'),(1201,'CollectionValidationRule.in.query','es','<li>the entered value is not included in the list of valid values</li>'),(1202,'Go to participation','en','Go to Leaderboard'),(1203,'Participation','it','Partecipazione'),(1204,'Manage icons','fr',NULL),(1205,'oidquestion','fr',NULL),(1206,'Filter by badge level','it','Filtra per livello di badge'),(1207,'PowerIndex.Next','it','successivo'),(1208,'latest-badges','en','Latest Badges'),(1209,'errors.footer','fr','</ul>'),(1210,'Forum Badge','it',NULL),(1211,'YouGotReviewStore','es','Tienes crÃ©ditos porque hiciste una revisiÃ³n'),(1213,'generationDate','fr',NULL),(1214,'company','es',NULL),(1215,'voteCount','en',''),(1216,'maxVersion','es',NULL),(1217,'releaseNotes','fr',NULL),(1218,'PowerIndex.From','fr','from'),(1219,'levelsort','es',''),(1220,'DatesCertificates','it',NULL),(1221,'Small Photo','it',NULL),(1222,'error.check.eqChecked','fr','<li>{1} items checked required</li>'),(1223,'Search','it','Cerca'),(1224,'All','es',NULL),(1225,'LowLevel','it',''),(1226,'encodedtitle','en',''),(1227,'Search','es',NULL),(1228,'type','es',NULL),(1229,'CheckedItemsValidationRule.eq','fr','<li>${itemCount} items checked required</li>'),(1230,'New Badge','fr',NULL),(1231,'Link145','it',NULL),(1233,'HQ.Of','en','out of'),(1234,'ActionForm2','es',''),(1235,'ValueLengthValidationRule.neq','it','<li>il valore inserito non deve essere lungo ${length} caratteri</li>'),(1236,'TagStore','en',''),(1237,'Number Of Employees','es',''),(1238,'Levels of the Area:','en',''),(1239,'HQ.UsersList.Filter','it','Filtra per compentenza'),(1240,'Company Name','fr',NULL),(1241,'Partecipation up','it',NULL),(1243,'question','en',''),(1244,'confirmRollback','fr',NULL),(1245,'Province','en',''),(1247,'Bundle Data','it',NULL),(1248,'XsdTypeValidationRule.invalid.decimal','en','<li>Invalid Decimal</li>'),(1249,'KB Level','es','Nivel Learn'),(1250,'Company Certified','it',NULL),(1251,'articleoid','it',NULL),(1252,'Query.To','es','to'),(1253,'message','it',NULL),(1254,'n.VoteUp','en',''),(1255,'HQ.PersonalCredits','fr',NULL),(1256,'HierarchicalIndex.From','es','from'),(1257,'PDF Certificate','es',NULL),(1258,'Most Important Badges','it',NULL),(1259,'View','en',''),(1260,'close','es',NULL),(1261,'Language','it',NULL),(1262,'Your last topics','es','Sus Ãºltimos temas'),(1263,'contactoid','es',NULL),(1264,'JobStatus.TriggerNextFireTimestamp','fr','Next Fire Timestamp'),(1265,'CompareValidationRule.gteq','fr','<li>the entered value should be greater than or equal to ${value}</li>'),(1266,'HQ.YourProfile.Student','en','Student'),(1268,'Scroller.First','es','first'),(1269,'YouGotReviewStore','it','Hai ottenuto punti per la review'),(1270,'shortDescription','es',NULL),(1271,'City','es',NULL),(1272,'Your last readings','es','Sus Ãºltimas lecturas'),(1273,'Manage Area','es',''),(1274,'Go to monthly','en','Go to Leaderboard'),(1275,'YouGotShareFacebook','it','Hai ottenuto punti per aver condiviso su Facebook'),(1276,'id','fr',NULL),(1277,'n.User total overall','es',''),(1278,'BooleanValidation','es','<li>the entered value must be a boolean (${pattern}) </li>'),(1279,'Forum Badge Title','fr',NULL),(1280,'Resource Type','es',NULL),(1281,'fileblob','it',NULL),(1283,'Administration','fr',NULL),(1284,'Last Events','fr',NULL),(1285,'question','es',NULL),(1286,'Version','it',NULL),(1287,'serverTimestamp','es',NULL),(1288,'Authorization','es',NULL),(1289,'HQ.UsersList.FilterLevel','es','Filtrar por Nivel mÃ­nimo'),(1290,'List of Areas','it',''),(1291,'PowerIndex.Previous','fr','previous'),(1292,'type','fr',NULL),(1293,'templatesblob','es',NULL),(1294,'Certification Level','en',''),(1295,'YouGotReviewStore','fr',NULL),(1296,'Level','en','Level'),(1297,'oidquestion','it',''),(1298,'Installation','en',''),(1301,'Areatomail','es',''),(1302,'role','fr',NULL),(1304,'Link95','it',''),(1305,'all actions','en',''),(1306,'Areatomail','it',''),(1307,'CompareValidationRule.gt.field','en','<li>the entered value should be greater than the value of ${otherFieldName} field</li>'),(1308,'Query.From','fr','from'),(1309,'Participation Company','fr',NULL),(1310,'HQ.slider.error','es',NULL),(1311,'NoWorkItemsFound','es',NULL),(1312,'Search bar','fr',NULL),(1313,'Check private','it',''),(1314,'HQ.store.authorBy','it',''),(1315,'Newsletter','it',''),(1316,'oidtag','it',''),(1317,'Query.Previous','fr','previous'),(1318,'Public profile','fr',NULL),(1319,'Area of Certificate','en',''),(1320,'disabled','es',NULL),(1321,'Query.To','it','a'),(1322,'Profile','fr',NULL),(1323,'custom description','it',''),(1324,'Badge Type','es',NULL),(1325,'sampleProject','en',''),(1326,'selectionfield.world','es',''),(1327,'Sort Badges Area','fr',NULL),(1328,'reissues','en',''),(1329,'Monthly Message','fr',NULL),(1330,'Password','en','Password'),(1331,'voteCount','it',''),(1332,'Scroller.First','it','primo'),(1333,'Score','it',''),(1334,'Level Badge','en',''),(1335,'Latest Badge','en','Latest Badges'),(1336,'tempThumbnailblob','es',NULL),(1337,'alias','it',''),(1338,'n.User total overall','en',''),(1339,'tempScreenshotsblob','fr',NULL),(1340,'HierarchicalIndex.NoItemsShownOf','it','Nessun elemento visualizzato su'),(1341,'All Reputation Actions','fr',NULL),(1342,'Action Area','fr',NULL),(1343,'tempThumbnailblob','en',NULL),(1344,'Previous Activity','es',NULL),(1345,'Level of Certificate','fr',NULL),(1346,'YouGotLogin','fr',NULL),(1347,'PowerIndex.Of','fr','of'),(1348,'Phone Number','it',''),(1349,'username','en','Username'),(1350,'selectionfield.noselection','fr','No selection'),(1351,'oid2','en',NULL),(1352,'HQ.YourProfile.Professional','en','Professional'),(1353,'Link49','it',''),(1354,'DecimalValidation','es','<li>the entered value must be a number</li>'),(1355,'Go to monthly','it','Vai alla Classifica Generale'),(1356,'oidtag','fr',NULL),(1357,'N/D','fr',NULL),(1358,'Query.NoItemsShownOf','es','no items shown of'),(1359,'Group','es',NULL),(1360,'YouGotVoteStore','it','Hai ottenuto per aver messo \"Mi Piace\"'),(1361,'Last Name','es',NULL),(1362,'Monthly Partecipation User','it',''),(1363,'time','fr',NULL),(1364,'description','es',NULL),(1365,'All badges','fr',NULL),(1366,'oid2','fr',NULL),(1367,'searchUser','es','Filtrar por nombre, empresa o universidad'),(1368,'Tags','fr',NULL),(1369,'Chunks','it',''),(1370,'Certified','en',NULL),(1371,'YouGotCoursePassed','fr',NULL),(1372,'Activationtable','es',''),(1373,'objectId','es',NULL),(1374,'Certifications history','fr',NULL),(1375,'HQ.UserDashboard ','en','User Dashboard '),(1376,'Go to monthly','fr',NULL),(1377,'PDF Certificate','fr',NULL),(1378,'Oid','es',NULL),(1379,'serialNumberoid','it',''),(1380,'Delete All','en',NULL),(1381,'Certification Dictionary','es',NULL),(1382,'Modify Level','fr',NULL),(1383,'Order','es',NULL),(1384,'Green Check','fr',NULL),(1385,'HQ.ErrorLogin','it',''),(1387,'Phone Number','en',NULL),(1388,'confirmDeletion','en',NULL),(1389,'Address','it',''),(1390,'HQ.rank.overall','es','Clasifica General'),(1391,'oidquestion','es',NULL),(1392,'HQ.TotalCredits','it','Punti totali'),(1393,'Type of Action','it',''),(1394,'confirmCancelProcess','it',''),(1395,'CompareValidationRule.gteq','es','<li>the entered value should be greater than or equal to ${value}</li>'),(1396,'error.validation.exception','fr','<li>The following exception occurred during form validation: {0}</li>'),(1397,'Logo','fr',NULL),(1398,'HQ.UsersList.Search','fr',NULL),(1399,'CollectionValidationRule.in.query','fr','<li>the entered value is not included in the list of valid values</li>'),(1400,'Process.metadata.updated','it',''),(1401,'ValueLengthValidationRule.neq','fr','<li>the entered value length must be different from ${length} characters</li>'),(1402,'HQ.ParticipationTab','fr',NULL),(1403,'HQ.PersonalBadges','en','Personal Badges'),(1404,'YouGotActivingClient','it','Hai impostato il profilo partner'),(1405,'itemId','es',NULL),(1406,'averageVote','fr',NULL),(1407,'user','es',NULL),(1408,'HQ.UserDashboard ','it','Dashboard Utente'),(1409,'Manage Level','es',''),(1411,'KB Badge Title','fr',NULL),(1412,'Rating','es',NULL),(1413,'Type of Badge','es',''),(1414,'PowerIndex.To','fr','to'),(1417,'tempTags','es',NULL),(1418,'ordinal','en',NULL),(1419,'levelsort','en',''),(1420,'XsdTypeValidationRule.mandatory','en','<li>mandatory field</li>'),(1421,'SmallPhoto','it',''),(1422,'Partecipation down','es',NULL),(1423,'noItemsFound','en',NULL),(1424,'contactoid','it',''),(1425,'HQ.YourProfile.Professor','it',''),(1426,'Process.metadata.invalid','en',NULL),(1427,'Community Participation','en',''),(1428,'ratingtag','en',NULL),(1429,'JobStatus.TriggerDescription','it','Description'),(1430,'HierarchicalIndex.Jump','fr','jump to'),(1432,'Mandatory6','fr','Insert the importance'),(1433,'Mandatory5','fr','Insert a needed score'),(1434,'Mandatory4','fr','Insert a title'),(1435,'Mandatory3','fr','Choose an area'),(1438,'Rating','fr',NULL),(1439,'LikeValidationRule.startsWith','es','<li>the entered value should begin with ${value}</li>'),(1440,'Green Check','es',NULL),(1441,'Certification Badge Title','fr',NULL),(1442,'Position','es',NULL),(1443,'by','fr',NULL),(1444,'downloadCount','en','Download'),(1445,'FormNotEmptyValidationRule.error','en','<li>at least one field must be filled</li>'),(1446,'Certified','fr',NULL),(1447,'Query.Of','it','di'),(1448,'HQ.PersonalBadges','es',NULL),(1449,'Score','fr',NULL),(1450,'tempTags','en',NULL),(1451,'at','fr',NULL),(1452,'time','it',''),(1453,'companycc','en',''),(1454,'Scroller.NoItemsShownOf','es','no items shown of'),(1455,'CompareValidationRule.neq','it','<li>il valore inserito deve essere diverso da ${value}</li>'),(1456,'questiondate','es',NULL),(1457,'store','es',NULL),(1458,'plugins','fr',NULL),(1459,'XsdTypeValidationRule.fraction.digits','en','<li>the entered value must have at most ${digits} fraction digits</li>'),(1460,'tempName','fr',NULL),(1461,'MandatoryIfTrue.error','en',''),(1462,'userPartner','en','Partner'),(1463,'longDescription','it',''),(1464,'expirationDate','es',NULL),(1465,'HQ.rank.monthly','es','Top users'),(1466,'descriptionTmp','fr',NULL),(1467,'Review','it',''),(1468,'editionoid','es',NULL),(1469,'Newsletter','en',''),(1470,'Position','en','Go to Leaderboard'),(1471,'title','it',''),(1472,'Previous Activity','it',NULL),(1473,'HQ.rank.monthly','en','Top Users'),(1475,'Title','en',''),(1476,'TimeValidation','en','<li>the entered value should be in the format ${pattern}</li>'),(1477,'Process.metadata.outdated','es',NULL),(1479,'Store Level','it',NULL),(1480,'Version','en',''),(1481,'User monthly position','en',''),(1482,'CollectionValidationRule.notIn.query','en','<li>the entered value is included in the list of forbidden values</li>'),(1483,'Query.From','es','from'),(1484,'KB Badge','it','Livello Learn'),(1485,'Course Title','fr',NULL),(1486,'HQ.is','en','is'),(1487,'User last topics','es','Temas de usuario'),(1488,'Monthly Partecipation up','it',''),(1489,'fileblob','fr',NULL),(1491,'latest-badges','it','Ultimi Badges'),(1492,'HQ.GoToMyPosition','fr',NULL),(1493,'FoundationYear','en',''),(1494,'Store','es',NULL),(1497,'Store Level','es','Nivel Store'),(1498,'YouGotShareGooglePlus','en','You got points because you shared on Google+ '),(1500,'CompareValidationRule.gteq','en','<li>the entered value should be greater than or equal to ${value}</li>'),(1501,'New Area','it',NULL),(1502,'Activationtable','fr',NULL),(1503,'Email','es',NULL),(1504,'HQ.Answer','es',NULL),(1505,'XsdTypeValidationRule.max.exclusive','fr','<li>the entered value should be smaller than ${value}</li>'),(1507,'HD Image','en',''),(1508,'disabled','en',''),(1509,'Positions','it',NULL),(1510,'rejectionReason','fr',NULL),(1511,'Bio','it','Biografia'),(1512,'Public','it',NULL),(1513,'HQ.NicknameNull','es','Usted no se ha definido ningun nickname, para ver su perfil pÃºblico volver atrÃ¡s y editar sus datos.'),(1514,'HQ.store.authorBy','fr',NULL),(1515,'YouGotAnswerTopic','en','You got points for your answer at question'),(1516,'fromPortal','en',''),(1517,'First Name','fr',NULL),(1518,'Process.metadata.outdated','fr',NULL),(1519,'levelCertificate','fr',NULL),(1520,'First Name','en','First Name'),(1521,'Vat Number','es',NULL),(1522,'YouGotQuestionVoteup','es','Tienes puntos porque su pregunta fue votada hasta'),(1523,'Mobile','fr',NULL),(1524,'Positions','es',NULL),(1525,'Scroller.Of','it','di'),(1526,'CompareValidationRule.eq','fr','<li>the entered value should be equal to ${value}</li>'),(1527,'city','es',NULL),(1528,'Link145','fr',NULL),(1530,'YouGotDownloadStore','en','You got points to have downloaded '),(1531,'thumbnailblob','it',NULL),(1532,'Query.Of','fr','of'),(1533,'answerdate','fr',NULL),(1534,'Modify Badge Level','es',''),(1535,'KB Action','fr',NULL),(1536,'Level','it','Livello'),(1537,'configuration','it',NULL),(1538,'datamodified','fr',NULL),(1540,'All badges','es','Ver el Ranking completo '),(1541,'tag','es',NULL),(1542,'Postal Code','it',NULL),(1543,'Authorization','fr',NULL),(1544,'Welcome','en','Welcome'),(1545,'imageChecked','es',NULL),(1546,'HQ.UsersList.FilterArea','fr',NULL),(1547,'Rank Participation Monthly','en',''),(1548,'YouGotShareTwitter','fr',NULL),(1549,'NotPublicProfile.Private','it',NULL),(1550,'XsdTypeValidationRule.max.length','fr','<li>the entered value length must be at most ${length} characters</li>'),(1551,'Student','it',NULL),(1552,'HQ.YourProfile.Student','fr',NULL),(1553,'level','es','nivel'),(1554,'Email','en',''),(1555,'List of Actions','it',NULL),(1556,'KB Badge Title','en',''),(1557,'Query.Next','en','next'),(1558,'HQ.NotPublicProfile','es',''),(1559,'Forum Level','fr',NULL),(1560,'LoginHQ','es',NULL),(1562,'CompareValidationRule.eq.field','fr','<li>the entered value should be equal to the value of ${otherFieldName} field</li>'),(1563,'Partecipation up','en',''),(1564,'tags','en',''),(1565,'Newsletter','fr',NULL),(1566,'User last topics','it','Ultime Domande utente'),(1567,'HQ.YourProfile.Public','es','Su perfil es pÃºblico'),(1568,'Pdf','es',NULL),(1569,'BooleanValidation','en','<li>the entered value must be a boolean (${pattern}) </li>'),(1570,'Company','it',NULL),(1571,'sortOrder','fr',NULL),(1572,'Postal Code','es',NULL),(1573,'Company Type','es',NULL),(1574,'userPartnerCert','en','Partner certified'),(1575,'Flow64','fr',NULL),(1576,'oidtag','es',NULL),(1577,'Flow65','fr',NULL),(1578,'LikeValidationRule.notEndsWith.field','en','<li>the entered value should not end with the value of ${otherFieldName} field</li>'),(1579,'error.stacktrace','en','{0}'),(1580,'PowerIndex.Previous','es','previous'),(1581,'HQ.store.update','en','Updated'),(1582,'HQ.NotFromSite','fr',NULL),(1583,'XsdTypeValidationRule.positive.value','fr','<li>Must be a positive value</li>'),(1584,'errors.header','fr','<ul>'),(1585,'ManagementEvent','fr',NULL),(1586,'role','en',''),(1587,'Date','es',NULL),(1588,'Forum Level','en','Forum Level'),(1589,'userPartnerCert','it',NULL),(1590,'PDF Certificate','it',NULL),(1591,'companypartner','fr',NULL),(1592,'Back','fr',NULL),(1593,'Pdf','it',NULL),(1594,'XsdTypeValidationRule.max.length','it','<li>il valore deve essere lungo al massimo ${length} caratteri</li>'),(1595,'My certificate','fr',NULL),(1596,'Administration','en',''),(1597,'searchUser','it','Filtra per nome, azienda o università'),(1598,'Certification','it','Certificazioni'),(1599,'HQ.YourProfile.Work','fr',NULL),(1600,'History certificate','it','Certificazioni acquisite'),(1601,'CompareValidationRule.neq','en','<li>the entered value should be different from ${value}</li>'),(1602,'close','en',''),(1603,'Flow42','fr',NULL),(1605,'Flow40','fr',NULL),(1606,'Flow41','fr',NULL),(1607,'Select a region','fr',NULL),(1608,'Tag','en',''),(1609,'XsdTypeValidationRule.invalid.integer','es','<li>Invalid Integer</li>'),(1611,'Badge Instance','es',NULL),(1613,'Flow46','fr',NULL),(1616,'Link11','it',NULL),(1617,'Serialtable','en',''),(1618,'CompareValidationRule.gt','fr','<li>the entered value should be greater than ${value}</li>'),(1619,'of','fr',NULL),(1621,'ActionForm','es',''),(1622,'lastModified','fr',NULL),(1623,'sortByLevel','it',NULL),(1624,'Badge:','en',''),(1625,'Logo','es',''),(1626,'KB Action','en',''),(1627,'message','es',NULL),(1628,'WROWNERID','it',NULL),(1629,'HQ.YourProfile.Work','it','Lavora'),(1630,'Add Badge','en',''),(1631,'tag','en',''),(1632,'Student','en',''),(1633,'Flow38','fr',NULL),(1634,'level','en',''),(1635,'Flow37','fr',NULL),(1636,'HierarchicalIndex.Next','fr','next'),(1637,'Flow39','fr',NULL),(1638,'Link24','it',NULL),(1639,'Password','es',NULL),(1640,'Department','en',''),(1641,'postedAnswer','en','posted an answer'),(1642,'List of Levels','fr',NULL),(1643,'User monthly position','it',''),(1644,'Scroller.NoItemsShownOf','fr','no items shown of'),(1645,'YouGotAnswerVoteup','en','You got points because your answer was voted up'),(1646,'Participation Company','it',''),(1647,'NotPublicProfile.Private','en','Private Profile'),(1648,'useridentifier','it',''),(1649,'ratingtag','it',NULL),(1650,'Text Chunk','es',NULL),(1651,'Update','en',''),(1652,'MaxLevel','es',''),(1653,'XsdTypeValidationRule.max.inclusive','en','<li>the entered value should be smaller than or equal to ${value}</li>'),(1654,'CertificateFinded','it',NULL),(1655,'Public profile','en',''),(1656,'YouGotAnswerApproved','it','Hai ottenuto punti per l\'approvazione della risposta'),(1657,'version','en',''),(1658,'Scroller.Last','fr','last'),(1659,'tempThumbnail','fr',NULL),(1660,'List of Badges','it',NULL),(1661,'deactivationDate','en',''),(1662,'Monthly Form','it',''),(1663,'XsdTypeValidationRule.invalid.float','es','<li>Invalid Float</li>'),(1664,'congratulations','fr',NULL),(1665,'CompareValidationRule.lt','fr','<li>the entered value should be smaller than ${value}</li>'),(1666,'HQ.YourProfile.PrintPdf','en','Download your certificate'),(1667,'Timestamp','en',''),(1668,'Badge Instance','it',NULL),(1669,'User monthly position','es',''),(1670,'HQ.YourProfile.Student','it',NULL),(1671,'type','en',''),(1672,'Last Events','es',NULL),(1673,'Email','it',NULL),(1674,'Text Chunk','it',NULL),(1675,'Edit labels','es',NULL),(1676,'serverTimestamp','fr',NULL),(1677,'go to homepage','es','Homepage'),(1678,'HQ.NoForumActivity','es','No Forum actividades '),(1679,'XsdTypeValidationRule.negative.value','it','<li>Deve essere un valore intero negativo</li>'),(1680,'latest-badges','fr',NULL),(1681,'shortDescription','fr',NULL),(1682,'activationDate','es',''),(1683,'congratulations','es',NULL),(1684,'Certifications','fr',NULL),(1685,'Filter by badge type','it','Filtra per tipo di badge'),(1686,'Search','en','Search'),(1687,'PowerIndex.Next','en','next'),(1688,'Position','fr',NULL),(1689,'Rank Participation Monthly','fr',NULL),(1690,'Edit Text','it',NULL),(1691,'minVersion','es',NULL),(1692,'CheckedItemsValidationRule.min','es','<li>check at least ${itemCount} items</li>'),(1693,'Filter by region','it','Filtra per regione'),(1694,'XsdTypeValidationRule.invalid.enum','en','<li>Invalid enumeration value (${values})</li>'),(1695,'Query.Last','en','last'),(1696,'Clear','en','Clear'),(1697,'LikeValidationRule.contains.field','es','<li>the entered value should contain the value of ${otherFieldName} field</li>'),(1698,'Link11','fr',NULL),(1699,'List of Badges','fr',NULL),(1700,'by','es',NULL),(1701,'Order','it',NULL),(1702,'userId','en',NULL),(1703,'visibility','es',NULL),(1704,'Select a region','en','Select a country'),(1705,'Monthly Participation','en',''),(1706,'Manage Action','fr',NULL),(1707,'MandatoryIfTrue.error','fr',NULL),(1708,'Geographical Area','en','Geographical Area'),(1709,'releaseType','fr',NULL),(1710,'HQ.Message','fr',NULL),(1711,'Link24','fr',NULL),(1712,'Locale','en',NULL),(1713,'CompareValidationRule.neq.field','en','<li>the entered value should be different from the value of ${otherFieldName} field</li>'),(1714,'MaxLevel','en',''),(1715,'Edit Text','es',NULL),(1716,'HQ.Back','en','Back'),(1717,'HQ.MyDashboard ','it','My Dashboard'),(1718,'sendEmail','fr',NULL),(1719,'Certifications','en','Certifications'),(1720,'CompareValidationRule.lteq.field','fr','<li>the entered value should be smaller than or equal to the value of ${otherFieldName} field</li>'),(1721,'ValueLengthValidationRule.max','fr','<li>the entered value length must be at most ${length} characters</li>'),(1722,'deactivationDate','es',NULL),(1723,'HQ.ReputationTab','en','Reputation'),(1724,'n.User total overall','it',''),(1725,'notNotesFound','fr',NULL),(1726,'Badge Type','fr',NULL),(1727,'HQ.rank.monthly','fr',NULL),(1728,'XsdTypeValidationRule.invalid.pattern','it','<li>formato invalido (${pattern})</li>'),(1729,'Tags','it',NULL),(1730,'error.security','en','A security exception occurred: {0}'),(1731,'HQ.YouAre','fr',NULL),(1732,'XsdTypeValidationRule.invalid.time','fr','<li>Invalid Time</li>'),(1733,'Manage Level','it',''),(1734,'HierarchicalIndex.From','fr','from'),(1735,'New Area','en',''),(1736,'Generic Message','fr',NULL),(1737,'shortDescription','en',NULL),(1738,'Link49','fr',NULL),(1739,'Store Item','it',NULL),(1740,'status','es',NULL),(1741,'Certification Score','fr',NULL),(1742,'Modify Badge Area','it',NULL),(1743,'Monthly Message','en',''),(1744,'city','it',NULL),(1745,'HQ.UsersList.Search','en','Search'),(1746,'Checked Image','it',NULL),(1747,'Certification Dictionary','it',''),(1748,'LikeValidationRule.endsWith.field','es','<li>the entered value should end with the value of ${otherFieldName} field</li>'),(1749,'User last readings','en','User last readings'),(1750,'CompareValidationRule.lt.field','it','<li>il valore inserito deve essere minore al valore del campo ${otherFieldName}</li>'),(1751,'encodedtitle','es',NULL),(1752,'SecondMenuLevel','it',NULL),(1753,'TimeValidation','es','<li>the entered value should be in the format ${pattern}</li>'),(1754,'HQ.UserDashboard.AllActions','fr',NULL),(1755,'HQ.RankCompanyNoResults','fr',NULL),(1756,'PowerIndex.NoItemsShownOf','it','Nessun elemento visualizzato su'),(1757,'HQ.Back','it',NULL),(1758,'Webratio Activation','es',NULL),(1759,'LikeValidationRule.startsWith','en','<li>the entered value should begin with ${value}</li>'),(1760,'CertificateFinded','es',NULL),(1761,'LikeValidationRule.notEndsWith','fr','<li>the entered value should not end with ${value}</li>'),(1762,'Update Badge','fr',NULL),(1763,'at','es',NULL),(1765,'LikeValidationRule.notStartsWith.field','fr','<li>the entered value should not begin with the value of ${otherFieldName} field</li>'),(1766,'Time','it',NULL),(1767,'LowLevel','es',''),(1768,'Logo','en',''),(1769,'duration','fr',NULL),(1770,'Back','es',NULL),(1771,'HQ.UsersList.SearchField','en','Search User:'),(1772,'selectionfield.world','it',NULL),(1773,'TagStore','it',NULL),(1774,'HQ.UsersList.Users','en','Leaderboard'),(1775,'My certificate','en',''),(1776,'Level of Certificate','es',''),(1777,'description','en',''),(1778,'HQ.MyDashboard ','en','My Dashboard'),(1779,'Partecipation down','it',NULL),(1780,'HQ.store.download','es',NULL),(1781,'HQ.UsersList.SearchField','es','Buscar Usuario:'),(1782,'Checked Image','en',''),(1783,'Certification Dictionary','en',''),(1784,'Choose the Display Sorting','en',''),(1785,'fromPortal','fr',NULL),(1786,'version','es',NULL),(1787,'HQ.UsersList.Search','es','Buscar'),(1788,'LikeValidationRule.startsWith','it','<li>il valore inserito deve iniziare con ${value}</li>'),(1789,'HierarchicalIndex.Last','it','ultimo'),(1790,'YouGotPublishStore','it','Hai ottenuto punti per la pubblicazione'),(1791,'releaseType','es',NULL),(1792,'HQ.slider.error','en',''),(1793,'tempIconblob','it',NULL),(1794,'QueryUnit.noDiplayAtrributes','en','No selected display attributes'),(1795,'question','it',NULL),(1796,'screenshotsblob','es',NULL),(1797,'History Certificates','it','Certificazioni acquisite'),(1798,'Scroller.First','en','first'),(1799,'WebRatio Certification','en',''),(1801,'Update Badge','it',NULL),(1802,'HQ.TotalCredits','en','Total points'),(1803,'userId','es',NULL),(1804,'HQ.UserOf','fr',NULL),(1805,'HQ.Credits','fr',NULL),(1806,'Badge:','es',NULL),(1807,'Region','it',NULL),(1808,'PowerIndex.Of','it','di'),(1809,'Monthly Partecipation down','en',''),(1810,'ModuleID','it',NULL),(1811,'CheckedItemsValidationRule.max','es','<li>check at most ${itemCount} items</li>'),(1812,'XsdTypeValidationRule.invalid.boolean','es','<li>Invalid Boolean</li>'),(1813,'Order','fr',NULL),(1814,'LikeValidationRule.contains','en','<li>the entered value should contain ${value}</li>'),(1815,'FormNotEmptyValidationRule.error','it','<li>almeno un campo deve essere riempito</li>'),(1816,'XsdTypeValidationRule.min.length','es','<li>the entered value length must be at least ${length} characters</li>'),(1817,'Show Pdf','en',''),(1818,'YouGotApprovedAnswer','fr',NULL),(1819,'Creation Date','fr',NULL),(1821,'moduleDomainName','en',''),(1822,'disabled','fr',NULL),(1823,'Certified','es',NULL),(1824,'All badges','it','Vai alla Classifica Completa'),(1825,'CheckedItemsValidationRule.eq','it','<li>${itemCount} elementi selezionati richiesti</li>'),(1826,'go-to-store','it','Vai allo Store'),(1827,'oid','en',''),(1828,'HQ.ManageProfile','it',''),(1829,'JobStatus.TriggerId','es','ID'),(1830,'Participation','es','ParticipaciÃ³n'),(1831,'Modify Badge Area','fr',NULL),(1832,'error.check.eqChecked','en','<li>{1} items checked required</li>'),(1833,'Region','fr',NULL),(1834,'Create','en',''),(1835,'go-to-lms','es','Ir al Learn'),(1836,'Getting Started','es','GuÃ­a de Usuario'),(1837,'Version','fr',NULL),(1838,'oid','fr',NULL),(1839,'PowerIndex.To','it','a'),(1840,'HQ.no.badges','it',NULL),(1841,'Department','it',NULL),(1842,'HQ.rank.overall','en','Overall Leaderboard'),(1843,'BigPhoto','fr',NULL),(1844,'Store','fr',NULL),(1845,'Locale','fr',NULL),(1846,'Position','it',NULL),(1847,'Set the Icons','es',NULL),(1848,'History Certificates','es',''),(1849,'HQ.YouAre','it','Sei'),(1851,'HQ.YourActions','fr',NULL),(1852,'Community User','es',NULL),(1853,'HQ.no.certificates','it',NULL),(1854,'Article','es',NULL),(1855,'Add Area','en',''),(1856,'Oid','fr',NULL),(1857,'Company Name','en','Company Name'),(1858,'QueryUnit.wrongGrouping','es','All the displayed attributes must be used as grouping or aggregate function'),(1859,'History Diagram','es',''),(1861,'Password','fr',NULL),(1862,'users','it','utenti'),(1863,'HQ.RankCompanyNoResults','en','No companies in this rank with this properties'),(1864,'CollectionValidationRule.notIn.query','es','<li>the entered value is included in the list of forbidden values</li>'),(1865,'Last Badge','en',''),(1866,'type','it',NULL),(1867,'value','es',NULL),(1868,'JobStatus.JobId','it','ID'),(1869,'Labels','es',NULL),(1870,'date','es',NULL),(1871,'name','it',NULL),(1872,'error.empty','it','{0}'),(1873,'time','en',''),(1874,'HQ.NotPublicProfile','it',''),(1875,'Go to participation','fr',NULL),(1876,'Countrytable','it',NULL),(1877,'visibility','it',NULL),(1878,'LikeValidationRule.notContains.field','it','<li>il valore inserito non deve contenere il valore del campo ${otherFieldName}</li>'),(1879,'levelsort','fr',NULL),(1880,'YouGotActivingClient','es','Usted setted perfil de socio'),(1881,'Group','en',''),(1882,'ActivityInstanceNotAvailable','en',''),(1883,'Common Data','en',''),(1884,'expirationDate','it',NULL),(1886,'answer','it',NULL),(1887,'ajax.computingRequest','fr','Computing request..'),(1888,'Query.Previous','it','precedente'),(1890,'HQ.UsersList.GoTo','en','List of Users'),(1891,'updateTimestamp','en',''),(1892,'XsdTypeValidationRule.invalid.float','it','<li>Numero non valido</li>'),(1893,'Webratio Activation','it',NULL),(1894,'PowerIndex.Jump','fr','jump to'),(1895,'YouGotShareGooglePlus','fr',NULL),(1896,'n.Answer','fr',NULL),(1897,'History badges','es',''),(1898,'YouGotQuestionVoteup','it','Hai ottenuto punti per il voto alla tua domanda'),(1899,'HierarchicalIndex.First','en','first'),(1900,'WROWNERID','fr',NULL),(1901,'HQ.Answer','fr',NULL),(1902,'RegularExpressionValidationRule.error','en','<li>the entered value is not valid</li>'),(1903,'DatesCertificates','en',''),(1904,'CompareValidationRule.lteq','it','<li>il valore inserito deve essere minore o uguale di ${value}</li>'),(1905,'Date','en','Date'),(1906,'NotPublicProfile.Private','fr',NULL),(1907,'WebSite','it','Sito Web'),(1908,'Type of Action','fr',NULL),(1909,'title','en',''),(1910,'thumbnail','en',''),(1911,'HQ.PersonalBadges','it',NULL),(1912,'Nickname','es',NULL),(1915,'CompareValidationRule.neq.field','es','<li>the entered value should be different from the value of ${otherFieldName} field</li>'),(1916,'n.Answer','it',NULL),(1917,'Contact','fr',NULL),(1918,'Rank Participation Overall','en',''),(1919,'Modify Badge Level','fr',NULL),(1920,'CreditCardValidationRule.error','en','<li>invalid credit card number</li>'),(1921,'List of Areas','fr',NULL),(1922,'areasort','es',''),(1923,'Resource Type','it',NULL),(1924,'ActionForm','fr',NULL),(1925,'HQ.YourProfile.Public','fr',NULL),(1926,'HQ.YourLastAction','fr',NULL),(1927,'visibility','en',''),(1928,'New Badge','es',NULL),(1929,'All Reputation Actions','es',''),(1930,'YouGotApproveAnswer','en','You got points because you have approved the answer'),(1931,'Date','it',NULL),(1932,'HQ.store.download','fr',NULL),(1933,'reviewCount','es',NULL),(1934,'of','es','de'),(1936,'User latest','it',''),(1937,'CreditCardValidationRule.error','es','<li>invalid credit card number</li>'),(1938,'Creation Date','es',NULL),(1939,'Latest topics','en','Latest Topics'),(1940,'Labels Text','it',NULL),(1941,'invioEffettuato','it',NULL),(1942,'Language','es',NULL),(1943,'articleOid','en',''),(1945,'link','en',''),(1946,'Certified','it',NULL),(1947,'lastVersionUpdateTimestamp','es',NULL),(1948,'Scroller.Next','en','next'),(1949,'Check private','es',NULL),(1950,'document','it',NULL),(1952,'description (search engine)','it','Cerca'),(1953,'oidquestion','en',''),(1954,'Last Events','it',''),(1955,'lms','es','LMS'),(1956,'tempScreenshots','it',NULL),(1957,'XsdTypeValidationRule.max.length','en','<li>the entered value length must be at most ${length} characters</li>'),(1958,'XsdTypeValidationRule.total.digits','it','<li>il valore deve avere al massimo ${digits} cifre</li>'),(1959,'screenshotsblob','fr',NULL),(1960,'Most Important Badges','en',''),(1961,'Address','en',''),(1963,'Time','en',''),(1964,'LoginHQ','en',''),(1965,'Your last downloads','it','Ultimi tuoi download'),(1966,'Latest Published Learning Objects','it','Nuovi Materiali'),(1968,'tempTags','it',NULL),(1969,'Store Badge','es',NULL),(1970,'HQ.YourProfile.Work','en','Works'),(1971,'Number Of Employees','it',NULL),(1972,'XsdTypeValidationRule.invalid.timestamp','it','<li>Timestamp non valido</li>'),(1973,'HQ.GoToMyPosition','it','Vai alla Mia Posizione'),(1974,'Overall Participation','en',''),(1975,'Action Instance','es',''),(1976,'Participation Company','en',''),(1977,'sampleProjectblob','it',NULL),(1978,'Forum Badge Title','en',''),(1979,'ratingtag','es',NULL),(1980,'YouGotVoteStore','fr',NULL),(1981,'Forum Actions','en',''),(1982,'HQ.YourProfile.Student','es',''),(1983,'Area of Site','it',''),(1987,'Link1','it',NULL),(1988,'History badges','fr',NULL),(1989,'Add level','es',''),(1990,'n.Answer','en',''),(1991,'Link5','it',NULL),(1992,'articleoid','en',''),(1993,'noProcessInstancesFound','fr',NULL),(1994,'NoWorkItemsFound','en',''),(1996,'HQ.UsersList.Search','it','Cerca'),(1998,'HQ.YourProfile.Certificate','en',''),(1999,'tags','it',NULL),(2000,'HQ.is','es','es'),(2001,'Oid','it',NULL),(2002,'ValueLengthValidationRule.min','fr','<li>the entered value length must be at least ${length} characters</li>'),(2003,'Certification Badge Title','en',''),(2004,'Question','es',NULL),(2005,'HQ.PersonalCertificates','fr',NULL),(2006,'XsdTypeValidationRule.min.exclusive','en','<li>the entered value should be greater than ${value}</li>'),(2008,'YouGotReadingWiki','en','You got points because you have read'),(2009,'HQ.View','es',NULL),(2010,'UserName','it',''),(2011,'HQ.UsersList.Users','fr',NULL),(2012,'MaxLevel','fr',NULL),(2013,'lastModified','it',''),(2014,'getting-start','fr',NULL),(2015,'int','it','Only Integer Number'),(2016,'Involved Actions Associated','en',''),(2017,'Image','it',NULL),(2018,'HQ.UsersList.GoTo','es','Lista Usuarios'),(2019,'Role','fr',NULL),(2020,'Show Activity Details','en',''),(2021,'TimestampValidation','en','<li>the entered value should be in the format ${pattern}</li>'),(2022,'IntegerValidation','it','<li>il valore inserito deve essere di tipo intero</li>'),(2023,'value','fr',NULL),(2024,'XsdTypeValidationRule.invalid.integer','it','<li>Intero non valido</li>'),(2025,'HQ.YourProfile.NoDeveloper','it',''),(2026,'CertificateForm','es',NULL),(2027,'tempIcon','es',NULL),(2028,'Filter by region','fr',NULL),(2029,'confirmNoteDeletion','fr',NULL),(2030,'Manage actions','fr',NULL),(2031,'status','en',''),(2032,'N/D','es',NULL),(2033,'TimestampValidation','it','<li>il valore inserito deve essere nel formato ${pattern}</li>'),(2034,'releaseNotes','en',''),(2035,'notAttachmentsFound','fr',NULL),(2036,'SmallPhoto','es',NULL),(2037,'`key`words (search engine)','it','Cerca'),(2038,'noActionBadge','fr',NULL),(2039,'sendEmail','es',NULL),(2040,'Private Message','it',NULL),(2041,'templatesblob','en',''),(2042,'TypeValidationRule.error','fr','<li>the entered value should have the format ${pattern}</li>'),(2043,'Update','es',NULL),(2044,'noProcessesFound','fr',NULL),(2045,'Store','en','Store'),(2046,'sortByLevel','fr',NULL),(2047,'version','fr',NULL),(2048,'CompareValidationRule.gt.field','fr','<li>the entered value should be greater than the value of ${otherFieldName} field</li>'),(2049,'Action Area','es',''),(2050,'role','it',NULL),(2051,'articleOid','fr',NULL),(2052,'HQ.NicknameNull','en','You haven\'t set any nickname, to see your public profile go back and edit your data.'),(2053,'Query.First','it','primo'),(2054,'linkConfirmMessage','it','Continuare?'),(2056,'Search bar','it','Cerca'),(2057,'contactoid','fr',NULL),(2058,'Modify Actions','es',NULL),(2059,'Certification Score','en','Certification Score'),(2060,'Internal','es',NULL),(2061,'Latest reading','en','Latest Learning Objects'),(2062,'HQ.UsersMonthly','it','Top Users'),(2063,'YouGotPublishStore','en','You got points because you published'),(2064,'Countrytable','es',NULL),(2065,'User latest','fr',NULL),(2066,'CreditCardValidationRule.error','fr','<li>invalid credit card number</li>'),(2067,'address','fr',NULL),(2068,'Disable','es',NULL),(2069,'id','es',NULL),(2070,'reviewCount','it',NULL),(2071,'confirmCancelProcess','es',NULL),(2072,'Monthly Partecipation up','en',''),(2073,'Item','it',NULL),(2074,'Dashboard','fr',NULL),(2075,'KB Level','it','Livello Learn'),(2076,'Latest reading','es','Ãšltimos Learning Objects'),(2077,'Importance','en',''),(2078,'HQ.CertificateTab','fr',NULL),(2079,'Tags','en',''),(2080,'Badge Selected','it',NULL),(2082,'Forum Level','it','Livello Forum'),(2083,'document','fr',NULL),(2084,'HierarchicalIndex.Last','es','last'),(2085,'Filter','en',''),(2086,'CaptchaValidationRule.error','es',' <li>the entered value did not match what was displayed</li>'),(2087,'HQ.store.update','it',NULL),(2088,'Modify Badge','es',NULL),(2089,'Areas','es',''),(2090,'Scrool Actions','fr',NULL),(2091,'Hide Activity Details','fr',NULL),(2092,'CompareValidationRule.eq.field','es','<li>the entered value should be equal to the value of ${otherFieldName} field</li>'),(2093,'active','fr',NULL),(2094,'Filter by badge level','es','Filtrar por nivel de badge'),(2095,'close','fr',NULL),(2096,'go-to-store','en','Go to Store'),(2097,'ManagementEvent','en',''),(2098,'YouBecome','en','You become'),(2099,'active','it',''),(2100,'Generic Message','en',''),(2101,'MandatoryValidationRule.error','en','<li>mandatory field</li>'),(2102,'Link5','fr',NULL),(2104,'Link1','fr',NULL),(2105,'certification','en','Certification'),(2106,'Last Badge','es',NULL),(2107,'Your last readings','en','Your last readings'),(2108,'serialNumberoid','fr',NULL),(2109,'licenseServerVersion','it',NULL),(2110,'List of Badges','es',''),(2111,'HQ.RankingOverall','en','Overall Leaderboard'),(2112,'Level Badge','es',''),(2113,'Scrool Actions','it',NULL),(2114,'HierarchicalIndex.Previous','es','previous'),(2115,'Company Certified','es',NULL),(2117,'KB Badge','es',NULL),(2118,'HierarchicalIndex.Of','es','of'),(2119,'multiselectionfield.deselectAll','es','Quitar filtros'),(2120,'CompareValidationRule.lt','es','<li>the entered value should be smaller than ${value}</li>'),(2121,'actionAlreadyInvolved','fr',NULL),(2122,'Create','es',NULL),(2123,'image','es',NULL),(2124,'SecondMenuLevel','es',''),(2125,'YouGotLogin','it','Hai ottenuto punti per aver effettuato l\'accesso'),(2126,'XsdTypeValidationRule.max.inclusive','es','<li>the entered value should be smaller than or equal to ${value}</li>'),(2127,'New Badge','en',''),(2128,'configuration','es',NULL),(2129,'go-to-forum','it','Vai al Forum'),(2130,'datamodified','it',NULL),(2131,'Partecipation User','it',''),(2132,'Scroller.Jump','it','vai a'),(2133,'congratulations','en',''),(2134,'family','it',NULL),(2135,'XsdTypeValidationRule.invalid.enum','fr','<li>Invalid enumeration value (${values})</li>'),(2136,'PowerIndex.NoItemsShownOf','es','no items shown of'),(2137,'Badge Type','en',''),(2138,'CompareValidationRule.eq','es','<li>the entered value should be equal to ${value}</li>'),(2139,'releaseNotes','it',NULL),(2140,'HQ.RankCompanyNoResults','it','Nessuna azienda in classifica con queste proprietà '),(2141,'SortCombination','es',NULL),(2142,'Certification Dictionary','fr',NULL),(2143,'Latest reading','it','Nuovi Materiali'),(2144,'Credit history','fr',NULL),(2145,'Badge Selected','fr',NULL),(2147,'Query.First','es','first'),(2148,'message','en',''),(2149,'date','en',''),(2150,'HQ.ManageProfile','fr',NULL),(2151,'Confirm Finish','it',NULL),(2153,'LikeValidationRule.endsWith','fr','<li>the entered value should end with ${value}</li>'),(2154,'country','it',NULL),(2155,'resourceType','en',''),(2156,'LikeValidationRule.notStartsWith','es','<li>the entered value should not begin with ${value}</li>'),(2157,'MostimportantBadge','en',''),(2158,'CompareValidationRule.gt','es','<li>the entered value should be greater than ${value}</li>'),(2159,'Scroller.Next','es','next'),(2160,'LinkParticipation','fr',NULL),(2161,'Active','it',''),(2162,'JobStatus.JobDescription','es','Description'),(2163,'ValueLengthValidationRule.max','it','<li>il valore inserito deve essere lungo al massimo ${length} caratteri</li>'),(2164,'JobStatus.TriggerId','fr','ID'),(2165,'IntegerValidation','en','<li>the entered value must be an integer</li>'),(2167,'Participation Monthly','it','Partecipazione mensile'),(2168,'HQ.no.certificates','fr',NULL),(2169,'Fax','fr',NULL),(2170,'Store','it',NULL),(2171,'Description','it',NULL),(2172,'WebRatio Certification','fr',NULL),(2173,'Store Item','fr',NULL),(2174,'YouGotActiving','it','Attivazione'),(2175,'YouGotMonthly','it','Hai ottenuto punti mensili'),(2176,'Delete','it',NULL),(2177,'Public','fr',NULL),(2178,'HierarchicalIndex.NoItemsShownOf','fr','no items shown of'),(2179,'languageCode','en',''),(2180,'address','it',NULL),(2181,'postedAnswer','it',NULL),(2182,'CompareValidationRule.lt','en','<li>the entered value should be smaller than ${value}</li>'),(2183,'supportedVersion','it',NULL),(2184,'postalCode','en',''),(2185,'MostimportantBadge','fr',NULL),(2186,'HQ.YourProfile.Certificate','fr',NULL),(2187,'HQ.YourProfile.PrintPdf','it',''),(2188,'activationDate','en',''),(2190,'HQ.UserDashboard.AllActions','es',NULL),(2191,'Manage Badge','it',NULL),(2192,'Name','es',NULL),(2193,'checkCode','en',''),(2194,'nickname','es',NULL),(2195,'Certification Level','fr',NULL),(2196,'error.button.not.pressed','it','<li>Prego premere il bottone di conferma</li>'),(2197,'HQ.YourProfile.NoDeveloper','fr',NULL),(2198,'rejectionReason','en',''),(2199,'RegularExpressionValidationRule.error','fr','<li>the entered value is not valid</li>'),(2200,'errors.header','es','<ul>'),(2201,'Modify Actions','fr',NULL),(2202,'Area of Site','fr',NULL),(2203,'Authorization','en',''),(2204,'Participation','en','Participation'),(2205,'noProcessesFound','es',NULL),(2206,'Add','es',NULL),(2207,'university','it',NULL),(2208,'Teacher','es',NULL),(2209,'HQ.UsersList.GoTo','fr',NULL),(2210,'close','it',NULL),(2213,'Welcome','it','Benvenuto'),(2214,'iconblob','en',''),(2215,'YouGotPostTopic','it','Hai ottenuto punti per aver postato una domanda'),(2216,'Confirm Finish','en',''),(2217,'url','es',NULL),(2218,'Set the Icons','en',''),(2219,'company','fr',NULL),(2220,'CaptchaValidationRule.error','en',' <li>the entered value did not match what was displayed</li>'),(2223,'language','fr',NULL),(2224,'Linkedin','it','LinkedIn'),(2225,'HQ.UserDashboard','en',''),(2226,'Certification Badge','it',''),(2227,'Monthly Message','it',''),(2228,'YouGotPostSubscription','es','Tienes crÃ©ditos por tu suscripciÃ³n'),(2229,'lastVersionUpdateTimestamp','en',''),(2230,'Monthly Form','en',''),(2231,'Password','it','Password'),(2232,'Logout','en','Logout'),(2233,'go-to-forum','es','Ir al Forum'),(2234,'Modify','it',NULL),(2235,'serialNumber3','en',''),(2236,'areasort','it',NULL),(2237,'HierarchicalIndex.To','es','to'),(2238,'licenseServerVersion','fr',NULL),(2240,'New Level','it',NULL),(2241,'Edit labels','en',''),(2242,'getting-start','en','Getting started'),(2243,'ToolVersion','fr',NULL),(2244,'articleoid','es',NULL),(2245,'All badges','en','See Full Leaderboard'),(2246,'Instructions','en',''),(2247,'HQ.UsersList.AnyLevel','en','Clear level filter'),(2248,'Link206','it',NULL),(2249,'Area of Certificate','fr',NULL),(2251,'Serialtable','fr',NULL),(2252,'Area of Certificate','it',''),(2253,'YouBecome','it',NULL),(2254,'PowerIndex.First','fr','first'),(2255,'Logout','fr',NULL),(2256,'Set','en',''),(2257,'name','en',''),(2258,'Add Area','it',''),(2259,'LikeValidationRule.notContains','it','<li>il valore inserito non deve contenere ${value}</li>'),(2260,'postalCode','it',NULL),(2261,'YouGotsetData','es','Usted setted'),(2263,'HD Image','it',NULL),(2264,'Delete','fr',NULL),(2265,'Activationtable','en',''),(2266,'userOid','en',''),(2267,'Question','it',NULL),(2268,'`key`','en',''),(2269,'releaseNotes','es',NULL),(2270,'JobStatus.TriggerDescription','en','Description'),(2271,'Scroller.Previous','fr','previous'),(2272,'Enable','fr',NULL),(2273,'timestamp2','it',NULL),(2274,'CompareValidationRule.lt.field','es','<li>the entered value should be smaller than the value of ${otherFieldName} field</li>'),(2275,'MostimportantBadge','es',NULL),(2276,'CompareValidationRule.gt.field','es','<li>the entered value should be greater than the value of ${otherFieldName} field</li>'),(2277,'YouGotQuestionVoteup','en','You got points because your question was voted up'),(2278,'Internal','fr',NULL),(2280,'Small Photo','fr',NULL),(2281,'custom description','es',NULL),(2282,'Back','en',''),(2283,'duration','es',NULL),(2284,'OID','fr',NULL),(2285,'noActionBadge','en','This Badge has no Actions associated'),(2286,'XsdTypeValidationRule.invalid.boolean','fr','<li>Invalid Boolean</li>'),(2287,'value','en',''),(2288,'publishingDate','fr',NULL),(2289,'HierarchicalIndex.First','it','primo'),(2290,'tempThumbnail','en',''),(2291,'KB Badge','fr',NULL),(2292,'Level','es','Nivel'),(2293,'Question','en',''),(2294,'JobStatus.TriggerGroup','es','Group'),(2295,'YouGotAnswerTopic','it','Hai ottenuto punti per aver risposto ad una domanda'),(2296,'itemId','fr',NULL),(2297,'DatesCertificates','es',NULL),(2298,'Modify Badge Area','en',''),(2299,'Number Of Employees','fr',NULL),(2300,'selectionfield.noselection','es','No selection'),(2301,'BigPhoto','es',NULL),(2302,'go-to-store','fr',NULL),(2305,'LikeValidationRule.notContains','es','<li>the entered value should not contain ${value}</li>'),(2306,'Query.Jump','fr','jump to'),(2307,'resourceType','fr',NULL),(2308,'errors.header','it','<ul>'),(2309,'Image','es',NULL),(2310,'Postal Code','en',''),(2311,'community.users','it','utenti'),(2312,'Group','fr',NULL),(2313,'HQ.UserDashboard.Public','en','Dashboard'),(2314,'HQ.Of','es','de'),(2317,'Link5','en',''),(2318,'Certificates','es',NULL),(2319,'itemId','it',NULL),(2320,'Link1','en',''),(2321,'WebRatio Certification','es',NULL),(2322,'XsdTypeValidationRule.invalid.length','fr','<li>the entered value length must be ${length} characters</li>'),(2323,'LikeValidationRule.notStartsWith','en','<li>the entered value should not begin with ${value}</li>'),(2324,'Public Profile','en',''),(2325,'Clear','es','Borrar filtros'),(2326,'companycc','es',NULL),(2327,'WebSite','es',NULL),(2328,'linkConfirmMessage','en','Continue?'),(2329,'Your last readings','fr',NULL),(2330,'Process.metadata.updated','fr',NULL),(2331,'averageVote','es',NULL),(2332,'HierarchicalIndex.From','it','da'),(2333,'XsdTypeValidationRule.mandatory','it','<li>campo obbligatorio</li>'),(2335,'Hide Activity Details','es',NULL),(2336,'HierarchicalIndex.From','en','from'),(2337,'HQ.YourProfile.At','es','en'),(2338,'User last readings','es','Lecturas de usuario'),(2339,'Forum','it',NULL),(2341,'JobStatus.TriggerDescription','fr','Description'),(2342,'Item','fr',NULL),(2343,'Instructions','fr',NULL),(2344,'MostimportantCertificate','fr',NULL),(2345,'actionAlreadyInvolved','en','For this Area all the available actions have been selected'),(2346,'screenshots','es',NULL),(2348,'HQ.UserDashboard.Public','fr',NULL),(2349,'area','it',NULL),(2350,'Modify Badge','fr',NULL),(2351,'view','en',NULL),(2352,'status','it',NULL),(2353,'Filter by badge level','fr',NULL),(2355,'Twitter','en','Twitter'),(2356,'Manage Badge','es',NULL),(2357,'HQ.UserDashboard.Private','es',NULL),(2358,'User no actions','fr',NULL),(2359,'ValueLengthValidationRule.min','en','<li>the entered value length must be at least ${length} characters</li>'),(2360,'generationDate','it',NULL),(2361,'CropImageUnit.noImage','it',NULL),(2362,'HQ.NoForumActivity','en','No Forum activities'),(2363,'editStatus','en',NULL),(2364,'Forum','en','Forum'),(2365,'Latest Badge','it','Ultimi Badges'),(2366,'YouGotPostSubscription','it','Hai ottenuto punti per la sottoscrizione'),(2367,'Partecipation User','es',NULL),(2368,'Scroller.From','it','da'),(2369,'Latest downloads','it','Nuovi Componenti'),(2370,'confirmAttachmentDeletion','fr',NULL),(2371,'Query.Last','it','ultimo'),(2372,'New Area','fr',NULL),(2373,'Industry Type','it',NULL),(2374,'HQ.PersonalCredits','es','CrÃ©ditos personales'),(2375,'HQ.YourProfile.PrintPdf','es','Descarga tu certificado'),(2376,'languageCode','fr',NULL),(2377,'HQ.YourProfile.Professor','es',''),(2378,'CompareValidationRule.lt.field','fr','<li>the entered value should be smaller than the value of ${otherFieldName} field</li>'),(2379,'Back','it',NULL),(2380,'Timestamp','it',NULL),(2382,'User no actions','en','User no actions'),(2383,'HQ.UsersList.AnyLevel','es','Eliminar filtro de nivel'),(2384,'HierarchicalIndex.Last','en','last'),(2385,'OID','it',NULL),(2387,'TagStore','fr',NULL),(2388,'reissues','fr',NULL),(2389,'No results','fr',NULL),(2390,'FloatValidation','fr','<li>the entered value must be a float</li>'),(2391,'ajax.computingRequest','it','Attendere prego..'),(2392,'Areegeografiche','fr',NULL),(2393,'Manage Action','it',''),(2394,'description','fr',NULL),(2395,'CompareValidationRule.eq','en','<li>the entered value should be equal to ${value}</li>'),(2396,'SmallPhoto','en',NULL),(2397,'FloatValidation','es','<li>the entered value must be a float</li>'),(2398,'Bundle Data','fr',NULL),(2399,'Create','it',''),(2400,'LikeValidationRule.startsWith.field','en','<li>the entered value should begin with the value of ${otherFieldName} field</li>'),(2401,'Go to ranking','it','Vai alla Classifica'),(2402,'YouGotDownloadStore','it','Hai ottenuto punti per il download'),(2403,'History Certificates','fr',NULL),(2404,'YouGotSignUp','es','Usted se inscribe'),(2405,'ValueLengthValidationRule.eq','es','<li>the entered value length must be ${length} characters</li>'),(2407,'XsdTypeValidationRule.max.exclusive','es','<li>the entered value should be smaller than ${value}</li>'),(2408,'HQ.UsersList.FilterArea','es','Filtrar por Especialidad'),(2409,'User last readings','it','ultimi materiali utente'),(2410,'XsdTypeValidationRule.positive.value','es','<li>Must be a positive value</li>'),(2411,'sampleProjectblob','es',NULL),(2412,'userOid','fr',NULL),(2413,'Number Of Employees','en',NULL),(2415,'Monthly Partecipation up','fr',NULL),(2416,'companypartner','en',NULL),(2417,'n.VoteUp','es',NULL),(2418,'Areegeografiche','es',NULL),(2420,'CompareValidationRule.gt','en','<li>the entered value should be greater than ${value}</li>'),(2421,'QueryUnit.wrongExtAttributeCondition','it','Una o più condizioni sono basate su attributi fuori dal table space'),(2422,'deactivationDate','fr',NULL),(2423,'Participation Monthly','fr',NULL),(2424,'descriptionTmp','en',NULL),(2426,'Chunks Text','en',NULL),(2427,'HierarchicalIndex.Next','it','successivo'),(2428,'HQ.Of','fr',NULL),(2429,'Needed Score','it',''),(2430,'XsdTypeValidationRule.min.length','fr','<li>the entered value length must be at least ${length} characters</li>'),(2431,'HQ.YourProfile.Private','en','Your Profile is private'),(2432,'Flow42','en',NULL),(2433,'BirthDate','fr',NULL),(2434,'Flow41','en',NULL),(2438,'n.User total overall','fr',NULL),(2439,'Flow46','en',NULL),(2440,'Module','es',NULL),(2443,'Mandatory3','it','Choose an area'),(2444,'Type of Badge','it',''),(2445,'Mandatory4','it','Insert a title'),(2446,'supportedVersion','es',NULL),(2448,'Levels of the Area:','es',''),(2449,'Mandatory5','it','Insert a needed score'),(2450,'Reputation','it','Reputazione'),(2451,'isoCode','fr',NULL),(2452,'Query.First','fr','first'),(2453,'User last downloads','it','Ultimi download utente'),(2454,'Labels','en',NULL),(2455,'confirmCancelProcess','fr',NULL),(2456,'Flow40','en',NULL),(2457,'Flow37','en',NULL),(2458,'Answer','fr',NULL),(2459,'FoundationYear','fr',NULL),(2460,'screenshots','en',NULL),(2461,'Area Badge','it',''),(2462,'Flow39','en',NULL),(2463,'Flow38','en',NULL),(2465,'HQ.slider.error','fr',NULL),(2466,'TimestampValidation','fr','<li>the entered value should be in the format ${pattern}</li>'),(2467,'ValueLengthValidationRule.max','en','<li>the entered value length must be at most ${length} characters</li>'),(2468,'HQ.YourProfile.ProfileUnknown','en','Profile Unknown'),(2469,'Flow64','en',NULL),(2470,'QueryUnit.wrongExtAttributeCondition','fr','One or more conditions are based on attributes outside the table space'),(2471,'PowerIndex.Last','es','last'),(2472,'Flow65','en',NULL),(2473,'Action Type','es',''),(2474,'timestamp','it',''),(2475,'go to monthly','it','Vai alla Classifica Generale'),(2477,'History badges','en','Badge History'),(2478,'Add area','en',''),(2479,'HQ.UserOf','en','Users of'),(2480,'serialNumber','en',NULL),(2481,'ActivityInstanceNotAvailable','it',''),(2483,'noNews','en','No news'),(2484,'Manage actions','en',NULL),(2485,'error.check.minChecked','fr','<li>check at least {1} items</li>'),(2487,'KB Level','en','Learn Level'),(2488,'Certification Level','es',''),(2489,'thumbnail','it',''),(2490,'ValueLengthValidationRule.min','it','<li>il valore inserito deve essere lungo almeno ${length} caratteri</li>'),(2491,'PowerIndex.Last','fr','last'),(2492,'User Data','fr',NULL),(2493,'isoCode','es',NULL),(2494,'noNews','es',NULL),(2495,'oid22','it',NULL),(2496,'oid23','it',NULL),(2497,'Clear','fr',NULL),(2498,'family','en',NULL),(2499,'Mandatory6','it','Insert the importance'),(2500,'HQ.YourProfile.Private','fr',NULL),(2501,'HQ.rank.overall','fr',NULL),(2502,'PowerIndex.First','es','first'),(2503,'publishingDate','it',NULL),(2504,'Edit','it',NULL),(2506,'SmallPhoto','fr',NULL),(2507,'Forum Badge','fr',NULL),(2508,'ajax.computingRequest','es','Computing request..'),(2509,'PowerIndex.Previous','en','previous'),(2510,'HQ.rank.overall','it','Classifica Generale'),(2511,'sequence','fr',NULL),(2512,'Version','es',NULL),(2513,'User no actions','es','Usuario no tiene acciones'),(2514,'userOid','it',NULL),(2515,'ModuleID','es',NULL),(2516,'Participation Monthly','en','Monthly Participation'),(2517,'postalCode','es',NULL),(2518,'noActionBadge','es',NULL),(2519,'Forum','es','Forum'),(2520,'message','fr',NULL),(2521,'HQ.YourActions','it',NULL),(2523,'XsdTypeValidationRule.mandatory','es','<li>mandatory field</li>'),(2525,'sendEmail','it',NULL),(2526,'CheckedItemsValidationRule.max','en','<li>check at most ${itemCount} items</li>'),(2527,'noItemsFound','fr',NULL),(2528,'QueryUnit.noDiplayAtrributes','it','Nessun display attribute selezionato'),(2529,'HierarchicalIndex.Next','es','next'),(2530,'JobStatus.JobId','es','ID'),(2531,'text','es',NULL),(2532,'thumbnail','es',NULL),(2533,'linkConfirmMessage','es','Continue?'),(2534,'CompareValidationRule.lt.field','en','<li>the entered value should be smaller than the value of ${otherFieldName} field</li>'),(2535,'XsdTypeValidationRule.min.length','en','<li>the entered value length must be at least ${length} characters</li>'),(2536,'PowerIndex.NoItemsShownOf','fr','no items shown of'),(2537,'HQ.GoToMyPosition','en','Go to My Position'),(2538,'CheckedItemsValidationRule.eq','en','<li>${itemCount} items checked required</li>'),(2539,'Manage Action','en',''),(2540,'YouGotsetData','fr',NULL),(2541,'YouGotShareGooglePlus','it','Hai ottenuto punti per aver condiviso su Google+'),(2543,'Level Badge','it',''),(2544,'JobStatus.JobId','fr','ID'),(2545,'CollectionValidationRule.in','fr','<li>the entered value must be one of: ${values}</li>'),(2546,'imageChecked','en',''),(2547,'Public profile','es',''),(2548,'checkCode','it',NULL),(2549,'answerOf','fr',NULL),(2550,'confirmRollback','en',''),(2551,'Last Name','fr',NULL),(2552,'All Reputation Actions','en',''),(2553,'LowLevel','fr',NULL),(2554,'all actions','es',NULL),(2556,'Mandatory3','en','Choose an area'),(2558,'Labels Text','en',''),(2559,'tempScreenshots','es',NULL),(2560,'community.users','es','usuarios'),(2561,'Area of Certificate','es',''),(2562,'JobStatus.TriggerGroup','en','Group'),(2563,'Teacher Name','fr',NULL),(2564,'ActionForm2','en',''),(2565,'selectionfield.world','en','All'),(2566,'Modify','es',NULL),(2567,'HierarchicalIndex.Last','fr','last'),(2568,'CompareValidationRule.eq.field','it','<li> il valore inserito deve essere uguale al valore del campo ${otherFieldName}</li>'),(2569,'lms','it',NULL),(2570,'YouGotPostTopic','en','You got points to have posted question'),(2571,'Postal Code','fr',NULL),(2572,'alias','en',''),(2573,'LikeValidationRule.notEndsWith','it','<li>il valore inserito non deve finire con ${value}</li>'),(2574,'Check private','fr',NULL),(2575,'Company Type','en',''),(2576,'Show Pdf','es',NULL),(2577,'error.validation.exception','en','<li>The following exception occurred during form validation: {0}</li>'),(2578,'sampleProjectblob','fr',NULL),(2579,'Region','es',NULL),(2580,'author','en',''),(2581,'screenshots','it',NULL),(2582,'HQ.UsersList.Filter','fr',NULL),(2583,'Go to monthly','es','Ir al Ranking'),(2584,'Review2','en',''),(2585,'retries','fr',NULL),(2586,'Sort Badges Area','en',''),(2587,'Installation','es',NULL),(2588,'universitycc','es',NULL),(2589,'Delete All','es',NULL),(2590,'Nickname','it',NULL),(2591,'companypartner','it',NULL),(2592,'HQ.YourProfile.Public','en','Your profile is public'),(2593,'icon','es',NULL),(2594,'HQ.no.certificates','en','No certificates'),(2595,'moduleDomainName','fr',NULL),(2596,'encodedtitle','fr',NULL),(2597,'postedAnswer','es',NULL),(2598,'User last downloads','fr',NULL),(2599,'Add Badge','it',''),(2600,'DemoUsers','es',''),(2601,'Fax','it',NULL),(2602,'XsdTypeValidationRule.mandatory','fr','<li>mandatory field</li>'),(2605,'Show Pdf','it',NULL),(2606,'Monthly Partecipation User','es',NULL),(2607,'Monthly Partecipation up','es',NULL),(2608,'LikeValidationRule.notEndsWith','en','<li>the entered value should not end with ${value}</li>'),(2610,'Process.metadata.invalid','es',NULL),(2611,'Course Students Number','fr',NULL),(2612,'error.stacktrace','it','{0}'),(2613,'error.empty','fr','{0}'),(2614,'title','es',NULL),(2615,'Store Badge Title','it',NULL),(2616,'answerdate','es',NULL),(2617,'HQ.GoToMyPosition','es','Ir a Mi PosiciÃ³n'),(2618,'shortDescription','it',NULL),(2619,'HQ.store.update','es',NULL),(2620,'HQ.RankNoResults','fr',NULL),(2621,'List of Actions','es',NULL),(2623,'CollectionValidationRule.in','es','<li>the entered value must be one of: ${values}</li>'),(2624,'HQ.PersonalData','en','Personal Info'),(2625,'Country','fr',NULL),(2626,'HQ.TotalCredits','fr',NULL),(2628,'Filter by badge type','en','Filter by badge type'),(2629,'CollectionValidationRule.notIn','fr','<li>the entered value must not be one of: ${values}</li>'),(2630,'HQ.CreditsTab','fr',NULL),(2631,'users','en','users'),(2632,'XsdTypeValidationRule.fraction.digits','es','<li>the entered value must have at most ${digits} fraction digits</li>'),(2633,'Score','es',NULL),(2634,'forum','es','Forum'),(2635,'YouGotsetData','it','Hai impostato'),(2636,'tag','it',NULL),(2637,'actionAlreadyInvolved','es',''),(2638,'CompareValidationRule.gteq.field','it','<li>il valore inserito deve essere maggiore o uguale al valore del campo ${otherFieldName}</li>'),(2639,'PowerIndex.Of','en','of'),(2640,'Community Participation','fr',NULL),(2641,'Active','fr',NULL),(2642,'Latest Published Learning Objects','es','Ãšltimos Learning Objects'),(2643,'MostimportantCertificate','en',NULL),(2644,'Resource Type','en',NULL),(2645,'List of Levels','it',''),(2646,'Latest topics','fr',NULL),(2647,'go to monthly','es','Ver el Ranking Completo'),(2648,'useridentifier','fr',NULL),(2649,'CompareValidationRule.lteq.field','es','<li>the entered value should be smaller than or equal to the value of ${otherFieldName} field</li>'),(2650,'Checked Image','es',NULL),(2652,'YouGotAnswerTopic','fr',NULL),(2653,'Email','fr',NULL),(2654,'HQ.YourActions','es','Sus acciones'),(2655,'CompareValidationRule.neq','es','<li>the entered value should be different from ${value}</li>'),(2656,'User last readings','fr',NULL),(2657,'ToolVersion','en',NULL),(2658,'ModuleName','fr',NULL),(2659,'tempScreenshots','fr',NULL),(2660,'Certification','en','Certification'),(2661,'YouGotMonthly','es','Tienes crÃ©ditos mensuales'),(2662,'HQ.UserDashboard','es',NULL),(2663,'imageChecked','it',NULL),(2664,'HQ.no.badges','es',NULL),(2665,'Getting Started','it','Come iniziare'),(2666,'Latest Published Learning Objects','en','Latest Learning Objects'),(2667,'Rank Participation Monthly','es',''),(2668,'`key`words (search engine)','fr',NULL),(2669,'Certifications history','it','Certificazioni acquisite'),(2670,'HQ.View','fr',NULL),(2671,'XsdTypeValidationRule.invalid.time','en','<li>Invalid Time</li>'),(2672,'Edit labels','it',NULL),(2673,'Modify Area','fr',NULL),(2674,'username','fr',NULL),(2675,'Title','es',NULL),(2676,'answer','en',NULL),(2677,'Area Badge','fr',NULL),(2678,'CertificateForm','fr',NULL),(2679,'PowerIndex.To','en','to'),(2680,'CollectionValidationRule.notIn','es','<li>the entered value must not be one of: ${values}</li>'),(2681,'History certificate','fr',NULL),(2682,'CollectionValidationRule.notIn.query','it','<li>il valore inserito è incluso nei valori vietati</li>'),(2683,'HQ.PersonalCertificates','it',NULL),(2684,'SecondMenuLevel','en','Community'),(2685,'Time','es',NULL),(2686,'ModuleID','fr',NULL),(2687,'levelCertificate','it',''),(2688,'Monthly Participation','it',''),(2690,'HQ.UsersList.Profile','fr',NULL),(2691,'HierarchicalIndex.NoItemsShownOf','es','no items shown of'),(2692,'Creation Date','it',NULL),(2693,'lastModified','en',NULL),(2694,'LikeValidationRule.contains','fr','<li>the entered value should contain ${value}</li>'),(2695,'Store Level','fr',NULL),(2696,'document','es',NULL),(2698,'Address','es',NULL),(2699,'NotPublicProfile.Message','it',NULL),(2700,'Latest Forum Topics','es','Ãšltimos Temas del Forum'),(2701,'Time','fr',NULL),(2702,'Pdf','en',NULL),(2703,'Website','fr',NULL),(2704,'MandatoryIfTrue.error','es',NULL),(2705,'LikeValidationRule.notEndsWith.field','it','<li>il valore inserito non deve finire con il valore del campo ${otherFieldName}</li>'),(2706,'Bundle Data','es',NULL),(2707,'ModuleID','en',NULL),(2709,'username','it','Username'),(2710,'XsdTypeValidationRule.min.exclusive','fr','<li>the entered value should be greater than ${value}</li>'),(2711,'go to homepage','en','Panoramica'),(2712,'Company','fr',NULL),(2713,'Labels','it',NULL),(2714,'isoCode','en',''),(2715,'timestamp','fr',NULL),(2716,'Headquarter','it','Panoramica'),(2717,'PowerIndex.Jump','es','jump to'),(2718,'HQ.UsersList.FilterArea','en','Filter by Expertise'),(2720,'oid22','en',''),(2721,'HQ.YourProfile.Private','it',''),(2723,'HQ.PersonalData','es',NULL),(2724,'oid23','en',''),(2725,'HierarchicalIndex.Of','fr','of'),(2726,'HQ.is','fr',NULL),(2727,'Save','en',''),(2728,'Importance','es',NULL),(2729,'editionoid','it',NULL),(2730,'XsdTypeValidationRule.max.length','es','<li>the entered value length must be at most ${length} characters</li>'),(2731,'Description','en','Description'),(2733,'ValueLengthValidationRule.eq','fr','<li>the entered value length must be ${length} characters</li>'),(2735,'Internal','en','Internal'),(2736,'My certificate','it',NULL),(2737,'downloadCount','es',NULL),(2738,'New Level','en',''),(2739,'User no actions','it','Nessuna azione compiuta'),(2740,'by','en',''),(2741,'Article','it',NULL),(2742,'serverTimestamp','en',''),(2744,'Modify Badge','it',NULL),(2745,'XsdTypeValidationRule.invalid.timestamp','es','<li>Invalid Timestamp</li>'),(2746,'Edit','en',''),(2747,'Scroller.Jump','fr','jump to'),(2748,'Description','es',NULL),(2749,'Country','es',NULL),(2750,'at','en',''),(2751,'styleProject','fr',NULL),(2752,'HQ.NotFromSite','es','Usted no puede ver este perfil, ir a la pÃ¡gina principal e iniciar sesiÃ³n.'),(2753,'HierarchicalIndex.To','fr','to'),(2754,'HQ.HomePage','en',''),(2755,'JobStatus.TriggerNextFireTimestamp','es','Next Fire Timestamp'),(2756,'LikeValidationRule.contains.field','fr','<li>the entered value should contain the value of ${otherFieldName} field</li>'),(2757,'oid22','es',NULL),(2758,'oid23','es',NULL),(2759,'iconblob','es',NULL),(2760,'HQ.store.authorBy','en','By'),(2761,'HQ.UserDashboard','fr',NULL),(2763,'areasort','en',''),(2764,'YouGotDownloadStore','fr',NULL),(2765,'Type','it',NULL),(2766,'ordinal','it',NULL),(2767,'HQ.View','en','Views'),(2768,'Profile','es',NULL),(2769,'valideType.error','it',NULL),(2770,'note','fr',NULL),(2771,'templatesblob','fr',NULL),(2772,'document','en',''),(2773,'Area','en','Area'),(2774,'country','en',''),(2775,'confirmDeletion','fr',NULL),(2776,'YouGotActiving','fr',NULL),(2777,'HQ.NoStoreActivity','fr',NULL),(2778,'fileblob','es',NULL),(2779,'noActionBadge','it',NULL),(2781,'thumbnail','fr',NULL),(2782,'Query.Last','es','last'),(2783,'HierarchicalIndex.First','fr','first'),(2784,'User overall position','en',''),(2785,'Community User','fr',NULL),(2786,'Search','fr',NULL),(2787,'supportedVersion','en',NULL),(2788,'Overall Participation','es',''),(2789,'Modify','en',NULL),(2790,'Tags','es',NULL),(2791,'HQ.UserDashboard.AllActions','en','All Actions'),(2792,'JobStatus.TriggerId','it','ID'),(2793,'Monthly Form','es',NULL),(2794,'accepted','fr',NULL),(2795,'Manage Area','it',NULL),(2797,'Article','en',NULL),(2798,'articleOid','es',NULL),(2799,'Set','es',NULL),(2800,'Modify Area','en',''),(2801,'Message','it',NULL),(2802,'HQ.RankNoResults','es','No hay usuarios en este rank con estas propiedades.'),(2803,'Timestamp','fr',NULL),(2804,'Partecipation User','en',NULL),(2805,'Monthly Partecipation down','it',''),(2806,'Participation Monthly','es','ParticipaciÃ³n mensual'),(2807,'HQ.Credits','es','crÃ©ditos'),(2808,'Bundle Data','en',NULL),(2809,'XsdTypeValidationRule.invalid.time','it','<li>Time non valido</li>'),(2810,'JobStatus.TriggerId','en','ID'),(2811,'timestamp2','fr',NULL),(2812,'CollectionValidationRule.notIn','en','<li>the entered value must not be one of: ${values}</li>'),(2813,'activationDate','it',''),(2814,'Participation Company','es',''),(2815,'Item','en',NULL),(2816,'PDF Certificate','en',NULL),(2817,'History certificate','en',NULL),(2818,'go to monthly','fr',NULL),(2820,'Website','it','Sito Web'),(2822,'Area','es',''),(2823,'Department','fr',NULL),(2824,'actionAlreadyInvolved','it','Per questa area tutte le azioni disponibili sono state compiute'),(2825,'Add level','fr',NULL),(2826,'EMailValidationRule.error','fr','<li>invalid email address</li>'),(2827,'HQ.YourProfile.Professional','it',''),(2828,'JobStatus.TriggerNextFireTimestamp','it','Next Fire Timestamp'),(2829,'url','fr',NULL),(2830,'LikeValidationRule.notStartsWith.field','en','<li>the entered value should not begin with the value of ${otherFieldName} field</li>'),(2831,'YouGotActivingClient','fr',NULL),(2832,'alias','es',NULL),(2833,'Badge Type','it',NULL),(2834,'articleOid','it',NULL),(2835,'Profile','en',NULL),(2836,'Edit labels','fr',NULL),(2837,'HQ.YourProfile.Developer','fr',NULL),(2838,'Bio','en','Bio'),(2839,'Type','en','Type'),(2840,'Go to account','fr',NULL),(2841,'XsdTypeValidationRule.total.digits','en','<li>the entered value must have at most ${digits} total digits</li>'),(2842,'forum','en','Forum'),(2843,'XsdTypeValidationRule.min.inclusive','es','<li>the entered value should be greater than or equal to ${value}</li>'),(2844,'Partner','es',NULL),(2845,'Linkedin','en','LinkedIn'),(2847,'HierarchicalIndex.Previous','it','precedente'),(2848,'Action Area','en',''),(2849,'History actions','fr',NULL),(2850,'DatesCertificates','fr',NULL),(2851,'Webratio Activation','en',NULL),(2852,'Partecipation down','fr',NULL),(2853,'serialNumber','it',NULL),(2854,'CheckedItemsValidationRule.min','en','<li>check at least ${itemCount} items</li>'),(2855,'XsdTypeValidationRule.min.inclusive','fr','<li>the entered value should be greater than or equal to ${value}</li>'),(2856,'Big Photo','es',NULL),(2857,'Sort Number','es',NULL),(2858,'Countrytable','fr',NULL),(2859,'WebSite','en','Website'),(2860,'language','it',NULL),(2861,'maxVersion','en',NULL),(2862,'Getting Started','en','Getting started'),(2864,'Credit history','es','CrÃ©ditos ganados'),(2865,'Add level','it',''),(2866,'YouGotQuestionVoteup','fr',NULL),(2867,'YouGotApprovedAnswer','es','Tienes crÃ©ditos porque usted ha aprobado la respuesta'),(2868,'HQ.Message','it',NULL),(2869,'title','fr',NULL),(2870,'Go to participation','es','Ir al Ranking'),(2871,'Action Type','it',''),(2872,'n.Answer','es',NULL),(2873,'LikeValidationRule.notStartsWith.field','it','<li>il valore inserito non deve iniziare con il valore del campo ${otherFieldName}</li>'),(2874,'university','es',NULL),(2875,'YouGotPostTopic','fr',NULL),(2876,'community.goToDashboard','fr',NULL),(2877,'CertificateForm','en',NULL),(2878,'HQ.NoForumActivity','it',''),(2879,'HQ.Answer','it',NULL),(2881,'`key`words (search engine)','en',''),(2882,'ordinal','fr',NULL),(2883,'Company Email','es',NULL),(2884,'FormNotEmptyValidationRule.error','es','<li>at least one field must be filled</li>'),(2885,'Levels of the Area:','fr',NULL),(2886,'XsdTypeValidationRule.invalid.length','es','<li>the entered value length must be ${length} characters</li>'),(2887,'linkConfirmMessage','fr','Continue?'),(2889,'error.check.maxChecked','fr','<li>check at most {1} items</li>'),(2890,'CertificationCRM','it',''),(2891,'HierarchicalIndex.Previous','fr','previous'),(2892,'LikeValidationRule.notEndsWith','es','<li>the entered value should not end with ${value}</li>'),(2893,'Chunks','en',NULL),(2894,'downloadCount','fr',NULL),(2895,'TimeValidation','fr','<li>the entered value should be in the format ${pattern}</li>'),(2896,'Module','it',NULL),(2897,'YouGotActiving','es','ActivaciÃ³n'),(2898,'levelsort','it',''),(2899,'YouGotPublishStore','fr',NULL),(2900,'serialNumberoid','es',NULL),(2901,'notNotesFound','it',NULL),(2902,'XsdTypeValidationRule.min.inclusive','en','<li>the entered value should be greater than or equal to ${value}</li>'),(2903,'timestamp','es',NULL),(2904,'Go to account','en','Go to Account'),(2905,'Website','es',NULL),(2906,'Latest Published Store Components','en','Latest Store Components'),(2907,'Name','it',NULL),(2908,'custom description','en',NULL),(2909,'Company','en',NULL),(2910,'CheckedItemsValidationRule.min','it','<li>selezionare almeno ${itemCount} elementi</li>'),(2911,'HQ.ParticipationTab','es','ParticipaciÃ³n'),(2912,'styleProjectblob','en',NULL),(2913,'country','es',NULL),(2914,'of','en','of'),(2915,'family','fr',NULL),(2916,'Generic Message','it',''),(2917,'HQ.NoStoreActivity','es',NULL),(2918,'Latest Forum Topics','it','Nuove Domande del Forum'),(2919,'Pdf','fr',NULL),(2920,'JobStatus.JobGroup','fr','Group'),(2921,'Hide Activity Details','it',''),(2922,'HQ.RankingOverall','it','Classifica Generale'),(2923,'Green Check','it',''),(2924,'Date','fr',NULL),(2926,'Course Title','it',''),(2927,'Link206','fr',NULL),(2928,'HQ.UsersList.Filter','es','Filtrar por Especialidad'),(2929,'LikeValidationRule.notContains','fr','<li>the entered value should not contain ${value}</li>'),(2930,'Welcome','fr',NULL),(2931,'Certification Score','es',NULL),(2932,'Manage Badge','fr',NULL),(2933,'ActionForm','it',''),(2934,'User','es',NULL),(2935,'Partner','it',''),(2936,'error.check.maxChecked','it','<li>selezionare al massimo {1} elemento</li>'),(2937,'Industry Type','es',NULL),(2938,'n.VoteUp','it',''),(2939,'Logo','it',''),(2941,'go-to-forum','en','Go to Forum'),(2942,'tempScreenshotsblob','it',''),(2944,'Last Name','en','Last Name'),(2945,'YouGotReadingWiki','es','Tienes crÃ©ditos porque usted ha leÃ­do'),(2946,'User','en',NULL),(2947,'Active','es',''),(2948,'HQ.UsersList.AnyArea','it','Reset'),(2949,'HQ.PersonalCertificates','en','Personal Certificates'),(2950,'minVersion','fr',NULL),(2951,'answerdate','it',''),(2953,'file','fr',NULL),(2954,'CompareValidationRule.gteq','it','<li>il valore inserito deve essere maggiore o uguale di ${value}</li>'),(2955,'Overall Participation','it',''),(2956,'selectionfield.world','fr',NULL),(2957,'Student','fr',NULL),(2958,'WebSite','fr',NULL),(2959,'no my actions','it','Nessuna azione compiuta'),(2960,'Add','en',''),(2961,'approved','en','was approved'),(2962,'Save','es',NULL),(2963,'thumbnailblob','es',NULL),(2964,'Message','en',''),(2965,'Involved Actions Associated','fr',NULL),(2966,'HQ.UserDashboard.Public','es',NULL),(2967,'LinkParticipation','en',''),(2968,'Link206','en',''),(2969,'Scroller.From','en','from'),(2970,'link','es',NULL),(2972,'tempIconblob','en',''),(2973,'Twitter','fr',NULL),(2974,'HQ.PersonalCredits','en','Personal Points'),(2975,'YouGotAnswerApproved','en','You got points because your answer was approved'),(2976,'useridentifier','en',''),(2977,'Installation','it',''),(2980,'productid','en',''),(2981,'CaptchaValidationRule.error','fr',' <li>the entered value did not match what was displayed</li>'),(2982,'level','fr',NULL),(2983,'Private Message','es',NULL),(2984,'XsdTypeValidationRule.invalid.date','it','<li>Data non valida</li>'),(2985,'Filter','es',''),(2986,'View','fr',NULL),(2987,'Needed Score','es',NULL),(2988,'Credit history','it','Punti & Premi'),(2989,'Scroller.To','fr','to'),(2990,'Phone Number','es',NULL),(2991,'noProcessesFound','en',''),(2992,'reviewCount','fr',NULL),(2993,'XsdTypeValidationRule.invalid.integer','en','<li>Invalid Integer</li>'),(2995,'active','en',''),(2996,'History certificate','es',''),(2997,'error.button.not.pressed','es','<li>Please press a button for form submission</li>'),(2998,'image','it',''),(2999,'vote','es',NULL),(3000,'Participation','fr',NULL),(3001,'oidtag','en',''),(3002,'NotPublicProfile.Message','es',NULL),(3003,'Involved Actions Associated','es',NULL),(3004,'getting-start','es','GuÃ­a de Usuario'),(3005,'Flow42','it',''),(3006,'Flow41','it',''),(3007,'tempIcon','it',''),(3008,'Flow40','it',''),(3009,'HQ.PersonalCredits','it','Punti Personali'),(3011,'Flow46','it',''),(3013,'PowerIndex.Last','it','ultimo'),(3016,'approved','fr',NULL),(3017,'Scroller.Previous','it','precedente'),(3018,'duration','it',''),(3019,'History badges','it','Badges acquisiti'),(3020,'file','it',''),(3021,'questiondate','en',''),(3022,'XsdTypeValidationRule.max.inclusive','it','<li>il valore inserito deve essere minore o uguale a ${value}</li>'),(3023,'HQ.YourProfile.Professor','en','Professor'),(3024,'No results','es',NULL),(3025,'Nickname_Crm','it',''),(3026,'Flow37','it',''),(3027,'Flow38','it',''),(3028,'Flow39','it',''),(3029,'Areas','it',''),(3030,'HQ.UserDashboard ','fr',NULL),(3031,'Flow64','it',''),(3032,'HierarchicalIndex.Jump','es','jump to'),(3034,'questiondate','fr',NULL),(3035,'id','en',''),(3036,'Linkedin','es',NULL),(3037,'Show Activity Details','es',NULL),(3038,'styleProject','es',NULL),(3039,'Company Email','en',''),(3040,'certification','fr',NULL),(3041,'Flow65','it',NULL),(3042,'Title','it',NULL),(3043,'noItemsFound','it',NULL),(3044,'Newsletter','es',NULL),(3045,'checkCode','es',NULL),(3046,'content','en',''),(3047,'Update','fr',NULL),(3048,'Rank Participation Overall','it',''),(3049,'HQ.MyDashboard ','fr',NULL),(3050,'RegularExpressionValidationRule.error','es','<li>the entered value is not valid</li>'),(3052,'licenseServerOperatingSyste','en',''),(3053,'Scroller.Previous','en','previous'),(3054,'multiselectionfield.deselectAll','fr','Deselect All'),(3055,'CheckedItemsValidationRule.max','it','<li>selezionare al massimo ${itemCount} elementi</li>'),(3056,'answer','fr',NULL),(3057,'HQ.PersonalBadges','fr',NULL),(3058,'Company Certified','fr',NULL),(3059,'Certificates','it',NULL),(3060,'ActivityInstanceNotAvailable','fr',NULL),(3061,'Headquarter','en','Overview'),(3062,'description (search engine)','fr',NULL),(3063,'imageChecked','fr',NULL),(3064,'Show Activity Details','it',NULL),(3065,'Latest Published Store Components','fr',NULL),(3066,'Certification Badge','en',''),(3067,'go-to-forum','fr',NULL),(3068,'Message','es',NULL),(3069,'SecondMenuLevel','fr',NULL),(3070,'QueryUnit.noDiplayAtrributes','fr','No selected display attributes'),(3071,'Teacher LastName','en',''),(3072,'user','fr',NULL),(3073,'Delete','es',NULL),(3074,'ModuleName','en',''),(3075,'Process.metadata.updated','es',NULL),(3076,'Scroller.Previous','es','previous'),(3078,'HQ.YourProfile.At','en','at'),(3079,'Student','es',NULL),(3080,'notAttachmentsFound','it',NULL),(3081,'UserOf','it','di'),(3082,'Areegeografiche','it',NULL),(3083,'fileblob','en',''),(3084,'HQ.UsersList.AnyArea','en','Clear area filter'),(3085,'oid22','fr',NULL),(3086,'oid23','fr',NULL),(3087,'HQ.YourProfile.NoData','fr',NULL),(3088,'FoundationYear','es',NULL),(3089,'Company Email','it',NULL),(3090,'Scroller.Of','fr','of'),(3091,'timestamp2','en',''),(3092,'QueryUnit.noDiplayAtrributes','es','No selected display attributes'),(3093,'tempThumbnail','es',NULL),(3094,'name','es',NULL),(3095,'Checked Image','fr',NULL),(3096,'Delete','en',''),(3097,'Tag','es',NULL),(3098,'N/D','it',NULL),(3099,'ajax.computingRequest','en','Computing request..'),(3100,'invioEffettuato','en',''),(3101,'Partecipation User','fr',NULL),(3102,'Previous Activity','en',''),(3103,'HQ.YourProfile.Work','es','Trabaja'),(3104,'Scroller.Of','en','of'),(3106,'XsdTypeValidationRule.invalid.enum','it','<li>Valore non previsto (${values})</li>'),(3107,'First Name','it',NULL),(3108,'user','it',''),(3109,'Forum Badge Title','es',''),(3110,'Monthly Partecipation User','en',''),(3111,'Your last topics','it','Ultime tue Domande'),(3112,'Reputation','es','ReputaciÃ³n'),(3113,'Item','es',NULL),(3114,'HQ.UsersList.FilterLevel','it','Filtra per livello minimo'),(3115,'HQ.store.vote','fr',NULL),(3116,'MaxLevel','it',''),(3117,'Partecipation up','es',NULL),(3118,'XsdTypeValidationRule.max.exclusive','it','<li>il valore inserito deve essere minore di ${value}</li>'),(3119,'XsdTypeValidationRule.negative.value','fr','<li>Must be a negative value</li>'),(3120,'History Diagram','fr',NULL),(3121,'Address','fr',NULL),(3122,'Name','en',''),(3123,'Manage actions','es',NULL),(3124,'QueryUnit.wrongGrouping','it','Tutti i display attribute devono essere utilizzati nel raggruppamento o nella funzione aggregata'),(3125,'productid','fr',NULL),(3126,'XsdTypeValidationRule.invalid.integer','fr','<li>Invalid Integer</li>'),(3127,'User','it',''),(3128,'DecimalValidation','en','<li>the entered value must be a number</li>'),(3129,'Area','fr',NULL),(3130,'HQ.YourProfile.Public','it',''),(3131,'rejectionReason','it',NULL),(3132,'go-to-lms','en','Go to Learn'),(3133,'LikeValidationRule.endsWith','en','<li>the entered value should end with ${value}</li>'),(3134,'retries','it',NULL),(3135,'Hide Activity Details','en',NULL),(3136,'LikeValidationRule.endsWith','it','<li>il valore inserito deve finire con ${value}</li>'),(3137,'Review','en',NULL),(3138,'HQ.store.vote','it',NULL),(3139,'WebRatio Certification','it',''),(3140,'tempName','it',NULL),(3141,'Scroller.Last','it','ultimo'),(3142,'HQ.UsersList.AnyArea','fr',NULL),(3143,'Query.From','it','da'),(3144,'LikeValidationRule.notContains','en','<li>the entered value should not contain ${value}</li>'),(3145,'text','en',NULL),(3146,'selectionfield.noselection','en','no selection'),(3147,'congratulations','it',NULL),(3148,'configuration','en',NULL),(3149,'HQ.NoStoreActivity','en','No Store activities'),(3150,'Nickname_Crm','fr',NULL),(3151,'Industry Type','fr',NULL),(3152,'multiselectionfield.selectAll','fr','Select All'),(3153,'TypeValidationRule.error','es','<li>the entered value should have the format ${pattern}</li>'),(3154,'Contact','en',NULL),(3155,'Add level','en',''),(3156,'Scroller.To','en','to'),(3157,'`key`','es',NULL),(3158,'lms','fr',NULL),(3159,'HD Checked Image','es',NULL),(3160,'QueryUnit.wrongExtAttributeCondition','es','One or more conditions are based on attributes outside the table space'),(3161,'Teacher LastName','es',NULL),(3162,'tempDescription','it',NULL),(3163,'Go to ranking','fr',NULL),(3164,'YouGotVoteStore','es','Tienes crÃ©ditos porque usted votÃ³ hasta'),(3165,'Language','en',NULL),(3166,'KB Level','fr',NULL),(3167,'PowerIndex.First','en','first'),(3169,'content','fr',NULL),(3170,'Add Level','en',''),(3171,'Latest Forum Topics','fr',NULL),(3172,'Installation','fr',NULL),(3173,'Mandatory6','en','Insert the importance'),(3174,'Linkedin','fr',NULL),(3175,'Mandatory4','en','Insert a title'),(3176,'Mandatory5','en','Insert a needed score'),(3177,'Areatomail','en',NULL),(3178,'NotPublicProfile.Message','fr',NULL),(3180,'address','en',NULL),(3181,'Scrool Actions','es',NULL),(3182,'YouBecome','fr',NULL),(3183,'Query.Jump','it','vai a'),(3184,'User monthly position','fr',NULL),(3185,'oid','es',NULL),(3186,'multiselectionfield.selectAll','es','Seleccionar todo'),(3187,'certification','it','Certificazioni'),(3188,'YouGotAnswerApproved','es','Tienes crÃ©ditos porque su respuesta fue aprobada'),(3189,'HQ.NicknameNull','fr',NULL),(3191,'Answer','it',NULL),(3192,'Last Name','it','Cognome'),(3193,'ActivityInstanceNotAvailable','es',NULL),(3194,'disabled','it',NULL),(3195,'date','fr',NULL),(3196,'file','en',NULL),(3197,'sequence','es',NULL),(3198,'HQ.UsersList.Filter','en','Filter by Expertise'),(3199,'community.users','en','users'),(3200,'vote','fr',NULL),(3201,'url','it',NULL),(3202,'styleProject','en',NULL),(3203,'Modify Area','es',NULL),(3204,'Process.metadata.updated','en',NULL),(3205,'screenshotsblob','it',NULL),(3206,'noNews','fr',NULL),(3207,'postalCode','fr',NULL),(3208,'publishingDate','es',NULL),(3210,'editStatus','it',NULL),(3211,'visibility','fr',NULL),(3212,'templates','it',''),(3213,'Your last readings','it','ultimi articoli letti'),(3214,'users','fr',NULL),(3215,'KB Badge','en','Learn Badge'),(3216,'Twitter','it',''),(3217,'HQ.ParticipationTab','en','Participation'),(3218,'Dashboard','es',NULL),(3219,'accepted','es',''),(3220,'All','en','All'),(3221,'link','it',''),(3222,'Nickname_Crm','en','Nickname'),(3223,'HQ.MyDashboard ','es',NULL),(3224,'Search bar','en',''),(3225,'Go to participation','it','Vai alla Classifica Generale'),(3226,'Sort Badges Area','it',''),(3227,'DecimalValidation','it','<li>il valore inserito deve essere di tipo numerico</li>'),(3228,'YouGotPostTopic','es','Tienes crÃ©ditos para haber publicado pregunta'),(3229,'Role','en',NULL),(3230,'HQ.UsersMonthly','en','Top Users'),(3231,'Latest reading','fr',NULL),(3232,'tempThumbnailblob','it',''),(3233,'LMS','es','Learn'),(3235,'KB Badge Title','es',NULL),(3236,'noProcessInstancesFound','it',''),(3238,'MostimportantBadge','it',''),(3239,'Tag','it',''),(3240,'serverTimestamp','it',''),(3241,'serialNumberoid','en',NULL),(3242,'Rating','en',NULL),(3243,'Score','en',NULL),(3244,'`key`word','en',NULL),(3245,'Filter by region','en','Filter by country'),(3246,'Geographical Area','fr',NULL),(3247,'CropImageUnit.noImage','fr',NULL),(3248,'Timestamp','es',NULL),(3249,'all actions','it',''),(3250,'Filter by badge level','en','Filter by badge level'),(3251,'Review2','es',NULL),(3252,'city','fr',NULL),(3253,'Mobile','en',NULL),(3254,'PowerIndex.Next','es','next'),(3255,'Public profile','it',''),(3256,'Mobile','es',NULL),(3257,'Certification Level','it',''),(3258,'Filter by badge type','fr',NULL),(3259,'RegularExpressionValidationRule.error','it','<li>il valore inserito non è valido</li>'),(3261,'Save','fr',NULL),(3262,'Logout','it','Logout'),(3263,'Type of Badge','en',''),(3264,'Message','fr',NULL),(3265,'Link95','fr',NULL),(3266,'Edit Text','en',NULL),(3267,'Monthly Partecipation down','es',NULL),(3270,'Select a region','it','Seleziona una regione'),(3272,'HQ.UsersList.Profile','it','Profilo'),(3275,'12_topMonthly','it','Top User di Dicembre'),(3276,'11_topMonthly','it','Top User di Novembre'),(3277,'10_topMonthly','it','Top User di Ottobre'),(3278,'09_topMonthly','it','Top User di Settembre'),(3279,'08_topMonthly','it','Top User di Agosto'),(3280,'07_topMonthly','it','Top User di Luglio'),(3281,'06_topMonthly','it','Top User di Giugno'),(3282,'05_topMonthly','it','Top User di Maggio'),(3283,'04_topMonthly','it','Top User di Aprile'),(3284,'03_topMonthly','it','Top User di Marzo'),(3285,'02_topMonthly','it','Top User di Febbraio'),(3286,'01_topMonthly','it','Top User di Gennaio'),(3287,'12_topMonthly','en','December Top Users'),(3288,'11_topMonthly','en','November Top Users'),(3289,'10_topMonthly','en','October Top Users'),(3290,'09_topMonthly','en','September Top Users'),(3291,'08_topMonthly','en','August Top Users'),(3292,'07_topMonthly','en','July Top Users'),(3293,'06_topMonthly','en','June Top Users'),(3294,'05_topMonthly','en','May Top Users'),(3295,'04_topMonthly','en','April Top Users'),(3296,'03_topMonthly','en','March Top Users'),(3297,'02_topMonthly','en','February Top Users'),(3298,'01_topMonthly','en','January Top Users'),(3299,'12_topMonthly','es','Top Usuarios de Diciembre '),(3300,'12_topMonthly','es','Top Usuarios de Diciembre '),(3301,'11_topMonthly','es','Top Usuarios de Noviembre'),(3302,'10_topMonthly','es','Top Usuarios de Octubre'),(3303,'09_topMonthly','es','Top Usuarios de Septiembre'),(3304,'07_topMonthly','es','Top Usuarios de Julio'),(3305,'06_topMonthly','es','Top Usuarios de Junio'),(3306,'05_topMonthly','es','Top Usuarios de Mayo'),(3307,'04_topMonthly','es','Top Usuarios de Abril'),(3308,'03_topMonthly','es','Top Usuarios de Marzo'),(3309,'02_topMonthly','es','Top Usuarios de Febrero'),(3310,'01_topMonthly','es','Top Usuarios de Enero'),(3311,'12_Monthly','it','Classifica di Dicembre'),(3312,'11_Monthly','it','Classifica di Novembre'),(3313,'10_Monthly','it','Classifica di Ottobre'),(3314,'09_Monthly','it','Classifica di Settembre'),(3315,'08_Monthly','it','Classifica di Agosto'),(3316,'07_Monthly','it','Classifica di Luglio'),(3317,'06_Monthly','it','Classifica di Giugno'),(3318,'05_Monthly','it','Classifica di Maggio'),(3319,'04_Monthly','it','Classifica di Aprile'),(3320,'03_Monthly','it','Classifica di Marzo'),(3321,'02_Monthly','it','Classifica di Febbraio'),(3322,'01_Monthly','it','Classifica di Gennaio'),(3323,'12_Monthly','en','December Ranking'),(3324,'11_Monthly','en','November Ranking'),(3325,'10_Monthly','en','October Ranking'),(3326,'09_Monthly','en','September Ranking'),(3327,'08_Monthly','en','August Ranking'),(3328,'07_Monthly','en','July Ranking'),(3329,'06_Monthly','en','June Ranking'),(3330,'05_Monthly','en','May Ranking'),(3331,'04_Monthly','en','April Ranking'),(3332,'03_Monthly','en','March Ranking'),(3333,'02_Monthly','en','February Ranking'),(3334,'01_Monthly','en','January Ranking'),(3335,'12_Monthly','es','RÃ¡nking de Diciembre'),(3336,'12_Monthly','es','RÃ¡nking de Diciembre'),(3337,'11_Monthly','es','RÃ¡nking de Noviembre'),(3338,'10_Monthly','es','RÃ¡nking de Octubre'),(3339,'09_Monthly','es','RÃ¡nking de Septiembre'),(3340,'08_Monthly','es','RÃ¡nking de Agosto'),(3341,'07_Monthly','es','RÃ¡nking de Julio'),(3342,'06_Monthly','es','RÃ¡nking de Junio'),(3343,'05_Monthly','es','RÃ¡nking de Mayo'),(3344,'04_Monthly','es','RÃ¡nking de Abril'),(3345,'03_Monthly','es','RÃ¡nking de Marzo'),(3346,'02_Monthly','es','RÃ¡nking de Febrero'),(3347,'01_Monthly','es','RÃ¡nking de Enero '),(3348,'08_topMonthly','es','Top Usuarios de Agosto'),(3349,'Help','es',NULL),(3350,'n.User 7 Days filtered','it',NULL),(3351,'Points','en','Points'),(3352,'Assign new reward','fr',NULL),(3353,'Event','es',NULL),(3354,'08_topMonthly','fr',NULL),(3355,'`key`word','fr',NULL),(3356,'Action Name','es',NULL),(3357,'Manage Containers','es',NULL),(3358,'Notification','en',NULL),(3359,'YouGotWebinar','en','You got points to have attended the webinar:'),(3360,'Add reward','fr',NULL),(3361,'Edit reward','es',NULL),(3362,'Newsletter Area','fr',NULL),(3363,'HQ.rank.7Days.home','en','7 Days Leaderboard'),(3364,'History points','fr',NULL),(3365,'Delivery Date','es',NULL),(3366,'ValueLengthValidationRule.blob.min','it','<li>dimensione file errata (>${length})</li>'),(3367,'Next Rewards','en',''),(3368,'Community Users Area','it',NULL),(3369,'Ok','it',NULL),(3370,'Community Participation Seven Days','it',NULL),(3371,'LeaderBoards','it',NULL),(3372,'Edit Mail','en',NULL),(3373,'Add container','en',NULL),(3374,'Availability','it',NULL),(3375,'Users','fr',NULL),(3376,'Edit reward','en',''),(3377,'Alias','en',NULL),(3378,'Insert Actions','it',NULL),(3379,'Flow106','en',NULL),(3380,'Actions','fr',NULL),(3381,'Flow97','it',NULL),(3382,'Actions History','es','Todo las AcciÃ³nes'),(3383,'ValueLengthValidationRule.blob.neq','es','<li>file size not allowed (${length})</li>'),(3384,'Status','fr',NULL),(3385,'New Reward','en',''),(3386,'New reward','it',NULL),(3387,'n.User 7 Days filtered','es',''),(3388,'Flow122','en',NULL),(3389,'Participation 7 Days','fr',NULL),(3390,'Events','fr',NULL),(3391,'Flow120','en',NULL),(3392,'Container','fr',NULL),(3393,'ValueLengthValidationRule.blob.eq','en','<li>wrong file size (${length})</li>'),(3394,'New Reward','es',NULL),(3395,'Monthly Points','en',NULL),(3396,'Newsletter Preview','it',NULL),(3397,'Error','es',NULL),(3398,'Container','es',NULL),(3399,'Presences','fr',NULL),(3400,'Creation date','es',NULL),(3401,'Iso Code','es',NULL),(3402,'Delivery date','es',NULL),(3403,'Footer','es',NULL),(3404,'Assign new action','en',NULL),(3405,'`key`word','en',NULL),(3406,'HQ.Ranking7Days','fr',NULL),(3407,'Alias','fr',NULL),(3408,'Rewards Area','en','Rewards Info'),(3409,'community.possible.reward','fr',NULL),(3410,'Manage Containers','fr',NULL),(3411,'Executor','fr',NULL),(3412,'Notification LeaderBoards','en',NULL),(3413,'Ok','en',NULL),(3414,'Total Credit','fr',NULL),(3415,'Manage Rewards','it',''),(3416,'Rank 7 Days','es',''),(3417,'Code','en',NULL),(3418,'Flow127','en',NULL),(3419,'email','en',NULL),(3420,'community.slider.step3title','es',NULL),(3421,'mail template','en',NULL),(3422,'Flow148','fr',NULL),(3423,'counter7DaysLeaderboard','es','en la clasifica de los ultimos 7 dÃ­as'),(3424,'Flow143','fr',NULL),(3425,'Overall Position','fr',NULL),(3426,'Delivery Date','en',NULL),(3427,'Needed Points','en',NULL),(3428,'Status','en',NULL),(3429,'Executor','es',NULL),(3430,'Newsletter Events','it',NULL),(3431,'Manage Rewards','es',''),(3432,'Text','fr',NULL),(3433,'n.User 7 Days filtered','en',NULL),(3434,'Leaderboards','en','Leaderboards'),(3435,'Reward','en',''),(3436,'Useroid','en',NULL),(3437,'Flow150','fr',NULL),(3438,'Preview','it',NULL),(3439,'forum.action.points','it',''),(3440,'Preview','en',NULL),(3441,'Language Code','en',NULL),(3442,'community.comingsoon','fr',NULL),(3443,'HQ.rank.7Days','it','Classifica degli Ultimi 7 Giorni'),(3444,'Leaderboards','fr',NULL),(3445,'Available','en',NULL),(3446,'community.slider.step2title','fr',NULL),(3447,'community.spent.points','en','Spent Points'),(3448,'Facebook','fr',NULL),(3449,'ValueLengthValidationRule.blob.min','es','<li>wrong file size (>${length})</li>'),(3450,'Delivery Date','it',NULL),(3451,'Reward Type','es',NULL),(3452,'YouGotClassroom','fr',NULL),(3453,'mail template','fr',NULL),(3454,'Flow122','fr',NULL),(3455,'Event','fr',NULL),(3456,'Flow127','fr',NULL),(3457,'Users list','fr',NULL),(3458,'useroid','it',NULL),(3459,'Subject','it',NULL),(3460,'Redirect','fr',NULL),(3461,'Download Certificates History','fr',NULL),(3462,'Event','en',NULL),(3463,'Text','it',NULL),(3464,'Seven Days Position','fr',NULL),(3465,'Flow97','fr',NULL),(3466,'View Actions History','es',''),(3467,'10_topMonthly','fr',NULL),(3468,'New reward','fr',NULL),(3469,'Needed points','it','Punti necessari'),(3470,'Action Name','en',NULL),(3471,'Rewards','it','Premi'),(3472,'New action','es',NULL),(3473,'7 Days Message','it',NULL),(3474,'mail template','it',NULL),(3475,'Notification Monthly','en',NULL),(3476,'HTMLMandatory.error','it',NULL),(3477,'Certification Area','fr',NULL),(3478,'community.all.rewards','es',''),(3479,'HTMLMandatory.error','fr',NULL),(3480,'Containers','en',NULL),(3481,'Participation 7 Days','es',''),(3482,'n.User overall','it',NULL),(3483,'06_topMonthly','fr',NULL),(3484,'forum.action.points','es',''),(3485,'Newsletter Events','en',NULL),(3486,'community.possible.reward','en','Your possible reward'),(3487,'Preview','es',NULL),(3488,'useroid','fr',NULL),(3489,'community.spent.points','it','Punti spesi'),(3490,'Needed Points','fr',NULL),(3491,'useroid','en',NULL),(3492,'Text Mail','fr',NULL),(3493,'counter7DaysLeaderboard','en','in 7 days leaderboard'),(3494,'Total Credit','en',NULL),(3495,'Containers','es',NULL),(3496,'ValueLengthValidationRule.blob.max','it','<li>dimensione file massima superata (${length})</li>'),(3497,'community.slider.step1title','it','Iscriviti alla community'),(3498,'Rewards History','en','Rewards History'),(3499,'Credits Spent','es',NULL),(3500,'Flow106','it',NULL),(3501,'From','fr',NULL),(3502,'Flow106','fr',NULL),(3503,'Newsletter Preview','fr',NULL),(3504,'Points','es','Puntos'),(3505,'community.slider.step3p2','it','e servizi concreti'),(3506,'community.slider.step3p1','it','e spendili in prodotti'),(3507,'Facebook','it',NULL),(3508,'Participation Overall','es',NULL),(3509,'Flow120','fr',NULL),(3510,'counterOverallLeaderboard','fr',NULL),(3511,'02_Monthly','fr',NULL),(3512,'Reward','it',NULL),(3513,'Credits Spent','fr',NULL),(3514,'forum.action.points','fr',NULL),(3515,'Creation date','fr',NULL),(3516,'community.available.points','it','Punti disponibili'),(3517,'Participation Seven Days','it',NULL),(3518,'Monthly Points','it',''),(3519,'7 Days Message','fr',NULL),(3520,'Needed points','en',NULL),(3521,'Community Participation Seven Days','fr',NULL),(3522,'Rewards History','es','Regalos Ganados'),(3523,'Seven Days Participation','en',NULL),(3524,'Edit Container','fr',NULL),(3525,'YouGotWebinar','it',NULL),(3526,'community.slider.step1p1','it','ed entra in contatto '),(3527,'community.slider.step1p3','it','di tutto il mondo'),(3528,'community.slider.step1p2','it',''),(3529,'Seven Days Position','es',NULL),(3530,'community.slider.step2p2','it','tra gli sviluppatori'),(3531,'Seven Days Position','it',NULL),(3532,'community.slider.step2p1','it','e guadagna reputazione'),(3533,'Reward Instance','en',''),(3534,'set area','es',NULL),(3535,'community.slider.step1p1','en','and connect'),(3536,'Overall Position','es',NULL),(3537,'community.slider.step1p3','en','around the world!'),(3538,'community.slider.step1p2','en',''),(3539,'Action Name','it',NULL),(3540,'Facebook','es',NULL),(3541,'Alias','es',NULL),(3542,'Users','es',NULL),(3543,'community.slider.step2p1','en','and build your reputation'),(3544,'ValueLengthValidationRule.blob.min','fr','<li>wrong file size (>${length})</li>'),(3545,'community.slider.step2p2','en','among developers'),(3546,'Flow127','it',NULL),(3547,'community.total.points','es','Puntos Totales'),(3548,'Flow122','it',NULL),(3549,'Gamification Area','it',NULL),(3550,'Community Participation Seven Days','en',NULL),(3551,'community.slider.step2title','it','Scala le classifiche'),(3552,'community.slider.step3p1','en','and use them for products'),(3553,'community.slider.step3p2','en','and concrete services'),(3554,'Footer','en',NULL),(3555,'ValueLengthValidationRule.blob.eq','es','<li>wrong file size (${length})</li>'),(3556,'09_Monthly','fr',NULL),(3557,'community.comingsoon','it',NULL),(3558,'HQ.rank.7Days.home','it','Classifica degli Ultimi 7 Giorni'),(3559,'Flow120','it',NULL),(3560,'Actions History','en','Actions History'),(3561,'community.slider.step3p1','fr',NULL),(3562,'community.slider.step3p2','fr',NULL),(3563,'Iso Code','fr',NULL),(3564,'LeaderBoards','fr',NULL),(3565,'`key`word','es',NULL),(3566,'community.slider.step1p2','fr',NULL),(3567,'community.slider.step1p1','fr',NULL),(3568,'community.slider.step1p3','fr',NULL),(3569,'Go Back','es',NULL),(3570,'community.slider.step2p1','fr',NULL),(3571,'Reward Title','es',NULL),(3572,'community.slider.step2title','es',NULL),(3573,'Gamification Area','en',NULL),(3574,'community.slider.step2p2','fr',NULL),(3575,'Actions','en',NULL),(3576,'HQ.rank.7Days.home','fr',NULL),(3577,'ValueLengthValidationRule.blob.neq','it','<li>dimensione file non permessa (${length})</li>'),(3578,'New Container','en',NULL),(3579,'Flow150','it',NULL),(3580,'YouGotClassroom','en','You got points to have attended the classroom training:'),(3581,'set area','it',NULL),(3582,'Header','fr',NULL),(3583,'New action','en',NULL),(3584,'n.User 7 Days Total','fr',NULL),(3585,'Photo','es',NULL),(3586,'Leaderboards','es',NULL),(3587,'HQ.Ranking7Days','en','7 Days Leaderboard'),(3588,'Notification LeaderBoards','es',NULL),(3589,'ValueLengthValidationRule.blob.neq','fr','<li>file size not allowed (${length})</li>'),(3590,'Footer','it',NULL),(3591,'Text Mail','it',NULL),(3592,'Status','es',NULL),(3593,'Newsletter Area','es',NULL),(3594,'Redirect','es',NULL),(3595,'Flow148','it',NULL),(3596,'Available','es',NULL),(3597,'New Container','es',NULL),(3598,'Flow143','it',NULL),(3599,'Participation Overall','en',NULL),(3600,'Actions History','it','Tutte le Azioni'),(3601,'Manage Rewards','en',''),(3602,'Text','en',NULL),(3603,'Attention','es',NULL),(3604,'HQ.rank.7Days.home','es','Clasifica de los Ultimos 7 DÃ­as'),(3605,'Reward Type','fr',NULL),(3606,'01_topMonthly','fr',NULL),(3607,'Reward','es',NULL),(3608,'Mails','es',NULL),(3609,'community.all.rewards','en','All rewards that you can riceive:'),(3610,'Credits Available','it',NULL),(3611,'Code','es',NULL),(3612,'View Rewards History','it',''),(3613,'Monthly Points','fr',NULL),(3614,'Useroid','es',NULL),(3615,'community.spent.points','es','Puntos Gastados'),(3616,'Monthly Points','es',''),(3617,'Assign new reward','en',''),(3618,'Notification LeaderBoards','fr',NULL),(3619,'n.User overall','fr',NULL),(3620,'Mail','fr',NULL),(3621,'Upload Actions File','it',NULL),(3622,'Community Users Area','en',NULL),(3623,'Body','fr',NULL),(3624,'Seven Days Participation','it',NULL),(3625,'YouGotClassroom','it',NULL),(3626,'View Rewards History','es',''),(3627,'Photo','en',NULL),(3628,'Credits Available','en',NULL),(3629,'noRewards','es',''),(3630,'Newsletter Events','es',NULL),(3631,'Actions History','fr',NULL),(3632,'n.User overall','en',NULL),(3633,'09_topMonthly','fr',NULL),(3634,'Needed Points','es',''),(3635,'Presences','es',NULL),(3636,'Edit Mail','it',NULL),(3637,'Alias','it',NULL),(3638,'Action','it',NULL),(3639,'Rewards Area','it','Info Premi'),(3640,'12_Monthly','fr',NULL),(3641,'Language Code','es',NULL),(3642,'Users list','es',NULL),(3643,'Go Back','en','Go Back'),(3644,'06_Monthly','fr',NULL),(3645,'Possible Rewards','fr',NULL),(3646,'Action Name','fr',NULL),(3647,'Subject','es',NULL),(3648,'Rewards','es',''),(3649,'To','es',NULL),(3650,'Body','en',NULL),(3651,'community.total.points','fr',NULL),(3652,'Total Credit','es',NULL),(3653,'community.slider.step1title','es',NULL),(3654,'noRewards','it',''),(3655,'History points','es',''),(3656,'Notification LeaderBoards','it',NULL),(3657,'Delivery date','it',NULL),(3658,'community.comingsoon','es',NULL),(3659,'Rank 7 Days','fr',NULL),(3660,'History points','it',''),(3661,'Participation Seven Days','en',NULL),(3662,'Photo','it',NULL),(3663,'Language Code','fr',NULL),(3664,'Action','en',NULL),(3665,'userDash','fr',NULL),(3666,'Events','en',NULL),(3667,'Flow97','es',''),(3668,'Add reward','es',NULL),(3669,'userDash','it',NULL),(3670,'Possible Rewards','en',''),(3671,'Needed points','es',''),(3672,'Google','fr',NULL),(3673,'Users','it',NULL),(3674,'Manage Containers','en',NULL),(3675,'LeaderBoards','es',NULL),(3676,'counterOverallLeaderboard','it','nella classifica generale'),(3677,'07_topMonthly','fr',NULL),(3678,'Next Rewards','it',''),(3679,'set area','fr',NULL),(3680,'Events','it',NULL),(3681,'community.comingsoon','en','Coming soon'),(3682,'Notification','it',NULL),(3683,'05_Monthly','fr',NULL),(3684,'Delivery Date','fr',NULL),(3685,'Credits Available','es',NULL),(3686,'File Exel','es',NULL),(3687,'Availability','es',NULL),(3688,'Header','es',NULL),(3689,'Google','en','Google+'),(3690,'Google','it',NULL),(3691,'Edit Mail','fr',NULL),(3692,'Certification Area','en',NULL),(3693,'Action','fr',NULL),(3694,'Seven Days Participation','fr',NULL),(3695,'Text','es',NULL),(3696,'Presences','it',NULL),(3697,'Availability','fr',NULL),(3698,'7 Days Message','es',''),(3699,'Users list','en',NULL),(3700,'Mails','it',NULL),(3701,'Delivery date','fr',NULL),(3702,'Container','it',NULL),(3703,'04_Monthly','fr',NULL),(3704,'LeaderBoards','en',NULL),(3705,'ValueLengthValidationRule.blob.max','en','<li>maximum file size exceeded (${length})</li>'),(3706,'Credits Spent','en',NULL),(3707,'Add container','fr',NULL),(3708,'Flow148','es',NULL),(3709,'Gamification Area','fr',NULL),(3710,'Notification','fr',NULL),(3711,'Flow143','es',NULL),(3712,'ValueLengthValidationRule.blob.neq','en','<li>file size not allowed (${length})</li>'),(3713,'Header','it',NULL),(3714,'Flow150','es',NULL),(3715,'Rewards History','it','Premi Acquisiti'),(3716,'Participation Seven Days','fr',NULL),(3717,'ValueLengthValidationRule.blob.max','fr','<li>maximum file size exceeded (${length})</li>'),(3718,'Possible Rewards','es',''),(3719,'Edit Mail','es',NULL),(3720,'mail template','es',NULL),(3721,'Community Users Area','es',NULL),(3722,'Reward Type','it',NULL),(3723,'From','it',NULL),(3724,'Available','it',NULL),(3725,'community.spent.points','fr',NULL),(3726,'n.User overall','es',NULL),(3727,'File Exel','fr',NULL),(3728,'Text Mail','es',NULL),(3729,'Upload Actions File','en',NULL),(3730,'Text Mail','en',NULL),(3731,'From','en',NULL),(3732,'Newsletter Area','en',NULL),(3733,'Language Code','it',NULL),(3734,'HTMLMandatory.error','en',NULL),(3735,'community.slider.step2p1','es',NULL),(3736,'Download Certificates History','es',NULL),(3737,'Preview','fr',NULL),(3738,'community.available.points','en','Available Points'),(3739,'From','es',NULL),(3740,'Edit reward','it',NULL),(3741,'Reward Instance','it',NULL),(3742,'Rewards History','fr',NULL),(3743,'Error','fr',NULL),(3744,'Reward Title','en',''),(3745,'user oid','en',NULL),(3746,'set area','en',NULL),(3747,'community.total.points','it','Punti totali'),(3748,'community.all.rewards','it',''),(3749,'Rewards Area','fr',NULL),(3750,'Needed points','fr',NULL),(3751,'Presences','en',NULL),(3752,'community.available.points','es','Puntos Disponibles'),(3753,'Slider','es',NULL),(3754,'Error','en',NULL),(3755,'Header','en',NULL),(3756,'Executor','it',NULL),(3757,'counterOverallLeaderboard','en','in overall leaderboard'),(3758,'Go Back','it','Indietro'),(3759,'Participation Seven Days','es',NULL),(3760,'Rewards Area','es',''),(3761,'community.slider.step3title','it','Guadagna punti'),(3762,'community.slider.step2title','en','Rise leaderboards'),(3763,'community.possible.reward','it',NULL),(3764,'Attention','it',NULL),(3765,'File Exel','en',NULL),(3766,'Assign new reward','it',NULL),(3767,'Edit Container','en',NULL),(3768,'Add container','es',NULL),(3769,'Mails','fr',NULL),(3770,'noRewards','fr',NULL),(3771,'Community Participation Seven Days','es',NULL),(3772,'userDash','es',NULL),(3773,'Newsletter Preview','es',NULL),(3774,'counterOverallLeaderboard','es','en la clasifica general'),(3775,'Executor','en',NULL),(3776,'n.User 7 Days Total','it',NULL),(3777,'ValueLengthValidationRule.blob.eq','it','<li>dimensione file errata (${length})</li>'),(3778,'Newsletter Area','it',NULL),(3779,'ValueLengthValidationRule.blob.eq','fr','<li>wrong file size (${length})</li>'),(3780,'community.all.rewards','fr',NULL),(3781,'04_topMonthly','fr',NULL),(3782,'Leaderboards','it',NULL),(3783,'user oid','it',NULL),(3784,'View Actions History','it',''),(3785,'community.slider.step3title','en','Gain points'),(3786,'07_Monthly','fr',NULL),(3787,'View Rewards History','en',''),(3788,'community.slider.step3p1','es',NULL),(3789,'community.slider.step3p2','es',NULL),(3790,'Insert Actions','es',NULL),(3791,'Ok','es',NULL),(3792,'New Container','fr',NULL),(3793,'Assign new reward','es',NULL),(3794,'03_Monthly','fr',NULL),(3795,'New action','fr',NULL),(3796,'Next Rewards','fr',NULL),(3797,'Points','fr',NULL),(3798,'01_Monthly','fr',NULL),(3799,'Body','it',NULL),(3800,'Upload Actions File','es',NULL),(3801,'Help','fr',NULL),(3802,'Overall Position','it',NULL),(3803,'Add reward','en',''),(3804,'Slider','it',NULL),(3805,'HQ.Ranking7Days','it','Classifica degli Ultimi 7 Giorni'),(3806,'Subject','en',NULL),(3807,'Useroid','it',NULL),(3808,'Credits Spent','it',NULL),(3809,'Error','it',NULL),(3810,'Body','es',NULL),(3811,'New Reward','fr',NULL),(3812,'community.total.points','en','Total gained Points'),(3813,'HQ.rank.7Days','es','Clasifica de los Ultimos 7 DÃ­as'),(3814,'Mail','it',NULL),(3815,'email','it',NULL),(3816,'New Reward','it',NULL),(3817,'Users list','it',NULL),(3818,'Action','es',NULL),(3819,'user oid','es',NULL),(3820,'My Points','fr',NULL),(3821,'community.slider.step1title','fr',NULL),(3822,'To','en',NULL),(3823,'New reward','en',''),(3824,'Gamification Area','es',NULL),(3825,'HQ.Ranking7Days','es','Clasifica de los Ultimos 7 DÃ­as'),(3826,'Event','it',NULL),(3827,'7 Days Message','en',NULL),(3828,'Rewards','en','Rewards'),(3829,'Creation date','en',NULL),(3830,'Slider','en',NULL),(3831,'useroid','es',NULL),(3832,'Slider','fr',NULL),(3833,'Reward Instance','fr',NULL),(3834,'Reward','fr',NULL),(3835,'Edit reward','fr',NULL),(3836,'Seven Days Position','en',NULL),(3837,'community.possible.reward','es',NULL),(3838,'Containers','fr',NULL),(3839,'View Actions History','en',NULL),(3840,'Add reward','it',NULL),(3841,'community.available.points','fr',NULL),(3842,'Rewards','fr',NULL),(3843,'Insert Actions','en',NULL),(3844,'Points','it','Punti'),(3845,'Edit Container','es',NULL),(3846,'Rank 7 Days','it',NULL),(3847,'Certification Area','it',NULL),(3848,'Download Certificates History','it',NULL),(3849,'Container','en',NULL),(3850,'View Rewards History','fr',NULL),(3851,'08_Monthly','fr',NULL),(3852,'Creation date','it',NULL),(3853,'Containers','it',NULL),(3854,'Certification Area','es',NULL),(3855,'n.User 7 Days filtered','fr',NULL),(3856,'Assign new action','es',NULL),(3857,'Add container','it',NULL),(3858,'Rank 7 Days','en',NULL),(3859,'ValueLengthValidationRule.blob.min','en','<li>wrong file size (>${length})</li>'),(3860,'Newsletter Events','fr',NULL),(3861,'Redirect','it',NULL),(3862,'Notification Monthly','es',NULL),(3863,'HQ.rank.7Days','en','7 Days Leaderboard'),(3864,'Participation 7 Days','en',NULL),(3865,'Flow97','en',NULL),(3866,'Actions','it',NULL),(3867,'`key`word','it',NULL),(3868,'Mail','en',NULL),(3869,'To','fr',NULL),(3870,'Availability','en',NULL),(3871,'community.slider.step2p2','es',NULL),(3872,'community.slider.step3title','fr',NULL),(3873,'New action','it',NULL),(3874,'Reward Title','fr',NULL),(3875,'email','es',NULL),(3876,'My Points','es',''),(3877,'Available','fr',NULL),(3878,'Reward Instance','es',NULL),(3879,'Total Credit','it',NULL),(3880,'Code','it',NULL),(3881,'New reward','es',NULL),(3882,'n.User 7 Days Total','en',NULL),(3883,'Insert Actions','fr',NULL),(3884,'Go Back','fr',NULL),(3885,'Help','it',NULL),(3886,'Reward Title','it',NULL),(3887,'Manage Rewards','fr',NULL),(3888,'Community Users Area','fr',NULL),(3889,'user oid','fr',NULL),(3890,'File Exel','it',NULL),(3891,'Notification Monthly','fr',NULL),(3892,'02_topMonthly','fr',NULL),(3893,'Subject','fr',NULL),(3894,'Manage Containers','it',NULL),(3895,'Flow148','en',NULL),(3896,'Code','fr',NULL),(3897,'05_topMonthly','fr',NULL),(3898,'Events','es',NULL),(3899,'Assign new action','it',NULL),(3900,'ValueLengthValidationRule.blob.max','es','<li>maximum file size exceeded (${length})</li>'),(3901,'community.slider.step1p1','es',NULL),(3902,'Flow150','en',NULL),(3903,'community.slider.step1p2','es',NULL),(3904,'New Container','it',NULL),(3905,'Attention','en',NULL),(3906,'Notification Monthly','it',NULL),(3907,'counter7DaysLeaderboard','fr',NULL),(3908,'Notification','es',NULL),(3909,'03_topMonthly','fr',NULL),(3910,'YouGotClassroom','es',NULL),(3911,'Upload Actions File','fr',NULL),(3912,'Participation 7 Days','it',NULL),(3913,'My Points','it',''),(3914,'community.slider.step1title','en','Join the Community'),(3915,'12_topMonthly','fr',NULL),(3916,'Flow120','es',NULL),(3917,'Flow143','en',NULL),(3918,'Flow122','es',NULL),(3919,'11_topMonthly','fr',NULL),(3920,'Possible Rewards','it',''),(3921,'To','it',NULL),(3922,'Assign new action','fr',NULL),(3923,'Newsletter Preview','en',NULL),(3924,'Facebook','en','Facebook'),(3925,'Flow127','es',''),(3926,'HQ.rank.7Days','fr',NULL),(3927,'YouGotWebinar','fr',NULL),(3928,'userDash','en',NULL),(3929,'HTMLMandatory.error','es',NULL),(3930,'Google','es',NULL),(3931,'Participation Overall','fr',NULL),(3932,'Redirect','en',NULL),(3933,'Next Rewards','es',''),(3934,'Iso Code','en',NULL),(3935,'Edit Container','it',NULL),(3936,'counter7DaysLeaderboard','it','nella classifica degli ultimi 7 giorni'),(3937,'Attention','fr',NULL),(3938,'Participation Overall','it',NULL),(3939,'Delivery date','en',NULL),(3940,'Status','it',NULL),(3941,'YouGotWebinar','es',NULL),(3942,'n.User 7 Days Total','es',''),(3943,'Ok','fr',NULL),(3944,'View Actions History','fr',NULL),(3945,'Photo','fr',NULL),(3946,'Footer','fr',NULL),(3947,'email','fr',NULL),(3948,'Reward Type','en',''),(3949,'My Points','en','My Points'),(3950,'Actions','es',NULL),(3951,'11_Monthly','fr',NULL),(3952,'Useroid','fr',NULL),(3953,'History points','en','Points & Rewards'),(3954,'Credits Available','fr',NULL),(3955,'Iso Code','it',NULL),(3956,'10_Monthly','fr',NULL),(3957,'Users','en',NULL),(3958,'Overall Position','en',NULL),(3959,'community.slider.step1p3','es',NULL),(3960,'Mails','en',NULL),(3961,'Mail','es',NULL),(3962,'Needed Points','it','Punti necessari'),(3963,'Seven Days Participation','es',NULL),(3964,'Flow106','es',NULL),(3965,'Help','en',NULL),(3966,'forum.action.points','en','Answer at this question and get 70 points!'),(3967,'Download Certificates History','en',NULL),(3968,'noRewards','en','No Rewards'),(3969,'Badge','en','Badge'),(3970,'See Your Rewards','en','See Your Rewards'),(3971,'View Reward','en','View Reward'),(3972,'Reward Details','en','Reward Details'),(3973,'cannotGetRewards','en','You don\'t have enough points to get this reward'),(3974,'Needed Points','en','Needed Points'),(3975,'Get Reward','en','Get Reward'),(3976,'forum.goToDashboard','en','Go To User\'s Dashboard'),(3977,'English','en','English'),(3978,'Italian','en','Italian'),(3979,'English','it','Inglese'),(3980,'Italian','it','Italiano'),(3981,'cannotGetRewards','it','Non hai abbastanza punti per prendere il premio'),(3982,'community.possible.reward','it','Ecco un premio che puoi ottenere'),(3983,'See Your Rewards','it','Guarda i premi');
/*!40000 ALTER TABLE `bundle_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `common_data`
--

DROP TABLE IF EXISTS `common_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `common_data` (
  `oid` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `area` varchar(255) DEFAULT NULL,
  `hd_image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `common_data`
--

LOCK TABLES `common_data` WRITE;
/*!40000 ALTER TABLE `common_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `common_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `community_user`
--

DROP TABLE IF EXISTS `community_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `community_user` (
  `oid` int(11) NOT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `forum_level` int(11) DEFAULT NULL,
  `small_photo` varchar(255) DEFAULT NULL,
  `twitter` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `public_profile` tinyint(1) DEFAULT NULL,
  `geographical_area` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `big_photo` varchar(255) DEFAULT NULL,
  `bio` text,
  `linkedin` varchar(255) DEFAULT NULL,
  `certification_level` int(11) DEFAULT NULL,
  `kb_level` int(11) DEFAULT NULL,
  `store_level` int(11) DEFAULT NULL,
  `participation_monthly` decimal(19,2) DEFAULT NULL,
  `forum_badge` varchar(255) DEFAULT NULL,
  `certification_badge` varchar(255) DEFAULT NULL,
  `kb_badge` varchar(255) DEFAULT NULL,
  `store_badge` varchar(255) DEFAULT NULL,
  `kb_badge_title` varchar(255) DEFAULT NULL,
  `store_badge_title` varchar(255) DEFAULT NULL,
  `forum_badge_title` varchar(255) DEFAULT NULL,
  `certification_badge_title` varchar(255) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `participation` decimal(19,2) DEFAULT NULL,
  `credit` decimal(19,2) DEFAULT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `google` varchar(255) DEFAULT NULL,
  `iso_code` varchar(255) DEFAULT NULL,
  `small_photo_2` varchar(255) DEFAULT NULL,
  `small_photoblob` longblob,
  `big_photo_2` varchar(255) DEFAULT NULL,
  `big_photoblob` longblob,
  `registration_date` datetime DEFAULT NULL,
  `latitude` decimal(19,6) DEFAULT NULL,
  `longitude` decimal(19,6) DEFAULT NULL,
  PRIMARY KEY (`oid`),
  CONSTRAINT `fk_rank_usertable` FOREIGN KEY (`oid`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `community_user`
--

LOCK TABLES `community_user` WRITE;
/*!40000 ALTER TABLE `community_user` DISABLE KEYS */;
INSERT INTO `community_user` VALUES (2,NULL,'Luca','Galli','Notting Hill, London',NULL,NULL,NULL,'Italy',1,'Europe',NULL,NULL,NULL,NULL,NULL,NULL,NULL,5400.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2086-02-14',5400.00,5400.00,NULL,NULL,'en','LucaGalli_p3Nj_small.jpg','����\0JFIF\0\0\0\0\0\0��\0C\0    		\n\r

\Z\Z $.\' \",#(7),01444\'9=82<.342��\0C			
\r\r2!!22222222222222222222222222222222222222222222222222��\0\0@\0@\"\0��\0\0\0\0\0\0\0\0\0\0\0 	\n
��\0�\0\0\0}\0!1AQa \"q2���#B��R��$3br�	\n\Z%&\'()*456789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz���������������������������������������������������������������������������\0\0\0\0\0\0\0\0 	\n
��\0�\0 \0w\0!1AQ aq\"2�B����	#3R�br�\n$4�%�\Z&\'()*56789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz��������������������������������������������������������������������������\0\0\0?\0�9�����i��i/�0 �;�\0ס$�@[��^_A�i�7S�.� �r\0�,W�+[8�i4W.Y� o�x�H��\\4�)�6�L�=놽��5˥g\r��\0��8P3ںK/�:��_h��n�t����[�\'-����Z-���0�>j㿨ȭO]F<)vѺ��NC)�?�����hӋ�D��K|�������}cDў��$��l��!��\'m4�5#�E;�8�z����Č|��9$��xҹ�\Zȫ��#;t��\0]ts-����\0�\Z�+
mB�\0L��%�7��8܌��j���s;w�k��\0	��2�@#<g�_OX�S�eH���H𝮙�X�Jc��΄�q�#�?�j�nn5>70�4�)p�����#?�o�s��3��yS7�K���X#���W!��2
�\r���>E.�t#��t�F��������i�y<�+��ψ�I�\"�o�,�K �<n�
���:T%w������RY�\nr�� �f�\'�4v��q����xw�Z�5�ɨ�-��8�\Z ��^{U�KV2N�п�O��u\'�>��\\ӌH�OzI\r�]]�g�n|Q�&�.����#u����}Z�q$fd_��xׂt�ӴKmZ5f�\'f�T}�`�?^��6:���l�20� ��,ֺt��写c�aX���pn���	<\n��\\���G ���}6�]~�iv���b˂Ap?1^�=}u]B;h}��h���G�\'�q��\0^�#vi^v���%����T-j�2$Fǡ�(;�������!�y�%�r��R�\npEP����1��������*
�0�X��v�3��e�n�[B�}��&?ƾ�Q�����]\"x$�E��dݹ�G9�����m����.mY�hX2���_F��W�<�w!���=�Rs�2�RG��3X�6wFђj�O��Z����<v��S��K;g�ѷ�\'��Ѧ�G������WZ�����V����>�֓N��y#�:��6DNWb� ���_z̽�C�|���ǭt�T�#�P�w
�9�Ր��','LucaGalli_p3Nj_big.jpg','����\0JFIF\0\0\0\0\0\0��\0C\0    		\n\r

\Z\Z $.\' \",#(7),01444\'9=82<.342��\0C			
\r\r2!!22222222222222222222222222222222222222222222222222��\0\0�\0�\"\0��\0\0\0\0\0\0\0\0\0\0\0 	\n
��\0�\0\0\0}\0!1AQa \"q2���#B��R��$3br�	\n\Z%&\'()*456789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz���������������������������������������������������������������������������\0\0\0\0\0\0\0\0 	\n
��\0�\0 \0w\0!1AQ aq\"2�B����	#3R�br�\n$4�%�\Z&\'()*56789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz��������������������������������������������������������������������������\0\0\0?\0n���I�)�pŽ�������=0��O�\'�5r��Y��n�����^��ˊ������Z������(m�WoWcW��Kc��5F��=�{9�
A���z!�#&��O��j�j�4����G��ﱭ^�\0=v�?�3��!�٭��M�����q�ؽ��Ҵ�#�#�#\ZCem��������_֪iwV���\ZU��*�w>���p�}�\r\rC�jX�CX9���\0����sjL���Z���{���LH���A�\rm/_�1f��~��F��\';b��X�B	؏�Hx土ְ�I1��\'�a�\n�m��m��f�jM�%����\0Jd�=#n��Ԛm)�i*�֝�j����\0�8�JzP��)\\v$�\r��QE�c��S�f>�T����}!�5S�%Ǫ�֥������@/r�4l��2j�\'�}ݪ�a#���*��l���\Z:�>�Z1�����j��X��\0\\��	~Ka��s�P&��������0�h��,B�^T��̑�E\n�1�/>�+Gg��n��@^��εi��3�	T (�5��1���8a_,*��-,�L�v,i�*i�r��\\I�J��F)��;����D�3)��DX�֞��8��6ZW�\0��\\�^����~�5��:���t�Q�@ih-N����v��mx�(����n���Ȣe�YH\n�s��=�>���\Z\'�3��@E�BGE��p�\'���8޿��/��qq\rc|��M�\'q�j����r% ;����3��ъ;�\Z�G\'Zw�R/L��_� ��4���抌�4Q`�캈�\r�򨥘��O��
��_�� ��~�?6�)�)�KܤY�;\"�>��T���m�\0aEZ�8��>�\n�w�v����O �e/�U�JT���G!U �&�ˁh��dQ\\g���\'�m6\'�@	0~���:�λ%�bb-�\0\\w�Vl�rD�0\n	c�T����js����$�F4H��\0Nx�h[i��.�\'�5��w���u��?w�ݪ%;\"�
�f�f&a).O\\t��4�.����#�3��{ [��^i \n����u\rO�ݕ��M۷m�����\n���Z����8Xpv>��\0�X`������_Ү�um\n1�$�N@=�Yt�\"]����ֵ���1��1^<��4�.\0�o\'�z׋K�����g�}�t\ZN��7�����hsH3�!�d����ٿ՛PТ���x߿�vZ�\0�\Z{b� YTp;W��2����.��*�5!N�\\�\Zs1�kC\"E-;�0}�^q@���Թ��3J��f�	��=����m��dS�\'�K���\0Z�t�e��r�IǥX����z)j��ߜ�7�R�P\\|�(=��n����Q?�u�rh=��VA�0&�\n�
ٞ��Iܒ�9bO�5�����uy��6�f�,}�ȓ�հ=�h{������8⡼�b4t⡉��j\Z��A����%T��ǩ��c�좎�$U\n��^��2w9� ��L�t	��sT�餬j�`�Sn,��0�ԁ�>�aYL\n���\'7��S$>\\m�l7��_�PxJ�K��h�\08N�v��-\\:z�r@�q�|U4�M\\�?�����?\n��ij�U<�u\'ֺ2�\0�֫J 5�l�b�Z���^E�
BT?n�1��`��{\\����k���H��*����F��9�\00�j�lj��H��o��8�ػ��D��A�Ss�+*�!nM(�Ӂ�0��(ص�t^�X����t?��MG���a�4�no�\0vRX��͢��4�����i��\0�Ű�Bi��ױ�FMЭ�Y<��=�\0Ƽ�k��\\����^\'M�*�`���q��Z�N��@J�#@z��ϥ\r��\Z���9]�\'�j���`c�Mk]�Ri�(�X�F9r=qP��a�6�N���\r;3{��]�F�c�ɯk�m�I8�5���ʉ3�?*�5��,Q�U�s��t�Dm�8e�j�5e�ʱ��0�V⺊O��}j��|1�#H���%]�s�:Ԍ�\rS��ȉ�sPK��#�9�e����H�!8 �V�\Z�M���M��Չ�	O�4��)5c��ٌ&�.�I�5�皹���$�0�MS��:[�h-��\rQ#{QE\0����z(�����\0L��>��>.O�L���tc#Νh~k��Ҋ�RINo��sE�7���4K�������&�� &�����#��Ӽ^�vk����}���m��v�����TjZ�wb�x�3QQhm�v�w�:<cf�?���\0�����^Y5�1j~D\n9l���4�0�iJ�����9�GP�\Zo��?0Y�� ��;\ZVW��Z��J�p�qS\\<�%$�֭Y\'�k\Z񐸫�����}�&Ƒ��:��Xq�n�IK����}OJ][O���[]� ����\0\n���6�a�U[���I4��\\�`���<U�]�$�mc�a�d�X8?q�8��\rR��*���_Z�t�Z:������(�Mv��$%�;稤�gb�]��p\n��Sf�XӖ(�c�|W\'���;(�*��e ����7�������M�k��� ��\'�K�v(q�#|�Qx�kzmзҒO�d~���03�y 9�Y�z��Z����QmI[հ1�Zr�&y���ԇ �>����sϩ���q�/zkS��@��;�@�P ��G:�DQN��L�6*;ӻS��ʊ~�ʮ�Rj�+�7g�V����?AP�w�9��U[�M4�
ɉV������O�j6�3��@?rB�����E�şf+�+��\\��9ָk�����k�,�j$w��h\\��ۑ��#�hq���S�W>��\0m��%e(��o��֣+�뙉\0�)���K��f���A�X��\0$���� ��U�����@�H��p��n@�`�㹼䥪=R�BR!ܨ5���W5����џC],R\r��i\\�;6��@j��08�c�&�� �R�{QcS`R~�>��.Χ֮4�����2\\�HB��h�hZ���`jy�UG#\"D�kb	
� {U\r~��>pU�WBZ\Z\"ܘJ�0���k �/�,��t��עA�#�޸���<7\"�jʠ~9���0����C�)ݩV\'l�M)F�k��Mn����5u�B�)h� Rgފ\0���\rw;�@ �Ri�FO�\Z�C���eo�S�[l��j\r.X�#`bp<����^��P}��@�WAw��n���s���>���\\���U$Kd`��H́�FI�L~�zb+�d��]ޝr��Ekxʱ�Gú�R�gӴ)�r���ޢf�=�L@�E�p�f���8�\\߆�>ѦD{�����q����б	9��D�8����!\0�zV|���bFr@�I|�i\"	c�=j�G�fP�юC��4�ڕ��!��7��?�Xu��|�\'٪�Q����\r����u1�L�\0J��WV��;�m� �O���vRܷ���[����ki�i��ys�.r �S�����_.�;���^\'�\']:����#��������\Z���:4�ƙl���?J�+������C�%c!�MU(�r֩}�H�Q�T����I>�Pt�\0\Zr7�v�����ZH���Y-s����s�M���pq�Q-���)53[ Ri�j@)�z�
�R�c�� U�e!9\'4S
��F`�b�k��u�D[;w�8�B�%�\"H,UXn�>��\Zwa�[�I\ZI�m�b3��Y�#05vհ�}�j���7S�TfVO����YA<�ST�̠~4����B���w1[��`�q������!\r�X�4o�Ey熂\r\\�ѫ�����q�~�\"������L�g��5�%���{��E��\'�c�Wu�d ���\\���n���<�H2Ƽ�܊��5��B( �߽CW)=N�\\\Z��9��\r�17nN8��܉S �3Y�FE=CH�]Ң.O^f��P�[sE ?w9WR��׃T/��e;МzP��ղ������A��q�����Sr��Rp�ؚ�N�n�X��� �s^-ա���� J�\0,j=j����Y[c����E�+�ڷ������c�ڒi乙摉�C����!Ty��،y8�ާ�m�{�j�� �5f���jЂ�q�\Za�;pn �=���y8>�2�~lT�a �*!�$g��\\��H#@%�5,eQ�ڊ7EP�1�m$_�\\���S�q\\~읽9����2Α�����zUe9��Qrɠ��ds��#�9����ܬ��b�L>n8�?��x���m$b�1���R��U��|�;(Q�ȯg��˅ \n���w��%C�ІS�k��#����%�d\\,�=�ְ��������ʕ��Ҹ�CH����g�r��>��\\E�7.r:c���L�nb��`��a�z^���#a�}��+r�R;��@�ޱ5?�x�H$�n��~~�����:��jˏ�Ua\\�(/6�ї��V��L�z�<Gcr�ȺB�p�pzzU��$1�H�����j\\K�͍N�[�\'�|�\0�-e��y�81A�ǯz��_���m\r������95��;e��z�p��ʥK��@�p�u4�����1�����7�U��ܹbs�Q���H8��ф�(�!�\Z�u3�\0;\\U��# ~]УoȦ#6o:Q��/�&
��V�֑�[Yx��ޕ�AV �A���?��J)���:˝Q.c�v�9���0����?*��0��*?�<LeI �ϡ�Q�O�*z�Uք�J��lr8�@�nb� x=�p@#�J�F� d���j��s�jC�z�ֺ
���C�bq+�m߼�ta�5�c=�iT� �ړW);l}9����Y�<��H�EE=�3��v�/�o�v���.�lA &\rݛ�?\Z�?<3s�W;V:�+��Wޏ��y�Pə#o61� �\0��lu\0~��\'h�p������\"ڤ/��T����m$������\'7���y�s�>#�F��8�Sx���}�-[�4���x��J��~ǧǗQ��f� ��t�n:i^i�Y���Kǩ�����Ҳ9���b1�֟%�(S����гԞշcm�_�=�*�Kml!P:��\Zs(m��O����M	��S\r��5��8�Z)��1��t��͛I88�*��\n�y����Z;~q�y�e_&��nP������{�P�)���ƊB7�ZR1ȨVC��H�Ny�0�&0z�C �,��T����`�*��G_zP�
���,8^2S�ҵ��V^�� j\0�h��9��v#5�,9�a=��V1�w�)�\"v��F� 9��X�F����8��7��%* Ǳ�IVC�9sS(ܸɦ}��ck��j+��d\\nۚ�ZN��j��ve�eU�>�{Wc��V>����Y�\'lp#��þk��2j�jC@Ѧ��S�Ƅ�c��W�^�Myu,�y���OZ��?�g�5����j�p�s���׀\0ɭaNW*�G ڕF\0dյ�<��8�$ArB��Ё��OZՄn�J���*�`Fq@��E(��q���)F�w��dE<���@�
���1��c�W\0c�\0Tb���NrXn={U�7�c�A�5Q�;�zP���\"����(�#2(!G��S��T�F3E\r��za�=h��\'O��y=9�X\r� }h��+H�F���O���Z���W?Z�4Q�ݰ�(��\r�wn���\\���x�g�}�bWdRYP�}>�����\\~���LT*84QZ-���@7�����P2�(�OqR����E\ZN���\0)p94Q@(��l\\w��A0�0�$+u�����E\0�d\rNz�E��','2014-10-21 17:12:51',0.898862,-0.003374),(3,NULL,'Chiara','Pasini','Notting Hill, London',NULL,NULL,NULL,'Italy',1,'Europe',NULL,NULL,NULL,NULL,NULL,NULL,NULL,2550.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2085-11-04',2550.00,2550.00,NULL,NULL,'en','ChiaraPasini_u7RP_small.jpg','����\0JFIF\0\0\0\0\0\0��\0C\0    		\n\r

\Z\Z $.\' \",#(7),01444\'9=82<.342��\0C			
\r\r2!!22222222222222222222222222222222222222222222222222��\0\0@\0@\"\0��\0\0\0\0\0\0\0\0\0\0\0 	\n
��\0�\0\0\0}\0!1AQa \"q2���#B��R��$3br�	\n\Z%&\'()*456789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz���������������������������������������������������������������������������\0\0\0\0\0\0\0\0 	\n
��\0�\0 \0w\0!1AQ aq\"2�B����	#3R�br�\n$4�%�\Z&\'()*56789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz��������������������������������������������������������������������������\0\0\0?\0·��-g�x�c9�u��Ӧ3���krf��:�@�d�=?<f���Ɵf�0
4�,���=\0#5�I��n]8_W���/ZM=��
J-��a��~�ιk�\\\\���4�8�����+&]�皙tٮ��@�1S���w�-��&۱�q�����[:V�Wp���up:g 5�<?� ����9��2�\n1l�}F	����W<���m�[����]r_�\\���m�K%�{h<������:/N����W�����
,nc�{���\0�V��S[D^�q2l\'\n(�k̗g��E����Dp�噲2��� ך�5�纼;����y?�5�su�O1=�����9�m�$\0	��������D�M��^|��k�4�
h�JF:W�[�K�=���K�c���I�>g7wc��Tc���,C��\"G�*�݄S�^`z�Pk*���F�V�X��{�_b1�sW�/�K(�[������j�c��\'�C\\Y���ŞҸؕ�E�6���-�\ZG$I,鼖�N}��7s���R�/5w2�@GZ�2���N��D_ˑ�t,�?����-.-���T�T��v�+�IDZv�o�׀OL��>�$pC<,U�n�����Uk��3�ΧI�M6y +�-�Ϩ���5��5�X��{�!��2g) 8������X�]\r޹�z��g�4嶎0ۆ�8����)TV�����V?�U�\\��2Ľ��J���$/F�8����s:���j Rs�V��Id�#��q:ƣ����p��O����n�j�QK���/!,s��%��B�N��T$ԋ�`��+�#̹���jڿ�\'����x�<y���+���� ������ף$k5�2�+\Z�S���dw��3$lW��,��\n\n�rOm��ά��F1޳��GL��{\n�+�����k�\"���1F0�R;�6�y�1���RjS�����@�T�l�T�w��95�dp�NN���','ChiaraPasini_u7RP_big.jpg','����\0JFIF\0\0\0\0\0\0��\0C\0    		\n\r

\Z\Z $.\' \",#(7),01444\'9=82<.342��\0C			
\r\r2!!22222222222222222222222222222222222222222222222222��\0\0�\0�\"\0��\0\0\0\0\0\0\0\0\0\0\0 	\n
��\0�\0\0\0}\0!1AQa \"q2���#B��R��$3br�	\n\Z%&\'()*456789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz���������������������������������������������������������������������������\0\0\0\0\0\0\0\0 	\n
��\0�\0 \0w\0!1AQ aq\"2�B����	#3R�br�\n$4�%�\Z&\'()*56789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz��������������������������������������������������������������������������\0\0\0?\0���b��&D�����N�\0���L�ʫ�*�j2lV��	���n������&�\re]R�9r��l�\0�Gqm��<��]�
5�������{ƱgC�1pJ�\0��?�)n4T�\n�r\09�]�6�&uUF/�l<U(WlRv<c�+h��d`řQ�\0�_��Q{\"�l�_���Y� �7 ��(�bx�O���e�}�V<�Cey��\"��e����+��g/�.�m��FpG@�sՒ籤 �r�1m���f.v��G_��)(� 2@���w����z��l�n/}�\0a]���cn?�DTI��IK��C� �� ֳ��o�Dn wXI\'�0�F?ýb5��aԨ��u�=��(&F؇�\0�T�w-�6��\'mj����h������\0-\0>f���X��[d�~A%T+`�O#��qQ]�c%��a���/�/���K�>�*��u2�nA0�}�jh�a��ٮ}o�#$�t�v�d$��2:jy\Z)r3M␱S���f�.��r� 9��n����І�+����A�����t��:�|�&���+�#����Ҹ\Z�/�U��#�lF��9�{p}�\0ɮul��8m��x\"���f�(�M�ۣ�g���=MnZ+3�45���\0R�����W\"YV&��e\r�8�Z���K䶋 ���kkpڠ�-[䏇+�MYZE��n$����P��Z�Zg��9�����B���/�B��~2rh�ٖY%.��>�U�vg�_�s���9?�hY��\"�F�;px#���1����o98����o b�cul~R	��G��Fv/�ƫ�D_�+���c���??¹��1���E`y�����h����%B�[  ְ�!U�i7�\0c%�����*(�H��v$	�1ZFZ9�D�Cc��(��u�Y$\"c�+c�`��}�8���7�O)1�;s� S\\�nN��5y����!��Dz�k��I_|��ǦMK)����7|�}j�D���z�ӡfnƣe#��X�]Ѹ�皳268-�Q�R���b�� �3�\'��z�\0^����*=���ù#�����c�)0 i�1��N=jd�u�YpO_ƭĨ}r}\r+�.Cr�̾�W���FJ��\0{�EQ[h�w�i�6��=Xg��Ⱥ����h�(���J��Ҷ,�+9�����{c�?�u�� �`�����3\'U09e~U��F[\Z�%�=*x��F�ۺ���6L�J�,T��v��m�Wzd����������֚�ɒ͊�뽣n�����* �� �IsD�5ë/�@��[���f$�In��rT���\n�ti�v
��s�V�L5��Ғ3����k6�NRK�V�%��4������Y�2�果����*�u?ٮ�	|�p�\0tt�?��-I�\\�\nB��9�\0h����}f�ǨZ2;+^��]M��\\�u�}�M�%\rݍ�3������0M������;}��;����{�M>s*��rY���kWO�.4�^ܘ�̭�b?��݄c9>��rV����J��e]����.�(�������z�hr�#;�s���޷uyR\r2�s�f}�\'<-Ў�����5�Mq$�rx�h� V]b��V�j\'E<��5;���08~��z�9��x�>�)�ǵG��88�z�f��ʀ\"�ك񜚭埭m�\'�֮E����Q��Er���0���������]��da��y#֠:,�M
�\'�� ~T�����n]>x�8BP��M�3���/ܰ?f�q����z�;�\Z� 9�����\0�ꮅ�%У�2�IV��t�$b�0j�C*��|� ���	�N��7q�ӹ-X�$!��T�t�	�Ha��CL�~� ���Ę� �A�Ei�_�N&�؜a��\0�ke��{}*h�\n0l��\01CW��&��#�OJ��\0��}�Ѽ��i $�AXo�_��c�=�Y��#C�^�+7r�(�\\h�س�=r���q�>抮D.#�u��j����s�\r����x����CEl<)�ǃ�1�>��ˉMƯ�2mS&1�7g���Gq${�G*QUNI9�����szX�-n��\Z�K�am�����kʓ��3��\0��l�\Z�w���C�6�Z�@\0�
��錓�����<5��$�HyI}���1Ԏ���T�y+�߷\0+L��P��x�b>�L�\r��~�o���j��\'�]��E\'8�_ӵb�\"�<T����n�\'����\0�S\'�j�J��,����:rM&9数�zH�sTAbi���՞�,�d�AҗK�P�\0�ֺkHWh�TJv:)ӹF�KA��ֶ-�v����1#��\Z�NW:���j��[Ds��*�`/jE�_�	��Sk8�`J�=R5�>Q�*y�U�r�f�\0�r>�M��1n4�
|����۟�M��2{�\ruR�� \"�:�� �K��pOs����!6�y�
u�ֹ{�^Yu�vX���=_I��#\"T� ����5;��(�G���R+1گ�\Zt�|�&\\�8W�k<����r���v��&!U�\\��ޡe���08�֐�jtܸa�@�\0\nb���ϧj*�>j�\"�;�E=\"��Ny�߀O^��5��s8��,�W<���_��\\��b�����9��?�5���@��C	8 ~��SQ�hU5i+�Z��m��ܬD��B�ۧoưo��cl��2��`~�>j}Jo8�`���\0��f����1��\ZP�6�=L�a�q�Tg���c�6~�������F�{Z��e�@�-�:U�E̊(t�*<�>淭��Yv0l�ry5���&wRZ\Z��*�e�<L����T�Q��\rҨ��P�ٳ�V�+��X\\�c�Y�k��;�5/�%:|ch$�kEO�E���\0�h��`x�)!���c�{\Z��\n��Y�،�e��@и=*�匁֩�r�P�=H��>ƫ�n\n�+.�^h%1[Deri�˩\\d�u �\0��~�(�*�d2�O�xY$@�x ���Oa)ۖ�����G�ΒE���?����*2��1#������p��y�g2�����7��x���:�w�=�c��b�B2�4U�X��\nO�QQ�͕5c[U��ձ�0��xq�k6�Q�Ò�	�_��s���K���m�g ��^� �6�2V;�8\',O\\���W�����<�\'�{����� ���Z�4軁� �54R����1��cۏ��0�
�H$��U�#�br�Znp(9\'����Aר�U��Z�� �Y1.��萁�N�Ҕy�k�\0H� >���B�c���=N�ޤ����SзA�i���p�#�8d�i�)�G�A��&���P�
���k��{�q$�J�n�\0V�Lw���U4�,�#�/�9Q�8H=~�U�-�X��#�rFG�Xv�t�-��cE���s�����x.���3m搅#�FM[Jۙ�&�b����l%�짏ʵ�n~o��
$de\\�-�.�����3�t�]Έ��:G��Cgֹ���[�L�օ��1�$����{���r��1Or�E!Q�7EQ�jϻ����M�$q:�L���s�� �[��Fe���F9<?:��\0K:�ֺ�<�c~�=21Zǖڜ�RoC��5^�v�9����8�ߟ�VƳ(���q����?��v�eΟ%�����c���6��7��#u�\ZM�#	�s�\"���� C�T4� _j�u[U:d�Nӏ�X�A�Bq�5q��sC߱��H��U�$�\0�Ǚ��G9u17�+�\rYX���;���<�w��\0\\�l���ګ*CD���w⵩�8)h�ǴC#�\0�{v���n�e$� ?�Q�d������J��.���-���[$b�d�g���F�/��7|�?�Z��\\��)�VΚ��Ɨ�Z\"�G�5�Ky���&6`;��m�z*�\\,yD����]��A�sY�Ťp��2ƍ�[��x5>�6>_J$�iÒv:���U��Eg���j1�����ĉ>̭��ڤ[e_C�S\"T���;�+��\"\0`\01ޘ� �.>�x�u�4�Xȧ��J�
�ϭE��\n�)�8�q2�!�(hHj/���Zu��� \"�Fi>�uQ�1�\r�эj2v�C֯�H��Z�ݠ��\\�k[*\\D���mv!%r��QI�9�g�0?\n�4|R��J�7�@|�ǥs�\Z�y�����\\���n$A��V�~�0�R���`�(O�9VgW\"8�RD�d�!�c�ĥ�]�ԟ�5N;���I���>���i��ڠ\0_������m�x�ܺ�\0�������Q[�B_�2=q��MM��EU�1S�\0֪�R�����+n�(�4��a��� E�n�\'�k�ܘ�e��܇�zW
y#Go��c8��\0�ζ4]^\'�(��\'�Is���s�TuѭkFE��1{o��\0�z�=�&;I4����2;�N��Jc<�@�i�:e�+Ch\'�|��+�-\'����jC�\ZĄ�}��T��[P��5�2�mW\"##5��C\n���E	��檅ېG5��{��\0\0&�{��5M�JdpJ����X��l6{�V�rq�5��P\n��2�&(_�\ZԊ{`��cU����*��#4Kend�3�S�hL��xv�<����N�NCu��� �\r���]H���0�=*�E�9e#���N�P��OC\'S�`��a)[���t���v�H�v�W%�S�+h/u�u���v12�(�п��Z++|�h���S�Z�RB�v��x�N���sE,Mh}�,k ��\0<�9����!�$\Z��4�y�������j�7��`t��\r��K������J��#�W}HF���f��A���\0��������F+S\';�9��=���E�yw/�aI8�1��g���p�4>�\0x�t�^$�[l�vɆ2�$\0���W�.]s�ޫ�Ñ�f��3XՔOB������V��$����f�ӎ���d�֘`s�Hޕ�g9�S
��8�$�t�}M��7*lb6�`@9�`�u�fѺdF2Ì�c�K7�3W\0zSY=y���mV8؇�E��4k�9���?�Z��H�##Ҩ�f�dLf�XiG�2�v�8#�4�� %T~uq�̈~�T� ��*�;��cE�̛�Zis�\"~���v�5��Jp�v��3���C�*Ԗv�Q�~�z���0đ�������⭼�b�GJ��/� ��\0�J�+#
�S��=��J����5=���ey������͖-��]iYX�g.i\\�`pb�X�Z�ElWP��=��)ЫhD�,��	fB?�Py��c�L�R�ų��C��$sI��>S�\nl��¸���L��9fW,s��~�1-�TA�o8xH;��}��Ҝ� $��*��o.U=� 5nP&��q��ǵ0!���{��G2e��h�d����Na��x��\0t}�T��z�ݷ���u�s~n_��s� �q\\������w��5��� �5͡F,��]27-\\�6��f\"�G2���k!.A���e�pj,i�i�$qQ�sҙ\r�<\Z��1�P��+�����Ka1����G*���$ŏ�ȦU�E�T!\nGa�9�ߟJ��5`\'|�(�I��fRi�
3\0\0�k���M줖+go�MY���-��1�!���ֆ
ugr����8�!
j�:�y�F|� bc\'\'���H�T��\0j��t#������ �EH�c�Oz(A�
�Z��a��3Zh<�ڥc��1�M�u�$C�+N ������1�5���ֳ����G��L	�!��A�\Z�����◀��޼��9�e�\0r�2�ֆ4khq]d�ֻx̈�\\���::s�t�\'\0/�y�vS�Kl橽�\'�ҕx����5�a
�\\Ԋ]~�~0���/�чJ.2ZvC�jd�YW\r�*Ԗ��T����Lr9��U�X=\0�G4@d\ZH-���SI���}9��6Sָm^y%�m�H����p��W��{���X�z�d$4��$��Wu	�]��ҳ��\0X��4���)�f�1� 9�+J0V(�������\0�Z� �G둏L�M\0�J�Np�IQ��ϸ4P2��@4�)<u4QOP\04X`b������1ހ	��E�\Z��J��i�@A������d��\ZӅ��7�}(��rn�
)�뤶p��;I�e�\\n�zs]]��a����]9hk�ܝ�Q�,7�y���OQQcdT�\"��*=hH�y�<�}1Hm�I1�ɪsHH9��Uɬ�A$���H�ʱ��s�j쪱�~�:F#A�*��ޭ̭A��}k�\'m�1�j쵻���\0y�+�=kX���~Y�(c��8��7 ��H��K~�[!U© �z��@�A�z��\0?J���y�)c�c���s���ڊ��RA%�x>��As��\0�E8�������>�(<3�4QLH�\0F*��?hT�����Et4�c� 0����2F�c/ՆN(��f�N�|�zR���VGH�sO$�׭T�ʳ� �1(�{�E2	g5F鈌��QEZ3��x�F(<7Z����J(����2 b[vy5`1(3���l�L�dR[�*�?��h��Gk�jl,ܡ%�V9=�QAG��','2014-10-21 17:17:04',0.899039,-0.003634),(4,NULL,'Piero','Fraternali','Notting Hill, London',NULL,NULL,NULL,'England',1,'Europe',NULL,NULL,NULL,NULL,NULL,NULL,NULL,100.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2062-04-19',3800.00,3800.00,NULL,NULL,'en','PieroFraternali_rru4_small.jpg','����\0JFIF\0\0\0\0\0\0��\0C\0    		\n\r

\Z\Z $.\' \",#(7),01444\'9=82<.342��\0C			
\r\r2!!22222222222222222222222222222222222222222222222222��\0\0@\0@\"\0��\0\0\0\0\0\0\0\0\0\0\0 	\n
��\0�\0\0\0}\0!1AQa \"q2���#B��R��$3br�	\n\Z%&\'()*456789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz���������������������������������������������������������������������������\0\0\0\0\0\0\0\0 	\n
��\0�\0 \0w\0!1AQ aq\"2�B����	#3R�br�\n$4�%�\Z&\'()*56789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz��������������������������������������������������������������������������\0\0\0?\0�{~�r\\�xA��3�s���zd��cU��a�:��9���{�L6v�I\r�(�\'��?:�����2�Au��dé$�ǌQ�<QZ��k��?v�-[���n��J	v\\����v�\0��6�Ȁ��\0���\0������ʹ�4�:ʞ;�W��\0�.��*0G��U�f�����tMs�N%�r���Z�q�ӯg�n㸄���g��Z�/RӢ�P��}\rvS���Rw-��I�]�� ��2����Mu�Kkh
<�V=������Oq�z����L�NV����-�:��Nz���m�%ҼI,��ܪ�v�\0?ʽ�J�<�.�e[��|q3\0�=��^=G�j{TU㡋��wF��ZT�(��?\\�m.H�\"�#�8\"�� ���d&�y#!R.@�T���M\"K������=	���(ǫ6��VFD�M��yJ���r+W��M]i���He�G����1��3܇  `��t\ZY�P��hS/� Q��?:������Y��Hޤ8��F]��!\'�\Z�8.s�\"�\'���Cql�UH�����Xc�������t�)tx�Jŧ�|�9r�tb0H���+�ҥ{�i��T�I�q�\nN3��k�<�F\Z1��w��o���8ES�����f�i��pE�*�� ��f�����l�x�Uh��n����*X\\^���Ab���O8� ��\0^����6�\Z~�p�Ɓ�[�q����dT��ѥ���q	r�r}9�iY���Z�\'k�+{��X*BPs�S�t�)���\"2F{rvSo�#�����Ґ�T���)
���Q_H�],�un���a�^��jm�ip����d4jۂ��g��O��=\ZI�
ef݆����?L\n���E$zY}Gw�6qjRE��27F#�\Z��f��n�-W��9>�j��/u���j�� S�r?#^{�kZ��)k��\\𣠮]�����U�Sv��{���Ж�N�\r���\08�b�)���q�]��%��1�VS�{���z8x�J��7;F�� >ߝ4���\r75�y���','PieroFraternali_rru4_big.jpg','����\0JFIF\0\0\0\0\0\0��\0C\0    		\n\r

\Z\Z $.\' \",#(7),01444\'9=82<.342��\0C			
\r\r2!!22222222222222222222222222222222222222222222222222��\0\0�\0�\"\0��\0\0\0\0\0\0\0\0\0\0\0 	\n
��\0�\0\0\0}\0!1AQa \"q2���#B��R��$3br�	\n\Z%&\'()*456789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz���������������������������������������������������������������������������\0\0\0\0\0\0\0\0 	\n
��\0�\0 \0w\0!1AQ aq\"2�B����	#3R�br�\n$4�%�\Z&\'()*56789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz��������������������������������������������������������������������������\0\0\0?\0v�7Ty�  ?8v���\rwD��=j�����y1�R����j�ElԒ���ܫ��k���-�n�^F���<\0���*Vw�N�tU�#��UIc�lZ@z���9�+��ϱ�2��Eb6��m��p��8d^��\0��˝2ym|Ս��9r\'������k\'Q�j�+X�=]�w[ά��t�g�iZ�L	?x\0�{\Z�f�\"f\n�r=H5�X�rI@FQ�s֪5Z�z/�\")�=	a��R��F0U$u�5�iwS2�I!1���=����n
�UI����zp(u�%B-\\���߯�m���#�\0��r\Z���Ӱ ����Y�M��o1��AV��Gc��U��HI��T�{S��ן�K�ˋ�$O��_�t:e���e0��[;�\Z�9�1�>]��=.��6h�Z�>�n5�7P���j\rԻ�N��_u���}&��u!j�o4T;��E|\ZG)CI�=���U�/-m��_� \'�>����O�&�#�-N\'�ڐ��LP��E�bsǯҨ�sgm`�\Zc(��;z��A���^�=#Tv\"9�-�C�GF�0s�MS��<�$���}�����+��}�5,a�E�W����h\'�A��\0�5v�����|΄�\\ޞ��\0�P���t�ڶT�\n7L�~��5�s|�kN�$?.
t#�=3S�*�B]�++�rD��x�_c�ƜI-�B�K�<�Q	6|�|ĆV�?��Ti$����屓Z�C&��:Ia�dy��������]$��1=�O�LH��=���qoo\'�.�����O^���Zv���E�$Y�T\0�Nz�;c��婤]��ݻį�$v!FҿQ�\n�y������-����4`�J�ʕ�#��+��+�$UE&h��G��9 \r�q].��$R��ϖ�g9�}>��+zd\Z�9Y�~�Z�c\'���q�pA� 9��I\\?��VY�	�_���b�c+��q�\Z)sN�F*���Z\\R�@��isN�&�h�)�Rb�	�)qEbE���sZԥ���-�Q���d�{(��ַݼ�.X(䞝�լ��W�wv�خ�{��9���A��\\C�X��~�� L���p8���\0�6v����8^�;~\"�X�rMqN�;��Pt�3��ܟ�+f���*�i��	6���8>���޹�;hu�ԡ�Y* ����8��5���C�\'5��_[��4^|��a��gs���gYڽ��1�̟u \'=��?#Q\Z�4t�r�ѕ�\0����m��&�o�8��Q���,���W�k1\0�֣��<le�O��MU��
0�~���%ك�z��|T\rar#*%}��;��t+\nC@#�>����\nqK��R93ݜ������7:��+L��>R0k���H������A�CW�N��fy #�r�`y�.	�3J�ێ��5��{G#+�n<d��xg�N�m�ܲ7�p}\rpH�6�5�����c8j��\"QM��ʍ�[M�[��_L��V�]\'#�c6\Z6�vihͦ����	�deNh�O�(&��ڊ�w���գ�-�{b �7��z�8/��[�0��;bF<1�c��s�?�R�� R��S�J�}@�i�f�r���� �n����>i��Z���\'Keo1��LG|��\0@�QZ�&��G�c�6fc�(����U
�E��1����\n��? �����o�xju����K]]F��������-6:`��N)�uU��w����9c��?\n�[
�	deM��p�y�צ8��ɯ`Ѽ#e�@�hD�u;��_s�M�𭄓��$)�Oo�KL�2�ǖã�+1�R02#8��\0Z����&[��	#�k�C�R#�y }���Q]i�аC\Z���W���h�Y�H�T�}��БW�*�� ��W}<
2\0�sYsDa1o��\0Z\\����>����Q�X:����uRJ��F�dp2�[�l��*�{�M#��˚+�u\\��տ��,E\\����[�=�*2��^�ЎnTrVmk6�C�DU�<�\0�����;�Z9\0�8�\Z�n-e��>^�;��,�q=·�M��N1�=���u���_��������c���~x�e=�u�	]U#f>�ӶQ�����.ivQ����\Z]�m�,6�\\QE�嵛�su@�ű��~\"��1[���}�?�榽y���ˇ�?��fD���]�����\0�g��2��g��#{@��5)\"�Lny9�9=�:�ʽ�\0B�����!�A nn��s^g�6���q
�8� �	���=븂�F�V<��}+ +3�\n��@W ���UKI�X��\\	��w5W�^R��=8��N3� \\�\\��cS���q:ωm�y#�v��-�ʳh���Z��-�9bQ�+�֥�;���8*�Ey�69����S{�sm\Z�D\'1n�y��O$T3��H���\"Z��\Z�$܅<\Z�;�ft�i��ҩ�tbbS}I�\'\rZ3l�����-D�o��G�T�Q2xКض�.��q�\rZ3�ŠhfIbD#�r��n-b�>�lzqҼ����8!���!X,�EbA�9�{�E��o}.��F(�0�&�Bi�~���7�B3@�(�m��\\�<A���N1�Ie���\0\\��n�!g9�:��O�]/�#i%�\n~e����0���{qm���TV�G�I�(���G��P��02s��@ҵp��|� ��Y��$���c�ש�N�[���>�Q�>��rL짹�V$%�g��ܨ��?td���UB� E���i�F�$��L��)&�6qM�ψ��_rŸ�9��yu�W,eV�5�k�V�O�T&FQ�F3�z\\f�z��UZ#�p?�t�����5�l\r���baI_���\0k����F��j�,�:���ӕ�Ƣ4∈	�2�Pk��΍�	cYa��1��2�A��X�\"����@�^~����+ع&���\0f}��ޝ�d;_�Z.e�+H$fLK\Z�ڽ�ˏ�A�溝~�y���3Z������L\n~\\�;U�H���N\raCyzJ\0��k|g!]�� եby�S�--�Uhe)��NG�^���H��P��8�}T��l�B���q]F�g4�q+�G�x@}}MtSn�-T��D�qR1Z��[��e��\ZvE(\"��.M?�Ji4\0��~P/�-���ʃ&99ϡ�02��\' ��kеXZm.�;�n����=�Eo��	�{�q��+��+��gx�E��$Ln/� ]���5�� �-��Z�/}�\\�+m\'��ǰ���^8\\J��\Z�*9���� xa��c���)e\\�*\r��>���;���J̻��ME�����&�\"��\0��sQ��E��C�Y:\\ܾp��\\��}Y|�i��\0<`��P���`�i$��Bp����P�l���\"��ټ�ےf���\0\\���?T�᳹dBc9���ץ�Z��1Ol��ಶZ�&��ƣ7�m![�峓���m2eM5���e\n
H������dy\0T~�k�_�V[�y��E�]��ob�d�]�L��V��>�-YCB��@�\Z0�Lwn�<���\0�&��i���S&O*�$g޽N�N�/$�#���� 5���	`e|A\nqY�5�7MKDy
��X�wB���C���Gҫ,����Ek�xv�yq��Wh��dWY��q�tO��\"4�\rڢX���*E��2sB�9E��K�9���`ۀ�+���.aFS��7!�+)�&xh��U=���A,�x�[B�&�5J<��:͆��Վ1L#Һ��G�҅5&)6�qF3Js�M�a�z)3E\0l���F�^j 5�*>V��8�=�P�t��� �φ;{���|�#����Ŵ��C������h5-\'R�Q�Ș#����b�M�q菡�a�*�Y�m&ݢ��i�yX`�tۭz߀&y�u,8Q�ی�:�It�7h���kr��=k��t���x-�#�ݒ9汌ܷ.t�5F�9�<S£����s�jX�FkT��J=��vQ2�ȅqЀEr��]��d�-n�I���WM�_��\n��<����gk�iN�Ԁ�j\Z<Mp��r�\rSh�/]R�O<�in\n3�5p:޷=Ӵa��p\0�W��^O��Д�s`��=�Zږ��g�Az�Z	nY��}3���y��5O>�R6��Օ�E��Ry�7�� ��s�\Z��p/��~Ї��a�Ʃ6��	���	�j�����=+2w{�@�j@ �յ&D�H���9��iJ�ʜT���Xv��x�����b�h6�j��a�\0��E�B9��u&�#x��5�V��#����`���*IB��iA5\'�F�W}�*�CJ\\ӄt�:@G��Ӷ\n\n�Lc1E?m\\)��A�&Q�ه#�����$��;Ha��#��5����\"�Nk���G{�r�<���#�5���kΏk��}��F5�ѓ\\�2��5� #М\Z�yH޻zm�.������3�;�gQ��t�����7��_���}k���h��E9&��Ȩdr3�5);S5]�$�yb�{�Q��>����I\07I�w��{c,�8��e�C�@�Y�����k+��(˶p\0��M����;I072�����N�+_i����,��}+\0��6Inn�� �����Z�:��9#L���u#޼����g\"��Ȯ�}z��_�6�}�8��X��Nsw��$��bǒZ#ZSъ�n��u*MH���s�ja��<�q�LS\r��F��[�c��\"RX�񓞾�j��`	[��Q��3mL�����\n�\0�
�C��er���X?�x��u͎��A��\0�>?���Fs��CѺ�(\"�@ԛ���E\0�@I�CL&�u�EG��,+�������v\Z.�5��W
�W���;�Y�\Zj�����Tr+Y��G�̏G�E������&�۞cn��\0��WJ�	dy�Y	�\0+̭���50��K���.̽3���x�K��[���Ѻz}Gj�R�nǵB������9S��nH��hW���I�9�Ea{�+��s�riQ�r�\'ӎ��d�Ϊ�)�^I���y��绿�V��Ŷ�\\�O����@�&��[���Eu|��$�0S�3��mL��a�@d
��޴�
����\'���U�e�)]���[�[�\\\0A��4�uƒ��\"��\'<WS�[\\Mn��1޸{�R��`w$UA��&���$�,��I�\r���ƶ�æ�v\"�S����g�C-��ެ�}Ó��s�)Q@r3ץ&A(9��<��1�_�X9�ߥtV��[F��Q��ta֧&)�����+��\0i3�L�h\0�i	�;�Sq�\0nM�\n(
�0�U�\n�H���@�0<L1�Y��M+Tb���>V��5���:�v6˪_Go�f�o�Ɍ����[�[�ӧL����SU���fh�Ȭ~p �={�H�h�1�\0qڼ�+3�N�g+�;v���\\�4�Bq��Y�[��rj.3J���@\0��Njh�ݜ���j�W�\n�\0{���$����\r>��[Eg;y\rr���1�W�8>�J�QףV&G�\'��k��MX�ϝ�隨��9Y���$� �Y�@8<{\n��\0�n{�WLZG<��q���}�L��PO�^� ��\0������W4�c���O��G���mS�,��\r��\0�~���Ү��Ö:�v\"�<�m8���Z��
F�\\\Z9�Vϭ┊1@Ģ��P\Z�\'�L\0ӇZI�VO\Z��}�}�S�C�*����\0�E�;�2Jv���\Z��C��N\Z|�-���V^�d�+�p�Ў����>����T�W�4{p9
绵V-�w/Z�.�P!8����z5�A�H�z��[^1�W˓�G�b���J���\n�r�6y�z���\0��Wus���%��O��㞕��L\\�z����O9�5D�)9�yM�)DD����n�4��&�s����\'�Ǌr@:�hC�f\'\n	\'����֑�y�P�mc�?JƂ,��t��{9��T3���#�tQK�\\�~Gcx���p+�<�ؙ�搑I�P�y��K�#�!4 Ґ\ZR���4SZ(��&�($n㊃K��|Cb���k�\r+\n�
:�ۣٝ��Y���s�E�����X|���ToJ�J(��Os���-l�W����>H�q�+H̏b�qMp�\0b�+C1Dk��R�(���1(��|N��?�?ʊ+z_�O��|\'}q�A%��uq��{����Eޏ.[�4b�)��z\na�Ɗ)!��QE\0QE0?��','2014-10-22 11:19:35',0.898944,-0.003572),(5,NULL,'Davide','Vanoni','Notting Hill, London',NULL,NULL,NULL,'England',1,'Europe',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1750.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2078-09-05',1750.00,1750.00,NULL,NULL,'en','DavideVanoni_6IzV_small.jpg','����\0JFIF\0\0\0\0\0\0��\0C\0    		\n\r

\Z\Z $.\' \",#(7),01444\'9=82<.342��\0C			
\r\r2!!22222222222222222222222222222222222222222222222222��\0\0@\0@\"\0��\0\0\0\0\0\0\0\0\0\0\0 	\n
��\0�\0\0\0}\0!1AQa \"q2���#B��R��$3br�	\n\Z%&\'()*456789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz���������������������������������������������������������������������������\0\0\0\0\0\0\0\0 	\n
��\0�\0 \0w\0!1AQ aq\"2�B����	#3R�br�\n$4�%�\Z&\'()*56789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz��������������������������������������������������������������������������\0\0\0?\0�ݴݤ�ȋ�y���I���\0<���u���L�<~տu%����X�B�q�\\e����,�(A�c{��z�O�Ѥ�\"���\\�&����� �\\<�(�6e���0ܡM���^�Pt�sP�m���+�� S��uxu�%�%)0Pe���d�>�ߡ�&RxG?�\Z��b�N�Y�b���}�w��z���C���\\��+�Cgd��?�<}�k�2���U��δ�m8�MA�2�A�F�68\r�>�0$~��~\'�LY5
�ă�U�`_��~\"����tp�O�\0?ҸMsH��\n;6�y���������wv3<w�q)e}��5�j\Z������&3匐�Wu�o���
e\n�}�{���KO��BI��x��;�������V�E�u
�2�s\Zn?�@}��e<���S�\r���$���@����i����;��9���||o�L�^��>��?��k;}{�S��!4M\'���G-�Rɪ@�!L$N����
��^C�*#���O�v>3R����\0\n�!�Gʼ��%���4�\"��X�o^�⹦�-O[
?q$gk��������c���=+�ӑ���O�\r�?��[:���Ioz���i��]FL��bA�pߝuA{��4�2w:i�(H��ں�\0��eԵ{��!Qz���	%���E�v�������#vB�,���wQ���~��5v�u8kTN�^O��A5^��
Y.neX��w3��VG\")x�;y�y�`�j��?��\Z�R�ܰ\\|��	��i���\ZY~M2�D6��1�9V��?�Q���f��H�y�cc���zUi��h���$��0/�`0�2�P�m�+���>� <�8��t������♧X�� \\y���[҄R��ԓv=Gᇅe�H�����e?��7} o�>�k��w��7ĚV��}���5����S�����W��+w?��','DavideVanoni_6IzV_big.jpg','����\0JFIF\0\0\0\0\0\0��\0C\0    		\n\r

\Z\Z $.\' \",#(7),01444\'9=82<.342��\0C			
\r\r2!!22222222222222222222222222222222222222222222222222��\0\0�\0�\"\0��\0\0\0\0\0\0\0\0\0\0\0 	\n
��\0�\0\0\0}\0!1AQa \"q2���#B��R��$3br�	\n\Z%&\'()*456789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz���������������������������������������������������������������������������\0\0\0\0\0\0\0\0 	\n
��\0�\0 \0w\0!1AQ aq\"2�B����	#3R�br�\n$4�%�\Z&\'()*56789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz��������������������������������������������������������������������������\0\0\0?\0���`�iH���W P�Z�;D��i�H\'��O�A$��zLsӁ�4fH�9�q���Ec8��8E!�l�aO�4r z������J�N�g֚
\r�J@OR����PI�ǽ6�O#ߠ�+�����p:�Q���\n1ӭ\02Lq���RRN=(nPd�}����Z�Q��M\n�-�_�6?Y�!�-��Q\n��{���E����Y+*�������J�ep$\'8�2s�\nJ抔���u�>倂�\'���j��s���P ���[K�c��H���Z��V��\\#Gy6����A�l��J-n} q�u�@�\0϶:����\0�\0��WO��#�s�(H\r��J�=�o0�# �K������ȥ���&2B�;�����O�&	b�\0zҔ�<u��@\\���4�!
\0q��E#c\'���N�c@�ߝ(\0pj5	��/�rð�K�FP:F_o��kg��h:tG�&�\\�\nzc��K�z�\0��c�>%�^=��#��!��,ӊB��F\n�0��4}��e.b�8\0�d�c ;kk�P���{�H\n�֘\\��<p*	$H��8�z�ڶ��b�DyOV�@�ZY��6]��D��tR��f�񶐇޳f�Xd�ҙ�0;�N�6OO\\��g�N�`��3Fr:�
�22��cӊ֔`���Ui~l���֒�5�G56�
��ޱ�t$llx�ɣ�~��r\'ppEh�֦3����xzKK��me���+����g3}�V�R^�\"��\0���Ve �psڑ\n;|�89 �5�*���R��z�Je@цp���y�F�s�k��<e�Ci��X�;R���`}>�ޣ+���enAS����9+3����\0�N\r/����g���f�=1�c�9��O�Ek�E±�A����� �Q���1���A�Rc4\0��7\' �S��M&��=}k��&���ivm��ep~����wU�Hҧ����x��pz4/-����6��l�泜���=.yk�aA^\0� ��R���Q n�\\�C\Z�L�	5͹�+$H\\c�;�I%c��qW��sU\'NN޾ԬReIe���^C�s�1RJ0ĞH���\0��E�a����g�T�\\� ?�Z�w���YҮ7v?Ωȉ�n8�mD\\�O �B�D����Q���-h�G<�E㺂H&Eh�r\rO��xO\\�L�.�M\\��~Ly�Ϧz�k5IG$��Ѩ�ǩ�r[�]�C���o	w9jӾ��[��sK�$n2�� ��K�ג��4&ͼ;vDw0h�s�s�=ǧ��^��q�V��4RsEH\ZΓ�S��T���-t��)2:\Z^�Pp3�C�)�?�7�@��|G_?N��\'��P��\\�VyR�Ԗp�Gz��U�\\ϧ�ǘdf���\\��jn�n$��x�*���>[�ޭ�T첈�ݘw�b��\Z�d��zdt��=.�� �ҫj6X�Z�F=:Q �:Z��r�2��\'ݑ���Y��g8a�������dG���KM:�c�0�ө�I=R.�: �q��#��9�T�\0�j���C\0�G�R�+\'=�\"�fװ<��}3Y��!�r=k��ԥ�FebNI��q��� ��+�sέ�ĕ����qQ2�3����qq�Wd���NA��kw�H��^է�f��D$(�	j�M�
���b���-����QVm i 9ℬL��ϋ��_A{Uh�r{w��ү>�c�M�Q��_0��������z�g4�xFػ�1���k%h���v\\�Nϵ�64��Px�Rf!���S�)()\'JZN�Ou�ri��A$��h�@��8��W?��\0�W%}+  �:����uw$�۔�Q�+�%|�8&���&{8x����k���)u5�j�*��;<�7��:�Sy��M)y��l���r:�ipş�� ����Ԫ�v�L�������xb�Iw]��h��w�q)9�k��ޔ�V���?�WQ�-���;R�-21�t�
����� $Wc/�ny��Ҹm}D�r3ڲ���;D��2O厙���v\r6
�eieP�m�+��>K�I��Vů����K%�e�A��]r���q����}��dl$��?Un-�o���2;Uy|Cgv�ˮ�)l���\\r3�D��S��-\"kk�ѩ�x��I5��X��d� =kR�*�� �ѣ-����6�q��8��\0�״|*i������q�zg����{o���z�\0�J������w\'(���&�	�/��sފ_ʊ��iw�Rw�S�Iڤ�3�\'JRs�I���g֖�@é�\Z���ӧ��P����\\��+`�����G�S\'dkF<�H�\\��֚`�z�52*���We�Nx�{Q�ƺ����V$�E����\'�k����޳&��sM\Z�J�=���@+Zu�A�ۥ%�Ҥ�� �Wぜ�z��+�\'��y!m?ָ�����]���88���!����\0�MuQ�oC\n�C���gfAϥg��K\Z��WU<k\"�0��rZ,�9���r�\\�M:0�Z�N�I�g�lã�[��j�Zb�� ���{��l�V�x����
BD�����h�	8����ʹ `�t��vL�ds^/#���D�}��#�4�l�b �z�O�=欩\Zx\'��#���\0�CMP������Ҷ��N)-nn�ފ^=(�h�q���I��I�z�h�8)3�\n:��<\Z%\0\'5���`n��$�1�]���\n[QS��˞3ڳ����yq�#�s��*pPc�R������)�\"�W\r�V\r��+*Ʃt�����ʹ�k�|�I	��?��\0֪5M#N��[�$�\0.A�m+�W7�̶�c�a�+0�1��[�3�:LU+gl@x� u�#L��[^�n�<b���$-~b/�ls�?�jk����������s-e����vQI#���zw�,f9��׭:4ÿ9�b�xP?Gv�*\0���˦�(��\'�;Ձ!Q���֡��w<����I8���R�c�����8�dM�(bA\0qޜą�4�`=09�ⵄNi��W��y<`o�&��kݼnm|%��G�� ���ׇ�̷&�&\\��Q�{���V\n������tS�0��]8�CE>�T\\湤}\r\'ӭ�֠� ғ��G��bw�(=:�g�h�R\\o�#o�#����v]z��3�+���QQ^&�y���U;��^N#�9�z���P��CPߟ2Ɉ�8�sX�\"�h���yeϔ�\n;7�]���J��pk��=6�s�G_ʬi� ���Y�p;q���1lwm�%����{Vu��Eo��$jԺݶ1y�J�q��Τ̟�z�&_�m�9��Vpw7E�r�\\\0Q��Ǌ�N�a\nmRO�>��x�]6c�W�	�U�;�T��ؤv�goO�.�0א+!Ĉ���/�e!w��4�5R��D����*�6̞��6��ٔs�# TL�\06y��\n��r��)s�vs�z��o.ܝ����H����Ҫލ�J��}G5�os��H��ZI]���־��.�h�p)������\r�խ�\0v�ߚ���F����\"+�$�QE7���汥M\'�)�i*D 8��zR�\0�<���\'���悀�G�v�9���ZǊ���h��:�T]�<�>b�G�4�Q����L�B�Tq���$��C�\\�Z��Y�w�B鐟ݓڶm�7h��\Z�L:ԶA9=�b�v���(Rhޞ����&!���%�^Fؘ��=^�qc$c�s�jw��bp8�����5u�6�OAʬ�u�8�;�6G%Y��El��T�x=s֫��\\���Z郱�ꦌ\'���R�Jy.�d��9ϭmH�c��Y��#��#��j��9n��\"6�K���*�i���O2�v�9#�5�r�8����s��Ĕ���=?�8�,���
L�; R@>��U���I������w�#\r�8�sZ�ns9ٔ>h����\\F�U	���^�=x�\"�D\n�\0v�����.f\'>�R��j)\\�K<����J3P��h�M�)(CA�~(����\\bu��O#�w�4����OY�U� ʓ.����s�#���zg�4�ﴦ��Bw�}}Ey���]���u�5fwQ��Ml�ȻTn�ny����Ķ��	`�A��2�ج>lc�*yN���[�%�B6� VL�[儥�=#�mL�al68&��T��P;3���W�A�%C�\rL�k*o� ri��G��Fާ��T�V$�3�w���Z݆O�b�#ЊϒO-��j��ЙNGlVi�#���k�s�u,�/��2\0T�v�\'9?6O�Q��ǌո�d���7s_K��PԠ��\r�>юk�

H�la��b8�(�+��u៱C��p�e�q\n��O;����@�\0�#�g&d�ޢ����A�Z� >�Qǭ���&8��r(�qP!3HM-�(���Siz�c n��z�ҟ�\0]ej:힟u�H\Z�c��i�D:���\rd���0�<��-~os��Hx�5�j0|L��$]���$O�\Z��B�����1�s��G�����y���\0�#�bj͞��3��B\Z������|�c�b�D�m�q��t��R�o\\�+�K��j���3�\0�\\�Ҩ\n��?�;U��\0�`e�G#5� s�Z��\r��&�&�\\m��\Z���\0�?|g�Y�\\���
�j�DiE0\n���3�i`��lg��H���p7�W�\03�ֵf	�qʜu��^���\0�rj���|�l�`ѩ�\0���\\��t��5�k�|�\\��Fy����⳶�#�\Z�T�\0�e)[A�9UU@Q�\0�Ҏ���A�X�q��:��o�.{�`\0qڊAE+�y4�ފ3�R �&s���Qށ�Fh�4�������I�`��$�Xe��r\\N�QF~���7^/�<h��ٖ\r�s�[��q�}jx��3��/NE��+�/����Oj�OK���Z���W/�X$^2��0p}?\n��`�L��r6�2G ���鼱�O�u�-�\0�>����5�j�lW���ZV#�F�?m?����V�\\ޅgON�1�i��	=�+�����r���=Ny��닍!Q/q=���\"�7�,?��B\rTs阙Xr85ȹ����5tqr��y\0�t�gO\nЌz\Z�4x�?��>3�+\n�E��:1]p�0�Y�\"B���Ӗ%�}jW���J�T_f���t=��-�L�T�q�ؚ�6�g����{���do;�ucZ�V� �J��q\r�����8�)��!{�0=N\r{�|��k�P�(r�s�S�Ezw�o��x��������w<�bO_��1��L���):�(<���A�(�?*N�h�@�j)	��E\0i��*\r(�E����+�~ �׏8��6��ʣ�V��h����$�*��Z6������\"Ǆ/\'�񖞱HTL�7���\0�#�\n�m��U�*�G	^�k>;w ��ۊ(�f\"�����e��Sm:�q*.q!���c�^K}�Nծmm�u�$*��h����M-�

�f�w��y���$a�&�+ ���̹EB�\0ǡ��c]ǎ�+���BX0z�Z(�\"�QZ�.��;I�]�9�+(��?J\0�Ï�yv��Ekc��}7��B�R����o�m�O�t �W<���Hz�(��a�}(����','2014-10-22 11:32:33',0.899002,-0.003607),(6,NULL,'Andrea','Rizzoli','Notting Hill, London',NULL,NULL,NULL,'England',1,'Europe',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1950.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2062-07-05',1950.00,1950.00,NULL,NULL,'en','AndreaRizzoli_QA9v_small.jpg','����\0JFIF\0\0\0\0\0\0��\0C\0    		\n\r

\Z\Z $.\' \",#(7),01444\'9=82<.342��\0C			
\r\r2!!22222222222222222222222222222222222222222222222222��\0\0@\0@\"\0��\0\0\0\0\0\0\0\0\0\0\0 	\n
��\0�\0\0\0}\0!1AQa \"q2���#B��R��$3br�	\n\Z%&\'()*456789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz���������������������������������������������������������������������������\0\0\0\0\0\0\0\0 	\n
��\0�\0 \0w\0!1AQ aq\"2�B����	#3R�br�\n$4�%�\Z&\'()*56789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz��������������������������������������������������������������������������\0\0\0?\0�aҳ5�j�F�2^�p!Tr[�*���Y��r�\0v%,\n��ź�K%�������PEҤ��:]#�$\Z=�����1k�$3�1�1��Z���%��,�@����~W��g
 
� �]�n��.;�
ū��c�m\\�}7X��Z�����Gkk+��d*s�\0��=k��q�%��&��t�>C�.�#�\0��]�q]�wW<�䕂��P[��<�T�M�B0|mzl�\'y\"�Y���l�_?�Lg�fS��^��Z�SA���#̽\\��6�x���.�3���ψi�3��\\�۵*
��5�de�H9#�זwm*��/�c]=�����mylY�ث��$W���=*X�u�l{��pl,YG�殹.CĎ�Pk�|�ks�SI$�NBJx\0����\"p����@��s�ly؅��cFK���}�5�|u�B��<�Ϋz#8�\"׍�C��\"(.-�͏wC���n+�MɆ�`pE}\Z�J��5�爭\"��n��3��=\r��]	�����GF\0�]g��{c�I��O�2�\0�������>��f$b��a-�<��ЎB�x�g��\n�����\r�9.��5]�B�뷒�\0�)�(-�>֤�~����\"�$�ޜH�5)hIqq�汮�-ޥ�|�����h��&�k\'��R�Ouӂw�����y:jO\ZMp�+9���-�Zȸ��F^V��d��SZ>�D�t���舤ԙ�,�7(
�#��C��زo| *u�+�����aӥu6��el$|E���)�ǡN2��C�ҵ[[�i؁�9�����O,����r�ys��� ��$w��_��o!� ��pk��{���GY,�5JW��pk��,ra�\0�����-�c��','AndreaRizzoli_QA9v_big.jpg','����\0JFIF\0\0\0\0\0\0��\0C\0    		\n\r

\Z\Z $.\' \",#(7),01444\'9=82<.342��\0C			
\r\r2!!22222222222222222222222222222222222222222222222222��\0\0�\0�\"\0��\0\0\0\0\0\0\0\0\0\0\0 	\n
��\0�\0\0\0}\0!1AQa \"q2���#B��R��$3br�	\n\Z%&\'()*456789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz���������������������������������������������������������������������������\0\0\0\0\0\0\0\0 	\n
��\0�\0 \0w\0!1AQ aq\"2�B����	#3R�br�\n$4�%�\Z&\'()*56789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz��������������������������������������������������������������������������\0\0\0?\0�jQM��	�(��^���X5V��#%��攇x珎ne�^��b=�3�+�ڼ�Ű�����Ud�Zܵ��VVQZ�*�%
�ҳ��-�DY$���Q�ɻ\0��+�.>$]7�m��QC�E�e��u���?fz�>W�T�{�W���\0&���Շ�t��+.�4s�#:�����V@=뚻�h��-�P\rB�|7�np�Ojh����ּE�9�_�]��z�~\\E}u���wD� ׊��##��U�%QH�(��(��(��J)��L�\n؁Nq�Z=���Vn��A�X��F�d�RƑ����ڙ�;וk-����LV3�sT��\\��4��G��\\n�s,����c9�F��hH�
n>��!�!Kf���/ܟZ��2}��u��.���pj<��c2��\n�i�g��L��j<�L1���W-&����٪�@�0��W��D�f�f[��yob�����3[�o��\rQ��*��nk1t���dV�*t�]�4i0I�溣#��P�tK�[��# ��֟��k#E���l�a���ֵ�#5��f�-�)%�Pf�h�!4�.i��&�-�Lh~�EV2sE!�Ҋm8\Z�̊��m�i\\�Tq^7�-b�P�`�`�W�x�頲\06v�,������z�:i��-w$��Uc��;��Z7DH��I���9L�4,PƜ��:Uul�f��zW4�vBU�C֬�R1MǥH��+Q���p�����28�Vܑ[J�4�����E���S���[��j�X��dV��J�+��\'@����З:<�DyNS×��_�[�Ck��>��=��1��wѯ �ߍ�9WZV<��?4f��\\ӱ���4��I��i�&�M&1X��ۊWj���Ke$0�4TNz�Sr�:^h�zPkc#���ʩ�2y*I�2��1���j]Y�S���W\ru ��;�媎�L�y2�i7{�����ָ��Ǎ�j6�QG<U�G9�y#����J�1���:DG V,�n�p*�$0����HC��i�5l���g�_2(V��WN� \roB\Z<��a�G\"<�XӬ�m��%��\0��}�����A�ZzL�`�������.�,j��&ꍍ&껜�%�I��/L/J�)��iqQ�]�R�R�3IU%����U�\\����j\"�y���c��Eg�W)�n��aH�J6;Mݴ�z��\\���4�>eBk��d���S����`�z�����*��>�}3�F�n8��c~z�,��R�������	⢚E�\\����I�8�nuFJ\'C�׌�z� J�N� /NMe��sY�W4X���)9�h�:��q���p��]Ef���tluS�&n��WmQX���jx�d��XT*w5�D���H�(&��t���^uo�8s�0V�����s]��G$���)�2�Y���w���ou剒6�/���،a�� d��}&ꈷ�4�h�ȑ#=0�F�L/�P�ZC�� zc�U�\\VR�J!#��)��>YF\rQ�Zɲ�\Z��Ɗ��|ƊC=�\0�~�\"Ţ��P�Ү����⋴����Ɍc�M��֧�YǺ���4�\0Lƨ��2M�J�|�95e���k��N�]�BBҐ:V<���j��H٪�iè����6�1wă�&��� ;T��6�8e�%���Z)�I��9��mio$�\0���aA�\0�VƓ�*��:�X֊jGMq�a�¸��M��A��J{1w�6������*K��=qY��s��2Z�I,̀ğc[�Q�!�ӵb�i����;}k��Ӯ�8����+yYlsAI�o�{U�k�9�y��2�B�OZ�N�54i#+�Z��6\0�UNB�@�4�B_ޘ^���\"B�IQ����7\"��Uw��ޫ;�l��J�4��e~�FW���O\'�h�KsEU���e���� ZMuṖ\"{].�^�wc,-�e8�SShϜ-�6�c�u�
cp=�CV���[��t(�q���+6�L};�,���q#�n��Yk$�\r�ҮA2�9�Y�����ڪKl�<U�(���8 ��L]�%��!w��
X�u�8Y{�t�+H�y���1��w�\ZD���ݻ�*���إw;z\Z�A�K{�bp Z��n#���)�\r���G��? �ױ�͹z���\"N�U��1����Ĩ�ر�<��`:����9=	�Z-E��D2q]U�}��P��5�%��]X�_�az��0�[g(�z�ޘ�Q3ԌV���һ�i�cez�+T�=T��BD�\"��L$抻zq4��=��e���^�e�X����c���ڧ�3��6\n��Z�� ��:��^ ����ʛz�c4mI�s�ӑ����m��FVU��%s�.œ/�֠w-ҢyfED\';O���-��-\n�u��_O�zf�J� $�BJI5�*������-� Kc*�[ҵ��qua���ֹ�.��((\nw�v+,�\0ұ�;�Ծ���n������<VS��v��Q��q�b�����΋B��^���\"�V��&r0OJ�-���b��.���^�g��1��w1Z�f��Z�g�b;�wjWz�ښ%�v��jFj��R%�\'�)��EQ7=(�n�f�7\nݲ,+�e�⼇�V?g�����\"�w<�+�����ۜ\r̽j&\\4<�b �zTJ�\0Z��)�N\rd�*G9�Z7R*M+#m4ԑO�������^\rJc�>��ڐҹ\n��`��3Z�4YI@�l���x���P��9r)\\�1�P��D�.�]���Q��΋K�u�e?��&ә!>\\���k6��܊�\" |�[�6�#����\rM��K��v�^�����OLv�1Rw�Q�yb�c�4�A����d�Z;�wZ��M-Q��Y�j�jb���
5+5B�TK��f�Q1�D���E34S$�}ԛ����\r��v�4Ҷ�Q����
DM,�ƅ�}������<y��jEi g�`�<V�<}q�O%���v�����\\�\'}ܒZR�\"�:)\n����k\"8K]���y�
�RpzՈY&�x8���f�4hwF�*�Ł�*2+U���8��g$�i�r�Z>0Զ�r��Fk|����SFx��es�4�)�A$@g5�\Z3��V�,�\0�ևH��`�lәEX�i` �Y �	�]��B��Nw��fYER�R7/J�-���\Z�E$rNZ�y<�
sHZ�\'�+����54�F�TH;T,Ԭ�52X��&4��lj���j&4�5\Z�K�L��=
v��ϵy�Ŀ41���f� �5o�\n���M��5�ZƩ>�t�\\�gnI���ё3*�9$��I�_5Z���O�-N{�S�)�l��C2��Ճ�i��9������k;�1�s�]Y\0�6Es`� �$Ү���%+Wh��K���G W<�I#�W��!>�:2ӑ�؀\n�~��Ia��d�R�v\0�*�`p9����M�M�ݏ�uO�i�0lHH��Z����������򘠴ې�����\Z��̄�EoKމψ�dz���L-T��-�B�ʻ��JϜҥŦg�q幨٩���e����^�fɡ��&�X�cL&���f���&�54�b抏4P+�F���,N*I�Gjm��o\'��7~5��nA �S�V���/LVU\ri��v�Z
�VZ��Z6�ֹdu��˴Ա�¤x�.E6�Vw6�-@�{V��,8���b�-S�v6�4T�\n�lrrMQc�b��yHX�VVrf�ܨ�|yv�=�*y\\� �zVv�u��W=���h�ҽ\\<m��ϚGE*�<{g(��=jm;ŷ1�[��G�-�V_��\\��G#���	�)�z5��ix�l�5\\ݑ��y|M��֯ů]�1�{VN�-Tg|��e��\\�\"��\r\"��֥��ms��#zT:es�M��&�$�#֚O4�`��j2iI�@�4Rf�x�lw\Z�QEt�t:T�����E��s����(�Y��؏�]5@�E�:ː��+~�QY���Nj��#%��N8��p�s�N	k�O\'5�i�Q^�-�\Z����i�G^+&��V�ȯjIc�m�>eVc/@�Ĺ����l���(��64۩�y�tc��=h����\Zi�QIER��','2014-10-22 11:43:08',0.899149,-0.003672),(7,NULL,'Giorgia','Baroffio','Notting Hill, London',NULL,NULL,NULL,'England',1,'Europe',NULL,NULL,NULL,NULL,NULL,NULL,NULL,2800.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2090-09-22',2800.00,2800.00,NULL,NULL,'en','RobertoAcerbis_U7Bm_small.jpg','����\0JFIF\0\0\0\0\0\0��\0C\0    		\n\r

\Z\Z $.\' \",#(7),01444\'9=82<.342��\0C			
\r\r2!!22222222222222222222222222222222222222222222222222��\0\0@\0@\"\0��\0\0\0\0\0\0\0\0\0\0\0 	\n
��\0�\0\0\0}\0!1AQa \"q2���#B��R��$3br�	\n\Z%&\'()*456789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz���������������������������������������������������������������������������\0\0\0\0\0\0\0\0 	\n
��\0�\0 \0w\0!1AQ aq\"2�B����	#3R�br�\n$4�%�\Z&\'()*56789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz��������������������������������������������������������������������������\0\0\0?\0����I�)�^�	qϨ���:��zc�{�6Y\\�7u�Q�\0���h�_�Z�V �X�ˏ$�	�O#��ET��{�\"F��N
z�mc��ʼ�jҎ%��>U�&wS��#��ѥ&3ӪsQ�(�|2���]�������)\"@e=E%;��:\\��KKpI�� �g�\"�Q7J�r��u�j[
����X���:����Y38=O�|~U�Fi���_�E��Ќ�[�\"G��{��)o|���<u\0�c�?���y�-�\"�m\0���\'ߚ֯�̩�H�kx��q�.*0�\'<z\nְԣ�\r� �85J+h�\n`�1ϥO�2ȿ*�&\0�^v��נ^�9A1�	ﱉ���?�U��f�H�:0�V�3[�ʬ��v�CPO�(\0d��hӠ�]�g5�;˫�>댅������0����~�#�.8K�\\I*�$��i[�����~���I4�:�\\Z��.�<�A�#�#��������7�r��3Iw`�تCG�N���`D��� � ��*����4���6o:�z��<})��$��\n7?�[ұmo�337��oc�]�0ω %�ܧŢwg�NWVÙ����ݐ�?1UuYD	�����_��ې6��X���d�ˍ���
�\n���h����R�X�aA�ʭ�s��a�\02+��c�pX�k���e�܎\0��G�롣h��Wf�V]��0Ϯ*��yr`G��sW�ݥ�$C�=��e��X6s������w15X�A�~f�mi ||�P~S�U���DB��̌z������E*1�5�/���sQ�c��;z��:�yә V�\nӌ�d�~�L���i�jևa\"V#�8�j�R|�ӑ�N���2�|���W]�]5��Wd��k����(����','RobertoAcerbis_U7Bm_big.jpg','����\0JFIF\0\0\0\0\0\0��\0C\0    		\n\r

\Z\Z $.\' \",#(7),01444\'9=82<.342��\0C			
\r\r2!!22222222222222222222222222222222222222222222222222��\0\0�\0�\"\0��\0\0\0\0\0\0\0\0\0\0\0 	\n
��\0�\0\0\0}\0!1AQa \"q2���#B��R��$3br�	\n\Z%&\'()*456789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz���������������������������������������������������������������������������\0\0\0\0\0\0\0\0 	\n
��\0�\0 \0w\0!1AQ aq\"2�B����	#3R�br�\n$4�%�\Z&\'()*56789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz��������������������������������������������������������������������������\0\0\0?\0�ZH�DL�u1�U�;(��\0�ҝ*��\0��H�C3�8�8�:�\0����f�6���Pqߌ�����\\R69��?�~�� *d�\n���ӌ������[(h��\n��<�<t��� Б�dR*��!H,�$��~=B�FP��C>�Ae�H���~���)	�FK|���6Q��� ��\0o,���ǧ��O΋\n�|������/N�H��**�Z��C/�#s�t.��JiRE3��\rߦ�>��5�V�*Au��u�����;�g�fY2Y�-�̠��?�uI����v-��jwS5�P)\n���q�Sd��˻�2zt�S�u6�	5r+�H�y1�^3��bIyr���y\'=�M[��ZLgr��lg!�DzkQ�sU
lT�H��Ü��΢��*��@�=kK�\'�1�Ǌ��̃h�&,�#�W$��i��S�+]���(�Vm<�����rl-�C8�U��ւ���A�Y��l��0.EZ��0H����$ϽKl�ؾF*��{m�V�	AH$RE��Bp:U¼S�j�(s�N�Z�ϸ�yn+��$s���?C\\U����I	������`�x��u?��ҳu���ia��<�9ǧ��02+�0�G��&�E�Ѱ�T�8\"�ы[��%��)����H���($�1뜞��\0����\r9Nch��~9�z���H�7}��u�>���#��\0I�q����瞙z���ݾJ��6���	�}8��N�՜���<HOOFi\0��\0]W�z������\0�ߞBp:�ɥ�*�26LjW=�Q������e�Ȳ�\0�z�߸�S�,p�dQ߸\\z��*1n�J@RvЗN��D��e�z�\'��y8o�ՕY�\r)Ǚ���gH��3���?\0�R�P�r�g8\0���jU��\'�������#29�\'���\rJ�\\)��d����Ɯ4�%���z����`EU��֖8�s�V1i������ؗ���U��@1����NTW4PE�(tUylcn��l�Fɞ1Bbq9���#����P��H��7\003]C�*�ЂzU)4e*i��\r�\\�zg�֞ʯ��y���{�@�V`-
���������)�o.�PǞկo�+����A�\0Z�E�?-�\\=�4ikfh�\n�<��H�/�Nx ���}:T��	1�\0�\0�� ����m
�\03� �=y��s�[ѥ���j�e���P��*(<�������zd(s�AL�r=�<�{�����T�����鎿����@u���ߦԂ?ڮ��Ǽ��S\\��]E�;]� �ڊ�Cq6�T.G���� =���z��Vd�\'������\0������/rJ��� ��;.��i ���n�����\0qz���]O\\�q�<u!��r �� ����ş�v9\'
�SG�J_��9� �
u����L��^H� �A�_}����(ƀ,�ߺWz�=G�\\g�4�,���4p�%	9�1����{�R� �q�܏�EK#��\07�=�����V�s�zc��r�]�ӇD�\\6=I�pb4.0I�f�3���\Z���my�6���J?z���\n�qPF0y�+ҩ#T(Zx\\zRr)�E�Tl;�R�4ϛq���`!d �V�>\r]`{�yT� �qA,���VUݨi8Q�[��EQ�1�>�q1�)�fx�8�e�1���65���6��G�Xw\0���\"�G;gc E���9$����y���o�m\0�oN�AOF:pTrhi�,\0c�0����ӏl���$��)?2���O�^��〽�wW9^�)\0l
��Q��zoN~c\r�	qs��*�;;�Y�@s��\'ܚYե���X��@� Y�����\no�<�%�}K��ڌ��E�y��R&d��;\"���\Z*������+�	��?q��4\rIH�����d��{��݉=)��lyg��@=�f=A�6}v�������W=��`�9�6�#<�\0��T��z�G���~����A�-�~�$.Nyۂr�s����Qd���-��G������������O�COL��R\0�`\"4���rq��\'��Z̒Bxc�3��;<Q� ��($z��\0��u�+g��N��ǉGE\r�\Zy\r�B�� ��(�͚±�2�c�n!TQ��On��֧\\j1��5m�����C#����\\�9\0�آ��M���A3ڤ�l�^+��A �J�o1��҂������m9�T�g������Epc�1�s��杯�.V4�B;TGB0j�\Z��)�ч��V��u>m�&?�wRq�Ȥ<�Ug�v*I�GC��v|w���23�\"4� ���y�n�z�Ω:�v#\0�_¹��\0�ȭ�9dn��[I0z�� ��H�@�)(;�T�{���*���\n6	ċ�:�8�y�kh���yXo��@n 8��WU\'x�T܎Ֆ-���o�a�<��\0vV��M�`1�X�9$yY�=V�4abx�?*2�貯���Մ ����9\'״MZ���(/���r3�dT~���E�\\\\\0͌zc�=�����.GBzxg�9�����O��c���rs�����_�6 A��,˔�e�
�ʀ=:����Җ9C�W��p}n��{P1�1��9��?\\����\n@F P��i����q�ӱ<�4�2�\0��6��m�K�71�G���ca�����KZ\0Y�CC��g�:��\0�\'�=Y���B�=�8�V��3(���N����O	�Ow�8�k#��+i���u�8�m��o�����PҠ��Pq�O5b�Ѯ!� {W&��qM-\n��#A2�V��G���۶Bs�ў*t�+`m9�z\Z����&�o=�xWj	:(�8�2I�)>���2	9�b����+6�O1��7 ʁ@A��Ո����͛�u��y�ҳ��9wf���$��b���?�_�VF�A�]ÚijK9�5y��գh������X��_�T�J��b���\rĲ��J��+a[�;������#��1�y�[p2Jh�.��Y�0�f���¡I${��N�$��3F����B�
7���zs]e�c�⹘U���E��:�;�Z��kb�d�R�>���Z��<HI��g��GC��T��V�c�*�����D�?��� ����3j��g=Uf29C͓��7��G��{S�wtWp�\0Y�������\0][�7���y,�Tc�C��µ����0p�����c����b�2$Kx7�r1��@9�X�C_Cl�XI#�z��-���	Ih�N~�\'h#�28U�d�SgE��bNwn{s�t��P2j���&W�\\� ߞ��O<U�d�ǘ�X��>�;��I�.�3�ܬ�J3�7~9�{u�\n�ꞣ��x���s�s�|���s�Q�JЅ}��\0l�^x�}=�����`�R%���f8����I�3��� \r�0��\0��M���1��.��Y��
�LgNEmD�b�q�x�z�XޏQ,W�o�W02*��|�=����W��!�J�ȥ-�B�g�9#ȩDC�AHӕEL����l�T�|����k�e��������#`RL�ո�\nqM���?/@z���25�� �8zQq[RǡOʐ��В�@��\rWx�����P��2Ն���Y�})�\'�Ja���ԐYG����N{\ntQ�ҁ�{���vB˜�;SWw)�n\"�M�
���8~By��I�?�f�\0�kA�^�a�$������AWz*�`��g�v��S�\0|��û;��wq32��NJ������%�\'���
�������z�$�Hy@����7����������=6���\0\\��]g(�5���\'\'����\'�����G��s��F�\0��\0=��\Z�C*Mt��y�����A��h���8�\0��?/\'���IǶZ��!��味�� �y�!�˹ծn�(Nv�1ܞ��#R䆓f����ӹ�,�:|�ǹ��[�+bv����\0�5�hM����9�\0\Z�*��g$r~���+�Qt\rX�����~R}����� ^1T��c�?6��\0x\r] $��?ҹ��ltQZ�c�(��?�5n%��m�1�\0
��hG��i-N�r�,�޵+�X�㚅���ҡ�`	���~Kv69��U���M�G<d\Z��\n��M��#� \n���.(�dש=j�bM��z��ɟ��ƚ.ӌ63I���5VR:��C���)Q���ኰR�T�w+�ި�s��SW�`��Y�nW�J���z�\\&��;E[��ɍ��¯SU�G�d�����_��\r�dy�S�3%O٣V�B�l<c���j Ȍ���8﵏�QI�ɷ�<���\0שa���h�А?��i;L婬 1R·;Y��0YǸ�*�Gp��Ę�=fz{Lݪ��D���\'�B{T4�s\Z(����E��\0�b��BE�[̼��1�V4S�cXu�GBG����\\��vv,�$ь�Ku���و�7)�}q�~���ZZH\n���\\���)֭�FB����E��\"��U?��Yڕ��d��LRxv2�#4\0�=@`��\Z�rѪ0�1�y�̟~�Ҭ\'�I�����Ӷږ<�$�9=k\n��6���3�jp��y�D����l/���#�����Ux��8?Ҳ�6�����~˔���dc:�ӸRy q[�I����T��1 \n�.��Ѣ;Y4�X����;�N �}\rhB4o�0ܬŔ)8�3�s�a��W����EoU�u��[+ �m|F���,3!QϛPI��3�ŞK6� ��H=�~��V���\"B �����Fr\r&�f��܆�Ày�Nx=+Y����\"�;�L��Ż�� ~���էاi�Ҷnf	 �\\�ҙ�I���� �*=\n����v�|�1�&��&��&���C�*�}��9�ΖJ�l��\0	�Z��*�u�W�Ь�yKX�8�	�\r.@d���P�J�I��6��$���G�\0�Рɴvoè��\0~�d� _h�_kZu�ʐ���>���;��.r����ߞ�Z*{%
	��i����T\\C.�ppܐxq�������H��ѽ�\0\Zߙdu�) �r�q�3�������$(�A�A�O\0\\f��3�	��pH# �aY��v�жUdʐx����e�$�*9��J�cI%	ur�`~e>��Tr$�sJ3˱���\\��T�6ې:�H����N�!�z���>腈��\\�]�t�V��\Z�F_�J�
֚I\\�_�\n�[i�&�Z���ӏ�R���Z]$���j�w�r_m:]�M��C�Q]-��ʪ�� �A��J66��_H�\0��(ǀ)� f�s�P���Q֡�`T�.\0#9�40#��%�n��	�OJ$�*�qTV���8�����ɹ�b���_QU�8v�6�kR6/A���o�	�\"�O[�;6sWm�B}�8�${S��o�4.�+C��f�x`<U�>L;/~�5��.2}sV\"o.�٠f�)�0��*{����>HZ%e����W�(����U�Q����\'ڮ$H���2�Rz�q����letsIY��]��Dl\0��{c��(�a�ZE? p1�A=T7���0�m���Y��t�G���Uٕo�$`ǧ �c�\"�2��W�8R����O�\'�\\��d���<u=���)t�G �1��~O�R�����F����j�H&���yF ؅\\�����lmw#������d�TU�]���d*����wwҧM�\Z���#��W8;� v��ݝkc2a��o�N�\0B=p)�.���p)�|���4 f�6�yg8�֞�$�g�w($\'�ұ��i���Л�.� ֒�I��t����8#�����i/�^}j�Y6>Y\n���Rf�].�H�7�\'_Z?�ݺ�ا�����>��
�Q�$眅�uzp��0*_)S�L�
�����HX\0:d�����}9��E;I�us�� =9�/Q�{�-%��z��\'�0�g� APct�������}jV��4��A�q�J\Z6��HOWN��\0�z���[#lvP{dq�y��\0�k\"��r=A�={~����>DD�1�;���\0]#�ں(�j-G��?g\n�Ԇ�#��G�E@�w)݈^� ���J�[XٍG��;�ǰi?��N��I�}�t�ǯcߊ�y�y\r��PI��Ÿ)�:nء@��C�CȤP���x���?�l#�⣸YUfY{\0	�X}�DWql1�#�A�����k����я��EeY�cZkR�����U����I�`G�Uy���rF�P0MqKs�l@�a}���R��v�n�:Z�um����\0�ƚ1%�t�ս31_���O��iDa�\\�3P�Jd�]��Q����\0��V�8�6�:X�Y\ri�\'\0\Z��uv�+��(�\0搱=�*5bxɧ�E�>����S@�J��@\"98Z�u��=��s�º9q���u5�ܱ��wʣJ#�3�ɕ6�A�Hϩ�j�q�`�Z	�L��\0ҢUP��=8Ϧkx��ET��@;�Aۚ�����4�]��\'#�ؐ��Q�[ܲ�9�3�l���d�!����$70��\r������ut�fD�tW�m���ϰ
��El
a\"7m�\'�RsEt�X��Tc���줃�qM�*\r��g�\\�\'���4��a�&��[��x�������C�P �׿5@C(0ǽ�\0�3�{�U\'�U�t�[66�a��f%�8�}����\"�]E�]��\0d�qX�5�DHX����P�Q�\'�үܮ��$\nŝ��x%���N��<X����7>�p{R�.Ac��Ο���1%�,l@� �P�!-+HGt}?��˄� ��6� �\0�����@�X�G�N�Li^I�aO��:�[p��Pj��aY\"�I�RX���ȩ��d�)s�RQ���&��l���fb ��f�����\n�u��x�\0SDH�`�X��7AU]J�w\0�j�u�d���7�$�测1�+�&c�����8�[���7ʃ�\0z֢[4�#!\\�ϧ�T�)���A8�ߑ]ֈ�\\+��8Ͽ�\0��7g	WQ��޺�>ٖ\0A��8|B��ұ䃎=h��%�~�V�G5���y�/�)Ap���V��۵EXv\06�e�Q�<N>�a����Es��&u�~� �\\����{�(�w��v.Y���H����2A�h��4%7ÂT�N��b��(�mfE��G��8�E=��p<�;S���QY3ru)�qE%\"A��E�V�\r��I����<{f�*�*8�
w�+>~۸�Eq1��*�̿��n�չbX|���ב�4QC4�\"]�z�7�]u�(L\0\0~�QZS3�O�Ȣ�+s#��','2014-10-22 11:46:28',0.899037,-0.003585),(9,NULL,'John','Miller','London',NULL,NULL,NULL,'England',1,'Europe',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1100.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1987-01-01',1100.00,1100.00,NULL,NULL,'en',NULL,NULL,NULL,NULL,'2014-12-15 11:53:34',0.898960,-0.003632),(10,NULL,'William','Smith','London',NULL,NULL,NULL,'England',1,'Europe',NULL,NULL,NULL,NULL,NULL,NULL,NULL,900.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1987-01-01',900.00,900.00,NULL,NULL,'en',NULL,NULL,NULL,NULL,'2014-12-15 12:24:10',0.899001,-0.003532);
/*!40000 ALTER TABLE `community_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `community_user_credits_availab`
--

DROP TABLE IF EXISTS `community_user_credits_availab`;
/*!50001 DROP VIEW IF EXISTS `community_user_credits_availab`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `community_user_credits_availab` (
  `oid` tinyint NOT NULL,
  `der_attr` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `community_user_credits_spent_v`
--

DROP TABLE IF EXISTS `community_user_credits_spent_v`;
/*!50001 DROP VIEW IF EXISTS `community_user_credits_spent_v`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `community_user_credits_spent_v` (
  `oid` tinyint NOT NULL,
  `der_attr` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `containers_mail`
--

DROP TABLE IF EXISTS `containers_mail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `containers_mail` (
  `oid` int(11) NOT NULL,
  `language_code` varchar(255) DEFAULT NULL,
  `text` text,
  `alias` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `containers_mail`
--

LOCK TABLES `containers_mail` WRITE;
/*!40000 ALTER TABLE `containers_mail` DISABLE KEYS */;
INSERT INTO `containers_mail` VALUES (1,'en','<p>New User Registration ---</p>\r\n','Header Email Registration');
/*!40000 ALTER TABLE `containers_mail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gamified_application`
--

DROP TABLE IF EXISTS `gamified_application`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gamified_application` (
  `oid` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `api_key` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gamified_application`
--

LOCK TABLES `gamified_application` WRITE;
/*!40000 ALTER TABLE `gamified_application` DISABLE KEYS */;
INSERT INTO `gamified_application` VALUES (1,'SES',NULL);
/*!40000 ALTER TABLE `gamified_application` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gamifiedapplication_thematic_a`
--

DROP TABLE IF EXISTS `gamifiedapplication_thematic_a`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gamifiedapplication_thematic_a` (
  `gamified_application_oid` int(11) NOT NULL,
  `thematic_area_oid` int(11) NOT NULL,
  PRIMARY KEY (`gamified_application_oid`,`thematic_area_oid`),
  KEY `fk_gamifiedapplication_themati` (`gamified_application_oid`),
  KEY `fk_gamifiedapplication_thema_2` (`thematic_area_oid`),
  CONSTRAINT `fk_gamifiedapplication_thema_2` FOREIGN KEY (`thematic_area_oid`) REFERENCES `thematic_area` (`oid`),
  CONSTRAINT `fk_gamifiedapplication_themati` FOREIGN KEY (`gamified_application_oid`) REFERENCES `gamified_application` (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gamifiedapplication_thematic_a`
--

LOCK TABLES `gamifiedapplication_thematic_a` WRITE;
/*!40000 ALTER TABLE `gamifiedapplication_thematic_a` DISABLE KEYS */;
INSERT INTO `gamifiedapplication_thematic_a` VALUES (1,1),(1,2),(1,3),(1,4);
/*!40000 ALTER TABLE `gamifiedapplication_thematic_a` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `geographical_area`
--

DROP TABLE IF EXISTS `geographical_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `geographical_area` (
  `oid` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `geographical_area`
--

LOCK TABLES `geographical_area` WRITE;
/*!40000 ALTER TABLE `geographical_area` DISABLE KEYS */;
INSERT INTO `geographical_area` VALUES (1,'North America'),(2,'Latin America'),(3,'Europe'),(4,'Asia, Africa & Oceania');
/*!40000 ALTER TABLE `geographical_area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `goal`
--

DROP TABLE IF EXISTS `goal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `goal` (
  `oid` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `completion_date` date DEFAULT NULL,
  `community_user_user_id` int(11) DEFAULT NULL,
  `badge_type_oid` int(11) DEFAULT NULL,
  `active` bit(1) DEFAULT NULL,
  `consumption` decimal(19,2) DEFAULT NULL,
  PRIMARY KEY (`oid`),
  KEY `fk_goal_community_user` (`community_user_user_id`),
  KEY `fk_goal_badge_type` (`badge_type_oid`),
  CONSTRAINT `fk_goal_badge_type` FOREIGN KEY (`badge_type_oid`) REFERENCES `badge_type` (`oid`),
  CONSTRAINT `fk_goal_community_user` FOREIGN KEY (`community_user_user_id`) REFERENCES `community_user` (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `goal`
--

LOCK TABLES `goal` WRITE;
/*!40000 ALTER TABLE `goal` DISABLE KEYS */;
INSERT INTO `goal` VALUES (1,'Starter Saver',NULL,2,17,'',0.40),(2,'Saving Scout',NULL,2,18,'',0.35),(3,'Saving Ambassador',NULL,2,19,'',0.25);
/*!40000 ALTER TABLE `goal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `goal_action_type`
--

DROP TABLE IF EXISTS `goal_action_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `goal_action_type` (
  `goal_oid` int(11) NOT NULL,
  `action_type_oid` int(11) NOT NULL,
  PRIMARY KEY (`goal_oid`,`action_type_oid`),
  KEY `fk_goal_action_type_goal` (`goal_oid`),
  KEY `fk_goal_action_type_action_typ` (`action_type_oid`),
  CONSTRAINT `fk_goal_action_type_action_typ` FOREIGN KEY (`action_type_oid`) REFERENCES `action_type` (`oid`),
  CONSTRAINT `fk_goal_action_type_goal` FOREIGN KEY (`goal_oid`) REFERENCES `goal` (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `goal_action_type`
--

LOCK TABLES `goal_action_type` WRITE;
/*!40000 ALTER TABLE `goal_action_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `goal_action_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `group_moduletable`
--

DROP TABLE IF EXISTS `group_moduletable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `group_moduletable` (
  `groupoid` int(11) NOT NULL,
  `moduleoid` int(11) NOT NULL,
  PRIMARY KEY (`groupoid`,`moduleoid`),
  KEY `idx_group_moduletable_grouptab` (`groupoid`),
  KEY `idx_group_moduletable_siteview` (`moduleoid`),
  CONSTRAINT `fk_group_moduletable_grouptabl` FOREIGN KEY (`groupoid`) REFERENCES `grouptable` (`oid_2`),
  CONSTRAINT `fk_group_moduletable_siteviewt` FOREIGN KEY (`moduleoid`) REFERENCES `siteviewtable` (`oid_2`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `group_moduletable`
--

LOCK TABLES `group_moduletable` WRITE;
/*!40000 ALTER TABLE `group_moduletable` DISABLE KEYS */;
/*!40000 ALTER TABLE `group_moduletable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `grouptable`
--

DROP TABLE IF EXISTS `grouptable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grouptable` (
  `oid_2` int(11) NOT NULL,
  `groupname` varchar(255) DEFAULT NULL,
  `siteviewoid` int(11) DEFAULT NULL,
  PRIMARY KEY (`oid_2`),
  KEY `idx_grouptable_siteviewtable` (`siteviewoid`),
  CONSTRAINT `fk_grouptable_siteviewtable` FOREIGN KEY (`siteviewoid`) REFERENCES `siteviewtable` (`oid_2`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grouptable`
--

LOCK TABLES `grouptable` WRITE;
/*!40000 ALTER TABLE `grouptable` DISABLE KEYS */;
INSERT INTO `grouptable` VALUES (1,'Administrator',1),(2,'Community User',2),(3,'Customer',3);
/*!40000 ALTER TABLE `grouptable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `headquarter_user_partecipation`
--

DROP TABLE IF EXISTS `headquarter_user_partecipation`;
/*!50001 DROP VIEW IF EXISTS `headquarter_user_partecipation`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `headquarter_user_partecipation` (
  `oid` tinyint NOT NULL,
  `partecipation` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `headquarter_user_participation_monthly`
--

DROP TABLE IF EXISTS `headquarter_user_participation_monthly`;
/*!50001 DROP VIEW IF EXISTS `headquarter_user_participation_monthly`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `headquarter_user_participation_monthly` (
  `oid` tinyint NOT NULL,
  `participation_monthly` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `headquarter_user_participation_seven_days`
--

DROP TABLE IF EXISTS `headquarter_user_participation_seven_days`;
/*!50001 DROP VIEW IF EXISTS `headquarter_user_participation_seven_days`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `headquarter_user_participation_seven_days` (
  `oid` tinyint NOT NULL,
  `participation_seven_days` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `job_blob_triggers`
--

DROP TABLE IF EXISTS `job_blob_triggers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_blob_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `BLOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `SCHED_NAME` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `JOB_BLOB_TRIGGERS_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `job_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_blob_triggers`
--

LOCK TABLES `job_blob_triggers` WRITE;
/*!40000 ALTER TABLE `job_blob_triggers` DISABLE KEYS */;
/*!40000 ALTER TABLE `job_blob_triggers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_calendars`
--

DROP TABLE IF EXISTS `job_calendars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_calendars` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `CALENDAR_NAME` varchar(200) NOT NULL,
  `CALENDAR` blob NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`CALENDAR_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_calendars`
--

LOCK TABLES `job_calendars` WRITE;
/*!40000 ALTER TABLE `job_calendars` DISABLE KEYS */;
/*!40000 ALTER TABLE `job_calendars` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_cron_triggers`
--

DROP TABLE IF EXISTS `job_cron_triggers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_cron_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `CRON_EXPRESSION` varchar(120) NOT NULL,
  `TIME_ZONE_ID` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `SCHED_NAME` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `JOB_CRON_TRIGGERS_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `job_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_cron_triggers`
--

LOCK TABLES `job_cron_triggers` WRITE;
/*!40000 ALTER TABLE `job_cron_triggers` DISABLE KEYS */;
INSERT INTO `job_cron_triggers` VALUES ('_job.scheduler','trg1','WEBRATIO','00 00 00 1 * ?','Europe/Berlin'),('_job.scheduler','trg1q','WEBRATIO','00 00 1 * * ?','Europe/Berlin');
/*!40000 ALTER TABLE `job_cron_triggers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_fired_triggers`
--

DROP TABLE IF EXISTS `job_fired_triggers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_fired_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `ENTRY_ID` varchar(95) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `INSTANCE_NAME` varchar(200) NOT NULL,
  `FIRED_TIME` bigint(13) NOT NULL,
  `PRIORITY` int(11) NOT NULL,
  `STATE` varchar(16) NOT NULL,
  `JOB_NAME` varchar(200) DEFAULT NULL,
  `JOB_GROUP` varchar(200) DEFAULT NULL,
  `IS_NONCONCURRENT` varchar(1) DEFAULT NULL,
  `REQUESTS_RECOVERY` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`ENTRY_ID`),
  KEY `IDX_JOB_FT_TRIG_INST_NAME` (`SCHED_NAME`,`INSTANCE_NAME`),
  KEY `IDX_JOB_FT_INST_JOB_REQ_RCVRY` (`SCHED_NAME`,`INSTANCE_NAME`,`REQUESTS_RECOVERY`),
  KEY `IDX_JOB_FT_J_G` (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  KEY `IDX_JOB_FT_JG` (`SCHED_NAME`,`JOB_GROUP`),
  KEY `IDX_JOB_FT_T_G` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `IDX_JOB_FT_TG` (`SCHED_NAME`,`TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_fired_triggers`
--

LOCK TABLES `job_fired_triggers` WRITE;
/*!40000 ALTER TABLE `job_fired_triggers` DISABLE KEYS */;
/*!40000 ALTER TABLE `job_fired_triggers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_job_details`
--

DROP TABLE IF EXISTS `job_job_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_job_details` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `JOB_NAME` varchar(200) NOT NULL,
  `JOB_GROUP` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(250) DEFAULT NULL,
  `JOB_CLASS_NAME` varchar(250) NOT NULL,
  `IS_DURABLE` varchar(1) NOT NULL,
  `IS_NONCONCURRENT` varchar(1) NOT NULL,
  `IS_UPDATE_DATA` varchar(1) NOT NULL,
  `REQUESTS_RECOVERY` varchar(1) NOT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  KEY `IDX_JOB_J_REQ_RECOVERY` (`SCHED_NAME`,`REQUESTS_RECOVERY`),
  KEY `IDX_JOB_J_GRP` (`SCHED_NAME`,`JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_job_details`
--

LOCK TABLES `job_job_details` WRITE;
/*!40000 ALTER TABLE `job_job_details` DISABLE KEYS */;
INSERT INTO `job_job_details` VALUES ('_job.scheduler','job1','WEBRATIO','Check Consumption Badge','com.webratio.rtx.jobs.QuartzJob','0','0','0','0','��\0sr\0org.quartz.JobDataMap���迩��\0\0xr\0&org.quartz.utils.StringKeyDirtyFlagMap�����](\0Z\0allowsTransientDataxr\0org.quartz.utils.DirtyFlagMap�.�(v\n�\0Z\0dirtyL\0mapt\0Ljava/util/Map;xpsr\0java.util.HashMap ���`�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0t\0\rjobInstanceIdt\0job1x\0'),('_job.scheduler','job17x','WEBRATIO','Reset Participation 7 Days','com.webratio.rtx.jobs.QuartzJob','0','0','0','0','��\0sr\0org.quartz.JobDataMap���迩��\0\0xr\0&org.quartz.utils.StringKeyDirtyFlagMap�����](\0Z\0allowsTransientDataxr\0org.quartz.utils.DirtyFlagMap�.�(v\n�\0Z\0dirtyL\0mapt\0Ljava/util/Map;xpsr\0java.util.HashMap ���`�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0t\0\rjobInstanceIdt\0job17xx\0');
/*!40000 ALTER TABLE `job_job_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_locks`
--

DROP TABLE IF EXISTS `job_locks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_locks` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `LOCK_NAME` varchar(40) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`LOCK_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_locks`
--

LOCK TABLES `job_locks` WRITE;
/*!40000 ALTER TABLE `job_locks` DISABLE KEYS */;
/*!40000 ALTER TABLE `job_locks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_paused_trigger_grps`
--

DROP TABLE IF EXISTS `job_paused_trigger_grps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_paused_trigger_grps` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_paused_trigger_grps`
--

LOCK TABLES `job_paused_trigger_grps` WRITE;
/*!40000 ALTER TABLE `job_paused_trigger_grps` DISABLE KEYS */;
/*!40000 ALTER TABLE `job_paused_trigger_grps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_scheduler_state`
--

DROP TABLE IF EXISTS `job_scheduler_state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_scheduler_state` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `INSTANCE_NAME` varchar(200) NOT NULL,
  `LAST_CHECKIN_TIME` bigint(13) NOT NULL,
  `CHECKIN_INTERVAL` bigint(13) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`INSTANCE_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_scheduler_state`
--

LOCK TABLES `job_scheduler_state` WRITE;
/*!40000 ALTER TABLE `job_scheduler_state` DISABLE KEYS */;
/*!40000 ALTER TABLE `job_scheduler_state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_simple_triggers`
--

DROP TABLE IF EXISTS `job_simple_triggers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_simple_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `REPEAT_COUNT` bigint(7) NOT NULL,
  `REPEAT_INTERVAL` bigint(12) NOT NULL,
  `TIMES_TRIGGERED` bigint(10) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `SCHED_NAME` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `JOB_SIMPLE_TRIGGERS_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `job_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_simple_triggers`
--

LOCK TABLES `job_simple_triggers` WRITE;
/*!40000 ALTER TABLE `job_simple_triggers` DISABLE KEYS */;
/*!40000 ALTER TABLE `job_simple_triggers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_simprop_triggers`
--

DROP TABLE IF EXISTS `job_simprop_triggers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_simprop_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `STR_PROP_1` varchar(512) DEFAULT NULL,
  `STR_PROP_2` varchar(512) DEFAULT NULL,
  `STR_PROP_3` varchar(512) DEFAULT NULL,
  `INT_PROP_1` int(11) DEFAULT NULL,
  `INT_PROP_2` int(11) DEFAULT NULL,
  `LONG_PROP_1` bigint(20) DEFAULT NULL,
  `LONG_PROP_2` bigint(20) DEFAULT NULL,
  `DEC_PROP_1` decimal(13,4) DEFAULT NULL,
  `DEC_PROP_2` decimal(13,4) DEFAULT NULL,
  `BOOL_PROP_1` varchar(1) DEFAULT NULL,
  `BOOL_PROP_2` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `JOB_SIMPROP_TRIGGERS_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `job_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_simprop_triggers`
--

LOCK TABLES `job_simprop_triggers` WRITE;
/*!40000 ALTER TABLE `job_simprop_triggers` DISABLE KEYS */;
/*!40000 ALTER TABLE `job_simprop_triggers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_triggers`
--

DROP TABLE IF EXISTS `job_triggers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `JOB_NAME` varchar(200) NOT NULL,
  `JOB_GROUP` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(250) DEFAULT NULL,
  `NEXT_FIRE_TIME` bigint(13) DEFAULT NULL,
  `PREV_FIRE_TIME` bigint(13) DEFAULT NULL,
  `PRIORITY` int(11) DEFAULT NULL,
  `TRIGGER_STATE` varchar(16) NOT NULL,
  `TRIGGER_TYPE` varchar(8) NOT NULL,
  `START_TIME` bigint(13) NOT NULL,
  `END_TIME` bigint(13) DEFAULT NULL,
  `CALENDAR_NAME` varchar(200) DEFAULT NULL,
  `MISFIRE_INSTR` smallint(2) DEFAULT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `SCHED_NAME` (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  KEY `IDX_JOB_T_J` (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  KEY `IDX_JOB_T_JG` (`SCHED_NAME`,`JOB_GROUP`),
  KEY `IDX_JOB_T_C` (`SCHED_NAME`,`CALENDAR_NAME`),
  KEY `IDX_JOB_T_G` (`SCHED_NAME`,`TRIGGER_GROUP`),
  KEY `IDX_JOB_T_STATE` (`SCHED_NAME`,`TRIGGER_STATE`),
  KEY `IDX_JOB_T_N_STATE` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`,`TRIGGER_STATE`),
  KEY `IDX_JOB_T_N_G_STATE` (`SCHED_NAME`,`TRIGGER_GROUP`,`TRIGGER_STATE`),
  KEY `IDX_JOB_T_NEXT_FIRE_TIME` (`SCHED_NAME`,`NEXT_FIRE_TIME`),
  KEY `IDX_JOB_T_NFT_ST` (`SCHED_NAME`,`TRIGGER_STATE`,`NEXT_FIRE_TIME`),
  KEY `IDX_JOB_T_NFT_MISFIRE` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`),
  KEY `IDX_JOB_T_NFT_ST_MISFIRE` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`,`TRIGGER_STATE`),
  KEY `IDX_JOB_T_NFT_ST_MISFIRE_GRP` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`,`TRIGGER_GROUP`,`TRIGGER_STATE`),
  CONSTRAINT `JOB_TRIGGERS_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) REFERENCES `job_job_details` (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_triggers`
--

LOCK TABLES `job_triggers` WRITE;
/*!40000 ALTER TABLE `job_triggers` DISABLE KEYS */;
INSERT INTO `job_triggers` VALUES ('_job.scheduler','trg1','WEBRATIO','job1','WEBRATIO',NULL,1427839200000,-1,5,'WAITING','CRON',1427320480000,0,NULL,0,''),('_job.scheduler','trg1q','WEBRATIO','job17x','WEBRATIO',NULL,1427328000000,-1,5,'WAITING','CRON',1427320480000,0,NULL,0,'');
/*!40000 ALTER TABLE `job_triggers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `max_date_action_instance`
--

DROP TABLE IF EXISTS `max_date_action_instance`;
/*!50001 DROP VIEW IF EXISTS `max_date_action_instance`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `max_date_action_instance` (
  `action_type_oid` tinyint NOT NULL,
  `rank_oid` tinyint NOT NULL,
  `maxDate` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `mostimportant_badge`
--

DROP TABLE IF EXISTS `mostimportant_badge`;
/*!50001 DROP VIEW IF EXISTS `mostimportant_badge`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `mostimportant_badge` (
  `oid` tinyint NOT NULL,
  `rankoid` tinyint NOT NULL,
  `area` tinyint NOT NULL,
  `title` tinyint NOT NULL,
  `importance` tinyint NOT NULL,
  `checked_image_2` tinyint NOT NULL,
  `checked_imageblob` tinyint NOT NULL,
  `hd_checked_image_2` tinyint NOT NULL,
  `hd_checked_imageblob` tinyint NOT NULL,
  `sort_number` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `notification`
--

DROP TABLE IF EXISTS `notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notification` (
  `oid` int(11) NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `code` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `delivery_date` timestamp NULL DEFAULT NULL,
  `language_code` varchar(255) DEFAULT NULL,
  `rank_oid` int(11) DEFAULT NULL,
  `reward_type_oid` int(11) DEFAULT NULL,
  `text_mail_oid` int(11) DEFAULT NULL,
  PRIMARY KEY (`oid`),
  KEY `idx_notification_rank` (`rank_oid`),
  KEY `idx_notification_reward_type` (`reward_type_oid`),
  KEY `idx_notification_text_mail` (`text_mail_oid`),
  CONSTRAINT `fk_notification_rank` FOREIGN KEY (`rank_oid`) REFERENCES `community_user` (`oid`),
  CONSTRAINT `fk_notification_reward_type` FOREIGN KEY (`reward_type_oid`) REFERENCES `reward_type` (`oid`),
  CONSTRAINT `fk_notification_text_mail` FOREIGN KEY (`text_mail_oid`) REFERENCES `text_mail` (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notification`
--

LOCK TABLES `notification` WRITE;
/*!40000 ALTER TABLE `notification` DISABLE KEYS */;
INSERT INTO `notification` VALUES (1,'2015-02-25 14:50:35','community.newBadge','Created',NULL,'en',2,NULL,NULL),(2,'2015-02-25 14:51:06','community.newReward','Send Failed Text Mail Null',NULL,'en',2,1,18),(3,'2015-02-25 14:51:49','community.newBadge','Created',NULL,'en',2,NULL,NULL),(4,'2015-02-25 14:53:25','community.newReward.onlyOne','Send Failed Text Mail Null',NULL,'en',2,2,22),(5,'2015-02-25 14:55:22','community.newBadge','Created',NULL,'en',2,NULL,NULL),(6,'2015-02-25 14:55:22','community.newBadge','Created',NULL,'en',7,NULL,NULL),(7,'2015-02-25 14:55:22','community.newBadge','Created',NULL,'en',9,NULL,NULL),(8,'2015-02-25 14:56:13','community.newBadge','Created',NULL,'en',4,NULL,NULL),(9,'2015-02-25 14:56:13','community.newBadge','Created',NULL,'en',5,NULL,NULL),(10,'2015-02-25 14:56:13','community.newBadge','Created',NULL,'en',7,NULL,NULL),(11,'2015-02-25 14:56:59','community.newBadge','Created',NULL,'en',2,NULL,NULL),(12,'2015-02-25 14:56:59','community.newBadge','Created',NULL,'en',4,NULL,NULL),(13,'2015-02-25 14:57:00','community.newReward.onlyOne','Send Failed Text Mail Null',NULL,'en',4,2,22),(14,'2015-02-25 14:57:00','community.newReward','Send Failed Text Mail Null',NULL,'en',5,1,18),(15,'2015-02-25 14:57:01','community.newReward','Send Failed Text Mail Null',NULL,'en',7,1,18),(16,'2015-02-25 15:10:23','community.newBadge','Created',NULL,'en',6,NULL,NULL),(17,'2015-02-25 15:11:16','community.newReward','Send Failed Text Mail Null',NULL,'en',6,1,18),(18,'2015-02-25 15:12:03','community.newBadge','Created',NULL,'en',3,NULL,NULL),(19,'2015-02-25 15:12:03','community.newReward','Send Failed Text Mail Null',NULL,'en',3,1,18),(20,'2015-02-25 15:12:14','community.newReward.onlyOne','Send Failed Text Mail Null',NULL,'en',3,2,22),(21,'2015-02-25 15:12:44','community.newBadge','Created',NULL,'en',7,NULL,NULL),(22,'2015-02-25 15:12:45','community.newReward.onlyOne','Send Failed Text Mail Null',NULL,'en',7,2,22);
/*!40000 ALTER TABLE `notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reward_instance`
--

DROP TABLE IF EXISTS `reward_instance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reward_instance` (
  `oid` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `score` decimal(19,2) DEFAULT NULL,
  `rank_oid` int(11) DEFAULT NULL,
  `reward_type_oid` int(11) DEFAULT NULL,
  PRIMARY KEY (`oid`),
  KEY `idx_reward_instance_rank` (`rank_oid`),
  KEY `idx_reward_instance_reward_typ` (`reward_type_oid`),
  CONSTRAINT `fk_reward_instance_rank` FOREIGN KEY (`rank_oid`) REFERENCES `community_user` (`oid`),
  CONSTRAINT `fk_reward_instance_reward_type` FOREIGN KEY (`reward_type_oid`) REFERENCES `reward_type` (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reward_instance`
--

LOCK TABLES `reward_instance` WRITE;
/*!40000 ALTER TABLE `reward_instance` DISABLE KEYS */;
/*!40000 ALTER TABLE `reward_instance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reward_type`
--

DROP TABLE IF EXISTS `reward_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reward_type` (
  `oid` int(11) NOT NULL,
  `needed_points` decimal(19,2) DEFAULT NULL,
  `available` tinyint(1) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `language_code` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `image_2` varchar(255) DEFAULT NULL,
  `imageblob` longblob,
  PRIMARY KEY (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reward_type`
--

LOCK TABLES `reward_type` WRITE;
/*!40000 ALTER TABLE `reward_type` DISABLE KEYS */;
INSERT INTO `reward_type` VALUES (1,1200.00,1,NULL,'en','Home Water and Energy Saving Kit','<p style=\"font-size: 12.8000001907349px; line-height: 20.7999992370605px;\">This award winning box of nine water saving devices (including educational&nbsp;and easy fit products) can save you up to &pound;283 per year, and includes:</p>\r\n\r\n<ul style=\"font-size: 12.8000001907349px; line-height: 20.7999992370605px;\">\r\n	<li>Water saving slimline shower head - compatible with electric showers</li>\r\n	<li>Kitchen tap aerator</li>\r\n	<li>2 bathroom tap aerators</li>\r\n	<li>Shower timer</li>\r\n	<li>Cistern water saver</li>\r\n	<li>Folding water bottle</li>\r\n	<li>Room thermometer</li>\r\n	<li>Trumps cards</li>\r\n	<li>Wrist band</li>\r\n</ul>\r\n','Home Water and Energy Saving Kit_ulpM_big.jpg','����\0JFIF\0\0\0\0\0\0��\0C\0    		\n\r

\Z\Z $.\' \",#(7),01444\'9=82<.342��\0C			
\r\r2!!22222222222222222222222222222222222222222222222222��\0\0�\0�\"\0��\0\0\0\0\0\0\0\0\0\0\0 	\n
��\0�\0\0\0}\0!1AQa \"q2���#B��R��$3br�	\n\Z%&\'()*456789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz���������������������������������������������������������������������������\0\0\0\0\0\0\0\0 	\n
��\0�\0 \0w\0!1AQ aq\"2�B����	#3R�br�\n$4�%�\Z&\'()*56789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz��������������������������������������������������������������������������\0\0\0?\0��(��\n(��\n(��\n(��\n(��\n(��\n(��\n(��\n(�=\r\0W�g��\0s�y�����N��S�J�؟�G��(j(��\n(��\n(��\n(��\n(��\n(��\n(��\n+XדO��J$#�Ǒ�@ $�+��Y�/�n/d�_%QŃׅ���>��4W�i����+˙I�A���2	�\0���g�k�_�˟�_J\0آ�(\0��(oi�󘥒T�� 16�ށ��̼���Ys���G��z��t�.�l��0��@�Fz�s����Eٞ�����pMztppj2�S�q��xJP�ow}?��d�+�C�f\\���4��O4�E����!?��:��IpѶY�\\)\0(���[�-uMm��CE^`���}���������s|]Z���M��=L�����[�d�|�[���V����\r;M�FmKN�!�كE4@�`1� ��]m��u�R���1��F*�����*M�өk����2�QY�AEPEPEPEPY\Z��ۯ��o˾B:��\0S�kן����X�{�>D��\0�@
�u��Bgj7�Kqm#nX̻\n�*�q��\0{o?Ӧe��[�p�8���_8U
��n�N@�)��ooos�go���T�;��� ���Y�.Wwo-7�őG\0rH�s����Z���۷���D<���\\�~e=q�W�ݠw�+Y �q�~A�����5�kn��Oqm�HRBSiI7T��X�d�dz��<=�K5�֓}�[_��Rv��GO��4ߩ%A# �(�0��(�|g���g?��RE�co�SP_�\'��t#y���0�n�D���~t?�ָ�$8���W�a=����+�b����ԡu(���p�7ę%6�B\0����^\\�\\���l�r���T�ì\\&N#\0 ����qަ#$~�Ԓq�C���\Z�����]]ꫧ�n��a]��<gh�ڽ�%	\"�\0\0�+��j�x�\'\"(T���+�+��\ZSP]�ɢ�IՓ�z}è����h(��\0(��\0(��\0(��\0+�����I=��|@��z��\0��(�yf�e/32��0@6�<`d��#��ة2�*Y	h�q����#ֵ|Usg���v����H2p�A�\0�r~���䅌n$�c���v�7���&�%��
��CI1K6s���{W�[[Ci\n�A�Zm���Y���1�sV�J��I�,n$g��E���Q��\n螊`���N��s��w~�D�-�l�p�1#�ghz�q�0%K rk����Ǩ�F�I\"�� �\nH��3\\���%�6V`�8���`��#�s{���d��q���M+����r�\08{\ZVR*֩�[i������!�:�\0��5VB�]P�qM��Q���x���C�bYLA��0��$��Ls\\�Żg$���\0#�WF+�1Sr�+������iᠣ�_�EV xQE\0QE\0QE\0QE\0QE��\0���+{&�Q�X��x�R ��?/���q}ge-���!=��t�;������#4�s&�9G�	?�ڏ�	7�$�$�\0Yy�ci$/N�`cڷS~ϔ����;�(��62ui��r[�\0�PMQ`P�C��岿Ҵ5%zw����VY�g�>�\0f\0���=;u��o�uRm1e��B\neOp_vkSG25�iv�ci���\\ƥ��[\\���e���\n �x��]����U��\r#�Go�ө	F	�e���Q�;�y��\'�ݩ9\"F��hc����9����b���z�=�ω���Y��\0��?  ��ZK��ae*\n��\0\0g9�QҊ^_��x�&�\0��5�Ip\'�*��8\'��\'��Ҳ��U�jv��Y���R��q�Ӂ��rB�޶���L[��g�|=��t)\"\'�L� Ѐ�k��\\�c���`c?����Nk��q�i#���� ��-	(���=��(\0��(\0��(\0��(\0��(��-�XZ�N��	�/Rw��I7��w@�Y=��*�?ҧ��I4�^�����\Z#g>T��*����Dr!�$S���\n?�T�!�z�@�H�N�,�u��m�yR������q�\\���\\�����¤k�tH���9��Ө��W���ms�V�d��i������æ��ų�:�e,O��v8���U�U���)�\\�vgxV��s�Mz�{ou̸b0���ڻmv��2w1ǻ\\~��Y\Z}��pǆt�����~���#��{$=V �TN�j��k��\ZT\'ioc�|@�|O�����\0�E��qsyY 3����=���U�huw��Ĉo�����/����\0��oh˥�$D�6���7�s�+�X�*J��<	eUg�|�ѽ�~_^]�������@������JV�Igw�v%J���?3]�BطnW�����Kg�\n�c*�nv�\'�KF��ݔ<e=���&�P謧��k��ϭfD��m��)�e��V�rV��77��p�#B���EVGHQE\0QE\0QE\0Rt�>��w��6%�֣k/UiF��u��{�搠����~��\r�L��X�<����k6���T���5
�����8�\0�V�ͫ��օ�sG�6�<�\'�tE�L�t�����~�X��f��o%ʖof����/��u[t���9�A��zd:U��f��7RME�ls���ʼS�)uC�@��}�1]��w�k4lf�cX����1K\'�Zuc��p��@g���zRxb�E�bs\Z�^1�c�oΐ�GM�:������<��,x��؏����n1���k�$2
׆�\'���L�3ӓ��]��tk6��-���1�R0�޸$��<���bW�T��1�}訓MGo1S������q:>�:l��̲O~O$�l]�H�����ڽ.(�p�u@��?$V1Z%��E��� \0=z�] (��$s�ܥ�#��
����\Z6��\0	��a<�8�Ux����l�=��N8�@+� �� �#B�2�=�\\-�\\���v1�//.���{�Qs<�\n��:�,no,l��
/�\'�����\'�(#��x��,�yef
��\'�T����B�hI��`3�����m��r�j2�Ė���i�+�V-��:����Z��TW2��&�/��m��c.���d��3�޺��T(�+����\"���袊�@��(���\0g��o-��Զ�f��\0��>����-���y�Q[�,J������HD+!,��\\�5pq_��H���#���n�rG�n��#�I$���g����R|Ҽ6H�ۼ�g���-ď6�:!on�%֟g<a.Q(!H8�� �+UR	��Xz�Wsw<��N�5�h�=}�j��y:��zz�}���h�����9��P���k��!9Tf�ۓۿ�?!X��i�32�����d��W|����\\��w��e�}�#�������9��sZ�M����x5J�k\nAEڜ�sN�I�I�k)=[:��Y+\ZT�Ls����I\'�Z�A���/���̳\\2�ݠ�}�����n�]��Թ?��\0>Ԛ)>�E$Q̻d�]}+��� �t�ye���Y��!��$��9�Һ��{h��S���%B���i��Is+�MI��3ʮ�-j��,zg����#�\\)��\nа��%ڭ�h�q~�1�3F�\'���ڹ��1D�V]>92\"h.�3���nk��W��x�K�}B^Vi���9�!ʝ���שV�	S�;_���t*֌�;��Mq�爆��%���[0Y�����ʐè8<�\Z�3�|�uu���\\O%ش���1�� ��ryz�խ;X��}��\0kO+�r~�k�j���~����l����1ګ]Y%����u�QX�\n�\nX�^�.�e `1RFq��5���piVf�rv�*���z\\N-K����Nv����M���oL6`����U���`����C��4x4�)��7�� ��V�:��.V�-�����?U:����J�n~�-�ܖ���X՚���Pjq��NQ������Z�&���Ӝg(��>�(�,(��\0*���N��O��P qsw
�1�)�$�W�Ҹ-���p������\\���7�ֱ\'�[Gm��x��/C��/.#�9���Rwˌc�=?:���]#���Gu�zք��	u���%p1�9��>SYm�Ɗ>�?21O&3#�B��� �q��ѓwz��$�����Y�VS����&�8=�ϥ[����6�5�l����q�>��eԵSi��o�_do��A��צO�=*�\0�<�۠{ϲ̮�}�N��T6���R:����:��!����&&(�%%a�^�{֩��c�X���G+�=~���4�G������D����N�뎝8��
Z$\"kv��H�#GA��T˗h�C�-fj��\r\n�L�l�89Ǳ��eu�OBA�\0_ ��H�J��\"���(ܧ�����f7ř5�*�L�41��XU�\\�H8��\0v犻�uk�#R�ut���!��ڡI�\0c�}�z��7��4$20���ժ c��~�j�Kso!��C�C#]�������\"r���f~��AX�\0��q�W��[�Z1�R������V����lⴴ�EK�v���X�#ǽ �e`Wp��G�o��}��*�m���X�l�A>��0mO��iR������omncF�da���GQ�����a,\\0��5�$^�e�X�X�<R��?Ps���ӋN���@�L\\�K�NNI���ۡ|�J�����]�\"%uX����\0?���J�(�� �(EQ��0\0�T��Nr�m�Х젡��d�QEA�QE\0QE\0�P�T�5VkQ,{�^�ڮSXdqױ�&�B����9̗y����#-���A��1�T��\\{m����\'�I��1�l\n�5�8� �\Z��\\D��~�͚�wk3?f��#UC�2\\�<��J�}^���5����Tƃ4,�\nƹ����I��?�\Z�@]��el��1���Z��eϋ.�$0ڑ�N� ��5�q�D�ZiKHy��f?ֺI|;e\Z�9NO��u�� ��O���ɺE�ت:�zi!7$g&�n$�[M1a�ڿ{�Z0�ڔɖ�t$r�[\Z}ݵ���m�m�(�X����{���E����H(����d8���ѓS�.�Ԗ����!����G\"ʁ������|N#���)s��\0��WOׯ#�d����\0��h�������Q^B%������!�$t�^A�.��������xy6�� s���˖�n��c��o�^�5����|� T�� ��\0�\nq��پ��ҕ%Q�)5d�ӳ��Ҥ��6��\\���D ���y�8�z%�����,d�a����\0�+��p����S�#��~l�g>��\0����ЏҺV&3���9k��W�u}]�g_�:�������R*��K(�`����V}��#�X��o.$�)�Y�p@5��˴��A�p��\0JÓÑ=ɘ�n���~Ʌ;��#�ϩ�<rϛ��:i����;+
��c��rc�V�c�0�e�#@Ab#]��rN=�lS{����)(��\0(��\0�U,���=�MZ�˔��py�\rn�?��bT7q���z�Bc$����C�;5�/���)\0I�rq�y��Tdb)bq޳.l��Q;��ȟz�\n?h��+8[�\\���s	��jn�j�\rbK���.eeLn��>�O�\n�.|��El?�Ӑ=��5N�<s�[��w`���b$��F�G�j�\0�sӷR=�E2k��A�T *�r;�;��o�]\\(1�ɷn#\0��q�\0|��jY诼4�^Q���Ni��F2{hQ��nnY�FO�X`�?����E&1�6��V~�oN���|�K��9�j��e��dc����-ݝ1������x�7#����^��{�T�����,V`\0fm�tQ��\0���\0\0\0;\n��wͲ%�8�\"��|�N�de��.� �O��Ϋag,p���E6� �IUX�P	�85��Ze�����2Ė\n��	�WE,,kF�u��8qX�FqIh�s��{n��3\"�h�� 9-��]��t?A\\���\r���!E��B\0<\0}~��Ֆ)~Un}*%B4��~�l=YU����p����\03���Ⲣ֢MU�d��>}�#)*Ƿn{����ԫt7w[�4�����*���\0�?�V�L�\Z�(��C\n(��\n(��\nB20zR�@f��5����0�+��Yk��t��6F�� �5�]����߀:�U&�qMY�r��[��Q\'�\"#e�>����M-�L�x+��*�s\\O9X���\0�Ҵҧa��FP:�J����*0����̅
�X��*�V��ga�&��mX�q�Mɫu6���0�f2��¯�`\0 ��QHaEP ��\0��}F;�-��D!?(u*	#�ܓ\\�|\'�aV[}V�P����>��k�h�U$��C�[����o��w2ZK�(?�ks,�\0~����?���Tlt1jH��$���Z=�{��[5�\0�흌���%���\0���֚�*�m�n���o%��Kq�+ ��ڽғ\0�^��9�p~\0�U߉.u��b��&�T0Il�~����\0��KY�wrдQE \n(��\n(��\n(��\n�ue�Q2n�r9��\0tG�5E�Q���(\0��(\0��(\0��(\0��(\0��(\0��(\0��(\0��(��'),(2,2000.00,1,NULL,'en','Tankless Water Heater','<p><span style=\"color: rgb(0, 0, 0); font-family: roboto, helvetica, arial, sans-serif; font-size: 14.3999996185303px; line-height: 20.7999992370605px; background-color: rgb(251, 251, 251);\">Mini Portable Gas Tankless Water</span></p>\r\n','Tankless Water Heater_AIpX_big.jpg','����\0JFIF\0\0\0\0\0\0��\0C\0    		\n\r

\Z\Z $.\' \",#(7),01444\'9=82<.342��\0C			
\r\r2!!22222222222222222222222222222222222222222222222222��\0\0�\0�\"\0��\0\0\0\0\0\0\0\0\0\0\0 	\n
��\0�\0\0\0}\0!1AQa \"q2���#B��R��$3br�	\n\Z%&\'()*456789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz���������������������������������������������������������������������������\0\0\0\0\0\0\0\0 	\n
��\0�\0 \0w\0!1AQ aq\"2�B����	#3R�br�\n$4�%�\Z&\'()*56789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz��������������������������������������������������������������������������\0\0\0?\0��(��\n(��\n(��\n(��\n(����\0D�ㅌ��,\0�\0?�![��?�\\�֧U\n0)h��g=��}�o�:r�i?��j�E\0f����+�۷�7��L+�[��, �E���~��E\0d&������֬������i�$r�x�]OB�\"��I�� �Vd�4bC-��Z�{�x?Q���F��Y7�pO7��Fz�\n(��\n(��\n(��\n(��\n(��\n(��\n(��\na�@=�O�doEPEPEPEPI>[�d?��~�n��3wo����b�\n(��\n(��\n(��\n(��\n(��\n(��\0$�������\0h�b����be��e�~�9�+����@E�J�8�y9���@�E�Gf�y�?4gCr�1\n��p3�{t�l�4��,����f;�LÓ�X���E�먬���6\\���08*���\Z�VWV{\Z\0u�s�y\Z������4��q�t�5�9A�d#�G�5(��^[ܜE*�늱@Q@�\r�?ݏ���Uә� �p>���PEPEPEPEPEPU�����{{�c�	92�� �Y�=(ε�^��m\r֝)�	K.}��ǰ\"��O���\n��+�r�б�c �$W��4�Ґ1�|>�m��Ѯd�V�|����Ұ����c�2F�s�Ȥ�\0���E}s��T0�qI�IS�����@*E��y�i{l	�Σ�#hǰ�K뫒�1��y%lz�\r߉j�����V�[-ǫ������z�Ꮕ�\'m��A1o�4��u��c�i�WQG嬑J�#9�ql�\0�Z�������C����\"|�����.9�8Wq?�})����x��DGQ�\0?�d\\| �W�R�c
C�\0���;\n��7�[$6x ���%����H��H�㜞��X�D�/6�u���>�
c���W�G�W�O�n,?�Ѥv#�\0��N�Wd��=R��.A�\0F�&=��9��S�Z,��/�Uc3�( ��{�εb�+��XdI#a�t`A�\\���?h��ik,��g�̙F$�WD]b��8�>Z¯�`��bу�҆$b���v�*�Am\n�ۤ(\0DT{T�\0QE\0QE\0QE\0QE\0QE\0R7�4����Xg\"�\'+R����\0�Lq��]W��*�wPAG(I��{\Z�����X�O�pW���X�#��A���= \\��ک����4{{�]�r+�6	��\0�4�\0��IKLDi�\0���>w�g�@�Jb���\0�N�$H� p���O7�����\0���\0�P�M{Ζ��<�0J��P��-�y��Yc�.��Ҷl,1���u�
���=qN��QE\0QE\0QE\0QE\0QE\0QE\0DEF�#>�+t���c\0˵�#�# �mލeu�U�����Z�=*6���)�m��V��,PD6�(��߽!���Bi��(4�b�\0���V\'ے1�a���sU��?c�\Z��;���0p:g��\nl�Rw\r��,I+���V~Ҷ�\0a�#�k�v��\0���J�L�>Q��z��Y�@�e�6ϑ��)�:s֖�(QE\0QE\0QE\0QE\0QE\0QE�)���T�\rF�tԆ�n��!~�8�[���0#\"�jG��\"���ԗ�V@GFrzc�ڣ�\0���\Z[�m����ל���aXa��wg�۷8�i�V�G
!��+\n�]��o��9��8�P?Z�Ӹ�A���h��QE\0QE\0QE\0QE\0QE\0QE\0F�j*���f�i��ӫ��5m:��.�(���G�2�IfU �\' �s�S@��\rD*��w.���2�|y�6p�&�@N?V��\0z�ԍQ\ZD��/Ɩ��]��� \'���5��T?SL�K��F�2<�w�\0��\0Zb)����t�*v3)-���z.qӯ=��Ҙ$#8�c#�k�%�cV�>`=;�\n�{�}k~�xm,l��x���Z9V��?�oʐ#b�(����(\0��(\0��(\0��(\0��(\0��():�F������
`� A��\0��vV�����)CFѝ�	��gЃR-��n��&	U||�c$z���L�QNn��*�o\r��6��#X�L��T\0O\'�:�ޞǊ��0��<�f�!��\0�C��h�1\\#E4j�ȥXd2�>��d����\0z��4	e7�6�ϫ]�0�	d�\"�Er\0�A\'=9����{IeӴ3f�Q�Sdv�����X���<V��k�SP�%Y\Z191�l*&w��  ��Z��=���B^14z��s,,v��A�L��6�h��C\n(��\n(��\n(��\n(��\n(��\n(�� ��T6d犞~��Q�Y�ݼ���>l��������ncQ2�:�Պ��<�dd�Z,A�\\#i����c�܉.e���� �$�l���g	��(��V��
In���w#8�R�V��>�{g�ە@�y 9e�g,�  h:�\0@6A�C��Uoo��D�F�Fp�/;}Ȫ�Z�4ؚ3�;<�\r�A��@�q$G����ߍ6�nL{������S?���V��џ��A�Ï\r;ḵo��3�(�-B�Q����d�AX��7��\0,֫���yrʎ��Kf2(|��3߯=G^�R\"\\�\\t����I�Ik�/d�I^ݣ��+���Ol�϶h�C���-\n(��\n(��\n(��\n(��\n(��\n(��2�+Ǵ��\n��
a�S�Im{ʝ��U���qm\r�M���v�	����x���d��9�Oק�&�1`�� rj7�R� �P�{P,��	\0t\0��W!�j7����AaqJw<�����z��דUol࿷h\'cn��#I����[��Э�\\��?�B�����c�+x�8P\" ª�\0)��T\\l�\0|S<H��3x	�[�`�M�S���t�#8���\0�_�P #djq�r�w�^/\\�⟥Y}���<0 ���\ZY�j�����\0���M�/�����?
��\0\Z�O����7�qD�*�V�K2I)\0_���U)i��.z�LaEPEPEPEPEPEPEP5~��ٱ�>Ʈ�@���H����t8�Z�%��Si�^)�M0�қI~LR���VM�c*Gv�
I�7����������Φխ%�4;�Hv��G�K���G�\r�\Zԅ���=x��\"���+�w#L�+J|�(�{�W��޺;X��DH�c�Q�3�\0�4�a�C�/jg�I�?A�8�%�v���\0<R�E0\n(��\n(��\n(��\n(��\n(��\n(��\n(��\n(��\n(��\n�wck
Cws��EQE\0r��.+�x.�\"\'�DW$\'���]\\V��©\n`(�,K}O4QH
QE0\n(��\n(��\n(��\n(��?��');
/*!40000 ALTER TABLE `reward_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `siteviewtable`
--

DROP TABLE IF EXISTS `siteviewtable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `siteviewtable` (
  `oid_2` int(11) NOT NULL,
  `moduledomainname` varchar(255) DEFAULT NULL,
  `siteviewid` varchar(255) DEFAULT NULL,
  `modulename` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`oid_2`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `siteviewtable`
--

LOCK TABLES `siteviewtable` WRITE;
/*!40000 ALTER TABLE `siteviewtable` DISABLE KEYS */;
INSERT INTO `siteviewtable` VALUES (1,'community','sv2x','Community Administration'),(2,'community','sv1x','Community Private Area'),(3,'community','sv2z','Consumer Portal');
/*!40000 ALTER TABLE `siteviewtable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `text_chunk`
--

DROP TABLE IF EXISTS `text_chunk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `text_chunk` (
  `oid` int(11) NOT NULL,
  `languagecode` varchar(255) DEFAULT NULL,
  `key` varchar(255) DEFAULT NULL,
  `message` text,
  PRIMARY KEY (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `text_chunk`
--

LOCK TABLES `text_chunk` WRITE;
/*!40000 ALTER TABLE `text_chunk` DISABLE KEYS */;
INSERT INTO `text_chunk` VALUES (1,'en','HomePage.GettingStarted.Intro','<h3>What is the Community?</h3>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"background-color:rgb(255, 255, 255); color:rgb(102, 102, 102); font-family:avenir lt,lucida sans unicode,lucida grande,sans-serif; font-size:15px\">Our </span><strong>Community </strong><span style=\"background-color:rgb(255, 255, 255); color:rgb(102, 102, 102); font-family:avenir lt,lucida sans unicode,lucida grande,sans-serif; font-size:15px\">is the place where you can connect and share knowledge with users around the world. With our innovative game system you can challenge other users and rise Leaderboards, earn Points, and build your Reputation.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n'),(2,'en','HomePage.GettingStarted','<h3><span style=\"color:#78a02d\"><span style=\"font-size:13px\">You can enjoy the gaming experience and earn Points immediately.</span></span></h3>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3>What are the meanings of Participation and Reputation?</h3>\r\n\r\n<p>Participation is based on the Points gained doing activities and is related to your engagement with the Community. There are 2 participation leaderboards: 7 days and overall.</p>\r\n\r\n<p>Reputation is based on the Badges won and is related to the experience gained in the Community.</p>\r\n\r\n<h3>&nbsp;</h3>\r\n\r\n<h3>What happens if I set my Community profile to private?</h3>\r\n\r\n<p>If you set your profile to private, you will no longer be visible in user rankings, your Points will be suspended, and you will not gain new Badges or Points until you will make your profile public again.</p>\r\n\r\n<p>&nbsp;</p>\r\n'),(3,'en','HomePage.PrivatePublicMessage','Set your profile public'),(4,'en','HQ.MonthlyWarning','No monthly action'),(5,'en','HQ.CongratulationsBadges','Congratulation!'),(6,'en','HQ.SessionOrNotAccess','Session expired'),(7,'en','HQ.NicknameNull','Nickname null'),(8,'en','NotPublicProfile.Message','<p>This profile is private. You can&#39;t see any user information.</p>\n'),(9,'en','Session expired','Session expired'),(10,'en','User not authorized or invalid credentials','User not authorized or invalid credentials'),(11,'en','Unable to compute the authorization signature','Unable to compute the authorization signature'),(12,'en','HomePage.SomethingMonthyMessage','<p>\n	No action yet. Do you want to become first?</p>\n'),(13,'it','HomePage.GettingStarted.Intro','<h3>Che cos&#39;&egrave; la Community ?</h3>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style=\"background-color:rgb(255, 255, 255); color:rgb(102, 102, 102); font-family:avenir lt,lucida sans unicode,lucida grande,sans-serif; font-size:15px\">La nostra&nbsp;</span><strong>Community</strong><span style=\"background-color:rgb(255, 255, 255); color:rgb(102, 102, 102); font-family:avenir lt,lucida sans unicode,lucida grande,sans-serif; font-size:15px\">&nbsp;&egrave; il luogo dove potrai entrare in contatto e condividere informazioni con utenti di tutto il mondo!&nbsp;Attraverso un innovativo sistema di </span><em>gamification</em><span style=\"background-color:rgb(255, 255, 255); color:rgb(102, 102, 102); font-family:avenir lt,lucida sans unicode,lucida grande,sans-serif; font-size:15px\">potrai sfidare gli altri utenti, scalare le classifiche, guadagnare Punti e costruire la tua reputazione.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n'),(14,'it','HomePage.GettingStarted','<h3 style=\"color:rgb(0, 51, 102); font-style:normal; text-align:start\"><span style=\"color:rgb(120, 160, 45)\">Partecipa alla nuova gaming experience e ottieni Punti immediatamente.</span></h3>\r\n\r\n<h3 style=\"color:rgb(0, 51, 102); font-style:normal; text-align:start\">&nbsp;</h3>\r\n\r\n<h3 style=\"color:rgb(0, 51, 102); font-style:normal; text-align:start\"><span style=\"color:#000000\">Che cosa si intende per Partecipazione e Reputazione?</span></h3>\r\n\r\n<p style=\"text-align:start\">La Partecipazione &egrave; basata sui Punti guadagnati svolgendo diverse attivit&agrave; all&rsquo;interno della Community, come la condivisione sui social network, o pi&ugrave; semplicemente l&rsquo;accesso all&rsquo;interno del proprio Account. Esistono due tipi di classifiche: classifica basata sugli ultimi 7 giorni e classifica generale.</p>\r\n\r\n<p style=\"text-align:start\">La Reputazione &egrave; basata sui badge accumulati ed &egrave; relativa alla propria esperienza all&rsquo;interno della Community.</p>\r\n\r\n<h3 style=\"color:rgb(0, 51, 102); font-style:normal; text-align:start\">&nbsp;</h3>\r\n\r\n<h3 style=\"color:rgb(0, 51, 102); font-style:normal; text-align:start\"><span style=\"color:#000000\">Che cosa accade se imposto il mio profilo privato?</span></h3>\r\n\r\n<p style=\"text-align:start\">Se imposti il tuo profilo privato, non sarai pi&ugrave; visibile nelle classifiche, i tuoi Punti saranno sospesi e non avrai pi&ugrave; la possibilit&agrave; di guadagnare nuovi badge fino a quando non imposterai nuovamente il tuo profilo pubblico.</p>\r\n\r\n<h3>&nbsp;</h3>\r\n'),(15,'it','HomePage.PrivatePublicMessage','<p>\n	Imposta il tuo profilo pubblico</p>\n'),(16,'it','HQ.MonthlyWarning','Nessuna azione mensile'),(17,'it','HQ.CongratulationsBadges','Congratulazioni!'),(18,'it','HQ.SessionOrNotAccess','<p>\n	Sessione scaduta</p>\n'),(19,'it','HQ.NicknameNull','<p>\n	Nickname non valido</p>\n'),(20,'it','NotPublicProfile.Message','<p>Questo &egrave; un profilo privato. Non &egrave; possibile visualizzare le informazioni dell&#39;utente.</p>\n'),(21,'it','Session expired','Sessione scaduta'),(22,'it','User not authorized or invalid credentials','<p>\n	User non autorizzato o credenziali non valide</p>\n'),(23,'it','Unable to compute the authorization signature','Unable to compute the authorization signature'),(24,'it','HomePage.SomethingMonthyMessage','Nessuna azione mensile. Vuoi essere il primo?'),(25,'fr','HomePage.GettingStarted.Intro','Introduction to the community'),(26,'fr','HomePage.GettingStarted','Introduction to the community'),(27,'fr','HomePage.PrivatePublicMessage','Set your profile public'),(28,'fr','HQ.MonthlyWarning','No monthly action'),(29,'fr','HQ.CongratulationsBadges','Congratulation!'),(30,'fr','HQ.SessionOrNotAccess','Session expired'),(31,'fr','HQ.NicknameNull','Nickname null'),(32,'fr','NotPublicProfile.Message','Private profile'),(33,'fr','Session expired','Session expired'),(34,'fr','User not authorized or invalid credentials','User not authorized or invalid credentials'),(35,'fr','Unable to compute the authorization signature','Unable to compute the authorization signature'),(36,'fr','HomePage.SomethingMonthyMessage','No action yet.Do you want become first?'),(37,'es','HomePage.GettingStarted.Intro','<h3>&iquest;Qu&eacute; es Community?</h3>\r\n\r\n<div>&nbsp;</div>\r\n'),(38,'es','HomePage.GettingStarted','<h3><span style=\"color:#78a02d\">La comunidad est&aacute; actualmente en versi&oacute;n Beta.</span></h3>\r\n\r\n<h3><span style=\"color:#78a02d\">Disfruta jugando y gana Puntos inmediatamente,</span></h3>\r\n\r\n<h3><span style=\"color:#78a02d\">Si encuentras bugs u otros problemas, por favor env&iacute;anos un correo a:&nbsp;</span></h3>\r\n\r\n<p>&nbsp;</p>\r\n'),(39,'es','HomePage.PrivatePublicMessage','<p>\n	Configures tu perfil p&uacute;blico</p>\n'),(40,'es','HQ.MonthlyWarning','<p>\n	Ninguna acci&oacute;n mensual</p>\n'),(41,'es','HQ.CongratulationsBadges','<p>\n	Felicitaciones!</p>\n'),(42,'es','HQ.SessionOrNotAccess','<p>\n	La sesi&oacute;n ha expirado</p>\n'),(43,'es','HQ.NicknameNull','<p>\n	Nickname&nbsp;no es v&aacute;lido</p>\n'),(44,'es','NotPublicProfile.Message','<p>Este es un perfil privado. No se puede mostrar la informaci&oacute;n del usuario.</p>\n'),(45,'es','Session expired','<p>\n	La sesi&oacute;n ha expirado</p>\n'),(46,'es','User not authorized or invalid credentials','<p>\n	Usuario no autorizado o credenciales no v&aacute;lidas</p>\n'),(47,'es','Unable to compute the authorization signature','Unable to compute the authorization signature'),(48,'es','HomePage.SomethingMonthyMessage','<p>\n	Ninguna acci&oacute;n! Quieres ser el primer?</p>\n'),(49,'it','community.text.rewards','<p>Qui puoi spendere i punti guadagnati per ottenere i tuoi premi!</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"\" src=\"BootstrapStyle/img/profile_big.jpg\" /></p>\r\n'),(50,'en','community.text.rewards','<p>Here you can spend your earned points and get your rewards!</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"\" src=\"BootstrapStyle/img/profile_big.jpg\" /></p>\r\n'),(51,'es','community.text.rewards',''),(52,'fr','community.text.rewards',''),(53,'it','KB_1',''),(54,'it','KB_2',''),(55,'it','KB_3',''),(56,'it','KB_4',''),(57,'it','KB_5',''),(58,'it','KB_6',''),(59,'it','KB_7',''),(60,'it','KB_8',''),(61,'it','KB_9',''),(62,'en','KB_1',''),(63,'en','KB_2',''),(64,'en','KB_3',''),(65,'en','KB_4',''),(66,'en','KB_5',''),(67,'en','KB_6',''),(68,'en','KB_7',''),(69,'en','KB_8',''),(70,'en','KB_9',''),(71,'es','KB_1',''),(72,'es','KB_2',''),(73,'es','KB_3',''),(74,'es','KB_4',''),(75,'es','KB_5',''),(76,'es','KB_6',''),(77,'es','KB_7',''),(78,'es','KB_8',''),(79,'es','KB_9',''),(80,'it','Forum_1',''),(81,'it','Forum_2',''),(82,'it','Forum_3',''),(83,'it','Forum_4',''),(84,'it','Forum_5',''),(85,'it','Forum_6',''),(86,'it','Forum_7',''),(87,'it','Forum_8',''),(88,'it','Forum_9',''),(89,'en','Forum_1',''),(90,'en','Forum_2',''),(91,'en','Forum_3',''),(92,'en','Forum_4',''),(93,'en','Forum_5',''),(94,'en','Forum_6',''),(95,'en','Forum_7',''),(96,'en','Forum_8',''),(97,'en','Forum_9',''),(98,'es','Forum_1',''),(99,'es','Forum_2',''),(100,'es','Forum_3',''),(101,'es','Forum_4',''),(102,'es','Forum_5',''),(103,'es','Forum_6',''),(104,'es','Forum_7',''),(105,'es','Forum_8',''),(106,'es','Forum_9',''),(107,'it','Store_1',''),(108,'it','Store_2',''),(109,'it','Store_3',''),(110,'it','Store_4',''),(111,'it','Store_5',''),(112,'it','Store_6',''),(113,'it','Store_7',''),(114,'it','Store_8',''),(115,'it','Store_9',''),(116,'en','Store_1',''),(117,'en','Store_2',''),(118,'en','Store_3',''),(119,'en','Store_4',''),(120,'en','Store_5',''),(121,'en','Store_6',''),(122,'en','Store_7',''),(123,'en','Store_8',''),(124,'en','Store_9',''),(125,'es','Store_1',''),(126,'es','Store_2',''),(127,'es','Store_3',''),(128,'es','Store_4',''),(129,'es','Store_5',''),(130,'es','Store_6',''),(131,'es','Store_7',''),(132,'es','Store_8',''),(133,'es','Store_9',''),(134,'it','Certification_1',''),(135,'it','Certification_2',''),(136,'it','Certification_3',''),(137,'it','Certification_4',''),(138,'it','Certification_5',''),(139,'it','Certification_6',''),(140,'it','Certification_7',''),(141,'it','Certification_8',''),(142,'it','Certification_9',''),(143,'en','Certification_1',''),(144,'en','Certification_2',''),(145,'en','Certification_3',''),(146,'en','Certification_4',''),(147,'en','Certification_5',''),(148,'en','Certification_6',''),(149,'en','Certification_7',''),(150,'en','Certification_8',''),(151,'en','Certification_9',''),(152,'es','Certification_1',''),(153,'es','Certification_2',''),(154,'es','Certification_3',''),(155,'es','Certification_4',''),(156,'es','Certification_5',''),(157,'es','Certification_6',''),(158,'es','Certification_7',''),(159,'es','Certification_8',''),(160,'es','Certification_9',''),(161,'it','actions_KB',''),(162,'it','actions_Forum',''),(163,'it','actions_Store',''),(164,'it','actions_Certification',''),(165,'en','action_KB',''),(166,'en','actions_Forum',''),(167,'en','actions_Store',''),(168,'en','actions_Certification',''),(169,'es','actions_KB',''),(170,'es','actions_Forum',''),(171,'es','actions_Store',''),(172,'es','actions_Certification','');
/*!40000 ALTER TABLE `text_chunk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `text_mail`
--

DROP TABLE IF EXISTS `text_mail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `text_mail` (
  `oid` int(11) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `language_code` varchar(255) DEFAULT NULL,
  `body` text,
  `description` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `containers_oid_header` int(11) DEFAULT NULL,
  `containers_oid_footer` int(11) DEFAULT NULL,
  PRIMARY KEY (`oid`),
  KEY `idx_text_mail_containers_mail` (`containers_oid_header`),
  KEY `idx_text_mail_containers_mai_2` (`containers_oid_footer`),
  CONSTRAINT `fk_text_mail_containers_mail` FOREIGN KEY (`containers_oid_header`) REFERENCES `containers_mail` (`oid`),
  CONSTRAINT `fk_text_mail_containers_mail_2` FOREIGN KEY (`containers_oid_footer`) REFERENCES `containers_mail` (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `text_mail`
--

LOCK TABLES `text_mail` WRITE;
/*!40000 ALTER TABLE `text_mail` DISABLE KEYS */;
INSERT INTO `text_mail` VALUES (1,'community.registration','it','<p>Dear $$firstname$$,</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>congratulations, you are now a member of the Community!</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n','Registration into the community','Welcome into the Community!',NULL,NULL),(2,'community.registration','en','<p>Dear $$firstname$$,</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>congratulations, you are now a member of the &nbsp;Community!</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Now you can:</p>\r\n\r\n<ul>\r\n	<li>earn Points&nbsp;for your participation and spend them to receive gift</li>\r\n	<li>earn Badges&nbsp;for you reputation</li>\r\n	<li>appear in the Leaderboards</li>\r\n	<li>access to all of your dashboard&nbsp;features</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n','Registration into the community','Welcome into the Community!',NULL,NULL),(3,'community.registration','es','','Registration into the community','Welcome into the Community!',NULL,NULL),(4,'community.registration','fr','','Registration into the community','Welcome into the Community!',NULL,NULL),(5,'community.publicProfile','it','<p>Dear $$firstname$$,</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>congratulations, you are now a member of the Community!</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Now you can:</p>\r\n\r\n<ul>\r\n	<li>earn Points&nbsp;for your participation and spend them to receive gift</li>\r\n	<li>earn Badges&nbsp;for you reputation</li>\r\n	<li>appear in the Leaderboards</li>\r\n	<li>access to all of your Dashboard&nbsp;features</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n','Change on profile visibility in public','Welcome into the Community!',NULL,NULL),(6,'community.publicProfile','en','<p>Dear $$firstname$$,</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>congratulations, you are now a member of the Community!</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Now you can:</p>\r\n\r\n<ul>\r\n	<li>earn Points&nbsp;for your participation and spend them to receive gift</li>\r\n	<li>earn Badges&nbsp;for you reputation</li>\r\n	<li>appear in the Leaderboards</li>\r\n	<li>access to all of your Dashboard&nbsp;features</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n','Change on profile visibility in public','Welcome into the Community!',NULL,NULL),(7,'community.publicProfile','es','','Change on profile visibility in public','Welcome into the Community!',NULL,NULL),(8,'community.publicProfile','fr','','Change on profile visibility in public','Welcome into the Community!',NULL,NULL),(9,'community.privateProfile','it','<p>Dear $$firstname$$,</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>we are sorry you decided to leave our Community.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>We remember you that <strong>your Points will be suspended</strong>, and you will not gain new Badges or Points until you will make your profile public again.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>We hope to see you come back again.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n','Change on profile visibility in private','Leave the Community',NULL,NULL),(10,'community.privateProfile','en','<p>Dear $$firstname$$,</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>we are sorry you decided to leave our Community.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>We remember you that <strong>your Points will be suspended</strong>, and you will not gain new Badges or Points until you will make your profile public again.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>We hope to see you come back again.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n','Change on profile visibility in private','Leave the Community',NULL,NULL),(11,'community.privateProfile','es','','Change on profile visibility in private','Leave the Community',NULL,NULL),(12,'community.privateProfile','fr','','Change on profile visibility in private','Leave the Community',NULL,NULL),(13,'community.newBadge','it','','New badge acquired','Got New Badge',NULL,NULL),(14,'community.newBadge','en','<p>Dear $$firstname$$,</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Congratulations for your Badge!</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>$$badge$$</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p style=\"margin-left:150px\">Get more Badges doing different actions, for instance:</p>\r\n\r\n<p>$$other_actions$$</p>\r\n\r\n<p>&nbsp;</p>\r\n','New badge acquired','Got New Badge',NULL,NULL),(15,'community.newBadge','es','','New badge acquired','Got New Badge',NULL,NULL),(16,'community.newBadge','fr','','New badge acquired','Got New Badge',NULL,NULL),(17,'community.newReward','it','','New reward available','New Reward Available',NULL,NULL),(18,'community.newReward','en','','New reward available','New Reward Available',NULL,NULL),(19,'community.newReward','es','','New reward available','New Reward Available',NULL,NULL),(20,'community.newReward','fr','','New reward available','New Reward Available',NULL,NULL),(21,'community.newReward.onlyOne','it','','New reward available','New Reward Available',NULL,NULL),(22,'community.newReward.onlyOne','en','','New reward available','New Reward Available',NULL,NULL),(23,'community.newReward.onlyOne','es','','New reward available','New Reward Available',NULL,NULL),(24,'community.newReward.onlyOne','fr','','New reward available','New Reward Available',NULL,NULL),(25,'community.monthlySummary','it','<p>Hi $$firstname$$!</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>here you find what happend in the Community during the last month!</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>You earned $$seven_days_points$$ Points<br />\r\nYour total number of Points is $$overall_points$$<br />\r\nYou are $$seven_days_position$$&deg; in 7 Days Leaderboard<br />\r\nYou are $$overall_position$$&deg; in Overall Leaderboard</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>LEADERBOARDS</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>$$leaderboards$$</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n','Monthly summary','Monthly Summary',NULL,NULL),(26,'community.monthlySummary','en','<p>Hi $$firstname$$!</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>here you find what happend in the Community during the last month!</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>You earned $$seven_days_points$$ Points<br />\r\nYour total number of Points is $$overall_points$$<br />\r\nYou are $$seven_days_position$$&deg; in 7 Days Leaderboard<br />\r\nYou are $$overall_position$$&deg; in Overall Leaderboard</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>LEADERBOARDS</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>$$leaderboards$$</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n','Monthly summary','Monthly Summary',NULL,NULL),(27,'community.monthlySummary','es','','Monthly summary','Monthly Summary',NULL,NULL),(28,'community.monthlySummary','fr','','Monthly summary','Monthly Summary',NULL,NULL),(29,'community.monthlySummary.noSevenDays','it','<p>Hi $$firstname$$!</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>here you find what happend in the Community during the last month!</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>You earned $$seven_days_points$$ Points<br />\r\nYour total number of Points is $$overall_points$$<br />\r\nYou are $$overall_position$$&deg; in Overall Leaderboard</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>LEADERBOARDS</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>$$leaderboards$$</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n','Monthly summary','Monthly Summary',NULL,NULL),(30,'community.monthlySummary.noSevenDays','en','<p>Hi $$firstname$$!</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>here you find what happend in the Community during the last month!</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>You earned $$seven_days_points$$ Points<br />\r\nYour total number of Points is $$overall_points$$<br />\r\nYou are $$overall_position$$&deg; in Overall Leaderboard</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>LEADERBOARDS</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>$$leaderboards$$</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n','Monthly summary','Monthly Summary',NULL,NULL),(31,'community.monthlySummary.noSevenDays','es','<p>Hi $$firstname$$!</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>here you find what happend in the Community during the last month!</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>You earned $$seven_days_points$$ Points<br />\r\nYour total number of Points is $$overall_points$$<br />\r\nYou are $$overall_position$$&deg; in Overall Leaderboard</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>LEADERBOARDS</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>$$leaderboards$$</p>\r\n\r\n<p>&nbsp;</p>\r\n','Monthly summary','Monthly Summary',NULL,NULL),(32,'community.monthlySummary.noSevenDays','fr','','Monthly summary','Monthly Summary',NULL,NULL);
/*!40000 ALTER TABLE `text_mail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `thematic_area`
--

DROP TABLE IF EXISTS `thematic_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `thematic_area` (
  `oid` int(11) NOT NULL,
  `area_name` varchar(255) DEFAULT NULL,
  `checked_image` varchar(255) DEFAULT NULL,
  `hd_image` varchar(255) DEFAULT NULL,
  `hd_checked_image` varchar(255) DEFAULT NULL,
  `checked_imageblob` blob,
  `hd_checked_imageblob` blob,
  `hd_imageblob` blob,
  PRIMARY KEY (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `thematic_area`
--

LOCK TABLES `thematic_area` WRITE;
/*!40000 ALTER TABLE `thematic_area` DISABLE KEYS */;
INSERT INTO `thematic_area` VALUES (1,'Water Saving Insights','Water Saving Insights_Da7e_40.png',NULL,'Water Saving Insights_Da7e_60.png','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\0(\0\0\0(\0\0\0���m\0\0vIDATx���?(Ea��{���տ�\Z�����B1Z��+f��M���Q�U��j�B�dR�U���}빃ә��:���g�O�s������d|��WY�Pa g(�f�hA�o��iG�&`���{���
��#8���	Y�\\\n5so����k�N��[�M�\Z\\��m��P�)Y��4�C1�l\Z���}@� �}@� �\n�N3�i>��c\"����g8�SZ��?i8�GL�*`y���׸ĕ�
�hI�L�e��F��\\�Nq�6�ėGy�|܀�8�e5����QVhQ�dE�^\r4�\Z�7W�N<bKpL�d����9����Y���3���I��+��o|��HI;<\ndYkъ�׬��t�>��_��\0eHw��z4\0\0\0\0IEND�B`�','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\0<\0\0\0<\0\0\0:��r\0\0�IDATx��KlMQ��mU[i���B��*�5 f$B�abP1�� ��H�W�0��&�x�*H<c �m��q�W��CN���O���ɗ��V����g���s6lذ�
G1(�Lq\Z��`��`�\'$�HƖ�!���*04�\r`/8�C�8G��,\Z�E�љ`�>�_)��\0Y\ra�7��>�\n>�,r���̾�̆s��~�O��!����l���<�Ag\n£��0���������N�����,7k˒Y\n������<�۸��Zx<X	�c���$�\\NA�:i�q`���o�9�l��*��AS7²���`3�y�/i6a6a6a6a6a6a6a6��%�o=�R��=�G<�����6�s�����v�6�:|�\\j�.��5�O,��}1��i���(ܒt}���R�ށ���)���-����c�H�������<�W��9�i�DJ�R\Z]�
�|_ǉ�t��]�]<�r�3�Wȉ`�
چ�<R�n�J0�vA�M��5,z?uA�����$�vA�TE��y}���m��,��KpO:H��/��f�t,����>-�2�y[�јY����A��y��x��&���d�Pu�eE�mI2���i��\rp���L�N�Ki5�V�c|K�_tJ�iE��>{��?[s	cv�.��<4diO��n<�l����;ά�5��e\\�Cy��ţ�u|a�}��)��k��LgÆ\r�>~�m�#]��\0\0\0\0IEND�B`�',NULL),(2,'Saving Water','Saving Water_Mg7t_40.png',NULL,'Saving Water_Mg7t_60.png','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\0(\0\0\0(\0\0\0���m\0\0yIDATx��OHTQ���%b���ѕ.,Z�������-�@Ř\\�
!ɝ.�U&$��jYT.�V)ZF�(�E���t,Ǿ߅��8�{��,�������7��;����HG:�lp�d����9�j�\nAD�&�˱��V�\r��-U�5�	˜A�n$���vs�w�l���K�à\\{��6w���`����@3X�ÜA涀|?^��ÜA��G�{����!+M�B��/�3Ʋ@)s0g��j�������j�2�
����9�p���Q��?�V0����\Z����9>ɨ��(5/h5F��dQi����bp���謒�^��9.�؋�WP�-�dP��K��;�C�N�=�dp�u�������	�C������3��U? ƭ��Ѯt1x\nܷ9� n*��\Z�� �e	Jr�ൣ�l���(y5L�\"=���������٣�j�?��X�d+��x�J�ǸV�P�ږ���J��@�b��bw�z����\'k�%��.�Գ��4��gje%�imfٴ\\e铧��`;����\Z���O��F�/��������2(r��
����pn�� �LvÇY��\\u�a�ű2���=I�<���X:ұ_����\\]V~�\0\0\0\0IEND�B`�','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\0<\0\0\0<\0\0\0:��r\0\0�IDATx��{��E��̭�Rs�h+����$���c�����F���k������J2� ���(�Aeee�=M�\n-\nR´�Mk��k�n}���Ƕ�]�����=���罿��9gΜ�jV��լfZ����.v?q��N�YU�1S�\'���=��F���\n�� ��Yc��6_�A����N��(&T��ab�8M��	M��ό�;-x�xNl@�V>s�ثRŚON����M�ϴ���|�ى�T�\Z
b��nE���&t!�[|#��C*şs�j�Twb}���)Y��8O�$~������|ޙI�_���{ �b�x��,\n�*n�@l�\Z�95������J ��w}&�Κ�)���pt�y�̬��$q3���\0���=���r��4��%��DS�����R�T��s�\\�m
 4�vvU9�.˖�:�&�.��<m]_������� ���^em��
���n� 4�&FyF��}�h�L��!�����Kp/�et�����m.6҇(�|Չ���~,�G��^D?�\n���v���S�x�#��8�V��啒�$cřb�����PP�G\'�\"	^O�n���j�6��I��c����c$�ևF9HM��s�c^҂��ʓ�r�!R������׌
]�Ą�bj����@��~$���U�\Z1*��QL珈��l�S�#./FZ�� ~,\nUɯi��NF�sq S���6�{\"�����P#l~r�x�H���r/K ~� z;}if0�l �R\\�B�=$\0�y�k�ٖ��7ӗ��*!ɲԒZ�V��\'����V�/K�ɋ�^��X\Z����y�(�K\\���w���%爵^�V�xZ\\�	���.�l�,`�L���}������؂�C��T����0�W�+m���0=d.��|g5��V�����_�f{�Y)��ɭ��.ҭ��\'�;���e�Ҧ�I	�k��E�_�|�Y�����T�թ-��$^w�����I�#�t����h�LΠ��;��\"�q m�	�C\Zlj/az�S��%c�+����ד��X�[HC��iE�<��� ��jY�ե�G����6QZ}���qXD������lK���r��W����k������H�����+��hez����G9��wی96+�iV[���*-ڦ��L�.\"{\'�� 	Z&�m�=�e욢-G��5�c(�1#V���.�f�����!���
S9zf-9���t�+\\ me+g��O(m�����k��m��,�ڬ��R4���AXʳ&��1�J������0�g�]^!�Y�jV�=��/�t=��\0\0\0\0IEND�B`�',NULL),(3,'Participation','Participation_AfGP_40.png',NULL,'Participation_AfGP_60.png','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\0(\0\0\0(\0\0\0���m\0\0HIDATx�혽K[Q��5T�\Z3:�Ш�P:(�ڵ��;�I�*B���*J7��_��(i�?![
]]J5�P�@�> ����{��D�/�P���o�����bQ��:�xIF��jOTi�|��g��[�kA�G�{�x_��ރS��S�I��&<qm�
�@Ş�; 5o�2����i��\n<�6�y��uy�\Z�z*���-�%Q�op�V�g�3*�i�\Z���@	l�a�7
��嚿qF�3m^Õ<�/����g�,Ϥ}p�#����i���+�#R�����v(���k�^�p�R�BN;ς<*�$g<�uh��/�.��b^��^]����%��8.�U@OЋF��M�����M�B\\\\4��f^��gtԽ���ͧ�\r���\r�i���i��>�� �;����/�\\�g���Z��m^\"趫��שg�`���.�v�������2����ϴO����	g�p��k8�����P��Q�(`0\nx+��;�����U�����`�\n7&�j��ic�(g�<kŅ_�$��~G��ݧ�n?7��3an\0\0\0\0IEND�B`�','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\0<\0\0\0<\0\0\0:��r\0\0�IDATx��Yl\rQǋ\"�U<�/���vj\'v[-�Xx\"��NUx(�%��=�\"�%��>ؕ���������MNΝ;s�{)�K~�53��;w�̙onZZa���� C�x2��Z�S�u4}�2p\\\'���hZϴV�\0d�����3)�\r<&��?�u6�, ��\'�Ab��V�^���:�H2o�B*oyLhd�=桝�Xh�B$Y\rJ@�O��Xғ�}d�u6�	��Z���4{�+�����͒V|ncΟ�Zg�+�mN�\nG�
P���~�1�m>s�j����`/�\'*�� K��PG+��S$��\\̜�γtg�\\\0��?^\'͖��-����L륍�:n��.�V|��[.�.�R�DZ�7��`)8�*^?o�UL�F�)�CU{T� �&�u��}X����WX�;��{>ڤ��tP�[�Y�xց\Z�` 8Lvr[�\0�p���΀���6)��X���i$h 24�,!Y��ak������t.�:��`CF�[��½�Ǭ����o?���}���G�A̕�1�l�AG�;s�y�^��f-��SP�q�+%���\n�(Ķǎ-�B�s\r�Ξt�:�1�ih��g�Z���< �$m�_9��w��L���~�ǁk�#o~I������&� ��˅��6.f���(��Rk�
>D�B<ݲ��E���U6��Rk��_ϑ��V�q1�
\n�&Z�\"c��aU�i���pE\nW:�\r��Cá��ph�/3\\�`�2��W���VZb���Ұ�ք�|{acX���T6�l�hM9�Y\ZV�q����4���Z�ks�<\n�Cm\\D�8�ŶIM��Km5l��fΈT�w��ث/�g��-���ٮI��.i�hl�\r�\"sc��R����iWHZ����>�K��H�j�w0\\��ś��x��V;��$~	�8Y�4���#_k����b�@��.�k#�ˬ �[�o6��l��/�ڤ\Z��CY���Z��C�7�����M�a�Ӡu\n��,�)5|��K�a1?���������ph84\Z\r��Cá��p�A���Z����Ne-}�Rk��tR˞��|z9�<�hUD�O|�����s_���8��y`��^��~�&�Z���PZG��7�P�.�pK�<s%�w����o�?�\0 =��z \0\0\0\0IEND�B`�',NULL),(4,'Profiling','Profiling_0Har_40.png',NULL,'Profiling_0Har_60.png','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\0(\0\0\0(\0\0\0���m\0\0�IDATx��?hA�/���(�B����E�P,D�`\n
����kl�B
C��666� (I#��H�
E�B��E������\r�d�2ogNE ~ͱ���μ���$��h �=$������5��!0F�8�\"��m�Ϩg��۰�� ��-�R�<c4��Z�\0���b��Z�zl��\'Pu P�(3f�V�0	*��T��V\\��ܔ�l�N��*���|�T˝4���Q�!�686���̽��Y��0!�_����@�m`�0��gp��f����@�o=��̟��N��y����_`y �n~ٟ�8�����jW��Jw�a��g���c[\\0�����\r�5�-��	r\\��,��Y�8��\\�������y�U�����L����*�3੅�\"5͏=�i�/�8���	Y�B�5͏\Z͸/�t�S�ə�K��a�-(]b�������%�&�^�jwY�𨾯㖙\Z�e�&�8xaYf:\\��`��3t0ƅ�ou��T�Y0��-�xd���0
Um3/a�
��fAj�T�}�b,��+
��z |�aU?���I��+�>x\0�\Z\Z ߰v�X����O�u���g#��P����O�eIM�I_\0{Y\Z:���PHMm��Δ �<^>�N������Ԉ�����Ŀ��&�QWV�����([��`��2���_:Z�a����Ʉ��_`X�K��5ƹ]�/0�:N��r�ƠH�_O�2y|��w\\ǹD������
�t�����\0\0\0\0IEND�B`�','�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\0<\0\0\0<\0\0\0:��r\0\0ZIDATx��LUe��Я�`b�k
����xak���R����5��-���r�eX�m-p�eiV��ڪ�#�AH6˕[RV��E~xo=��9�x8�½�{���>c^�}����}���<�{,�����)d	9�!Oȇ<>���+��Ba��ZX/T
[������r�1:��V\"�����Nh��CB���|��=;�\Z���|ڞqW��QxcN�°0�߰0�\"��<�m�\nU�=#.���b�n�^��\'D��$J���>�	��2v��FxE���3������\'h[�QO��^�+�Z��%�i7]T��Y�|upiB�P$�~F0��(}72�t0~e
�Q{Rd����R�،>Y�`��0�BC�2�rƘf��t��j�L@�$���0�GLL��I��\'ś�?��\r���,는�8������ځ�z^�\0g�l�\'pd!�8[k��\'xJ8��*F��.<*��1�T�\'���8��Æ����o�ޔ\\�J�7����4�>\"�/,L��(�.���Ӹ#�W9\Z�O��n��Dވ�3��~�r<]����{�i1z�t�rb}؇p���ׅUzX��\r�W�_���l�̍gp>O�ő�VH]x�%�~ՠ��\n��U��*!���IPDp\\$����O��\r��A>]��ɻn>;U,�8��/�7f��>`Y<����>\ZlZZ�k�WX���b�i*.���G�eƪ.���j��e]~$�)���P���;���0��I��,J�\r\Z*��%��g8���r%!o��Y)��ɬ��`�0���r�\r7x�ì�7H+�r�|Ć�:�&��.�
�\n\na��5�x�v^O6�n�^���y��������,7#�T�v�
��M:��c��.ԓ��7z$��O�`�EH56^����G|r /�~��Jr2��mؖ������1J�U�á��v/��)����|��ȝ^��rJ�tZ^��\Z�<�o1�٠ON�]/�e2,y�-��>�a�|>˧t�K��a)���`8<DQ[�b�T����9�/0�aK˝�i���ER.�0����S�F����5�P�J��x���;�5m�\01>3��P|���U:]E��TJ�&nzh\0�\r\0&�-��\nG��i�)�V��S\0J�nL�x�R��Gj�>G��C�uRj.���W���0�iA�}e�������?�x:E<�e��*�����i�iM�#l�=�����e�E�\\`-��t�GB�xg��g�Bw\r�!]��x3�45��S覾��VK���4����pS9\'�����+XJ�4�Mv3��v��H��]�� S�TY�ٚ�v�sC�QsC|���%2��m�ml�7��Nk���,��7�m��{�!�8\\֏�g	Y��4���N�ȃ��*>Ԓ�R�6r��68���L;�4����� Բ��I0�Rh�k� c����a1��d�����ŷ��N���!Џdh7}ng��&������v����c}f[)�\"�6!�[-3 ��0�d={�cy2\nʯ��tl/E�^�E����E#�{��q���\n��:�b��QFi����~��!fA?J+u�f��E\rjm�/y̵f�eWCKIF*�ɯ�TX��5�xF�/j�Z�_�ʵ���f��/�5�
��U\0\0\0\0IEND�B`�',NULL);
/*!40000 ALTER TABLE `thematic_area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `internal` tinyint(1) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `groupoid` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  KEY `idx_usertable_grouptable` (`groupoid`),
  CONSTRAINT `fk_usertable_grouptable` FOREIGN KEY (`groupoid`) REFERENCES `grouptable` (`oid_2`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin@admin.com','admin123admin',1,'admin',1),(2,'luca.galli@polimi.it','xClpan/XKdbsTkkxAeBq3Q==',0,'LucaGalli',2),(3,'chiara.pasini@polimi.it','xClpan/XKdbsTkkxAeBq3Q==',0,'ChiaraPasini',3),(4,'piero.fraternali@polimi.it','xClpan/XKdbsTkkxAeBq3Q==',0,'PieroFraternali',2),(5,'davide.vanoni@webratio.com','xClpan/XKdbsTkkxAeBq3Q==',0,'DavideVanoni',2),(6,'andrea@idsia.ch','xClpan/XKdbsTkkxAeBq3Q==',0,'AndreaRizzoli',2),(7,'giorgia.baroffio@polimit.it','xClpan/XKdbsTkkxAeBq3Q==',0,'GiorgiaBaroffio',2),(9,'john.miller@smarth2o.com','xClpan/XKdbsTkkxAeBq3Q==',0,'johnmiller',2),(10,'smith@gmail.com','xClpan/XKdbsTkkxAeBq3Q==',0,'williamsmith',3);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_grouptable`
--

DROP TABLE IF EXISTS `user_grouptable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_grouptable` (
  `useroid` int(11) NOT NULL,
  `groupoid` int(11) NOT NULL,
  PRIMARY KEY (`useroid`,`groupoid`),
  KEY `idx_user_grouptable_usertable` (`useroid`),
  KEY `idx_user_grouptable_grouptable` (`groupoid`),
  CONSTRAINT `fk_user_grouptable_grouptable` FOREIGN KEY (`groupoid`) REFERENCES `grouptable` (`oid_2`),
  CONSTRAINT `fk_user_grouptable_usertable` FOREIGN KEY (`useroid`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_grouptable`
--

LOCK TABLES `user_grouptable` WRITE;
/*!40000 ALTER TABLE `user_grouptable` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_grouptable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `user_information`
--

DROP TABLE IF EXISTS `user_information`;
/*!50001 DROP VIEW IF EXISTS `user_information`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `user_information` (
  `oid` tinyint NOT NULL,
  `country` tinyint NOT NULL,
  `area_geografica` tinyint NOT NULL,
  `small_photo` tinyint NOT NULL,
  `big_photo` tinyint NOT NULL,
  `first_name` tinyint NOT NULL,
  `last_name` tinyint NOT NULL,
  `twitter` tinyint NOT NULL,
  `linkedin` tinyint NOT NULL,
  `website` tinyint NOT NULL,
  `bio` tinyint NOT NULL,
  `city` tinyint NOT NULL,
  `company_name` tinyint NOT NULL,
  `email` tinyint NOT NULL,
  `internal` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `action_instance_action_area_vi`
--

/*!50001 DROP TABLE IF EXISTS `action_instance_action_area_vi`*/;
/*!50001 DROP VIEW IF EXISTS `action_instance_action_area_vi`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `action_instance_action_area_vi` AS select `al1`.`oid` AS `oid`,`al2`.`area` AS `der_attr` from (`action_instance` `al1` left join `action_type` `al2` on((`al1`.`action_type_oid` = `al2`.`oid`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `action_instance_daily_vi`
--

/*!50001 DROP TABLE IF EXISTS `action_instance_daily_vi`*/;
/*!50001 DROP VIEW IF EXISTS `action_instance_daily_vi`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `action_instance_daily_vi` AS select `action_instance`.`action_type_oid` AS `action_type_oid`,cast(`action_instance`.`date` as date) AS `date`,count(0) AS `daily_occurrence` from `action_instance` group by `action_instance`.`action_type_oid`,cast(`action_instance`.`date` as date) order by `action_instance`.`action_type_oid`,cast(`action_instance`.`date` as date) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `action_instance_name_view`
--

/*!50001 DROP TABLE IF EXISTS `action_instance_name_view`*/;
/*!50001 DROP VIEW IF EXISTS `action_instance_name_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `action_instance_name_view` AS select `al1`.`oid` AS `oid`,`al2`.`name` AS `der_attr` from (`action_instance` `al1` left join `action_type` `al2` on((`al1`.`action_type_oid` = `al2`.`oid`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `badgeimportancebyuser`
--

/*!50001 DROP TABLE IF EXISTS `badgeimportancebyuser`*/;
/*!50001 DROP VIEW IF EXISTS `badgeimportancebyuser`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `badgeimportancebyuser` AS select `m`.`oid` AS `badge_instance`,`q`.`oid` AS `user`,`c`.`area` AS `nickname_area`,max(`c`.`importance`) AS `importance` from ((`badge_type` `c` join `badge_instance` `m`) join `community_user` `q`) where ((`m`.`badge_type_oid` = `c`.`oid`) and (`q`.`oid` = `m`.`rank_oid`)) group by `q`.`oid`,`c`.`area` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `badgetype_sortco`
--

/*!50001 DROP TABLE IF EXISTS `badgetype_sortco`*/;
/*!50001 DROP VIEW IF EXISTS `badgetype_sortco`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `badgetype_sortco` AS select `al1`.`oid` AS `oid`,(`al2`.`sort_number` or `al3`.`sort_number`) AS `der_attr` from ((`badge_type` `al1` join `badge_type` `al2`) join `badge_type` `al3`) where ((`al2`.`key` = 'area') and (`al1`.`area` = `al2`.`area`) and (`al3`.`key` = 'level') and (`al1`.`importance` = `al3`.`sort_number`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `community_user_credits_availab`
--

/*!50001 DROP TABLE IF EXISTS `community_user_credits_availab`*/;
/*!50001 DROP VIEW IF EXISTS `community_user_credits_availab`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `community_user_credits_availab` AS select `al1`.`oid` AS `oid`,(case when isnull((`al1`.`credit` - `al2`.`der_attr`)) then 0 else (`al1`.`credit` - `al2`.`der_attr`) end) AS `der_attr` from (`community_user` `al1` left join `community_user_credits_spent_v` `al2` on((`al1`.`oid` = `al2`.`oid`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `community_user_credits_spent_v`
--

/*!50001 DROP TABLE IF EXISTS `community_user_credits_spent_v`*/;
/*!50001 DROP VIEW IF EXISTS `community_user_credits_spent_v`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `community_user_credits_spent_v` AS select `al1`.`oid` AS `oid`,(case when isnull(sum(`al2`.`score`)) then 0 else sum(`al2`.`score`) end) AS `der_attr` from (`community_user` `al1` left join `reward_instance` `al2` on((`al1`.`oid` = `al2`.`rank_oid`))) group by `al1`.`oid` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `headquarter_user_partecipation`
--

/*!50001 DROP TABLE IF EXISTS `headquarter_user_partecipation`*/;
/*!50001 DROP VIEW IF EXISTS `headquarter_user_partecipation`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `headquarter_user_partecipation` AS select `al1`.`oid` AS `oid`,sum(`al2`.`score`) AS `partecipation` from ((`community_user` `al1` join `action_instance` `al2`) join `action_type` `al3`) where ((`al3`.`participation` = 1) and (`al1`.`oid` = `al2`.`rank_oid`) and (`al2`.`action_type_oid` = `al3`.`oid`)) group by `al1`.`oid` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `headquarter_user_participation_monthly`
--

/*!50001 DROP TABLE IF EXISTS `headquarter_user_participation_monthly`*/;
/*!50001 DROP VIEW IF EXISTS `headquarter_user_participation_monthly`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `headquarter_user_participation_monthly` AS select `r`.`oid` AS `oid`,sum(`al2`.`score`) AS `participation_monthly` from (`action_instance` `al2` join `community_user` `r` on((`r`.`oid` = `al2`.`rank_oid`))) where ((month(`al2`.`date`) = month(now())) and (year(`al2`.`date`) = year(now()))) group by `r`.`oid` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `headquarter_user_participation_seven_days`
--

/*!50001 DROP TABLE IF EXISTS `headquarter_user_participation_seven_days`*/;
/*!50001 DROP VIEW IF EXISTS `headquarter_user_participation_seven_days`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `headquarter_user_participation_seven_days` AS select `r`.`oid` AS `oid`,sum(`al3`.`score`) AS `participation_seven_days` from ((`community_user` `r` left join `action_instance` `al3` on((`r`.`oid` = `al3`.`rank_oid`))) left join `action_type` `al4` on((`al3`.`action_type_oid` = `al4`.`oid`))) where ((`al3`.`date` <= now()) and (`al3`.`date` >= (now() - interval 7 day)) and (`al4`.`participation` = 1)) group by `r`.`oid` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `max_date_action_instance`
--

/*!50001 DROP TABLE IF EXISTS `max_date_action_instance`*/;
/*!50001 DROP VIEW IF EXISTS `max_date_action_instance`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `max_date_action_instance` AS select `action_instance`.`action_type_oid` AS `action_type_oid`,`action_instance`.`rank_oid` AS `rank_oid`,max(`action_instance`.`date`) AS `maxDate` from `action_instance` group by `action_instance`.`action_type_oid`,`action_instance`.`rank_oid` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `mostimportant_badge`
--

/*!50001 DROP TABLE IF EXISTS `mostimportant_badge`*/;
/*!50001 DROP VIEW IF EXISTS `mostimportant_badge`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `mostimportant_badge` AS select `badge`.`badge_instance` AS `oid`,`rr`.`oid` AS `rankoid`,`dict`.`area` AS `area`,`dict`.`title` AS `title`,`badge`.`importance` AS `importance`,`dict`.`checked_image_2` AS `checked_image_2`,`dict`.`checked_imageblob` AS `checked_imageblob`,`dict`.`hd_checked_image_2` AS `hd_checked_image_2`,`dict`.`hd_checked_imageblob` AS `hd_checked_imageblob`,`dict`.`sort_number` AS `sort_number` from ((`badgeimportancebyuser` `badge` join `community_user` `rr`) join `badge_type` `dict`) where ((`dict`.`area` = `badge`.`nickname_area`) and (`dict`.`importance` = `badge`.`importance`) and (`rr`.`oid` = `badge`.`user`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `user_information`
--

/*!50001 DROP TABLE IF EXISTS `user_information`*/;
/*!50001 DROP VIEW IF EXISTS `user_information`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `user_information` AS select `r1`.`oid` AS `oid`,`r1`.`country` AS `country`,`r1`.`geographical_area` AS `area_geografica`,`r1`.`small_photo` AS `small_photo`,`r1`.`big_photo` AS `big_photo`,`r1`.`first_name` AS `first_name`,`r1`.`last_name` AS `last_name`,`r1`.`twitter` AS `twitter`,`r1`.`linkedin` AS `linkedin`,`r1`.`website` AS `website`,`r1`.`bio` AS `bio`,`r1`.`city` AS `city`,`r1`.`company_name` AS `company_name`,`c1`.`email` AS `email`,`c1`.`internal` AS `internal` from (`community_user` `r1` join `user` `c1` on((`c1`.`user_id` = `r1`.`oid`))) where (`r1`.`public_profile` = 1) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-03-26  8:30:50
